<?php

namespace App\Providers;

use App\Http\View\Composers\UsersComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewDataServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // first method - Every single view
        //share for all views
        // will hit db everytime we use it  
        // $users = collect([
        //     [
        //         "name" => "Naif",
        //         "role" => "Admin",
        //         "created_at" => "2023-02-11 14:50:16"
        //     ],
        //     [
        //         "name" => "omar",
        //         "role" => "author",
        //         "created_at" => "2023-02-11 14:50:16"
        //     ],
        //     [
        //         "name" => "ammar",
        //         "role" => "contributor",
        //         "created_at" => "2023-02-11 14:50:16"
        //     ]
        // ]);
        // View::share('users', $users);


        // second method - Granular view with wiidcards
        // share for views that we spicified
        // $users = collect([
        //     [
        //         "name" => "Naif",
        //         "role" => "Admin",
        //         "created_at" => "2023-02-11 14:50:16"
        //     ],
        //     [
        //         "name" => "omar",
        //         "role" => "author",
        //         "created_at" => "2023-02-11 14:50:16"
        //     ],
        //     [
        //         "name" => "ammar",
        //         "role" => "contributor",
        //         "created_at" => "2023-02-11 14:50:16"
        //     ]
        // ]);
        // View::composer(['pages.users.*'], function($view) use($users){
        //     $view->with('users', $users);
        // });

        // third method Dedicated class
        // share dash to to view from it own class
        View::composer('partials.users.*', UsersComposer::class);
    }
}
