<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;

class UsersComposer{

    public function compose(View $view)
    {
        $users = collect([
            [
                "name" => "Naif",
                "role" => "Admin",
                "created_at" => "2023-02-11 14:50:16"
            ],
            [
                "name" => "omar",
                "role" => "author",
                "created_at" => "2023-02-11 14:50:16"
            ],
            [
                "name" => "ammar",
                "role" => "contributor",
                "created_at" => "2023-02-11 14:50:16"
            ]
        ]);
        $view->with('users', $users);
    }
}