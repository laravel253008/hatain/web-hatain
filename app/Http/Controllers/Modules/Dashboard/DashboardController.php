<?php

namespace App\Http\Controllers\Modules\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $page_title = "dashboard";
        $footer = true;
        $breadcrumb = true;

        $url = BASE_PATH_API .  "/user";
        $res = \_route::get($url);
        $users = $res['data'];
        $more_data = compact('page_title', 'footer', 'breadcrumb', 'users');
        return view('pages.dashboard.index')->with($more_data);
    }
}
