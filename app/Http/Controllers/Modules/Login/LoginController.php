<?php

namespace App\Http\Controllers\Modules\Login;

use App\Http\Controllers\Controller;
use App\Http\Requests\Login\LoginRequest;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        return view('theme.argon.auth.login');
    }
    
    public function login(LoginRequest $request)
    {
        $request->validated();
        $data = $request->all();
        $url = BASE_PATH_API .  "/user/login";
        $res = \_route::post($url, $data);
        if ($res['success']) {
            $data = $res['data'];
            \Session::put("_user", $data['user']);
            \Session::put("_myToken", $data['token']['plainTextToken']);
            \Session::put("_impersonate", false);
            \Session::put("_oldUser", null);
            \Session::put("_oldToken", null);
            
            return redirect('dashboard');
        }
        $errors = ['email' => $res['message']];
        return redirect()->back()->withErrors($errors);
    }


    public function signup()
    {
        return redirect('/login');
    }

    public function logout()
    {
        $url = BASE_PATH_API .  "/user/signout";
        $res = \_route::post($url);
        if ($res['success']) {
            \Session::flush();
        }
        return redirect('/login');
    }
}
