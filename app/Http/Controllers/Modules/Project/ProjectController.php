<?php

namespace App\Http\Controllers\Modules\Project;

use App\Http\Controllers\Controller;
use App\Http\Requests\Login\LoginRequest;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Report\Xml\Project;

class ProjectController extends Controller
{
    public function index()
    {
        $data = \_assets::getAssets(
            [
                'plugins' => [
                    'toastr',
                    'dropzone'
                ],
                'assets_base' => [],
                'scripts_modulos' => [
                    'projects' => [],
                ],
                'assets_modulos' => [
                    'css_assets' => [],
                    'js_assets' => [
                        'projects',
                    ]
                ],
            ]
        );
        $page_title = "projects";
        $url = BASE_PATH_API .  "/project";
        $res = \_route::get($url);
        $projects = $res['data'];
        $more_data = compact('page_title', 'projects');
        return view('pages.projects.index', $data)->with($more_data);
    }

    public function updateProjectsPhoto(Project $request)
    {
        $request->validated();
        $params = $request->all();
        $url = BASE_PATH_API .  "/profile/update/photo";
        $res = \_route::post($url, $params);
        if ($res['success']) {
            $success = [$res['message']];
            return redirect("/projects")->with('success');
        }
        $errors = [$res['message']];
        return redirect()->withErrors($errors);
    }
}
