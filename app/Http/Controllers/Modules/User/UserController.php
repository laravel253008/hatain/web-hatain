<?php

namespace App\Http\Controllers\Modules\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserRequest;

class UserController extends Controller
{
    public function index(){
        $page_title = "profile";
        $footer = true;
        $breadcrumb = true;
        $more_data = compact('page_title', 'footer', 'breadcrumb');
        return view('pages.users.index')->with($more_data);
    }

    public function profile()
    {
        $data = \_assets::getAssets(
            [
                'plugins' => [
                    'toastr',
                    'dropzone'
                ],
                'assets_base' => [],
                'scripts_modulos' => [
                    'profile' => [],
                ],
                'assets_modulos' => [
                    'css_assets' => [],
                    'js_assets' => [
                        'profile',
                    ]
                ],
            ]
        );
        $page_title = "profile";
        $footer = true;
        $breadcrumb = true;
        $url = BASE_PATH_API .  "/profile";
        $res = \_route::get($url);
        logger($res);
        $user = $res['data'];
        $more_data = compact('page_title', 'footer', 'breadcrumb', 'user');
        return view('pages.profile.index',$data)->with($more_data);
    }

    public function updateProfilePhoto(UserRequest $request)
    {
        $request->validated();
        $params = $request->all();
        $url = BASE_PATH_API .  "/profile/update/photo";
        $res = \_route::post($url, $params);
        if ($res['success']) {
            $success = [$res['message']];
            return redirect("/profile")->with('success');
        }
        $errors = [$res['message']];
        return redirect()->withErrors($errors);
    }
    
    public function updateProfile(UserRequest $request)
    {
        $request->validated();
        $params = $request->all();
        $url = BASE_PATH_API .  "/profile/update";
        $res = \_route::post($url, $params);
        if ($res['success']) {
            $success = [$res['message']];
            return redirect()->back()->with('success');
        }
        $errors = [$res['message']];
        return redirect()->back()->withErrors($errors);
    }
}
