<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(request()->path() == "profile/photo"){
            return [
                'file'=> ['required']
            ];
        }
        return [
            'f_name'=> ['required' , 'string'],
            'l_name'=> ['required' , 'string'],
            'email'=> ['required' , 'email'],
            'phone_number'=> ['required' , 'integer'],
            'address'=> ['string'],
            'city'=> ['string'],
            'country'=> ['string'],
            'postal'=> ['string'],
            'about'=> ['string'],
        ];
    }
}
