<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(request()->path() == "projects/photo"){
            return [
                'id'=> ['required'],
                'file'=> ['required']
            ];
        }
        return [
            'f_name'=> ['required' , 'string'],
            'l_name'=> ['required' , 'string'],
            'email'=> ['required' , 'email'],
            'phone_number'=> ['required' , 'integer'],
            'address'=> ['string'],
            'city'=> ['string'],
            'country'=> ['string'],
            'postal'=> ['string'],
            'about'=> ['string'],
        ];
    }
}
