<?php

namespace App\Constants;

define('THEME_ASSETS', '/assets/theme/argon');
define('BASE_ASSETS', '/assets/admin/argon');
define('MODULOS_ASSETS', '/assets/admin/modulos');
define('BASE_ASSETS_IMG', '/assets/admin/img');



define('BASE_PATH_API', 'http://127.0.0.1:9090/api');
define('API_DOMAIN', 'http://127.0.0.1:9090/');
