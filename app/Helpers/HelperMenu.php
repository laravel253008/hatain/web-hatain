<?php

namespace App\Helpers;

class HelperMenu
{
    public function get_Menu()
    {
        return [
            [
                "titulo" => "Dashboard",
                "icono" => "dashboard.png",
                "url" => "/dashboard",
                "tiene_submenu" => 0,
            ],
            [
                "titulo" => "Ajustes",
                "icono" => "ajustes.png",
                "url" => "/ajustes",
                "tiene_submenu" => 0,
                "detalles" =>
                    [
                        [
                            "titulo" => "",
                            "icono" => "",
                            "url" => "/",
                            "tiene_submenu" => 0,
                        ],

                    ]
            ],
            [
                "titulo" => "Ayuda",
                "icono" => "ayuda.png",
                "url" => "/ayuda",
                "tiene_submenu" => 0,
            ],
        ];
    }
}





