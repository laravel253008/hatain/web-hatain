<?php

namespace App\Helpers;


class HelperUsr
{

    public function setSessionUsr($attrUpdate) {
        $usuario = \Session::get("_user");
        $usuario[$attrUpdate["key"]] = $attrUpdate["value"];

        return \Session::put("_user",$usuario);
    }

    public function permissions() {
       return [
            "Super admin" =>[
                "menu" => [
                    "Dashboard",
                    "nodes",
                    "Users Admin",
                    "Customers",
                ],
            ],
           "Company manager" =>[
               "menu" => [
                   "Dashboard",
                   "Conversion",
                   "Hot Zones",
                   "Activity",
                   "Devices",
                   "Users",
                   "Settings",
               ],
           ],
           "Super user" =>[
               "menu" => [
                   "Dashboard",
                   "Conversion",
                   "Hot Zones",
                   "Activity",
                   "Devices",
                   "Users",
                   "Customers",
                   "Settings",
               ],
           ],
           "Viewer" =>[
               "menu" => [
                   "Dashboard",
                   "Conversion",
                   "Hot Zones",
                   "Activity",
                   "Devices",

               ],
           ],
        ];
    }

    public function breadcrumb() {
        $path = \Request::path();
        $path = str_replace('_', ' ', $path);
        $path = str_replace('-', ' ', $path);
        $path = explode('/', $path);
        return $path;
    }
}
