<?php

namespace App\Helpers;

class HelperString{

    public function emptyVal($parametro){
        return (
            $parametro === "" ||
            $parametro === null ||
            $parametro === "null" ||
            $parametro === "0" ||
            $parametro === 0
        ) ? true : false;
    }
}
