<?php
namespace App\Helpers;

class HelperHtml{

    public function token()
    {
        return \Form::open(['id' => '_tokenizer']) . \Form::close();
    }

    public function getVersion($url){
        $url_complete = public_path($url);
        if (file_exists($url_complete)) {
            $ver = '?v='.filemtime($url_complete);
            return $url . $ver;
        }else{
            return $url;
        }
    }

    public function getVersionRandom($url){
        $rdm1 = mt_rand(1, 400);

        $ver = '?v=' . $rdm1;
        return $url . $ver;
    }
}
