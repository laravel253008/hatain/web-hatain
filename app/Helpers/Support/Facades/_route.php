<?php
/**
 * Created by PhpStorm.
 * User: Tecnologias
 * Date: 13/04/2018
 * Time: 01:27 AM
 */

namespace App\Helpers\Support\Facades;

use Illuminate\Support\Facades\Facade;

class _route extends Facade{
    public static function getFacadeAccessor()
    {
        return '_route';
    }
}