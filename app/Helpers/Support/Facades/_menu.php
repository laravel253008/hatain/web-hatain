<?php
/**
 * Created by PhpStorm.
 * User: Tecnologias
 * Date: 24/03/2018
 * Time: 03:53 PM
 */

namespace App\Helpers\Support\Facades;

use Illuminate\Support\Facades\Facade;

class _menu extends Facade
{
    public static function getFacadeAccessor(){
        return '_menu';
    }
}