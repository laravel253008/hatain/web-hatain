<?php

namespace App\Helpers\Support\Facades;

use Illuminate\Support\Facades\Facade;

class _html extends Facade{
    public static function getFacadeAccessor()
    {
        return '_html';
    }
}