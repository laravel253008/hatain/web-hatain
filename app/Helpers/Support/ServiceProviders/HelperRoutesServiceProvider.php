<?php
/**
 * Created by PhpStorm.
 * User: Tecnologias
 * Date: 13/04/2018
 * Time: 01:32 AM
 */

namespace App\Helpers\Support\ServiceProviders;

use Illuminate\Support\ServiceProvider;

class HelperRoutesServiceProvider extends ServiceProvider{
    public function register()
    {
        \App::bind('_route', \App\Helpers\HelperRoutes::class);
    }
}