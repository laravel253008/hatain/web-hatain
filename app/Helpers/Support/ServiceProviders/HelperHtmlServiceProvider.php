<?php

namespace App\Helpers\Support\ServiceProviders;

use Illuminate\Support\ServiceProvider;

class HelperHtmlServiceProvider extends ServiceProvider{
    public function register(){
        \App::bind('_html', \App\Helpers\HelperHtml::class);
    }
}