<?php
/**
 * Created by PhpStorm.
 * User: Tecnologias
 * Date: 24/03/2018
 * Time: 03:53 PM
 */

namespace App\Helpers\Support\ServiceProviders;

use Illuminate\Support\ServiceProvider;
class HelperMenuServiceProvider extends ServiceProvider
{
    public function register(){
        \App::bind('_menu',\App\Helpers\HelperMenu::class);
    }
}