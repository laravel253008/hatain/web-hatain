<?php

namespace App\Helpers\Support\ServiceProviders;

use Illuminate\Support\ServiceProvider;

class HelperStringServiceProvider extends ServiceProvider{
    public function register()
    {
        \App::bind('_string', \App\Helpers\HelperString::class);
    }
}