<?php

namespace App\Helpers\Support\ServiceProviders;

use Illuminate\Support\ServiceProvider;

class HelperAssetsServiceProvider extends ServiceProvider{
    public function register()
    {
        \App::bind('_assets', \App\Helpers\HelperAssets::class);
    }
}
