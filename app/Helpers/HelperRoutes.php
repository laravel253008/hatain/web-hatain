<?php

namespace App\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class HelperRoutes
{

    public function post($url, $parametros = [])
    {
        try {
            $client = new Client([
                'base_uri' => $url
            ]);
    
            $response = $client->request('POST', '', [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'X-Requested-With' => 'XMLHttpRequest',
                    'Authorization' => 'Bearer ' . \Session::get('_myToken')
                ],
                'form_params' => $parametros
            ]);
    
            return  json_decode($response->getBody(), true);
        } catch (ClientException $cx) {
            abort($cx->getResponse()->getStatusCode(), $cx->getResponse()->getReasonPhrase());
        }
    }


    public function put($url, $parametros = [])
    {
        try {
            $client = new Client([
                'base_uri' => $url
            ]);
    
            $response = $client->request('PUT', '', [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'X-Requested-With' => 'XMLHttpRequest',
                    'Authorization' => 'Bearer ' . \Session::get('_myToken')
                ],
                'form_params' => $parametros
            ]);
    
            return  json_decode($response->getBody(), true);
        } catch (ClientException $cx) {
            abort($cx->getResponse()->getStatusCode(), $cx->getResponse()->getReasonPhrase());
        }
    }


    public function get($url)
    {
        try {
            $client = new Client([
                'base_uri' => $url
            ]);
            $response = $client->request('GET', '', [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'X-Requested-With' => 'XMLHttpRequest',
                    'Authorization' => 'Bearer ' . \Session::get('_myToken')
                ]
            ]);
    
            return  json_decode($response->getBody(), true);
        } catch (ClientException $cx) {
            abort($cx->getResponse()->getStatusCode(), $cx->getResponse()->getReasonPhrase());
        }
    }
}
