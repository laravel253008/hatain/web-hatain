<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Session;

class HelperAssets{


    private function getAssetsList(){

        return [
            'select2' => [
                'css_plugins' => [
                    'vendors/css/forms/select/select2.min.css',
                ],
                'js_plugins' => [
                    'vendors/js/forms/select/select2.full.min.js'
                ],
            ],
            'grid-stack' => [
                'css_plugins' => [
                    'vendors/css/grid-stack/grid-stack.css',
                ],
                'js_plugins' => [
                    'vendors/js/grid-stack/grid-stack.js',
                ],
            ],
            'animate' => [
                'css_plugins' => [
                    'vendors/css/animate/animate.css',
                ],
                'js_plugins' => [],
            ],
            'calendar' => [
                'css_plugins' => [],
                'js_plugins' => [
                    'vendors/clipboard/dist/clipboard.min.js'
                ],
            ],
            'chart' => [
                'css_plugins' => [],
                'js_plugins' => [
                    'vendors/autosize/dist/autosize.js'
                ],
            ],
            'editor' => [
                'css_plugins' => [
                    'vendors/jstree/dist/themes/default/style.css',
                ],
                'js_plugins' => [
                    'vendors/jstree/dist/jstree.js'
                ],
            ],
            'extensions' => [
                'css_plugins' => [],
                'js_plugins' => [
                    'vendors/chart.js/dist/Chart.bundle.js',
                    'vendors/js/framework/components/plugins/charts/chart.init.js'
                ],
            ],
            'file-uploaders' => [
                'css_plugins' => [
                    'vendors/morris.js/morris.css',
                ],
                'js_plugins' => [
                    'vendors/morris.js/morris.js'
                ],
            ],
            'forms' => [
                'css_plugins' => [
                    'vendors/nouislider/distribute/nouislider.css',
                ],
                'js_plugins' => [
                    'vendors/nouislider/distribute/nouislider.js'
                ],
            ],
            'jkanban' => [
                'css_plugins' => [
                    'vendors/ion-rangeslider/css/ion.rangeSlider.css',
                    'vendors/ion-rangeslider/css/ion.rangeSlider.skinFlat.css',
                ],
                'js_plugins' => [
                    'vendors/ion-rangeslider/js/ion.rangeSlider.js'
                ],
            ],
            'modal' => [
                'css_plugins' => [
                    'vendors/summernote/dist/summernote.css',
                ],
                'js_plugins' => [
                    'vendors/summernote/dist/summernote.js'
                ],
            ],
            'perfect-scrollbar' => [
                'css_plugins' => [
                    'vendors/bootstrap-markdown/css/bootstrap-markdown.min.css',
                ],
                'js_plugins' => [
                    'vendors/markdown/lib/markdown.js',
                    'vendors/bootstrap-markdown/js/bootstrap-markdown.js',
                    'vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js'
                ],
            ],
            'pickers' => [
                'css_plugins' => [
                    'vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css',
                ],
                'js_plugins' => [
                    'vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js'
                ],
            ],
            'tables' => [
                'css_plugins' => [],
                'js_plugins' => [
                    'vendors/popper/dist/popper.js',
                ],
            ],
            'ui' => [
                'css_plugins' => [],
                'js_plugins' => [
                    'vendors/bootstrap-confirmation/bootstrap-confirmation.js',
                ],
            ],
            'weather-icons' => [
                'css_plugins' => [
                    'vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
                ],
                'js_plugins' => [
                    'vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
                ],
            ],
            'toastr' => [
                'css_plugins' => [
                    'vendor/extensions/css/toastr.css',
                ],
                'js_plugins' => [
                    'vendor/extensions/js/toastr.min.js',
                ],
            ],
            'bootstrap' => [
                'css_plugins' => [
                    'vendors\css\bootstrap\datepicker.css',
                ],
                'js_plugins' => [
                    'vendors\js\bootstrap\bootstrap-datepicker.js',
                ],
            ],
            'validation' => [
                'css_plugins' => [],
                'js_plugins' => [
                    'vendors/js/forms/validation/jquery.validate.min.js',
                ],
            ],
            'moment' => [
                'css_plugins' => [],
                'js_plugins' => [
                    'vendors/js/extensions/moment.min.js',
                ],
            ],
            'inputmask' => [
                'css_plugins' => [],
                'js_plugins' => [
                    'vendors/js/inputmask/inputmask.js',
                ],
            ],
            'datepicker' => [
                'css_plugins' => [
                    'vendors/css/pickers/pickadate/pickadate.css',
                ],
                'js_plugins' => [
                    'vendors/js/pickers/pickadate/picker.js',
                    'vendors/js/pickers/pickadate/picker.date.js',
                    'vendors/js/pickers/pickadate/picker.time.js',
                    'vendors/js/pickers/pickadate/legacy.js',
                ],
            ],
            'jqueryui' => [
                'css_plugins' => [],
                'js_plugins' => [
                    'vendors\js\extensions\jquery.ui.min.js',
                ],
            ],
            'dropzone' => [
                'css_plugins' => [
                    'vendor/dropzone/dist/dropzone.css',

                ],
                'js_plugins' => [
                    'vendor/dropzone/dist/dropzone.js'
                ],
            ],
            'datatable' => [
                'css_plugins' => [
                    'vendors/css/tables/datatable/datatables.min.css',
                    'vendors/css/tables/datatable/responsive.bootstrap.min.css',
                ],
                'js_plugins' => [
                    'vendors/js/tables/datatable/datatables.min.js',
                    'vendors/js/tables/datatable/dataTables.bootstrap4.min.js',
                    'vendors/js/tables/datatable/dataTables.buttons.min.js',
                    'vendors/js/tables/datatable/buttons.html5.min.js',
                    'vendors/js/tables/datatable/buttons.print.min.js',
                    'vendors/js/tables/datatable/buttons.bootstrap.min.js',
                    'vendors/js/tables/datatable/vfs_fonts.js',
                ],
            ],
            'bootstrap_validate' => [
                'css_plugins' => [ ],
                'js_plugins' => [
                    'vendors/js/forms/validation/jqBootstrapValidation.js',
                ]
            ],
            'noui_slider' => [
                'css_plugins' => [
                    'vendors/css/extensions/nouislider.min.css',
                    'css/plugins/extensions/noui-slider.css',
                ],
                'js_plugins' => [
                    'vendors/js/extensions/nouislider.min.js',
                ]
            ],
            'dynamic_functions' => [
                'css_plugins' => [ ],
                'js_plugins' => [
                    'vendors\js\dynamic_functions\_dynamicFunctions.js',
                ]
            ],
        ];
    }

    public function getAssets($assets)
    {
        $assets_css_plugins = array();
        $assets_js_plugins = array();
        $assets_js_base = array();
        $assets_scripts_modulos = array();
        $assets_css_modulos = array();
        $assets_js_modulos = array();
        $assets_list = self::getAssetsList();

        if (isset($assets["plugins"])) {
            foreach ($assets["plugins"] as $plugin) {
                foreach ($assets_list[$plugin] as $key_archivo => $archivos) {

                    if (!is_null($key_archivo) && $key_archivo == "css_plugins") {
                        foreach ($archivos as $archivo) {
                            array_push($assets_css_plugins, $archivo);
                        }
                    }

                    if (!is_null($key_archivo) && $key_archivo == "js_plugins") {
                        foreach ($archivos as $archivo) {
                            array_push($assets_js_plugins, $archivo);
                        }
                    }
                }
            }
        }

        if (isset($assets["assets_base"])) {
            foreach ($assets["assets_base"] as $asset) {
                $asset = $asset . "/" . $asset . ".js";
                array_push($assets_js_base, $asset);
            }
        }

        if (isset($assets["scripts_modulos"])) {
            foreach ($assets["scripts_modulos"] as $key => $scripts) {
                if (!is_null($key)) {
                    foreach ($scripts as $script) {
                        $script = $key . "/" . "scripts" . "/" . $script . ".js";
                        array_push($assets_scripts_modulos, $script);
                    }
                }

            }
        }

        if (isset($assets["assets_modulos"])) {
            foreach ($assets["assets_modulos"] as $key_archivo => $archivos) {
                if (!is_null($key_archivo) && $key_archivo == "css_assets") {
                    $lang = Session::get('lang');
                    $css = $lang == 'ar' ? '_ar.css' : '.css';
                    foreach ($archivos as $archivo){
                        $archivo = $archivo . "/" . $archivo . $css;
                        // $archivo = $archivo . "/" . $archivo . "_ar.css";
                        // $archivo = $archivo . "/" . $archivo . ".css";
                        array_push($assets_css_modulos, $archivo);
                    }
                }
                if (!is_null($key_archivo) && $key_archivo == "js_assets") {
                    foreach ($archivos as $archivo){
                        $archivo = $archivo . "/" . $archivo . ".js";
                        array_push($assets_js_modulos, $archivo);
                    }
                }
            }
        }

        $data = compact("assets_css_plugins", "assets_js_plugins", "assets_js_base", "assets_scripts_modulos", "assets_css_modulos", "assets_js_modulos");
        return $data;
    }
}
