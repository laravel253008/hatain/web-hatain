<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

include_once(app_path() . '/Constants/constantes_admin.php');

Route::group([
    'middleware' => 'verifyLogin',
    'namespace'  => '\App\Http\Controllers\Modules\Login'
], function () {
    Route::get("/login", "LoginController@index");
    Route::group(['prefix'=>'user'], function(){
        Route::post("/login", "LoginController@login");
    });
});


Route::group([
    'namespace'  => '\App\Http\Controllers\Modules\Login'
], function () {
    Route::group(['prefix'=>'user'], function(){
        Route::post("/logout", "LoginController@logout");
    });
});

Route::group([
    'namespace'  => '\App\Http\Controllers\Modules\User'
], function () {
    Route::group(['prefix'=>'users'], function(){
        Route::get("/", "UserController@index");
    });
    Route::group(['prefix'=>'profile'], function(){
        Route::get("/", "UserController@profile");
        Route::post("/photo", "UserController@updateProfilePhoto");
        Route::post("/", "UserController@updateProfile");
    });
});

Route::group([
    'namespace'  => '\App\Http\Controllers\Modules\Project'
], function () {
    Route::group(['prefix'=>'projects'], function(){
        Route::get("/", "ProjectController@index");
        Route::post("/photo", "ProjectController@updateProjectsPhoto");
    });
});

Route::group([
    'namespace'  => '\App\Http\Controllers\Modules\Dashboard'
], function () {
    Route::get("/dashboard", "DashboardController@index");
});

