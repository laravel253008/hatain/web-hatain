var _jsHelpers = (function () {
  return {
    /********** CONSTANTS ************/

    //Firebase configuration
    _FIREBASECONFIG: {
      apiKey: "AIzaSyBdJd7KdihAyprSCroql9RBd0dU7rZr9oI",
      authDomain: "linkers-iot.firebaseapp.com",
      databaseURL: "https://linkers-iot.firebaseio.com",
      projectId: "linkers-iot",
      storageBucket: "linkers-iot.appspot.com",
      messagingSenderId: "914900869078",
      appId: "1:914900869078:web:82a7044b8d3fc43e470e77",
    },

    _PREFIX_API_DOMAIN: "http://127.0.0.1:9090/",
    _PREFIX_ADMIN_API: "http://127.0.0.1:9090/api",

    // _PREFIX_API_DOMAIN: "https://beta-cxai-api.linkers.io/",
    // _PREFIX_ADMIN_API: "https://beta-cxai-api.linkers.io/api/v0.1",
    /**
     * hack modal over modal
     */
    _hack_modals: function () {
      var modal_lv = 0;
      $(".modal").on("shown.bs.modal", function (e) {
        $(".modal-backdrop:last").css("zIndex", 151 + modal_lv);
        $(e.currentTarget).css("zIndex", 152 + modal_lv);
        modal_lv++;
      });
      $(".modal").on("hidden.bs.modal", function (e) {
        modal_lv--;
        $(".modal").css({ "overflow-x": "hidden", "overflow-y": "auto" });
      });
    },

    _trans(key, replace = {}) {
      key = "msg." + key;
      let translation = key.split('.').reduce((t, i) => t[i] || null, window.translations);

      for (var placeholder in replace) {
        translation = translation.replace(`:${placeholder}`, replace[placeholder]);
      }

      return translation;
    },
    _isConfirmed: function (msg, handelConfirmation, handleNo = ()=>{},confirmBtnText = "Yes", cancelBtnText = "Cancel") {
      $("#confirmationModal").modal({backdrop: 'static', keyboard: false},"show");
      $("#msg").text(msg);
      $("#btnConfirm").text(confirmBtnText);
      $("#btnCancel").text(cancelBtnText);
      $("#btnConfirm").on("click", function () {
        handelConfirmation();
        $("#btnConfirm").off("click");
        $("#confirmationModal").modal("hide");
      });
      $("#btnCancel").on("click",function (e) {
        handleNo();
        $("#btnCancel").off("click");
        $("#confirmationModal").modal("hide");
      })
    },

    _handleInternalShowError: function (jqXHR, textStatus, errorThrown) {
      if (jqXHR.status === 0) {
        _jsHelpers._handle_notify("info", "Not connect: Verify Network.");
      } else if (jqXHR.status == 401) {
        _jsHelpers._handle_notify("info", "Usuario no Autorizado");
      } else if (jqXHR.status == 500) {
        _jsHelpers._handle_notify("danger", "Internal Server Error [500].");
      }
    },

    /**
     * Show notify
     * @param {string} type type of notify
     * @param {string} msg message of notify
     */
    _handle_notify: function (type, msg) {
      if (_jsHelpers.undefined(msg)) {
        msg = "";
      }
      if (type == "success") {
        toastr.success(msg, "Success", {
          positionClass: "toast-bottom-right",
          containerId: "toast-bottom-right",
        });
      }
      if (type == "danger") {
        toastr.error(msg, "Error", {
          positionClass: "toast-bottom-right",
          containerId: "toast-bottom-right",
        });
      }
      if (type == "info") {
        toastr.info(msg, "Info", {
          positionClass: "toast-bottom-right",
          containerId: "toast-bottom-right",
        });
      }
      if (type == "warning") {
        toastr.warning(msg, "Warning", {
          positionClass: "toast-bottom-right",
          containerId: "toast-bottom-right",
        });
      }
    },

    _blockUI: function (target, msg) {
      var loader =
        '<div class="overlay-loader">' +
        '    <div class="overlay__inner">' +
        '        <div class="overlay__content"><span class="spinner"></span></div>' +
        "    </div>" +
        "</div>";
      $("body").append(loader);
    },

    _unBlockUi: function (target) {
      $(".overlay-loader").remove();
    },

    _token: function () {
      return $("#_tokenizer").val();
    },

    _getHeaders: function () {
      return {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Requested-With": "XMLHttpRequest",
        Authorization: "Bearer " + localStorage.getItem("_myToken"),
      };
    },

    _cerrar_sesion() {
      $.ajax({
        url: "/admin/api/cerrar_sesion",
        type: "post",
        data: {
          _token: _jsHelpers._token(),
        },
      })
        .done(function (response) {
          _jsHelpers._blockUI();
          if (response.estatus == "exito") {
            localStorage.removeItem("_myToken");
            _jsHelpers.signOutFirebase();
            location.href = "/";
          } else {
          }
          _jsHelpers._unBlockUi();
        })
        .fail(function (err) {
          _jsHelpers._unBlockUi();
        });
    },

    _cerrar_sesion_impersonate() {
      $.ajax({
        url: "/admin/api/cerrar_sesion_impersonate",
        type: "post",
        data: {
          _token: _jsHelpers._token(),
        },
      })
        .done(function (response) {
          _jsHelpers._blockUI();
          if (response.estatus == "exito") {
            localStorage.setItem("_myToken", response.access_token);
            location.href = "/users/customers";
          } else {
          }
          _jsHelpers._unBlockUi();
        })
        .fail(function (err) {
          _jsHelpers._unBlockUi();
        });
    },

    undefined: function (parametro) {
      return typeof parametro === "undefined" ||
        typeof parametro === undefined ||
        parametro === undefined ||
        parametro == "" ||
        parametro == "" ||
        parametro == null ||
        parametro == 0 ||
        parametro == "0" ||
        parametro == "00:00"
        ? true
        : false;
    },

    undefined0: function (parametro) {
      return typeof parametro === "undefined" ||
        typeof parametro === undefined ||
        parametro === undefined ||
        parametro == "" ||
        parametro == "" ||
        parametro == null
        ? true
        : false;
    },

    _escapeRegExp: function (str) {
      return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    },

    _replace_all_to_one: function (str, find, replace) {
      for (var i = 0; i < find.length; i++) {
        str = str.replace(
          new RegExp(this._escapeRegExp(find[i]), "g"),
          replace
        );
        // return str.replace(new RegExp(this._escapeRegExp(find), 'g'), replace);
      }
      return str;
    },

    _isEmptyObj(obj) {
      return $.isEmptyObject(obj);
    },

    _responseError(response) {
      var msg_error = "";
      var objetoError = response["message"];
      if (objetoError === Object(objetoError)) {
        for (const obj in objetoError) {
          msg_error += objetoError[obj][0] + "<br>";
        }
      } else {
        msg_error += response["message"];
      }
      _jsHelpers._handle_notify("danger", msg_error);
    },

    _getParam(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
      return results === null
        ? ""
        : decodeURIComponent(results[1].replace(/\+/g, " "));
    },

    _onlyNumber(selector) {
      $(selector).on("input", function () {
        if (!$(this).hasClass("notApplyOnlyNumber")) {
          // this.value = this.value.replace(/[^0-9]/g, '');
          this.value = this.value.replace(/[^0-9,.]/g, "");
        }
      });
    },

    _handleAutocompleteOff: function () {
      $("body").on("DOMNodeInserted", function (event) {
        $('input').attr('autocomplete', 'nope');
        // $("input").attr("autocomplete", "off");
      });
    },

    _handleErrorPlacement: function (error, element) {
      var _parent = $(element).parent(".form-group");

      if (element.is("select")) {
        error.insertAfter(_parent.find(".select2-container"));
      }
      if (element.is("input")) {
        error.insertAfter(_parent.find("input"));
      }
    },

    _handleValidSelect: function () {
      $(".m-select2").change(function () {
        if (!_jsHelpers.undefined($(this).val())) {
          $(this).valid();
        }
      });
    },

    _loader: function () {
      var loader =
        '<div class="overlay">' +
        '    <div class="overlay__inner">' +
        '        <div class="overlay__content"><span class="spinner"></span></div>' +
        "    </div>" +
        "</div>";
      $("body").append(loader);
    },
    _remove_loader: function () {
      $(".overlay").remove();
    },

    _animateCSS(element, animationName, callback) {
      var node = document.querySelector(element);
      node.classList.add("animated", animationName);

      function handleAnimationEnd() {
        node.classList.remove("animated", animationName);
        node.removeEventListener("animationend", handleAnimationEnd);

        if (typeof callback === "function") callback();
      }

      node.addEventListener("animationend", handleAnimationEnd);
    },

    _errorImage: function (_this) {
      _this.src = "/assets/admin/img/misc/user_active.png";
    },

    _getSince: function (date) {
      let dt = moment(date);
      const day = dt.format("D");
      const month = dt.format("MMM");
      const year = dt.format("YYYY");

      let dateFormat = `${month}, ${day}<sup>th</sup>, ${year}`;
      $(".modal-detail .-since").html(dateFormat);
    },

    _getSinceComplete: function (date) {
      let dt = moment(date);
      const day = dt.format("D");
      const month = dt.format("MMM");
      const year = dt.format("YYYY");
      const hrs = dt.format("hh:mm a");

      return `Active since ${month}, ${day} <sup>th</sup>, ${year} at ${hrs}`;
    },
    //capitalize only the first letter of the string. 
    _capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    },
    _getLastConnection: function (date, haceCuanto) {
      let dt = moment(date);
      const day = dt.format("D");
      const month = dt.format("MMM");
      const year = dt.format("YYYY");
      const hrs = dt.format("hh:mm a");

      return `<p class="-info m-0 -last-connection" style="color: slategray; font-size: 12px"
                       data-toggle="tooltip" data-placement="top" title=""
                       data-html="true"
                       data-original-title="${month}, ${day} <sup>th</sup>, ${year} at ${hrs}">
                        ${haceCuanto}
                    </p>`;
    },

    _loginFirebase(token) {
      firebase.initializeApp(_jsHelpers._FIREBASECONFIG);
      firebase
        .auth()
        .signOut()
        .then(function () {
          console.log("Sign Out firebase");
          firebase
            .auth()
            .signInWithCustomToken(token)
            .then((user) => {
              console.log("Login with firebase");
              location.href = "/dashboard";
            })
            .catch((error) => {
              var errorCode = error.code;
              var errorMessage = error.message;
              console.log("*** ERROR LOGIN FIREBASE ***");
              console.log("codeError: ", errorCode);
              console.log("errorMsg", errorMessage);
              console.log("*** ERROR LOGIN FIREBASE ***");
            });
        })
        .catch(function (error) {
          console.log("Erron Sign Out firebase");
        });
    },
    signOutFirebase() {
      // firebase.initializeApp(_jsHelpers._FIREBASECONFIG);
      // firebase.auth().signOut().then(function() {
      //     console.log("Sign Out firebase")
      // }).catch(function(error) {
      //     console.log("Erron Sign Out firebase")
      // });
    },
  };
})();
