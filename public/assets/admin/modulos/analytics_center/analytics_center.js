$(document).ready(function () {
    var _PREFIX_ADMIN_API = _jsHelpers._PREFIX_ADMIN_API;
    var urlChart = _PREFIX_ADMIN_API + "/counter/analytic_center";
    var chart;

    var ALL_VARIABLES = ['visitors', 'customers', 'lost', 'outside_traffic',
        'returning_customer', 'stay_time', 'new_customer', 'turn_in_rate', 'sales_conversion', "sales_amount", "avg_ticket_size"
    ];

    var SELECTED_VARIABLES = []
    var percentVariables = ["returning_customer", "stay_time", "new_customer", "turn_in_rate", 'sales_conversion', "avg_ticket_size"]
    var SELECTED_VARIABLES = [];
    var ALL_COMPANY_BRANCHES = [];
    var SELECTED_BRANCHES = [];
    var allData;
    var FILTERS = {
        vs_analysis_of: true,
        vs_time_lapse: false,
        vs_marketing: false,
        analysis_of: "visitors",
        analysis: ["visitors"],
        branches: 'all_branches',
        branch_ids: [],
        time_lapse: 'this_month',
        time_lapse2: 'last_month',
        from_date: "",
        to_date: "",
        vs_from_date: "",
        vs_to_date: "",
        resolution: '1',
        results: "global",
        compare_period: "0",
    };

    /**
     * this method created to handle all the logic of the filters
     */
    const formateDateToString = (date) => {
        date = new Date(date)
        return `${String(date.getDate()).padStart(2, '0')}/${String(date.getMonth() + 1).padStart(2, '0')}/${String(date.getFullYear()).padStart(2, '0')}`;
    };

    function Num2Time(stay_time) {
        min = stay_time - Math.floor(stay_time / 60) * 60
        sec = (Number(min - Math.floor(min)).toFixed(2)) * 60;
        return `${String(Math.floor(stay_time / 60)).padStart(2, "0")}:${String(Math.floor(min)).padStart(2, "0")}:${String(Math.floor(sec)).padStart(2, "0")}`
    }

    const getData = async () => {
        if (ALL_COMPANY_BRANCHES.length == 0) {
            _jsHelpers._blockUI();
            await $.ajax({
                type: "POST",
                url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branchesWithGroups/",
                headers: _jsHelpers._getHeaders(),
            }).done((res) => {
                if (!res.error) {
                    ALL_COMPANY_BRANCHES = res.data.branches;
                    ALL_COMPANY_GROUPS = res.data.groups;
                    SELECTED_BRANCHES = ALL_COMPANY_BRANCHES.map(item => {
                        item.group_id = 0;
                        return item;
                    })
                    $('.list-branches').empty();
                    if (SELECTED_BRANCHES.length > 0) {
                        SELECTED_BRANCHES.forEach(item => {
                            FILTERS.branch_ids.push(item.id)
                            $('.list-branches').append(`<div><p>${item.name}</p></div>`);
                            $('#branchesList').append(`<option value="${item.name}">${item.name}</option>`);
                        });
                    } else {
                        $('.list-branches').append('<span style="color:#fd5454;">No Branches Selected</span>')
                    }
                }
                _jsHelpers._unBlockUi();
            }).fail((error) => {
                _jsHelpers._unBlockUi();
                _jsHelpers._handle_notify('danger', 'Server Error : Can\'t Fetch Company Branches');
            });
        }

        _jsHelpers._blockUI();
        await $.ajax({
            type: "GET",
            url: urlChart,
            headers: _jsHelpers._getHeaders(),
            data: FILTERS,
        }).done((res) => {
            if (!res.error) {
                allData = res.data;
                console.log('res : ', res);
            } else {
                _jsHelpers._handle_notify('danger', 'Wrong Data Input')
            }
            _jsHelpers._unBlockUi();
        }).fail((error) => {
            _jsHelpers._unBlockUi();
            _jsHelpers._handle_notify('danger', 'Server Error : Can\'t Fetch Data');
        });

        function getDaysArray(start, end, type = 1) {
            start = new Date(`${start.split("/")[1]}-${start.split("/")[0]}-${start.split("/")[2]}`);
            end = new Date(`${end.split("/")[1]}-${end.split("/")[0]}-${end.split("/")[2]}`);
            for (var arr = [], dt = new Date(start); dt <= end; dt.setDate(dt.getDate() + 1)) {
                arr.push(formateDateToString(dt));
            }
            switch (type) {
                case '0':
                    let new_arr = []
                    let allDaysHours = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"]
                    arr.forEach(day => {
                        allDaysHours.forEach(hour => {
                            new_arr.push(day + " " + hour)
                        });
                    });
                    return new_arr;
                case '1':
                    return arr;
                case '2':
                    let new_arr1 = [];
                    arr.forEach((item, idx) => {
                        let day = item.split("/")[0];
                        if ((Number(day) - 1) % 7 == 0) {
                            new_arr1.push(item);
                        }
                    });
                    return new_arr1;
                case '3':
                    let new_arr2 = [];
                    arr.forEach((item, idx) => {
                        let month = item.split("/")[1];
                        if (!new_arr2.includes(month)) {
                            new_arr2.push(month);
                        }
                    });
                    return new_arr2;

                default:
                    break;
            }
        };

        function fillDatesGaps(dates, data) {
            let arr = []
            dates.forEach(day => {
                let number = 0;
                data.forEach(ele => {
                    if (day == ele.date) {
                        number = ele.count;
                    }
                });
                arr.push(number);
            });
            return arr;
        };

        if (FILTERS.vs_analysis_of) {
            handleNextPrevButtons();
            if (FILTERS.results == 'global') {

                let series = allData.map(variable => {
                    return {
                        name: variable.caption,
                        data: variable.data
                    }
                })

                reRenderChart({
                    chart: {
                        type: "bar"
                    },
                    series: series,
                    xaxis: {
                        tickAmount: 5,
                        categories: allData[0].date,
                    }
                });

                // Handle Rendering the Chart if the Result was Global
                var tableData = [];
                var row_ID = 0;
                allData[0].date.forEach((date, idx) => {
                    row_ID++;
                    let row = {};
                    row["ID"] = row_ID;
                    row["Date"] = date;
                    row["Branches"] = "All Branches";
                    allData.forEach((variable) => {
                        if (!SELECTED_VARIABLES.find(x => x.name == variable.name)) {
                            SELECTED_VARIABLES.push({ name: variable.name, caption: variable.caption, unit: variable.unit, summaryType: variable.result == "count" ? "sum" : "avg" });
                        }
                        row[variable.name] = variable.data[idx];
                    });
                    tableData.push(row);
                });
                renderTable(tableData);

            } else { // Comparative
                $('#branchesList').empty();
                SELECTED_BRANCHES.forEach(branch => {
                    $('#branchesList').append(`<option value="${branch.name}">${branch.name}</option>`);
                })

                handleBranchArrows(0)

                var tableData = [];

                var row_ID = 0;
                SELECTED_VARIABLES = [];
                allData.forEach(branch_item => {
                    branch_item.date.forEach((date, idx) => {
                        let row = {};
                        row_ID++;
                        row["ID"] = row_ID;
                        row["Date"] = date;
                        row["Branches"] = branch_item.branch;
                        row["Variable"] = branch_item.name;
                        row["Value"] = branch_item.data[idx];
                        tableData.push(row);
                    });
                });
                renderTable(tableData);
            }
        }

        if (FILTERS.vs_time_lapse) {

            let series = [
                {
                    name: "First Period",
                    data: allData.time_lapse[0].data,
                    type: "bar"
                },
                {
                    name: "Second Period",
                    data: allData.vs_time_lapse[0].data,
                    type: "line"
                }

            ]

            var dates;
            if (allData.time_lapse[0].date.length >= allData.vs_time_lapse[0].data) {
                dates = allData.time_lapse[0].date
            } else {
                dates = allData.vs_time_lapse[0].date
            }

            reRenderChart({
                colors: ['#2E93fA', '#66DA26', '#546E7A', '#E91E63', '#FF9800'],
                series: series,
                stroke: {
                    width: [4, 4]
                },
                xaxis: {
                    tickAmount: 5,
                    categories: dates,
                }
            });

            tableData = dates.map((date, idx) => {
                let row = {};
                row["ID"] = idx + 1;
                row["Branches"] = "All Branches";
                let date1 = allData.time_lapse[0].date[idx];
                let date2 = allData.vs_time_lapse[0].date[idx];
                row["Date"] = `${date1 ? date1 : ""} - ${date2 ? date2 : ""}`;
                row["First Period"] = allData.time_lapse[0].data[idx] ? allData.time_lapse[0].data[idx] : 0;
                row["Second Period"] = allData.vs_time_lapse[0].data[idx] ? allData.vs_time_lapse[0].data[idx] : 0;
                return row;
            })

            renderTable(tableData);
        }

        if (FILTERS.vs_marketing) {

            let first = allData.data[0].date;
            let last = allData.data[allData.data.length - 1].date;
            let dates = getDaysArray(first, last, FILTERS.resolution);

            let count = fillDatesGaps(dates, allData.data);

            reRenderChart({
                chart: {
                    type: "bar",
                },
                series: [{ name: FILTERS.analysis_of, data: count }],
                xaxis: {
                    tickAmount: 5,
                    categories: dates,
                }
            });

            chart.removeAnnotation('start');
            chart.removeAnnotation('end');
            chart.removeAnnotation('during');
            chart.removeAnnotation('before');
            chart.removeAnnotation('after');

            chart.addXaxisAnnotation({
                id: 'before',
                x: dates[Math.floor(FILTERS.compare_period / 2)],
                strokeDashArray: 5,
                label: {
                    offsetX: 0, //-50,
                    offsetY: 50, //-20,
                    borderWidth: 0,
                    text: Number(allData.campaign.visitors.before_campaign.total).toFixed(2).toString(),
                    orientation: "horizontal",
                    style: {
                        background: '#3f3f3f',
                        fontSize: '16px',
                    },
                },
            });

            if (dates.indexOf(FILTERS.to_date) != -1) {
                chart.addXaxisAnnotation({
                    id: 'during',
                    x: dates[Math.floor(dates.length / 2)],
                    strokeDashArray: 5,
                    label: {
                        offsetX: 0, //-50,
                        offsetY: 50, //-20,
                        borderWidth: 0,
                        text: Number(allData.campaign.visitors.during_campaign.total).toFixed(2).toString(),
                        orientation: "horizontal",
                        style: {
                            background: '#3f3f3f',
                            fontSize: '16px',
                        },
                    },
                });

                chart.addXaxisAnnotation({
                    id: 'after',
                    x: dates[dates.indexOf(FILTERS.to_date) + 1],
                    strokeDashArray: 5,
                    label: {
                        offsetX: 0, //-50,
                        offsetY: 50, //-20,
                        borderWidth: 0,
                        text: Number(allData.campaign.visitors.after_campaign.total).toFixed(2).toString(),
                        orientation: "horizontal",
                        style: {
                            background: '#3f3f3f',
                            fontSize: '16px',
                        },
                    },
                });
            }

            chart.addXaxisAnnotation({
                id: 'start',
                x: FILTERS.from_date,
                label: {
                    offsetX: 0, //-50,
                    offsetY: 0, //-20,
                    text: 'Start',
                    orientation: "horizontal",
                    style: {
                        background: 'red',
                        // fontSize: '16px',
                    },
                },
            });

            chart.addXaxisAnnotation({
                id: 'end',
                x: FILTERS.to_date,
                label: {
                    offsetX: 0, //-50,
                    offsetY: 0, //-20,
                    text: 'End',
                    orientation: "horizontal",
                    style: {
                        background: 'red',
                        // fontSize: '16px',
                    },
                },
            });

            // Handle Rendering the Chart if the Result was Global
            var tableData = [];
            var counter = 0;
            for (let i = 0; i < dates.length; i++) {
                var row = {};
                row.ID = i + 1;
                row.Date = dates[i];
                row.Branchs = SELECTED_BRANCHES.length == ALL_COMPANY_BRANCHES.length ? 'All Brtanches' : FILTERS.analysis.join(",");
                row[FILTERS.analysis_of] = count[i];
                tableData.push(row);
            };

            renderTable(tableData);

            // Handle Rendering The Markiting Table;
            $('#markitingTable').removeClass('d-none');
            renderMarkitingTable(allData.campaign);

        }

        if (!FILTERS.vs_analysis_of && !FILTERS.vs_time_lapse && !FILTERS.vs_marketing) {
            let dates = allData[0].date;// getDaysArray(FILTERS.from_date, FILTERS.to_date, FILTERS.resolution);

            let tableData = [];
            var counter = 0;
            let processedData = allData.map(item => {
                item.date.forEach((date, idx) => {
                    var row = {}
                    counter++;
                    row["ID"] = counter;
                    row["Branch"] = item.branch
                    row["Date"] = date;
                    row[item.name] = item.data[idx];
                    tableData.push(row)
                })
                return {
                    name: item.branch,
                    data: item.data,
                }
            })

            reRenderChart({
                chart: {
                    type: "bar"
                },
                series: processedData,
                xaxis: {
                    tickAmount: 5,
                    categories: dates,
                }
            });

            renderTable(tableData);



        }

    };

    const handleNextPrevButtons = () => {
        $('.sliderDiv').hasClass("d-none") ? $('.sliderDiv').removeClass("d-none") : $('.sliderDiv').addClass("d-none");
        if (FILTERS.results == 'global') {
            if (!$('.sliderDiv').hasClass("d-none"))
                $('.sliderDiv').addClass("d-none")
        } else {
            $('.sliderDiv').removeClass("d-none")
        }
        $('#branchesList').val(SELECTED_BRANCHES[0].name);

    };

    function convertUnitToString(value, unit) {
        switch (unit) {
            case 'number':
                return value;
            case 'second':
                return Num2Time(value);
            case 'percent':
                return `${Number(value).toFixed(0)}%`
        }
    }

    const renderChart = (data = [], categories = []) => {
        var options = {
            series: data,
            chart: {
                type: 'line',
                height: 500,
                width: "100%",
                toolbar: {
                    show: true,
                    export: {
                        csv: {
                            filename: `${$("#companyName")[0].innerText + new Date().toLocaleDateString().split("/").join("")}` + '.xlsx',
                            columnDelimiter: ',',
                            headerCategory: 'category',
                            headerValue: 'value',
                            dateFormatter(timestamp) {
                                return new Date(timestamp).toDateString()
                            }
                        },
                        svg: {
                            filename: `${$("#companyName")[0].innerText + new Date().toLocaleDateString().split("/").join("")}`,
                        },
                        png: {
                            filename: `${$("#companyName")[0].innerText + new Date().toLocaleDateString().split("/").join("")}`,
                        }
                    },
                },
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '90%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            autoScaleYaxis: true,
            xaxis: {
                tickPlacement: 'on',
                categories: categories,
            },
            yaxis: {
                labels: {
                    formatter: function (value) {
                        var val = Math.abs(value)
                        if (val >= 1000) {
                            val = (val / 1000).toFixed(0) + ' K'
                        }
                        return val
                    }
                }
            },
            fill: {
                opacity: 1
            },
            legend: {
                showForSingleSeries: true,
                width: 800,
                height: 50,
                horizontalAlign: 'center',
                position: 'top',
                floating: false,
                fontSize: '14px',
                fontFamily: 'Helvetica, Arial',
                fontWeight: 400,
                markers: {
                    width: 12,
                    height: 12,
                    strokeWidth: 0,
                    strokeColor: '#fff',
                    fillColors: undefined,
                    radius: 12,
                    customHTML: undefined,
                    onClick: undefined,
                    offsetX: 0,
                    offsetY: 0
                },
                itemMargin: {
                    horizontal: 5,
                    vertical: 0
                },
                onItemClick: {
                    toggleDataSeries: true
                },
                onItemHover: {
                    highlightDataSeries: true
                },
                /*
                 * @param {string} seriesName - The name of the series corresponding to the legend
                 * @param {object} opts - Contains additional information as below
                 * opts: {
                 *   seriesIndex
                 *   w: {
                 *     config,
                 *     globals  
                 *   },
                 * }
                 */
                formatter: function (seriesName, opts) {
                    var item;
                    try {
                        if (FILTERS.results == 'global') {
                            item = allData.find(item => item.caption == seriesName);
                        } else {
                            item = allData.find(item => item.caption == seriesName && item.branch == $('#branchesList').val());
                        }
                        if (item) {
                            return `${seriesName} : ${convertUnitToString(item.total, item.unit)}`;
                        }
                        return seriesName;
                    } catch (error) {
                        return seriesName;
                    }
                }
            },
            tooltip: {
                followCursor: true,
                y: {
                    formatter: function (value, opt) {
                        var series_name = opt.w.globals.seriesNames[opt.seriesIndex]
                        try {
                            var item = allData.find(item => item.caption == series_name);
                            if (item) {
                                return `${series_name} : ${convertUnitToString(value, item.unit)}`;
                            }
                            return series_name;
                        } catch (error) {
                            return value;
                        }
                    },
                },
            }
        };

        chart = new ApexCharts(document.querySelector("#chart-line-data"), options);
        chart.render();
    };

    const reRenderChart = (newOptions = {}, redrawPaths = false, animate = true, updateSyncedCharts = true) => {
        chart.updateOptions(newOptions, redrawPaths, animate, updateSyncedCharts);
    };

    const handleFiltersLogic = function () {
        var height = $(window).height() - 110;
        $(".container-analytics").css("height", height)

        $('#vs-time-lapse').change(e => {
            handleTimeLapsFilter(e);
            handleNotMarketingFilters(e);
            handleNotCompare(e);
            handleChangeFilters(e);
        });

        $('#vs-markiting').change(e => {
            handleMarketingFilter(e);
            handleNotTimeLapse(e);
            handleNotCompare(e);
            handleChangeFilters(e);
            $('#resolution').val(1);
        });

        $('#vs-compare-branches').change(e => {
            handleCompareFilter(e);
            handleNotTimeLapse(e);
            handleNotMarketingFilters(e);
            handleChangeFilters(e);
        });

        const handleTimeLapsFilter = (e) => {
            $('#result option:first').prop('selected', true).trigger('change');
            e.target.checked ? $('#time-lapse').addClass('d-none') : $('#time-lapse').removeClass('d-none');
            e.target.checked ? $('#tow-time-laps-container').removeClass('d-none') : $('#tow-time-laps-container').addClass('d-none');
        }

        const handleMarketingFilter = (e) => {
            e.target.checked ? $('#value-vs-markiting').removeClass('d-none') : $('#value-vs-markiting').addClass('d-none');
            e.target.checked ? $('#resolutionDiv').addClass('d-none') : $('#resolutionDiv').removeClass('d-none');
            e.target.checked ? $('#resultDiv').addClass('d-none') : $('#resultDiv').removeClass('d-none');
        }

        const handleCompareFilter = (e) => {
            if (!$('.sliderDiv').hasClass("d-none"))
                $('.sliderDiv').addClass("d-none");
            $('#result').val('comparative').trigger('change');
            $('#select-all').prop('checked', false).trigger('change');
        }

        const handleChangeFilters = (e) => {
            $('#resolution option:first').prop('selected', true).trigger('change');
            e.target.checked ? $('#selectAllDiv').addClass('d-none') : $('#selectAllDiv').removeClass('d-none');
            $('#select-all').prop('checked', false).trigger('change');
            $('#visitors').prop('checked', true).trigger('change');
        }

        const handleNotMarketingFilters = (e) => {
            // for all changes but marketing compaign filter
            $('#branches option:first').prop('selected', true).trigger('change');
            $('#time-lape option:first').prop('selected', true).trigger('change');
            $('#time-lape1 option:first').prop('selected', true).trigger('change');
            $("#time-lape2").val('last-month').trigger('change');
            $('#value-vs-markiting').addClass('d-none');
            $('#resolutionDiv').removeClass('d-none');
            $('#resultDiv').removeClass('d-none');
            $('#vs-markiting').prop('checked', false)
            e.target.checked ? $('#resolution').prop('disabled', true) : $('#resolution').prop('disabled', false);
            e.target.checked ? $('#result').prop('disabled', true) : $('#result').prop('disabled', false);
        }

        const handleNotTimeLapse = (e) => {
            // hide time lapse option
            $('#vs-time-lapse').prop('checked', false);
            $('#time-lapse').removeClass('d-none');
            $('#tow-time-laps-container').addClass('d-none');
            $('#resolution').prop('disabled', false);
            $('#result').prop('disabled', false);
        }

        const handleNotCompare = (e) => {
            // hide compare option
            $('#vs-compare-branches').prop('checked', false);
        }

        $('#time-lape').change((e) => { handleDateChange(e) });
        $('#time-lape1').change((e) => { handleDateChange(e, '1') });
        $('#time-lape2').change((e) => { handleDateChange(e, '2') });

        function handleDateChange(e, type = '') {
            $('#dailyOption').removeClass('d-none');
            $('#hourlyOption').removeClass('d-none');
            $('#monthlyOption').removeClass('d-none');
            $('#weeklyOption').removeClass('d-none');
            let chosenDate = handleDateValues(e.target.value);
            console.log('chosenDate >>>',chosenDate);

            console.log('e.target.value >>>',e.target.value);
            switch (e.target.value) {
                case 'custom':
                    $(`.custom-date${type}`).removeClass('d-none')
                    break;
                case 'this-year':
                    $('#dailyOption').addClass('d-none');
                    $('#hourlyOption').addClass('d-none');
                    $('#resolution').val(3);

                    $(`.custom-date${type}`).addClass('d-none')
                    $(`#time-lape${type}-from`).val(toInputDateFormate(chosenDate.firstDay));
                    $(`#time-lape${type}-to`).val(toInputDateFormate(chosenDate.lastDay));
                    break;
                case "last-year":
                    $('#dailyOption').addClass('d-none');
                    $('#hourlyOption').addClass('d-none');
                    $('#resolution').val(3);

                    $(`.custom-date${type}`).addClass('d-none')
                    let chosenDate1 = handleDateValues(e.target.value);
                    $(`#time-lape${type}-from`).val(toInputDateFormate(chosenDate1.firstDay));
                    $(`#time-lape${type}-to`).val(toInputDateFormate(chosenDate1.lastDay));
                    break
                case "yesterday":
                    $('#resolution').val(1);
                    $('#monthlyOption').addClass('d-none');
                    $('#weeklyOption').addClass('d-none');
                    $(`.custom-date${type}`).addClass('d-none')
                    $(`#time-lape${type}-from`).val(toInputDateFormate(chosenDate.firstDay));
                    $(`#time-lape${type}-to`).val(toInputDateFormate(chosenDate.lastDay));
                    break;
                case "today":
                    $('#resolution').val(0);
                    $('#monthlyOption').addClass('d-none');
                    $('#weeklyOption').addClass('d-none');
                    $('#dailyOption').addClass('d-none');
                    $(`.custom-date${type}`).addClass('d-none')
                    $(`#time-lape${type}-from`).val(toInputDateFormate(chosenDate.firstDay));
                    $(`#time-lape${type}-to`).val(toInputDateFormate(chosenDate.lastDay));
                    break;
                case "this-week":
                    $('#resolution').val(2);
                    $('#monthlyOption').addClass('d-none');
                    $(`.custom-date${type}`).addClass('d-none')
                    $(`#time-lape${type}-from`).val(toInputDateFormate(chosenDate.firstDay));
                    $(`#time-lape${type}-to`).val(toInputDateFormate(chosenDate.lastDay));
                    break;
                case "last-week":
                    $('#resolution').val(2);
                    $('#monthlyOption').addClass('d-none');
                    $(`.custom-date${type}`).addClass('d-none')
                    $(`#time-lape${type}-from`).val(toInputDateFormate(chosenDate.firstDay));
                    $(`#time-lape${type}-to`).val(toInputDateFormate(chosenDate.lastDay));
                    break;
                default:
                    $(`.custom-date${type}`).addClass('d-none')
                    let chosenDate2 = handleDateValues(e.target.value);
                    $(`#time-lape${type}-from`).val(toInputDateFormate(chosenDate2.firstDay));
                    $(`#time-lape${type}-to`).val(toInputDateFormate(chosenDate2.lastDay));
                    break;
            }

            if (type = '') {
                if (e.target.value == 'today' || e.target.value == 'yesterday') {
                    $('#resolution').val(1);
                    $('#resolution').prop('disabled', true);
                } else {
                    $('#resolution').prop('disabled', false);
                }
            }
        }

        function toInputDateFormate(date) {
            var day = ("0" + date.getDate()).slice(-2);
            var month = ("0" + (date.getMonth() + 1)).slice(-2);

            return date.getFullYear() + "-" + (month) + "-" + (day);
        }

        $('#select-all').change(function (e) {
            if (this.checked) {
                $('#resolution').val(1);
                $('#resolution').prop('disabled', true);
            } else {
                $('#resolution').prop('disabled', false);
            }
        });

        $('.variable-swich').change(function (e) {
            if ($('#vs-time-lapse')[0].checked || $('#vs-markiting')[0].checked || $('#vs-compare-branches')[0].checked) {
                ALL_VARIABLES.forEach(item => {
                    if (item !== e.target.name) {
                        $(`#${item}`).prop('checked', false);
                    }
                });
            }

            if ($('#new_customer')[0].checked || $('#stay_time')[0].checked || $('#returning_customer')[0].checked) {
                if ($('#resolution').val() == 0) $('#resolution').val(1);
                $('#hourlyOption').prop('disabled', true);
            } else {
                $('#hourlyOption').prop('disabled', false);
            }
        });

        $('#select-all').change(e => {
            ALL_VARIABLES.forEach(item => {
                $(`#${item}`).prop('checked', e.target.checked);
            });
        });

        if ($('.list-branches').children().length == 0) {
            $('.list-branches').append('<span style="color:#fd5454;">No Branches Selected</span>')
        }

        $('#addBranches').on('click', (e) => {
            e.preventDefault();
            _jsBranchesModalHelper._showMoreBranshesModal__A(
                (selectedBranches) => handleBranchSelection_CB(selectedBranches),
                SELECTED_BRANCHES)
        });
        const handleBranchSelection_CB = (selectedBranches) => {
            SELECTED_BRANCHES = selectedBranches
            $('.list-branches').empty();
            $('#branchesList').empty();
            if (SELECTED_BRANCHES.length > 0) {
                SELECTED_BRANCHES.forEach(item => {
                    $('.list-branches').append(`<div><p>${item.name}</p></div>`);
                    $('#branchesList').append(`<option value="${item.name}">${item.name}</option>`);
                });
            } else {
                $('.list-branches').append('<span style="color:#fd5454;">No Branches Selected</span>')
            }
        };

        $('.img-icon-table').on('click', e => {
            $('#chartTable').removeClass('d-none');
            $('#downChart').addClass('d-none');
            $('.img-icon-chart').parent().removeClass('active');
            $('.img-icon-table').parent().addClass('active');

            if (!$('.sliderDiv').hasClass("d-none"))
                $('.sliderDiv').addClass("d-none")
        });

        $('.img-icon-chart').on('click', e => {
            $('#chartTable').addClass('d-none');
            $('#downChart').removeClass('d-none');
            $('.img-icon-chart').parent().addClass('active');
            $('.img-icon-table').parent().removeClass('active');
            if (FILTERS.results !== 'global')
                $('.sliderDiv').removeClass("d-none")
        });

        function checkCustomDate(type = '') {
            $('#dailyOption').removeClass('d-none');
            $('#hourlyOption').removeClass('d-none');
            var date1 = new Date($(`#time-lape${type}-from`).val());
            var date2 = new Date($(`#time-lape${type}-to`).val());
            var Difference_In_Time = date2.getTime() - date1.getTime();
            var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
            if (Difference_In_Days >= 364) {
                $('#dailyOption').addClass('d-none');
                $('#hourlyOption').addClass('d-none');
                $('#resolution').val(2);
            }
        }

        $('#resolution').change(function (e) {

        });

        $('#time-lape-from').change((e) => checkCustomDate())
        $('#time-lape-to').change((e) => checkCustomDate())
        $('#time-lape1-from').change((e) => checkCustomDate('1'))
        $('#time-lape1-to').change((e) => checkCustomDate('1'))
        $('#time-lape2-from').change((e) => checkCustomDate('2'))
        $('#time-lape2-to').change((e) => checkCustomDate('2'))


        document.getElementById(`time-lape1-from`).valueAsDate = new Date(new Date().getFullYear(), new Date().getMonth(), 2);
        document.getElementById(`time-lape1-to`).valueAsDate = new Date(new Date().setDate(new Date().getDate() - 1));

        document.getElementById(`time-lape2-from`).valueAsDate = new Date(new Date().getFullYear(), new Date().getMonth() - 1, 2);
        document.getElementById(`time-lape2-to`).valueAsDate = new Date(new Date().getFullYear(), new Date().getMonth(), 1);

        document.getElementById(`time-lape-from`).valueAsDate = new Date(new Date().getFullYear(), new Date().getMonth(), 2);
        document.getElementById(`time-lape-to`).valueAsDate = new Date(new Date().setDate(new Date().getDate() - 1));
        FILTERS.from_date = formateDateToString($('#time-lape-from').val());
        FILTERS.to_date = formateDateToString($('#time-lape-to').val());
    };

    const handleDateValues = (date) => {
        var firstDay, lastDay
        let todayDate = new Date();
        switch (date) {
            case 'this-month':
                firstDay = new Date(todayDate.getFullYear(), todayDate.getMonth(), 1);
                lastDay = new Date(new Date().setDate(new Date().getDate() - 1));
                return { firstDay, lastDay };
            case 'last-month':
                firstDay = new Date(todayDate.getFullYear(), todayDate.getMonth() - 1, 1);
                lastDay = new Date(todayDate.getFullYear(), todayDate.getMonth(), 0);
                return { firstDay, lastDay };
            case 'this-week':
                firstDay = getStartOfWeek();
                lastDay = getEndOfWeek();
                return { firstDay, lastDay };
            case 'last-week':
                let mydate = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
                firstDay = getStartOfWeek(mydate);
                lastDay = getEndOfWeek(mydate);
                return { firstDay, lastDay };
            case 'this-year':
                firstDay = new Date(`01-01-${todayDate.getFullYear()}`);
                lastDay = new Date(`12-31-${todayDate.getFullYear()}`);
                return { firstDay, lastDay };
            case 'last-year':
                firstDay = new Date(`01-01-${todayDate.getFullYear() - 1}`);
                lastDay = new Date(`12-31-${todayDate.getFullYear() - 1}`);
                return { firstDay, lastDay };
            case 'today':
                firstDay = todayDate;
                lastDay = todayDate;
                return { firstDay, lastDay };
            case 'yesterday':
                firstDay = new Date(new Date().setDate(new Date().getDate() - 1));
                lastDay = firstDay;
                return { firstDay, lastDay };
        }

        function getStartOfWeek(date) {
            // Copy date if provided, or use current date if not
            date = date ? new Date(+date) : new Date();
            date.setHours(25, 0, 0, 0);
            // Set date to previous Sunday
            date.setDate(date.getDate() - date.getDay());
            return date;
        }

        function getEndOfWeek(date) {
            date = getStartOfWeek(date);
            date.setDate(date.getDate() + 6);
            return date;
        }
    };

    function handleBranchArrows(idx) {
        if (SELECTED_BRANCHES[idx]) {
            $('#branchesList').val(SELECTED_BRANCHES[idx].name)
            let data = allData.filter(x => x.branch == SELECTED_BRANCHES[idx].name);
            let series = data.map(e => {
                return {
                    name: e.caption,
                    data: e.data
                }
            });
            reRenderChart({
                chart: {
                    type: "bar",
                },
                series: series,
                xaxis: {
                    tickAmount: 5,
                    categories: data[0].date,
                }
            });
        }
    };

    $("#branchesList").change((e) => {
        let currentBranch = Object.keys(allData).indexOf(e.target.value);
        handleBranchArrows(currentBranch);
    });

    const applyChanges = () => {
        $('#chartTable').addClass('d-none');
        $('#markitingTable').addClass('d-none');
        $('#downChart').removeClass('d-none');
        $('.img-icon-chart').parent().addClass('active');
        $('.img-icon-table').parent().removeClass('active');

        try {
            chart.removeAnnotation('start');
            chart.removeAnnotation('end');
            chart.removeAnnotation('during');
            chart.removeAnnotation('before');
            chart.removeAnnotation('after');
        } catch (error) { }

        FILTERS.vs_analysis_of = $('#vs-time-lapse')[0].checked == false && $('#vs-markiting')[0].checked == false && $('#vs-compare-branches')[0].checked == false
        FILTERS.vs_time_lapse = $('#vs-time-lapse')[0].checked;
        FILTERS.vs_marketing = $('#vs-markiting')[0].checked;
        FILTERS.resolution = $('#resolution').val();
        FILTERS.results = FILTERS.vs_marketing ? 'global' : $('#result').val();
        FILTERS.branch_ids = SELECTED_BRANCHES.map(e => e.id);
        SELECTED_VARIABLES = [];
        ALL_VARIABLES.forEach(item => {
            if ($(`#${item}`)[0].checked)
                if (!SELECTED_VARIABLES.includes(item))
                    SELECTED_VARIABLES.push(item);
        });
        FILTERS.analysis = SELECTED_VARIABLES;

        if (!FILTERS.vs_analysis_of) FILTERS.analysis_of = FILTERS.analysis[0];

        FILTERS.compare_period = FILTERS.vs_marketing ? $('#comparePeriod').val() : 0;
        if (!FILTERS.vs_time_lapse) {
            FILTERS.from_date = formateDateToString($('#time-lape-from').val());
            FILTERS.to_date = formateDateToString($('#time-lape-to').val());
            FILTERS.vs_from_date = '';
            FILTERS.vs_to_date = '';
        } else {
            FILTERS.from_date = formateDateToString($('#time-lape1-from').val());
            FILTERS.to_date = formateDateToString($('#time-lape1-to').val());
            FILTERS.vs_from_date = formateDateToString($('#time-lape2-from').val());
            FILTERS.vs_to_date = formateDateToString($('#time-lape2-to').val());
        }

        getData();
    };

    function renderTable(data, columns = []) {
        var summaryColumns = SELECTED_VARIABLES.map(variable => {
            return {
                "column": variable.name,
                "summaryType": variable.summaryType,
                customizeText(data) {
                    return convertUnitToString(data.value, variable.unit);
                }
            }
        });
        console.log('summaryColumns : ', summaryColumns);
        summaryColumns.push({
            "column": "First Period",
            "summaryType": "sum",
        })
        summaryColumns.push({
            "column": "Second Period",
            "summaryType": "sum",
        })
        $('#gridContainer').dxDataGrid({
            dataSource: data ? data : orders,
            keyExpr: 'ID',
            columnsAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: { visible: true },
            grouping: {
                autoExpandAll: false,
            },
            groupPanel: {
                visible: true,
            },
            headerFilter: { visible: true },
            filterBuilderPopup: {
                position: {
                    of: window,
                    at: 'top',
                    my: 'top',
                    offset: { y: 10 },
                },
            },
            showRowLines: true,
            showBorders: true,
            customizeColumns(columns) {
                try {
                    columns[0].width = 70;
                    columns[0].caption = '#';
                    columns[1].format = { type: 'dateTime' }
                    columns.forEach(c => {
                        if (c.dataField == "stay_time") {
                            c.calculateDisplayValue = function (rowData) {
                                return Num2Time(rowData['stay_time']);
                            }
                        }
                        if (percentVariables.includes(c.dataField)) {
                            c.calculateCellValue = (e) => {
                                if (c.dataField == "stay_time") {
                                    return e[c.dataField];
                                }
                                if (c.dataField == "avg_ticket_size") {
                                    return Number(e[c.dataField]).toFixed(2);
                                }
                                let value = Number(e[c.dataField]).toFixed(2);
                                return value;
                            }
                        }
                    });
                } catch (error) {

                }
            },
            paging: {
                pageSize: 10,
            },
            pager: {
                visible: true,
                allowedPageSizes: [10, 'all'],
                showPageSizeSelector: true,
                showInfo: true,
                showNavigationButtons: true,
            },
            sortByGroupSummaryInfo: [{
                summaryItem: 'count',
            }],
            summary: {
                totalItems: summaryColumns,
                groupItems: FILTERS.analysis.map(e => {
                    return {
                        'column': e,
                        'summaryType': percentVariables.includes(e) ? e != 'stay_time' ? 'avg' : 'avg' : "sum",
                        'valueFormat': percentVariables.includes(e) ? 'percent' : '',
                        customizeText(data) {
                            if (percentVariables.includes(e)) {
                                if (e == "stay_time") {
                                    return Num2Time(Number(data.value).toFixed(2));
                                }
                                if (e == "avg_ticket_size") {
                                    return Number(data.value).toFixed(2);
                                }
                                return Number(data.value).toFixed(2) + "%";
                            }
                            return Number(data.value);
                        },
                        'showInGroupFooter': false,
                    }
                })
            },
            export: {
                enabled: true,
            },
            onExporting(e) {
                const workbook = new ExcelJS.Workbook();
                const worksheet = workbook.addWorksheet('Linkers Analytics Center');

                DevExpress.excelExporter.exportDataGrid({
                    component: e.component,
                    worksheet,
                    autoFilterEnabled: true,
                }).then(() => {
                    workbook.xlsx.writeBuffer().then((buffer) => {
                        saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `${$("#companyName")[0].innerText + new Date().toLocaleDateString().split("/").join("")}` + '.xlsx');
                    });
                });
                e.cancel = true;
            },
        });
    };

    const renderMarkitingTable = (campaingData) => {
        const rows = [{
            'ID': 1,
            'Variables': 'Visitor Count',
            'BeforeCampain': Number(campaingData.visitors.before_campaign.avg).toFixed(2),
            'DuringCampain': Number(campaingData.visitors.during_campaign.avg).toFixed(2),
            'AfterCampain': Number(campaingData.visitors.after_campaign.avg).toFixed(2),
            'ChangeDuringCampain': Number(campaingData.visitors.change_during_campaign).toFixed(2),
            'CahngeAfter': Number(campaingData.visitors.change_after_campaign).toFixed(2),
        },
        {
            'ID': 2,
            'Variables': 'Turn In Rate',
            'BeforeCampain': Number(campaingData.turn_in_rate.before_campaign.avg).toFixed(2),
            'DuringCampain': Number(campaingData.turn_in_rate.during_campaign.avg).toFixed(2),
            'AfterCampain': Number(campaingData.turn_in_rate.after_campaign.avg).toFixed(2),
            'ChangeDuringCampain': Number(campaingData.turn_in_rate.change_during_campaign).toFixed(2),
            'CahngeAfter': Number(campaingData.turn_in_rate.change_after_campaign).toFixed(2),
        },
        {
            'ID': 3,
            'Variables': 'Returning Rate (%)',
            'BeforeCampain': Number(campaingData.returning_customer.before_campaign.avg).toFixed(2),
            'DuringCampain': Number(campaingData.returning_customer.during_campaign.avg).toFixed(2),
            'AfterCampain': Number(campaingData.returning_customer.after_campaign.avg).toFixed(2),
            'ChangeDuringCampain': Number(campaingData.returning_customer.change_during_campaign).toFixed(2),
            'CahngeAfter': Number(campaingData.returning_customer.change_after_campaign).toFixed(2),
        },
        {
            'ID': 4,
            'Variables': 'Outside Traffic',
            'BeforeCampain': Number(campaingData.outside_traffic.before_campaign.avg).toFixed(2),
            'DuringCampain': Number(campaingData.outside_traffic.during_campaign.avg).toFixed(2),
            'AfterCampain': Number(campaingData.outside_traffic.after_campaign.avg).toFixed(2),
            'ChangeDuringCampain': Number(campaingData.outside_traffic.change_during_campaign).toFixed(2),
            'CahngeAfter': Number(campaingData.outside_traffic.change_after_campaign).toFixed(2),
        },
        {
            'ID': 4,
            'Variables': 'Sales Conversion',
            'BeforeCampain': Number(campaingData.sales_conversion.before_campaign.avg).toFixed(2),
            'DuringCampain': Number(campaingData.sales_conversion.during_campaign.avg).toFixed(2),
            'AfterCampain': Number(campaingData.sales_conversion.after_campaign.avg).toFixed(2),
            'ChangeDuringCampain': Number(campaingData.sales_conversion.change_during_campaign).toFixed(2),
            'CahngeAfter': Number(campaingData.sales_conversion.change_after_campaign).toFixed(2),
        },
        {
            'ID': 5,
            'Variables': 'Average Visit Duration',
            'BeforeCampain': Number(campaingData.stay_time.before_campaign.avg).toFixed(2),
            'DuringCampain': Number(campaingData.stay_time.during_campaign.avg).toFixed(2),
            'AfterCampain': Number(campaingData.stay_time.after_campaign.avg).toFixed(2),
            'ChangeDuringCampain': Number(campaingData.stay_time.change_during_campaign).toFixed(2),
            'CahngeAfter': Number(campaingData.stay_time.change_after_campaign).toFixed(2),
        },
        ];

        $('#markitingGridContainer').dxDataGrid({
            dataSource: rows,
            keyExpr: 'ID',
            showBorders: true,
            selection: {
                mode: 'single',
            },
            columns: [{
                dataField: 'Variables',
                caption: 'Variables',
            },
            {
                dataField: 'BeforeCampain',
                caption: 'BEFORE CAMPAIGN (AVARAGE PER DAY)',
                dataType: 'number',
            },
            {
                dataField: 'DuringCampain',
                caption: 'DURING CAMPAIGN (AVARAGE PER DAY)',
                dataType: 'number',
            },
            {
                dataField: 'AfterCampain',
                caption: 'AFTER CAMPAIGN (AVARAGE PER DAY)',
                dataType: 'number',
            },
            {
                dataField: 'ChangeDuringCampain',
                caption: '% CHANGE DURING CAMPAIGN',
                format: 'percent',
            },
            {
                dataField: 'CahngeAfter',
                caption: '% CHANGE AFTER CAMPAIGN',
                format: 'percent',
            },
            ],
            export: {
                enabled: true,
            },
            onExporting(e) {
                const workbook = new ExcelJS.Workbook();
                const worksheet = workbook.addWorksheet('Linkers Markiting Analytics Center');

                DevExpress.excelExporter.exportDataGrid({
                    component: e.component,
                    worksheet,
                    autoFilterEnabled: true,
                }).then(() => {
                    workbook.xlsx.writeBuffer().then((buffer) => {
                        saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `${$("#companyName")[0].innerText + new Date().toLocaleDateString().split("/").join("")}` + '.xlsx');
                    });
                });
                e.cancel = true;
            },
        });
    };

    $('#btn-apply').on('click', e => applyChanges(e));

    $(".rightArrowDiv").on('click', e => {
        let idx = SELECTED_BRANCHES.findIndex(x => x.id == SELECTED_BRANCHES.find(x => x.name == $('#branchesList').val()).id);
        handleBranchArrows(idx + 1)
    });

    $(".leftArrowDiv").on('click', e => {
        let idx = SELECTED_BRANCHES.findIndex(x => x.id == SELECTED_BRANCHES.find(x => x.name == $('#branchesList').val()).id);
        handleBranchArrows(idx - 1);
    });

    handleFiltersLogic();
    renderChart();
    getData();
    return;
});