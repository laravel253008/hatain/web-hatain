var _PREFIX_ADMIN_API = _jsHelpers._PREFIX_ADMIN_API;
var urlChart = _PREFIX_ADMIN_API + "/reports/company_weekly";
var chart;
var SELECTED_VARIABLES = [];
var ALL_COMPANY_BRANCHES = [];
var ALL_COMPANY_GROUPS = [];
var SELECTED_BRANCHES = [];
var allData;
var FILTERS = {
  branch_ids: [],
  time_lapse: "this_month",
  type: "branches",
  from_date: '12/02/2022',
  to_date: '19/02/2022',
};
var linkersGraphColors = ['#186BA0', '#EA7268', '#CC549F', '#EE9A3A', '#5EC3AD', '#17A9DD'];

function Num2Time(stay_time) {
  min = stay_time - Math.floor(stay_time / 60) * 60
  sec = (Number(min - Math.floor(min)).toFixed(2)) * 60;
  return `${String(Math.floor(stay_time / 60)).padStart(2, "0")}:${String(Math.floor(min)).padStart(2, "0")}:${String(Math.floor(sec)).padStart(2, "0")}`
}

var sparkelOptions = {
  series: [{
    data: [],//[0, 20, 30, 0, 50, 60, 70, 80]
  }],
  chart: {
    type: 'area',
    height: 150,
    // width:150,
    sparkline: {
      enabled: true
    },
  },
  stroke: {
    curve: 'smooth'
  },
  fill: {
    opacity: 0.3,
  },
  yaxis: {
    min: 0
  },
  colors: ['#17A9DD'],
  title: {
    text: '0',//'$424,652',
    offsetX: 0,
    style: {
      fontSize: '12px',
      fontFamily: 'tahoma',
      color: '#525352'
    }
  },
  subtitle: {
    text: '',//'Sales',
    offsetX: 0,
    style: {
      fontSize: '18px',
      fontFamily: 'tahoma',
      color: '#525352'
    }
  }
};

var barChartOptions = {
  series: [{
    data: []
  }],
  chart: {
    type: 'bar',
    height: 350,
    zoom: {
      enabled: false,
    },
    toolbar: {
      show: false
    }
  },
  tooltip: {
    enabled: false
  },
  plotOptions: {
    bar: {
      borderRadius: 4,
      horizontal: false,
    }
  },
  dataLabels: {
    enabled: true
  },
  colors: linkersGraphColors,
  xaxis: {
    categories: ['visitors','customers','stayTime','lost','AVG_VisitDusration','outSideTraffice',
    'returningCustomers','newCustomers','turnInRate','salesConversion','salesAmount','avg_TicketSize'].map(item=>_jsHelpers._trans(`variables.${item}`))//['Visitors', 'Lost Customers', 'Customers', 'Outside Traffic', 'New Customers', 'Returning Customers', 'Stay Time', 'Turn In Rate', 'Sales Conversion'],
  },
};

var bubbleOptions = {
  series: [
    {
      name: "SAMPLE A",
      data: []
    },
    {
      name: "SAMPLE B",
      data: []
    },
    {
      name: "SAMPLE C",
      data: []
    }
  ],
  colors: linkersGraphColors,
  chart: {
    height: 350,
    type: "scatter",
    zoom: {
      enabled: false
    },
    toolbar: {
      show: false
    }
  },
  dataLabels: {
    enabled: true,
    formatter: function (val) {
      return parseFloat(val).toFixed(0);
    },
  },
  tootip: {
    enabled: false
  },
  xaxis: {
    categories: ['sa', 'su', 'mo', 'tu', 'we', 'th', 'fr'].map(item=>_jsHelpers._trans(`days.${item}`))
  },
  yaxis: {
    labels: {
      formatter: function (val) {
        return parseFloat(val).toFixed(0);
      }
    }
  }
};

companyName = $("#companyName")[0].innerText;
$(".cx-report-title").text(companyName + " " +_jsHelpers._trans("siteWeeklyReport"));
handleDateSelection();

var totVisitors = new ApexCharts(document.querySelector("#total-visitors"), sparkelOptions);
var totOutsideTrffic = new ApexCharts(document.querySelector("#total-outside-traffic"), sparkelOptions);
var totReturningCustomers = new ApexCharts(document.querySelector("#total-returning-customers"), sparkelOptions);
var totNewCustomers = new ApexCharts(document.querySelector("#total-new-customers"), sparkelOptions);
var totStayTime = new ApexCharts(document.querySelector("#total-stay-time"), sparkelOptions);
var totTurnInRate = new ApexCharts(document.querySelector("#total-turn-in-rate"), sparkelOptions);
var totCustomers = new ApexCharts(document.querySelector("#total-customers"), sparkelOptions);
var totLostCustomers = new ApexCharts(document.querySelector("#total-lost-customers"), sparkelOptions);
var totConversion = new ApexCharts(document.querySelector("#total-conversion"), sparkelOptions);
totVisitors.render();
totOutsideTrffic.render();
totReturningCustomers.render();
totNewCustomers.render();
totStayTime.render();
totTurnInRate.render();
totCustomers.render();
totLostCustomers.render();
totConversion.render();

var bubbleChartPercent = new ApexCharts(document.querySelector("#general-allPercent"), bubbleOptions);
var bubbleChartCount = new ApexCharts(document.querySelector("#general-allCount"), bubbleOptions);
bubbleChartPercent.render();
bubbleChartCount.render();

var thisWeek_general_allBar = new ApexCharts(document.querySelector("#thisWeek_general_allBar"), barChartOptions);
var lastWeek_general_allBar = new ApexCharts(document.querySelector("#lastWeek_general_allBar"), barChartOptions);
var WeekDays = new ApexCharts(document.querySelector("#WeekDays"), barChartOptions);
var WeekEnds = new ApexCharts(document.querySelector("#WeekEnds"), barChartOptions);
var bestDay = new ApexCharts(document.querySelector("#bestDay"), barChartOptions);
var otherDays = new ApexCharts(document.querySelector("#otherDays"), barChartOptions);
thisWeek_general_allBar.render();
lastWeek_general_allBar.render();
WeekDays.render();
WeekEnds.render();
bestDay.render();
otherDays.render();
/****************************************View *******************************************/
var totVisitors_view = new ApexCharts(document.querySelector("#total-visitors_view"), sparkelOptions);
var totOutsideTrffic_view = new ApexCharts(document.querySelector("#total-outside-traffic_view"), sparkelOptions);
var totReturningCustomers_view = new ApexCharts(document.querySelector("#total-returning-customers_view"), sparkelOptions);
var totNewCustomers_view = new ApexCharts(document.querySelector("#total-new-customers_view"), sparkelOptions);
var totStayTime_view = new ApexCharts(document.querySelector("#total-stay-time_view"), sparkelOptions);
var totTurnInRate_view = new ApexCharts(document.querySelector("#total-turn-in-rate_view"), sparkelOptions);
var totCustomers_view = new ApexCharts(document.querySelector("#total-customers_view"), sparkelOptions);
var totLostCustomers_view = new ApexCharts(document.querySelector("#total-lost-customers_view"), sparkelOptions);
var totConversion_view = new ApexCharts(document.querySelector("#total-conversion_view"), sparkelOptions);
totVisitors_view.render();
totOutsideTrffic_view.render();
totReturningCustomers_view.render();
totNewCustomers_view.render();
totStayTime_view.render();
totTurnInRate_view.render();
totCustomers_view.render();
totLostCustomers_view.render();
totConversion_view.render();

var bubbleChartPercent_view = new ApexCharts(document.querySelector("#general-allPercent_view"), bubbleOptions);
var bubbleChartCount_view = new ApexCharts(document.querySelector("#general-allCount_view"), bubbleOptions);
bubbleChartPercent_view.render();
bubbleChartCount_view.render();

var thisWeek_general_allBar_view = new ApexCharts(document.querySelector("#thisWeek_general_allBar_view"), barChartOptions);
var lastWeek_general_allBar_view = new ApexCharts(document.querySelector("#lastWeek_general_allBar_view"), barChartOptions);
var WeekDays_View = new ApexCharts(document.querySelector("#WeekDays_View"), barChartOptions);
var WeekEnds_View = new ApexCharts(document.querySelector("#WeekEnds_View"), barChartOptions);
var bestDay_View = new ApexCharts(document.querySelector("#bestDay_View"), barChartOptions);
var otherDays_View = new ApexCharts(document.querySelector("#otherDays_View"), barChartOptions);
thisWeek_general_allBar_view.render();
lastWeek_general_allBar_view.render();
WeekDays_View.render();
WeekEnds_View.render();
bestDay_View.render();
otherDays_View.render();

function handleDateSelection() {
  var date_element = document.getElementById(`dateInput`);
  var today_date = new Date(new Date().setDate(new Date().getDate() - 6));
  date_element.valueAsDate = today_date;
  date_element.max = today_date.getFullYear() + "-" + ("0" + (today_date.getMonth() + 1)).slice(-2) +
    "-" + ("0" + (today_date.getDate() + 1)).slice(-2);
}
async function getData() {
  _jsHelpers._blockUI();
  if (ALL_COMPANY_BRANCHES.length == 0) {
    await $.ajax({
      type: "POST",
      url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branchesWithGroups/",
      headers: _jsHelpers._getHeaders(),
    }).done((res) => {
      if (!res.error) {
        ALL_COMPANY_BRANCHES = res.data.branches;
        ALL_COMPANY_GROUPS = res.data.groups;
        SELECTED_BRANCHES = ALL_COMPANY_BRANCHES.map(item => {
          item.group_id = 0;
          return item;
        });
      }
    }).fail((error) => {
      _jsHelpers._handle_notify('danger', 'Server Error : Can\'t Fetch Company Branches : ' + error);
    });
  }

  if (ALL_COMPANY_BRANCHES.length !== SELECTED_BRANCHES.length) {
    $('.branches').text(`(${SELECTED_BRANCHES.map(e => e.name).join(',')})`);
  }
  $('#dateInput').trigger('change')

  $('.rpt-date').text(`${_jsHelpers._trans("from")} (${FILTERS.from_date}) ${_jsHelpers._trans("to")} (${FILTERS.to_date})`);

  FILTERS.branch_ids = SELECTED_BRANCHES.map(e => e.id);
  var error = false;
  await $.ajax({
    type: "GET",
    url: urlChart,
    headers: _jsHelpers._getHeaders(),
    data: FILTERS,
  }).done((res) => {
    allData = res.data
    if (res.error) {
      _jsHelpers._handle_notify('danger', res.message);
      _jsHelpers._unBlockUi();
      error = true
    }
  }).fail((error) => {
    _jsHelpers._handle_notify('danger', error);
  });
  _jsHelpers._unBlockUi();
  if (error) return;
  fillBranchesTable(allData.sammury);

  function RenderSparkelsCharts(chart, variable, title) {
    var thisWeekData = allData.daily;
    let count = thisWeekData.data.map(item => {
      return item.count[variable] == 0 ? 0.1 : item.count[variable];
    });

    if (['stay_time', 'turn_in_rate', 'returning_customer', 'new_customer', 'conversion'].includes(variable)) {
      chart.updateOptions({
        series: [{
          data: count
        }],
        title: {
          text: title,
        },
        subtitle: {
          text: variable == 'stay_time' ?
            Num2Time(Number(allData.daily.total[variable]).toFixed(1)) :
            Number(allData.daily.total[variable]).toFixed(1) + "%"
          // text: Number(allData.daily.total[variable]).toFixed(1) + (variable == 'stay_time' ? " min" : "%"),
        }
      });
    } else {
      chart.updateOptions({
        series: [{
          data: count
        }],
        title: {
          text: title,
        },
        subtitle: {
          text: variable == 'customers' ? allData.daily.total[variable] : allData.daily.total[variable],
        }
      });
    }
  }

  RenderSparkelsCharts(totVisitors, 'visitors', _jsHelpers._trans('variables.visitors'));
  RenderSparkelsCharts(totOutsideTrffic, 'outside_traffic', _jsHelpers._trans('variables.outSideTraffice'));
  RenderSparkelsCharts(totReturningCustomers, 'returning_customer', _jsHelpers._trans('variables.returningCustomers'));
  RenderSparkelsCharts(totNewCustomers, 'new_customer', _jsHelpers._trans('variables.newCustomers'));
  RenderSparkelsCharts(totStayTime, 'stay_time', _jsHelpers._trans('variables.stayTime'));
  RenderSparkelsCharts(totTurnInRate, 'turn_in_rate', _jsHelpers._trans('variables.turnInRate'));
  RenderSparkelsCharts(totCustomers, 'customers', _jsHelpers._trans('variables.customers'));
  RenderSparkelsCharts(totLostCustomers, 'lost', _jsHelpers._trans('variables.lost'));
  RenderSparkelsCharts(totConversion, 'conversion', _jsHelpers._trans('variables.salesConversion'));

  RenderSparkelsCharts(totVisitors_view, 'visitors', _jsHelpers._trans('variables.visitors'));
  RenderSparkelsCharts(totOutsideTrffic_view, 'outside_traffic', _jsHelpers._trans('variables.outSideTraffice'));
  RenderSparkelsCharts(totReturningCustomers_view, 'returning_customer', _jsHelpers._trans('variables.returningCustomers'));
  RenderSparkelsCharts(totNewCustomers_view, 'new_customer', _jsHelpers._trans('variables.newCustomers'));
  RenderSparkelsCharts(totStayTime_view, 'stay_time', _jsHelpers._trans('variables.stayTime'));
  RenderSparkelsCharts(totTurnInRate_view, 'turn_in_rate', _jsHelpers._trans('variables.turnInRate'));
  RenderSparkelsCharts(totCustomers_view, 'customers', _jsHelpers._trans('variables.customers'));
  RenderSparkelsCharts(totLostCustomers_view, 'lost', _jsHelpers._trans('variables.lost'));
  RenderSparkelsCharts(totConversion_view, 'conversion', _jsHelpers._trans('variables.salesConversion'));



  let returningCustomer = [];
  let newCustomer = [];
  let stayTime = [];
  let conversion = [];
  let turnInRate = [];

  let visitors = [];
  let customers = [];
  let lostCustomers = [];
  let outsideTrffic = [];
  let dates = [];
  allData.daily.data.forEach(item => {
    let day = new Date(item.date.split("/")[2], item.date.split("/")[1] - 1, item.date.split("/")[0]);
    dates.push(day.toDateString().split(" ")[0]);
    returningCustomer.push(item.count.returning_customer);
    newCustomer.push(item.count.new_customer);
    stayTime.push(item.count.stay_time);
    conversion.push(item.count.conversion);
    turnInRate.push(item.count.turn_in_rate);

    visitors.push(item.count.visitors);
    customers.push(item.count.customers);
    lostCustomers.push(item.count.lost);
    outsideTrffic.push(item.count.outside_traffic);
  });
  bubbleChartPercent.updateOptions({
    series: [
      {
        name: "Returning Customers",
        data: returningCustomer
      },
      {
        name: "New Customers",
        data: newCustomer
      },
      {
        name: "Stay Time",
        data: stayTime
      },
      {
        name: "Sales Conversion",
        data: conversion
      },
      {
        name: "Turn In Rate",
        data: turnInRate
      },
    ],
    title: {
      text: "General data Chart"
    },
    xaxis: {
      categories: dates
    },
  });

  bubbleChartCount.updateOptions({
    series: [
      {
        name: "Visitors",
        data: visitors
      },
      {
        name: "Customers",
        data: customers
      },
      {
        name: "Lost Customers",
        data: lostCustomers
      },
      {
        name: "Outside Traffic",
        data: outsideTrffic
      },
    ],
    xaxis: {
      categories: dates
    },
  });

  function handleNigativeNumbers(num) {
    return num < 0 ? 0 : num
  }

  thisWeek_general_allBar.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.week.this_week.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For The Week"
    },
  });

  lastWeek_general_allBar.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.week.last_days.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For The Last 30 Days"
    },
  });

  WeekDays.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.weekdays.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For WeekDays"
    },
  });

  WeekEnds.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.weekends.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For The WeekEnds"
    },
  });
  /***************************VEIEW******************************** */
  bubbleChartPercent_view.updateOptions({
    series: [
      {
        name: "Returning Customers",
        data: returningCustomer
      },
      {
        name: "New Customers",
        data: newCustomer
      },
      {
        name: "Stay Time",
        data: stayTime
      },
      {
        name: "Sales Conversion",
        data: conversion
      },
      {
        name: "Turn In Rate",
        data: turnInRate
      },
    ],
    title: {
      text: "General data Chart"
    },
    xaxis: {
      categories: dates
    },
  });

  bubbleChartCount_view.updateOptions({
    series: [
      {
        name: "Visitors",
        data: visitors
      },
      {
        name: "Customers",
        data: customers
      },
      {
        name: "Lost Customers",
        data: lostCustomers
      },
      {
        name: "Outside Traffic",
        data: outsideTrffic
      },
    ],
    xaxis: {
      categories: dates
    },
  });

  thisWeek_general_allBar_view.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.week.this_week.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.this_week.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For The Week"
    },
  });

  lastWeek_general_allBar_view.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.week.last_days.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.week.last_days.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For The Last 30 Days"
    },
  });

  WeekDays_View.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.weekdays.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For WeekDays"
    },
  });

  WeekEnds_View.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.weekends.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For The WeekEnds"
    },
  });

  findBestDay();

}

function fillBranchesTable(sumaryData) {
  var tableBody = document.getElementById("tableBody");
  var tableBody_view = document.getElementById("tableBody_view");
  tableBody.innerHTML = ""
  tableBody_view.innerHTML = ""
  Object.keys(sumaryData.total_week).forEach(branch => {
    var newRow_view = document.createElement("tr");
    var newRow = document.createElement("tr");

    var tdBranchName = document.createElement("td")
    var tdBranchName_view = document.createElement("td")

    tdBranchName.textContent = branch;
    tdBranchName_view.textContent = branch;

    tdBranchName.className = "branch-table-cell"
    tdBranchName_view.className = "branch-table-cell_view"

    newRow.append(tdBranchName)
    newRow_view.append(tdBranchName_view)
    Object.keys(sumaryData.total_week[branch][0]).forEach(item => {
      let td = document.createElement("td");
      let td_view = document.createElement("td");

      let div = document.createElement("div");
      let div_view = document.createElement("div");

      div.style.display = "flex"
      div_view.style.display = "flex"

      td.className = "branch-table-cell"
      td_view.className = "branch-table-cell_view"

      if (['stay_time', 'turn_in_rate', 'returning_customer', 'new_customer'].includes(item)) {
        div.innerHTML = item == 'stay_time' ? '<p>' + Number(sumaryData.total_week[branch][0][item]).toFixed(2) + " Min" + '</p>' : '<p>' + Number(sumaryData.total_week[branch][0][item]).toFixed(2) + "%" + '</p>';
        div_view.innerHTML = item == 'stay_time' ? '<p>' + Number(sumaryData.total_week[branch][0][item]).toFixed(2) + " Min" + '</p>' : '<p>' + Number(sumaryData.total_week[branch][0][item]).toFixed(2) + "%" + '</p>';
      } else {
        div.innerHTML = '<p>' + sumaryData.total_week[branch][0][item].toFixed(2) + '</p>';
        div_view.innerHTML = '<p>' + sumaryData.total_week[branch][0][item].toFixed(2) + '</p>';
      }
      let indecator = document.createElement("i");
      let indecator_view = document.createElement("i");

      if (sumaryData.avg_week[branch] && sumaryData.avg_last_days[branch]) {
        if (sumaryData.avg_week[branch][0][item] == sumaryData.avg_last_days[branch][0][item]) {
          indecator.className = "bi bi-caret-right-fill"
          indecator_view.className = "bi bi-caret-right-fill"
        } else {
          if (sumaryData.avg_week[branch][0][item] > sumaryData.avg_last_days[branch][0][item]) {
            indecator.className = "bi bi-caret-up-fill"
            indecator_view.className = "bi bi-caret-up-fill"
          } else {
            indecator.className = "bi bi-caret-down-fill"
            indecator_view.className = "bi bi-caret-down-fill"
          }
        }
      } else {
        indecator.className = "bi bi-caret-right-fill"
        indecator_view.className = "bi bi-caret-right-fill"
      }
      div.append(indecator)
      div_view.append(indecator_view)
      td.append(div)
      td_view.append(div_view)


      newRow.append(td);
      newRow_view.append(td_view);
    });
    tableBody.append(newRow);
    tableBody_view.append(newRow_view);
  })
}

function findBestDay() {
  let visitors = 0;
  let bestDayDate = '';
  let bestDayArr = [];
  let otherDaysArr = [];
  allData.daily.data.forEach(item => {
    if (item.count.visitors >= visitors) {
      visitors = item.count.visitors;
      bestDayDate = item.date;
    }
  });

  allData.daily.data.forEach(item => {
    if (item.date.split(" ")[0] == bestDayDate) {
      bestDayArr.push(item);
    } else {
      otherDaysArr.push(item);
    }
  });

  var bestDayVisitors = 0;
  var bestDayCustomers = 0;
  var bestDayOutsideTraffic = 0;
  var bestDayLostCustomers = 0;
  var bestDayReturningCustomers = 0;
  var bestDayNewCustomers = 0;
  var bestDayStayTime = 0;
  var bestDayTurnInRate = 0;
  var bestDayConversion = 0;
  bestDayArr.forEach(item => {
    bestDayVisitors = bestDayVisitors + item.count.visitors
    bestDayCustomers = bestDayCustomers + item.count.customers
    bestDayOutsideTraffic = bestDayOutsideTraffic + item.count.outside_traffic
    bestDayLostCustomers = bestDayLostCustomers + item.count.lost
    bestDayReturningCustomers = bestDayReturningCustomers + item.count.returning_customer
    bestDayNewCustomers = bestDayNewCustomers + item.count.new_customer
    bestDayStayTime = bestDayStayTime + item.count.stay_time
    bestDayTurnInRate = bestDayTurnInRate + item.count.turn_in_rate
    bestDayConversion = bestDayConversion + item.count.conversion
  })
  let bestDayAvg = {
    visitors: bestDayVisitors,
    customers: bestDayCustomers,
    outside_traffic: bestDayOutsideTraffic,
    lost: bestDayLostCustomers,
    returning_customer: bestDayReturningCustomers,
    new_customer: bestDayNewCustomers,
    turn_in_rate: bestDayTurnInRate,
    stay_time: bestDayStayTime,
    conversion: bestDayConversion,
  }

  var otherDaysVisitors = 0;
  var otherDaysCustomers = 0;
  var otherDaysOutsideTraffic = 0;
  var otherDaysLostCustomers = 0;
  var otherDaysReturningCustomers = 0;
  var otherDaysNewCustomers = 0;
  var otherDaysStayTime = 0;
  var otherDaysTurnInRate = 0;
  var otherDaysConversion = 0;
  otherDaysArr.forEach(item => {
    otherDaysVisitors = otherDaysVisitors + item.count.visitors
    otherDaysCustomers = otherDaysCustomers + item.count.customers
    otherDaysOutsideTraffic = otherDaysOutsideTraffic + item.count.outside_traffic
    otherDaysLostCustomers = otherDaysLostCustomers + item.count.lost
    otherDaysReturningCustomers = otherDaysReturningCustomers + item.count.returning_customer
    otherDaysNewCustomers = otherDaysNewCustomers + item.count.new_customer
    otherDaysStayTime = otherDaysStayTime + item.count.stay_time
    otherDaysTurnInRate = otherDaysTurnInRate + item.count.turn_in_rate
    otherDaysConversion = otherDaysConversion + item.count.conversion
  })

  let otherDaysAvg = {
    visitors: (otherDaysVisitors) / 6,
    customers: (otherDaysCustomers) / 6,
    outside_traffic: (otherDaysOutsideTraffic) / 6,
    lost: (otherDaysLostCustomers) / 6,
    returning_customer: otherDaysReturningCustomers / 6,
    new_customer: otherDaysNewCustomers / 6,
    turn_in_rate: otherDaysTurnInRate / 6,
    stay_time: otherDaysStayTime / 6,
    conversion: otherDaysConversion / 6,
  }

  bestDay_View.updateOptions({
    series: [{
      data: [
        Number(bestDayAvg.visitors).toFixed(2),
        Number(bestDayAvg.lost).toFixed(2),
        Number(bestDayAvg.customers).toFixed(2),
        Number(bestDayAvg.outside_traffic).toFixed(2),
        Number(bestDayAvg.new_customer).toFixed(2),
        Number(bestDayAvg.returning_customer).toFixed(2),
        Number(bestDayAvg.stay_time).toFixed(2),
        Number(bestDayAvg.turn_in_rate).toFixed(2),
        Number(bestDayAvg.conversion).toFixed(2),
      ]
    }],
    title: {
      text: "The BestDay : " + bestDayDate
    },
  });

  bestDay.updateOptions({
    series: [{
      data: [
        Number(bestDayAvg.visitors).toFixed(2),
        Number(bestDayAvg.lost).toFixed(2),
        Number(bestDayAvg.customers).toFixed(2),
        Number(bestDayAvg.outside_traffic).toFixed(2),
        Number(bestDayAvg.new_customer).toFixed(2),
        Number(bestDayAvg.returning_customer).toFixed(2),
        Number(bestDayAvg.stay_time).toFixed(2),
        Number(bestDayAvg.turn_in_rate).toFixed(2),
        Number(bestDayAvg.conversion).toFixed(2),
      ]
    }],
    title: {
      text: "The BestDay : " + bestDayDate
    },
  });


  otherDays_View.updateOptions({
    series: [{
      data: [
        Number(otherDaysAvg.visitors).toFixed(2),
        Number(otherDaysAvg.lost).toFixed(2),
        Number(otherDaysAvg.customers).toFixed(2),
        Number(otherDaysAvg.outside_traffic).toFixed(2),
        Number(otherDaysAvg.new_customer).toFixed(2),
        Number(otherDaysAvg.returning_customer).toFixed(2),
        Number(otherDaysAvg.stay_time).toFixed(2),
        Number(otherDaysAvg.turn_in_rate).toFixed(2),
        Number(otherDaysAvg.conversion).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For The Other Days"
    },
  });

  otherDays.updateOptions({
    series: [{
      data: [
        Number(otherDaysAvg.visitors).toFixed(2),
        Number(otherDaysAvg.lost).toFixed(2),
        Number(otherDaysAvg.customers).toFixed(2),
        Number(otherDaysAvg.outside_traffic).toFixed(2),
        Number(otherDaysAvg.new_customer).toFixed(2),
        Number(otherDaysAvg.returning_customer).toFixed(2),
        Number(otherDaysAvg.stay_time).toFixed(2),
        Number(otherDaysAvg.turn_in_rate).toFixed(2),
        Number(otherDaysAvg.conversion).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For The Other Days"
    },
  });
}

$('#brachSelection').on('click', (e) => {
  e.preventDefault();
  _jsBranchesModalHelper._showMoreBranshesModal__A(
    (selectedBranches) => handleBranchSelection_CB(selectedBranches),
    SELECTED_BRANCHES
  );
});

const handleBranchSelection_CB = (selectedBranches) => {
  SELECTED_BRANCHES = selectedBranches //list-branches
};

function getMonthFromString(mon) {

  var d = Date.parse(mon + "1, 2012");
  if (!isNaN(d)) {
    return new Date(d).getMonth() + 1;
  }
  return -1;
}

$('#dateInput').on('change', e => {
  let date = e.target.value.split("-");
  let from = new Date(date[0], date[1] - 1, date[2]);
  let to = new Date(date[0], date[1] - 1, Number(date[2]) + 6);
  let arr1 = from.toDateString().split(" ");
  let arr2 = to.toDateString().split(" ");
  FILTERS.from_date = `${arr1[2]}/${getMonthFromString(arr1[1])}/${arr1[3]}`
  FILTERS.to_date = `${arr2[2]}/${getMonthFromString(arr2[1])}/${arr2[3]}`
});

$('#btn-apply').on('click', e => {
  getData();
});

$('#nextIcon').on('click', e => {
  _jsHelpers._blockUI();
  $('#test').removeAttr('hidden');

  new Promise(function (resolve, reject) {
    try {
      setTimeout(function () {
        var element = document.getElementById('test');
        var opt = {
          margin: 0,
          filename: `Report-${new Date().toDateString()}.pdf`,
          image: { type: "jpeg", quality: 0.98 },
          pagebreak: { mode: "css", before: ".breackMode" },
          html2canvas: { scale: 1 },
          jsPDF: { unit: "in", format: "letter", orientation: "portrait" },
        };

        html2pdf(element, opt);

        resolve("anything");
      }, 500);
    } catch (error) {
      reject(error);
    }
  }).then(() => {
    $('#test').attr('hidden', true);
    _jsHelpers._unBlockUi();
  })
});

getData();