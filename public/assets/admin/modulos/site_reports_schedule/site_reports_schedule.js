const _PREFIX_ADMIN_API = _jsHelpers._PREFIX_ADMIN_API;
const modalAdd = $("#modal-add-schedule");
const modalDelete = $("#modal-delete-schedule");
const modalEdit = $("#modal-update-schedule");
var BRANCHES = [];
var GROUPS = [];
var SELECTED_BRANCHES = [];
var SELECTED_GROUPS = [];
var COMPANY_USERS = [];
var COMPANY_ID = $("#in-data").data("company-id");
var select_branch_add = $(".select2-branch");
var select_branch_update = $(".select2-branch-edit");

const updateSchedule = function (id) {
  const formParent_ = "#form-update-schedule ";
  const branchId = $(formParent_ + "#branch-id").val();
  const parent = "#card-" + branchId + " ";
  var $parent = $("#card-" + branchId);

  var data = {
    id,
    name: $("#schedule-name-edit").val(),
    branch_ids: SELECTED_BRANCHES.map((e) => e.id),
    user_ids: $("#select2-users-edit").val(),
    company_id: COMPANY_ID,
    report_type: $("#report-type-edit").val(),
    report_time: 1,
    report_date: 1,
  };

  _jsHelpers._blockUI();
  $.ajax({
    url: _PREFIX_ADMIN_API + "/reports/schedule_report",
    type: "PUT",
    headers: _jsHelpers._getHeaders(),
    data,
  }).done(function (response) {
    if (!response.error) {
      $(parent + ".closeOptions").click();
      $parent.data("name", data.name);
      $parent.data("users", data.user_ids);
      $parent.data("branches", data.branch_ids);
      $parent.data("report_type", data.report_type);
      _jsHelpers._handle_notify("success", response.message);
      modalEdit.modal("hide");
    } else {
      _jsHelpers._responseError(response);
    }
    _jsHelpers._unBlockUi();
  }).fail(function (err) {
    _jsHelpers._handle_notify(
      "danger",
      "Oops! There were found a problem."
    );
    _jsHelpers._unBlockUi();
  });
};

const add = function () {
  var data = {
    name: $("#modal-add-schedule #schedule-name").val(),
    branch_ids: SELECTED_BRANCHES.map((e) => e.id),
    user_ids: $("#form-add-schedule #select2-users").val(),
    company_id: COMPANY_ID,
    report_type: $("#report-type").val(),
    report_time: 1,
    report_date: '2022-01-01'
  };
  _jsHelpers._blockUI();
  $.ajax({
    url: _PREFIX_ADMIN_API + "/reports/schedule_report",
    type: "POST",
    headers: _jsHelpers._getHeaders(),
    data,
  }).done(function (response) {
    if (!response.error) {
      _jsHelpers._handle_notify("success", response.message);
      modalAdd.modal("hide");
      getTpl(response, false);
      // location.reload();
    } else {
      _jsHelpers._responseError(response);
    }
    _jsHelpers._unBlockUi();
  }).fail(function (err) {
    _jsHelpers._handle_notify(
      "danger",
      "Oops! There were found a problem."
    );
    _jsHelpers._unBlockUi();
  });
};

async function initFunction() {
  _jsHelpers._blockUI();
  await $.ajax({
    type: "POST",
    url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branchesWithGroups/",
    headers: _jsHelpers._getHeaders(),
  }).done((res) => {
    if (!res.error) {
      BRANCHES = res.data.branches;
      GROUPS = res.data.groups;
    }
  });
  await $.ajax({
    type: "POST",
    url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/usersByCompany/",
    headers: _jsHelpers._getHeaders(),
    data: {
      company_id: COMPANY_ID,
    },
  }).done((res) => {
    if (!res.error) {
      COMPANY_USERS = res.data;
    }
    _jsHelpers._unBlockUi();
  });
};

const deleteCard = function (id) {
  _jsHelpers._blockUI();
  $.ajax({
    type: "DELETE",
    url: _PREFIX_ADMIN_API + "/reports/schedule_report/" + id,
    headers: _jsHelpers._getHeaders(),
  })
    .done((res) => {
      if (!res.error) {
        $("#card-" + id).remove();
        updateCantCards();
        if ($(".container-cards .tpl-card").length == 0) {
          $(".container-no-data").show();
        }
        _jsHelpers._handle_notify("success", res.message);
      } else {
        _jsHelpers._responseError(res);
      }
      modalDelete.modal("hide");
      _jsHelpers._unBlockUi();
    })
    .fail((error) => {
      _jsHelpers._handle_notify(
        "danger",
        "Oops! There were found a problem."
      );
      _jsHelpers._unBlockUi();
    });
};

const updateCantCards = function () {
  var cantCards = $(".tpl-card").length;
  cantCards =
    cantCards == 1 ? cantCards + " schedule" : cantCards + " schedules";
  $(".-cant-card").text(cantCards);
};

function defineEvents() {
  let options = {
    dropdownAutoWidth: true,
    multiple: true,
    width: "100%",
    height: "30px",
    placeholder: `${_jsHelpers._trans("selectUsers")}`,
    allowClear: true,
    data: COMPANY_USERS.map(e => {
      return { 'id': e.id, 'text': e.email }
    }),
    cache: true,
    closeOnSelect: false,
  }

  $(document).on("click", ".closeOptions", function () {
    $(this).parents(".more-options").hide();
  });

  $(document).on("click", ".openOptions", function () {
    $(this).parents(".card-user").find(".more-options").show();
  });

  $(document).on("click", ".btnDeleteSchedule", (e) => {
    e.preventDefault();
    let id = $("#form-delete .id").val();
    deleteCard(id);
  });

  $(document).on("click", ".btn-save-schedule", (e) => {
    e.preventDefault();
    add();
  });

  $(document).on("click", ".btn-add-schedule", () => {
    SELECTED_BRANCHES = [];
    $(".select2-branch").empty();
    $("#select2-users").val(null).trigger('change');
    $(".select2-branch-edit").empty();
    modalAdd.modal();
  });

  $(document).on("click", ".btn-edit-schedule", function (e) {
    e.preventDefault();
    let id = $("#form-update-schedule #branch-id").val();
    updateSchedule(id);
  });

  $(document).on("click", ".editSchedule", function () {
    $(".select2-branch-edit").empty();
    $("#select2-users-edit").val(null).trigger('change');

    var $parent = $("#card-" + $(this).data("id"));
    const parentForm_ = "#form-update-schedule ";
    var name = $parent.data("name");
    var users = $parent.data("users");

    var branches = $parent.data("branches");
    var report_type = $parent.data("report_type");
    SELECTED_BRANCHES = branches.map(id => {
      let branch
      BRANCHES.forEach(item => {
        if (item.id == id){
          branch = item;
          branch.group_id = 0;
        }
      })
      return branch
    })

    renderSelectedBranches(SELECTED_BRANCHES, "-edit")

    modalEdit.modal();
    $(parentForm_ + "#schedule-name-edit").val(name);
    $(parentForm_ + "#report-type-edit").val(report_type);
    $(parentForm_ + "#select2-users-edit").val(users).trigger('change');
    $(parentForm_ + "#branch-id").val($(this).data("id"));

    // initSelectUsersEdit(users);
    // selected_branches = branches.map((item) => {
    //   item.group_id = 0;
    //   return item;
    // });

    // initSelectBranchEdit(branches);
  });

  function renderSelectedBranches(branches, type = "") {
    SELECTED_BRANCHES = branches
    $(`.select2-branch${type}`).empty();
    /** to render the selected branches names in the list_branch class */
    branches.map((branch) => {
      $(`.select2-branch${type}`).append(`
            <option id="${branch.id}" class="item-branch1 ckeckboxes "selected"
            " data-id="${branch.id}" data-name="">
            <p>${branch.name}</p>
            </option>
            `);
    });
    if (branches.length == 0) {
      $(`.select2-branch${type}`).empty();
      $(`.select2-branch${type}`).append(`<option>${_jsHelpers._trans("selectBranch")}</option>`);
      $(`#select2-branch${type}`).height("33px");
    } else {
      $(`#select2-branch${type}`).height("100px");
    }
  }

  const handleBranhs = (type = "") => {
    _jsBranchesModalHelper._showMoreBranshesModal__A(
      (data) => { renderSelectedBranches(data, type) },
      SELECTED_BRANCHES,
      SELECTED_GROUPS
    );
  }

  $(document).on("click", ".select2-branch", () => handleBranhs());

  $(document).on("click", ".select2-branch-edit", () => handleBranhs("-edit"));

  $(document).on("click", ".deleteSchedule", function () {
    let _this = $(this);
    let id = _this.data("id");
    $("#form-delete .id").val(id);
    modalDelete.modal();
  });

  $(".select2-users").select2(options);
  $("#select2-users-edit").select2(options);
};

const getTpl = function (res, fromFilters = false) {
  $(".container-no-data").hide();
  let tpl = ``;
  if (fromFilters) {
    $(".container-cards .row").empty();
    res.data.map((data) => {
      tpl += tplCard(data);
    });
  } else {
    tpl = tplCard(res.data);
  }
  $(".container-cards .row").append(tpl);
};

const tplCard = function (data) {
  let tpl = `
  <div id="card-${data.id}" class="col-xl-3 col-lg-3 col-md-4 col-sm-3 pb-1 tpl-card"
    data-company_id=${data.company_id} data-name=${data.name}  data-report_type=${data.report_type}  
    data-users=${JSON.stringify(data.user_ids)} data-branches=${JSON.stringify(data.branch_ids)} data-deleted_at=${new Date().toLocaleDateString()}
    data-created_at=${new Date().toLocaleDateString()} data-updated_at=${new Date().toLocaleDateString()}>
    <div class="card-user" style="min-height: 175px">
            <div class="options flex-end">
                <i class="bx bx-dots-vertical-rounded openOptions"></i>
            </div>

        <div data-id=${data.id} class="openModalDetail">
            <div class="profile mb-1">
                <div class="data">
                    <span class="-info m-0 w-600" id="" style="color: var(--fontColor); font-size: 12px">${_jsHelpers._trans('scheduleName')} 
                    ${data.name}</span>
                </div>
            </div>

            <div class="more-data">
                <p class="m-0 date-active" style="color: var(--fontColor); font-size: 9px">${_jsHelpers._trans('createdDate')}
                ${new Date().toLocaleDateString()}</p>

                <p class="m-0 date-active" style="color: var(--fontColor); font-size: 9px">${_jsHelpers._trans('updatedAt')} 
                ${new Date().toLocaleDateString()}</p>

                <input type="hidden" id="id-company" value=${data.company_id}>


            </div>
        </div>


        <div class="more-options" style="display: none">
            <div class="title">
                <span>${_jsHelpers._trans('options')}</span>
                <i class="bx bx-x closeOptions"></i>
            </div>
            <p data-id=${data.id} class="text-success editSchedule">${_jsHelpers._trans('update')}</p>
            <p data-id=${data.id} class="text-danger deleteSchedule">${_jsHelpers._trans('delete')}</p>
        </div>
    </div>
</div>
  `;
  return tpl;
};

$(document).on("keyup", ".search", function (e) {
  var filterReport = []
  REPORTS.forEach(rep => {
    if (rep.name.toLowerCase().includes(e.target.value.toLowerCase()) ) {
      filterReport.push(rep);
    }
  });
  renderScheduleCards(filterReport);
})
if ($(".container-cards .tpl-card").length == 0) {
  $(".container-no-data").show();
}
const renderScheduleCards = function (schedule) {
  $(".container-cards").empty();
  if (schedule.length == 0) {
    $(".container-cards").append(`
    <div class="container-no-data" style="">
        <p class="title">${_jsHelpers._trans('noOneHere')}</p>
        <p>${_jsHelpers._trans('noOneHereMsg')}</p>
        <button class="btn-dmg text-green" data-toggle="modal" data-target="#add-customer" class="">
        ${_jsHelpers._trans('addSchedule')}
        </button>
    </div>
    `)
  }
  schedule.forEach(data => {
    $(".container-cards").append(`
    <div id="card-${data.id}" class="col-xl-3 col-lg-3 col-md-4 col-sm-3 pb-1 tpl-card" style="padding-right: 0px;"
    data-company_id=${data.company_id} data-name=${data.name}  data-report_type=${data.report_type}  
    data-users=${JSON.stringify(data.user_ids)} data-branches=${JSON.stringify(data.branch_ids)} data-deleted_at=${new Date().toLocaleDateString()}
    data-created_at=${new Date().toLocaleDateString()} data-updated_at=${new Date().toLocaleDateString()}>
    <div class="card-user" style="min-height: 175px">
            <div class="options flex-end">
                <i class="bx bx-dots-vertical-rounded openOptions"></i>
            </div>

        <div data-id=${data.id} class="openModalDetail">
            <div class="profile mb-1">
                <div class="data">
                    <span class="-info m-0 w-600" id="" style="color: var(--fontColor); font-size: 12px"> ${_jsHelpers._trans('sheduleName')}  ${data.name}</span>
                </div>
            </div>

            <div class="more-data">
                <p class="m-0 date-active" style="color: var(--fontColor); font-size: 9px">${_jsHelpers._trans('createdDate')}             ${new Date().toLocaleDateString()}</p>

                <p class="m-0 date-active" style="color: var(--fontColor); font-size: 9px">${_jsHelpers._trans('updatedAt')} 
                ${new Date().toLocaleDateString()}</p>

                <input type="hidden" id="id-company" value=${data.company_id}>


            </div>
        </div>


        <div class="more-options" style="display: none">
            <div class="title">
                <span>${_jsHelpers._trans('options')}</span>
                <i class="bx bx-x closeOptions"></i>
            </div>
            <p data-id=${data.id} class="text-success editSchedule">${_jsHelpers._trans('update')}</p>
            <p data-id=${data.id} class="text-danger deleteSchedule">${_jsHelpers._trans('delete')}</p>
        </div>
    </div>
</div>
    `);
  })
}
initFunction().then(() => {
  defineEvents();
})