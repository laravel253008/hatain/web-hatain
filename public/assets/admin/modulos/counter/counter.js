const footFall = (function () {
  const modalAdd = $("#modal-add-footfall");
  const modalDelete = $("#modal-delete-camera");
  const modalUpdate = $("#modal-update-footfall");
  const modalHealth = $("#modal-health-check");

  let urlDelete = _jsHelpers._PREFIX_ADMIN_API + "/counter";
  let urlAdd = _jsHelpers._PREFIX_ADMIN_API + "/counter";

  const init = function () {
    $(document).on("click", ".openOptions", function () {
      $(this).parents(".card-user").find(".more-options").show();
    });

    $(document).on("click", ".closeOptions", function () {
      $(this).parents(".more-options").hide();
    });

    $(document).on("click", ".btn-add-footfall", function () {
      $("#form-add-footfall #branch-id1").val("");
      $("#form-add-footfall #access-token1").val("");
      $("#form-add-footfall #place1").val("");

      const companyId = $("#in-data").data("company-id");
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branchesByCompany",
        headers: _jsHelpers._getHeaders(),
      }).done((res) => {
        $("#form-add-footfall .select-branch").empty();
        if (!res.error) {
          $("#form-add-footfall .select-branch").append(
            `<option value="0">${_jsHelpers._trans("selectBranch")}</option>`
          );
          res.data.map((e) => {
            $("#form-add-footfall .select-branch").append(
              `<option value="${e.id}">${e.name}</option>`
            );
          });
        }

        $("#form-add-footfall .select-branch").select2({
          dropdownAutoWidth: true,
          width: "100%",
        });
      });
      modalAdd.modal();
    });

    $(document).on("click", ".btnDeleteFootfall", function (e) {
      e.preventDefault();
      let id = $("#form-delete .id").val();
      deleteCard(id);
    });

    $(document).on("click", ".btn-update-footfall", function (e) {
      e.preventDefault();
      let id = $("#form-update-footfall .counter-id").val();
      initEditCounterForm(id);
    });

    add();
    initModalDelete();
    initModalHealthCheck();
    initModalUpdate();
    initBranchesSelect();

    filtersControls();
  };
  const validateEdit = function (isSave = false) {
    let status = true;
    let elements = [];

    if (isSave) {
      elements = [
        "branch-id",
        "access-token",
        "place",
        "select2-users",
        "from-date",
        "to-date",
      ];
    } else {
      elements = [
        "branch-id",
        "access-token",
        "place",
        "select2-users",
        "from-date",
        "to-date",
      ];
    }

    for (let ele of elements) {
      var checkBoxPeriod = document.querySelector("#period");

      var value;
      let parent = $("#" + ele).parent(".controls");
      var secondParent = parent.prevObject[0];

      if (
        secondParent.type == "checkbox" ||
        secondParent.id == "from-date" ||
        secondParent.id == "to-date"
      ) {
        if (checkBoxPeriod.checked) {
          value = $("#" + ele).val();
        }
      } else {
        value = $("#" + ele).val();
      }

      if (_jsHelpers.undefined(value)) {
        status = false;
        parent.find(".help-block").text("This field required");
      } else {
        parent.find(".help-block").text("");
      }
    }

    return status;
  };

  const validateAdd = function (isSave = false) {
    let status = true;
    let elements = [];

    if (isSave) {
      elements = [
        "branch-id1",
        "access-token1",
        "place1",
        "select2-users1",
        "from-date1",
        "to-date1",
      ];
    } else {
      elements = [
        "branch-id1",
        "access-token1",
        "place1",
        "select2-users1",
        "from-date1",
        "to-date1",
      ];
    }

    for (let ele of elements) {
      var checkBoxPeriod = document.querySelector("#periodAdd");
      var value;
      let parent = $("#" + ele).parent(".controls");
      var secondParent = parent.prevObject[0];
      if (
        secondParent.type == "checkbox" ||
        secondParent.id == "from-date1" ||
        secondParent.id == "to-date1"
      ) {
        if (checkBoxPeriod.checked) {
          value = $("#" + ele).val();
        }
      } else {
        value = $("#" + ele).val();
      }

      if (_jsHelpers.undefined(value)) {
        status = false;
        parent.find(".help-block").text("This field required");
      } else {
        parent.find(".help-block").text("");
      }
    }

    return status;
  };

  const filtersControls = function () {
    $(document).on("keyup", ".filtros .search", function () {
      let _this = $(this);
      getFilter();
    });
    $(document).on("change", ".filtros .select-branch-search", function (e) {
      let _this = $(this);
      getFilter();
    });
  };

  const getFilter = function () {
    let branchId = $(".filtros .select-branch-search").val();
    let search = $(".filtros .search").val();
    branchId = _jsHelpers.undefined(branchId) ? 0 : branchId;
    $.ajax({
      type: "GET",
      url: _jsHelpers._PREFIX_ADMIN_API + "/counter",
      headers: _jsHelpers._getHeaders(),
      data: {
        branch_id: branchId,
        search: search,
      },
    }).done((res) => {
      if (!res.error) {
        getTpl(res, true);
      } else {
        dataEmpty();
      }
    });
  };

  const initBranchesSelect = function () {
    const companyId = $("#in-data").data("company-id");
    $.ajax({
      type: "POST",
      url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branchesByCompany",
      headers: _jsHelpers._getHeaders(),
    }).done((res) => {
      $(".select-branch").empty();
      $(".select-branch-search").empty();

      if (!res.error) {
        $(".select-branch").append(
          `<option value="0">${_jsHelpers._trans("selectBranch")}</option>`
        );
        $(".select-branch-search").append(
          `<option value="0">${_jsHelpers._trans("selectBranch")}</option>`
        );
        res.data.map((e) => {
          let branchText = document.querySelector("#branchText").innerHTML;
          var branchText2 = branchText.split(" ")[2];
          $(".select-branch-search").append(
            `<option  value="${e.id}">${e.name}</option>`
          );
          if (e.name == branchText2) {
            $(".select-branch").append(
              `<option selected value="${e.id}">${e.name}</option>`
            );

          } else {
            $(".select-branch").append(
              `<option value="${e.id}">${e.name}</option>`
            );
          }
        });
      }

      $(".select-branch").select2({
        dropdownAutoWidth: true,
        width: "100%",
                placeholder: `${_jsHelpers._trans("selectBranch")}`,
      });
      $(".select-branch-search").select2({
        dropdownAutoWidth: true,
        width: "100%",
        placeholder: `${_jsHelpers._trans("selectBranch")}`,
      });
    });
  };

  $(document).on("click", "#period", function () {
    var period = document.querySelector("#period");

    if (period.checked) {
      $("#fromToDate").show();
    } else {
      $("#fromToDate").hide();
    }
  });

  $(document).on("click", "#periodAdd", function () {
    var period = document.querySelector("#periodAdd");
    if (period.checked) {
      $("#fromToDateAdd").show();
    } else {
      $("#fromToDateAdd").hide();
    }
  });

  const initModalDelete = function () {
    $(document).on("click", ".deleteFootfall", function () {
      let _this = $(this);
      let id = _this.data("id");
      $("#form-delete .id").val(id);
      modalDelete.modal();
    });
  };

  const initModalUpdate = function () {
    $(document).on("click", ".updateFootfall", function () {
      let _this = $(this);
      let id = _this.data("id");
      $("#form-update-footfall .counter-id").val(id);
      $(".more-options .updateFootfall").val(id);

      var branchId = $(this).data("id");
      const parentForm_ = "#card-" + branchId;
      var branchCode1 = $(`${parentForm_} #branchCod`);
      var branchText = $(`${parentForm_} #branchText`)[0].innerHTML.split(
        " "
      )[2];
      var branchCodeText = branchCode1[0].innerHTML;

      $("#select2-users").val(branchText);
      $("#branchID").val(branchId);
      $("#branch-name").text(branchText);
      var branchCode3 = branchCodeText.split(" ")[2];

      $("#branch-id").val(branchCode3);
      modalUpdate.modal();
    });
  };

  const initEditCounterForm = function (counterId) {
    if (!validateEdit(true)) {
      return;
    }

    let branchCode = $("#form-update-footfall #branch-id").val();

    let branchId = $("#form-update-footfall #branchID").val();
    let accessToken = $("#form-update-footfall #access-token").val();
    let place = $("#form-update-footfall #place").val();
    let period = 0;

    let fromDate = $("#form-update-footfall #from-date").val();
    let toDate = $("#form-update-footfall #to-date").val();

    var checkboxPeriod = document.querySelector("#period");

    if (checkboxPeriod.checked) {
      period = 1;
    } else {
      period = 0;
    }

    const parent = "#card-" + branchId + " ";

    _jsHelpers._blockUI();

    $.ajax({
      type: "PUT",
      url: _jsHelpers._PREFIX_ADMIN_API + "/counter/" + counterId,
      headers: _jsHelpers._getHeaders(),
      data: {
        branch_code: branchCode,
        branch_id: branchId,
        token: accessToken,
        place: place,
        period: period,
        from_date: fromDate,
        to_date: toDate,
      },
    })
      .done((res) => {
        if (!res.error) {
          $(parent + ".closeOptions").click();
          updateCantCards();
          location.reload();
          _jsHelpers._handle_notify("success", res.message);
        } else {
          _jsHelpers._responseError(res);
        }
        modalUpdate.modal("hide");
      })
      .always((res) => {
        modalUpdate.modal("hide");
        _jsHelpers._unBlockUi();
      });
  };

  const initModalHealthCheck = function () {
    $(document).on("click", ".healthCheck", function () {
      let _this = $(this);
      let id = _this.data("id");
      $(".more-options .healthCheck").val(id);
      getHealthCheckData();
    });
  };

  const deleteCard = function (id) {
    _jsHelpers._blockUI();
    $.ajax({
      type: "DELETE",
      url: urlDelete + "/" + id,
      headers: _jsHelpers._getHeaders(),
    })
      .done((res) => {
        if (!res.error) {
          $("#card-" + id).remove();
          updateCantCards();
          if ($(".container-cards .tpl-card").length == 0) {
            $(".container-no-data").show();
          }
          _jsHelpers._handle_notify("success", res.message);
        } else {
          _jsHelpers._responseError(res);
        }
        modalDelete.modal("hide");
        _jsHelpers._unBlockUi();
      })
      .fail((error) => {
        _jsHelpers._handle_notify(
          "danger",
          "Oops! There were found a problem."
        );
        _jsHelpers._unBlockUi();
      });
  };

  const add = function () {
    $("#form-add-footfall").on("submit", function (e) {
      e.preventDefault();

      if (!validateAdd(true)) {
        return;
      }

      let branchCode = $("#form-add-footfall #branch-id1").val();
      var period = 0;
      let branchId = $("#form-add-footfall .select-branch").val();
      let accessToken = $("#form-add-footfall #access-token1").val();
      let place = $("#form-add-footfall #place1").val();
      let fromDate = $("#form-add-footfall #from-date1").val();
      let toDate = $("#form-add-footfall #to-date1").val();
      var checkboxPeriod = document.querySelector("#periodAdd");

      if (checkboxPeriod.checked) {
        period = 1;
      } else {
        period = 0;
      }

      _jsHelpers._blockUI();
      $.ajax({
        url: urlAdd,
        type: "POST",
        headers: _jsHelpers._getHeaders(),
        data: {
          branch_code: branchCode,
          branch_id: branchId,
          token: accessToken,
          place: place,
          period: period,
          from_date: fromDate,
          to_date: toDate,
        },
      })
        .done(function (response) {
          if (!response.error) {
            _jsHelpers._handle_notify("success", response.message);
            modalAdd.modal("hide");
            getTpl(response, false);
          } else {
            _jsHelpers._responseError(response);
          }
          _jsHelpers._unBlockUi();
        })
        .fail(function (err) {
          _jsHelpers._handle_notify(
            "danger",
            "Oops! There were found a problem."
          );
          _jsHelpers._unBlockUi();
        });
    });
  };

  var dataEmpty = function () {
    $(".container-no-data").hide();
    $(".container-cards .row").empty();
    $(".container-cards .row").append(
      `<p style="margin: auto;padding: 200px; font-size: 16px;">We can´t find anything like this</p>`
    );
  };

  const getTpl = function (res, fromFilters = false) {
    $(".container-no-data").hide();
    let tpl = ``;
    if (fromFilters) {
      $(".container-cards .row").empty();
      res.data.map((data) => {
        tpl += tplCard(data);
      });
    } else {
      tpl = tplCard(res.data);
    }

    $(".container-cards .row").append(tpl);
    updateCantCards();
  };

  const tplCard = function (data) {
    let id = data.id;
    let branchCode = data.branch_code;
    let place = data.place;
    let branch_id = data.branch_id;
    let branchName = data.branch.name;
    let token = data.token;
    var createdAt = data.created_at;
    var status = data.status;
    var last_online = data.last_online;

    var since = _jsHelpers._getSinceComplete(createdAt);
    let tpl = `
            <div id="card-${id}" class="col-xl-3 col-lg-3 col-md-4 col-sm-3 pb-1 tpl-card" style="padding-right: 0px; !important">
              <div class="card-user" style="min-height: 175px">
                  <div class="options flex-end">
                    <i class="bx bx-dots-vertical-rounded openOptions"></i>
                  </div>
                  <div class="openModalDetail">
                      <div class="profile mb-1">
                          <div class="data">
                    <p id="branchText" class="-info m-0 w-600" style=" font-size: 12px">${_jsHelpers._trans('BranchName')}  ${branchName}</p>
                    <span class="-info m-0 w-600" id="branchCod" style=" font-size: 12px">${_jsHelpers._trans('brachCode')}  ${branchCode}</span>
                         <br>
                              ${
                                status == "OFFLINE"
                                  ? `<br>
                              <span class="-info m-0 w-600" id="last_online" style=" font-size: 12px"> ${_jsHelpers._trans('LastOnline')}  ${last_online}</span>
                              <span class="container-ball">
                                <span class="-ball pulse-bg-danger pulse-danger">
                              </span>`
                                  : `
                         <br>

                    <span class="-info m-0 w-600" id="branchCod" style=" font-size: 12px">${_jsHelpers._trans('status')} :</span>    
                                  <span class="container-ball">
                                <span class="-ball pulse-bg-success pulse-success">
                              </span>`
      }

                              <input type="hidden" id="token" value="${token}" />

                          </div>
                      </div>


                  </div>
                  <div class="more-options" style="display: none">
                      <div class="title">
                          <span>Options</span>
                          <i class="bx bx-x closeOptions"></i>
                      </div>
                      <p data-id="${id}"  class="text-success updateFootfall">Update Counter</p>
                      <p data-id="${branchCode}"  class="text-success healthCheck">Health check</p>
                      <p data-id="${id}" class="text-danger deleteFootfall">Delete Counter</p>
                  </div>
              </div>
          </div>`;
    return tpl;
  };

  ////////////////////// health check///////////////////////////

  function getHealthCheckData() {
    let branchCode = $(".more-options .healthCheck").val();
    _jsHelpers._blockUI();
    $(".modal-body").hide();
    $.ajax({
      type: "GET",
      url:
        _jsHelpers._PREFIX_ADMIN_API + "/counter/health_checks/" + branchCode,
      headers: _jsHelpers._getHeaders(),
    }).done((res) => {
      if (!res.error) {
        healthCheckData(res);
        $(".modal-body").show();

        _jsHelpers._unBlockUi();
      } else {
        _jsHelpers._handle_notify(
          "danger",
          "Health check is't available fo this counter"
        );
        _jsHelpers._unBlockUi();
      }
    });
  }

  function healthCheckData(res) {
    modalHealth.modal();
    if (res.data.length != 0) {
      renderTable(res.data.map((e, i) => {
        e.ID = i + 1;
        return e;
      }))
    } else {
      $("#healthCheckTable").empty();
      $("#healthCheckTable").append(`<div class="text-center mt-3"><h4 class="text-center">${_jsHelpers._trans("curruntlyOnline")}</h4></div>`);
    }

  }

  function renderTable(data) {
    $("#healthCheckTable").dxDataGrid({
      dataSource: data ? data :[],
      keyExpr: 'ID',
      columnsAutoWidth: true,
      allowColumnResizing: true,
      allowColumnReordering: true,
      filterRow: { visible: true },
      noDataText: 'Currently Online',
      grouping: {
        autoExpandAll: false,
      },
      groupPanel: {
        visible: true,
      },
      headerFilter: { visible: true },
      filterBuilderPopup: {
        position: {
          of: window,
          at: 'top',
          my: 'top',
          offset: { y: 10 },
        },
      },
      showRowLines: true,
      showBorders: true,
      columns:["serial","last_offline","last_online"],
      customizeColumns(columns) {
        columns[0].width = 120;
      },
      paging: {
        pageSize: 10,
      },
      pager: {
        visible: true,
        allowedPageSizes: [10, 'all'],
        showPageSizeSelector: true,
        showInfo: true,
        showNavigationButtons: true,
      },
      sortByGroupSummaryInfo: [{
        summaryItem: 'count',
      }],
      summary: {
        totalItems: [],//summaryColumns,
        groupItems: [],
      },
      export: {
        enabled: true,
      },
      onExporting(e) {
        const workbook = new ExcelJS.Workbook();
        const worksheet = workbook.addWorksheet('Linkers Analytics Center');

        DevExpress.excelExporter.exportDataGrid({
          component: e.component,
          worksheet,
          autoFilterEnabled: true,
        }).then(() => {
          workbook.xlsx.writeBuffer().then((buffer) => {
            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `${$("#companyName")[0].innerText + new Date().toLocaleDateString().split("/").join("")}` + '.xlsx');
          });
        });
        e.cancel = true;
      },
    });
  };

  ////end///////////

  const updateCantCards = function () {
    var cantCards = $(".tpl-card").length;
    cantCards =
      cantCards == 1 ? cantCards + " footfall" : cantCards + " footfalls";
    $(".-cant-card").text(cantCards);
  };

  return {
    init: function () {
      init();
    },
  };
  ;
})();

footFall.init();
