const Users = (function () {
  var modalAddUser = $("#modal-add-user");
  var modalEditUser = $("#modal-edit-user");
  var modalDeleteUser = $("#modal-delete-user");
  var modalDetail = $("#modal-detail");
  var modalUpdatePassword = $("#modal-update-password");
  var modalUpdateStatus = $("#modal-update-status");
  var select_branch_add = $(".select2-branch");
  var select_branch_update = $(".select2-branch1");

  var selected_branches = [];
  let selected_branches_ids = [];
  var selected_groubs = [];

  const init = function () {
    $(document).on("click", ".openOptions", function () {
      $(this).parents(".custom-card").find(".more-options").show();
    });

    $(document).on("click", ".closeOptions", function () {
      $(this).parents(".more-options").hide();
    });

    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

    $(document).on("click", ".btn-filter-inactive", function () {});

    modalAddUser.on("shown.bs.modal", function () {
      $("#form-add-user")[0].reset();
    selected_branches = [];

    });

    initModalAddUserForm();
    initModalDetail();

    initEditInfoForm();
    initModalEditUser();

    initUpdatePasswordForm();
    initModalUpdatePassword();

    initModalUpdateStatus();
    initUpdateStatusForm();

    initModalDeleteUser();
    initDeleteUserForm();

    filtersControls();

    ///////////start branch modal //////

    if (selected_branches_ids == 0) {
      select_branch_add.empty();
      select_branch_add.append(`<option>${_jsHelpers._trans("selectBranch")}</option>`);

      select_branch_update.empty();
      select_branch_update.append(`<option>${_jsHelpers._trans("selectBranch")}</option>`);
      
    }
    /**
     * this function initialize the branches modal and execute a call back function when the user click the submit button of the modal
     * the callback function returns the selected branches in the model
     */

    $(document).on("click", ".select2-branch", function () {
      _jsBranchesModalHelper._showMoreBranshesModal__A(
        (data) => {
          selected_branches = data;
          renderSelectedBranches(selected_branches);
        },
        selected_branches,
        selected_groubs
      );
    });

    $(document).on("click", ".select2-branch1", function () {
        _jsBranchesModalHelper._showMoreBranshesModal__A((data) => {
          selected_branches = data;
          renderSelectedBranchesUpdate(selected_branches)
        }, selected_branches, selected_groubs);
      });
  };
  /////////// start branch select /////////

  /**
   * this function take the selected branches and analize it in the chart
   * @param {array} branches
   */
  const renderSelectedBranches = (branches) => {
    select_branch_add.empty();

    /** to render the selected branches names in the list_branch class */
    branches.map((branch) => {
      select_branch_add.append(`
        <option id="${branch.id}" class="item-branch1 ckeckboxes "selected"
        " data-id="${branch.id}" data-name="">
        <p>${branch.name}</p>
        </option>
        `);
    });

    if (branches.length == 0) {
      select_branch_add.empty();
      select_branch_add.append(`<option>${_jsHelpers._trans("selectBranch")}</option>`);
      $("#select2-branch").height("33px");
    } else {
      $("#select2-branch").height("100px");
    }
  };

  const renderSelectedBranchesUpdate = (branches) => {
    select_branch_update.empty();
    /** to render the selected branches names in the list_branch class */
    branches.map((branch) => {
      select_branch_update.append(`
        <option id="${branch.id}" class="item-branch1 ckeckboxes "selected"
        " data-id="${branch.id}" data-name="">
        <p>${branch.name}</p>
        </option>
        `);

      // }
    });

    if (branches == 0) {
      select_branch_add.empty();
      select_branch_add.append(`<option>${_jsHelpers._trans("selectBranch")}</option>`);
      $("#select2-branch1").height("33px");
    } else {
      $("#select2-branch1").height("100px");
    }
  };
  ///////// end select branch ///////

  const filtersControls = function () {
    $(document).on("keyup", ".filtros .search", function () {
      let _this = $(this);
      getFilter();
    });
    $(document).on("change", "input[name='radio-status']", function (e) {
      let _this = $(this).val();
      getFilter();
    });
    $(document).on("change", "input[name='radio-rol']", function (e) {
      let _this = $(this).val();
      getFilter();
    });
  };

  const getFilter = function () {
    let search = $(".filtros .search").val();
    let status = $("input:radio[name=radio-status]:checked").val();
    let rol = $("input:radio[name=radio-rol]:checked").val();
    data = {
      search: search,
      status: status,
      rol: rol,
    };
    $.ajax({
      type: "GET",
      url: _jsHelpers._PREFIX_ADMIN_API + "/users/CompanyManagerTeammate",
      headers: _jsHelpers._getHeaders(),
      data: {
        search: search,
        status: status,
        rol: rol,
      },
    }).done((res) => {
      if (!res.error) {
        getTpl(res, true);
      } else {
        dataEmpty();
      }
    });
  };

  const updateCantCards = function () {
    var cantCards = $(".custom-card").length;
    cantCards =
      cantCards == 1 ? cantCards + " teammate" : cantCards + " teammates";
    $(".-cant-card").text(cantCards);
  };

  const initUpdatePasswordForm = function () {
    $("#form-update-password").on("submit", function (e) {
      e.preventDefault();

      var parentForm_ = "#form-update-password ";
      var userId = $(parentForm_ + ".user-id").val();
      var parent = "#card-" + userId + " ";

      var password = $(parentForm_ + "#new_password").val();
      var password_confirmation = $(parentForm_ + "#confirm_password").val();

      _jsHelpers._blockUI();
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/user/updatePassword",
        headers: _jsHelpers._getHeaders(),
        data: {
          password: password,
          password_confirmation: password_confirmation,
          userId: userId,
        },
      })
        .done((res) => {
          $(parent + ".closeOptions").click();

          if (!res.error) {
            _jsHelpers._handle_notify("success", res.message);
          } else {
            _jsHelpers._responseError(res);
          }
          modalUpdatePassword.modal("hide");
        })
        .always((res) => {
          _jsHelpers._unBlockUi();
          modalUpdatePassword.modal("hide");
        });
    });
  };

  const initModalUpdatePassword = function () {
    $(document).on("click", ".updatePassword", function (e) {
      var userId = $(this).data("id-usuario");
      var parentForm_ = "#form-update-password ";
      $(parentForm_ + "#new_password").val("");
      $(parentForm_ + "#confirm_password").val("");
      $(parentForm_ + ".user-id").val(userId);

      modalUpdatePassword.modal("show");
    });
  };

  const initModalUpdateStatus = function () {
    $(document).on("click", ".updateStatus", function (e) {
      const userId = $(this).data("id-usuario");
      const $parent = $("#card-" + userId);
      const parentForm_ = "#form-update-status ";

      const details = $parent.data("details");
      const status = $parent.data("status");
      const stringStatus = status ? "Active" : "Inactive";
      $(parentForm_ + ".details").val(details);
      $(parentForm_ + "#current_status").val(stringStatus);
      $(parentForm_ + ".select-user-state").val(status);
      $(parentForm_ + ".user-id").val(userId);

      modalUpdateStatus.modal("show");
    });
  };

  const initUpdateStatusForm = function () {
    $("#form-update-status").on("submit", function (e) {
      e.preventDefault();

      const parentForm_ = "#form-update-status ";
      const userId = $(parentForm_ + ".user-id").val();
      const parent = "#card-" + userId + " ";
      const $parent = $("#card-" + userId);

      const details = $(parentForm_ + ".details").val();
      const status = $(parentForm_ + ".select-user-state").val();

      _jsHelpers._blockUI();
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/user/updateStatus",
        headers: _jsHelpers._getHeaders(),
        data: {
          status: status,
          user_id: userId,
          details: details,
        },
      })
        .done((res) => {
          $(parent + ".closeOptions").click();

          if (!res.error) {
            $parent.data("details", details);
            $parent.data("status", status);

            if (parseInt(status)) {
              $(parent + ".-bullet-status").removeClass("bullet-danger");
              $(parent + ".-bullet-status").addClass("bullet-success");

              $(parent + ".badge").removeClass("badge-danger");
              $(parent + ".badge").addClass("badge-success");
            } else {
              $(parent + ".-bullet-status").removeClass("bullet-success");
              $(parent + ".-bullet-status").addClass("bullet-danger");

              $(parent + ".badge").removeClass("badge-success");
              $(parent + ".badge").addClass("badge-danger");
            }

            _jsHelpers._handle_notify("success", res.message);
          } else {
            _jsHelpers._responseError(res);
          }
          modalUpdateStatus.modal("hide");
        })
        .always((res) => {
          modalUpdateStatus.modal("hide");
          _jsHelpers._unBlockUi();
        });
    });
  };

  const initModalDeleteUser = function () {
    $(document).on("click", ".deleteUser", function (e) {
      const userId = $(this).data("id-usuario");
      $("#form-delete-user .user-id").val(userId);

      modalDeleteUser.modal("show");
    });
  };

  const initDeleteUserForm = function () {
    $("#form-delete-user").on("submit", function (e) {
      e.preventDefault();

      var userId = $("#form-delete-user .user-id").val();
      _jsHelpers._blockUI();
      $.ajax({
        type: "DELETE",
        url: _jsHelpers._PREFIX_ADMIN_API + "/user/" + userId,
        headers: _jsHelpers._getHeaders(),
      })
        .done((res) => {
          if (!res.error) {
            $("#card-" + userId).remove();

            if ($(".container-cards .row .custom-card").length == 0) {
              $(".container-no-data").show();
            }
            updateCantCards();

            _jsHelpers._handle_notify("success", res.message);
          } else {
            _jsHelpers._responseError(res);
          }
          modalDeleteUser.modal("hide");
        })
        .always((res) => {
          modalDeleteUser.modal("hide");
          _jsHelpers._unBlockUi();
        });
    });
  };

  const initModalEditUser = function () {
    $(document).on("click", ".editUser", function (e) {
      var userId = $(this).data("id-usuario");
      var $parent = $("#card-" + userId);
      const parentForm_ = "#form-edit-user ";
      var branches = [];
      try {
        branches = JSON.parse($parent.data("branches"));
      } catch (error) {
        branches = $parent.data("branches");
      }
      selected_branches = branches.map(item => {
        item.group_id = 0;
        return item;
      });
      initSelectBranchEdit(branches);

      var firstname = $parent.data("name");
      var lastName = $parent.data("lastname");
      var email = $parent.data("email");
      var jobTitle = $parent.data("job-title");

      $(parentForm_ + "#e-firstname").val(firstname);
      $(parentForm_ + "#e-lastname").val(lastName);
      $(parentForm_ + "#e-email").val(email);
      $(parentForm_ + "#e-job").val(jobTitle);

      $(parentForm_ + ".user-id").val(userId);

      modalEditUser.modal("show");
    });
  };
  const initSelectBranchEdit = function (branches) {
    select_branch_update.empty();

    branches.map((branch, index) => {
      select_branch_update.append(`
       <option id="${branch.id}" class="item-branch1 ckeckboxes "selected"
       " data-id="${branch.id}" data-name="">
       <p>${branch.name}</p>
       </option>
       `);
    });
  };
  const initEditInfoForm = function () {
    $("#form-edit-user").on("submit", function (e) {
      e.preventDefault();

      const parentForm_ = "#form-edit-user ";
      const userId = $(parentForm_ + ".user-id").val();
      const parent = "#card-" + userId + " ";
      const $parent = $("#card-" + userId);

      const firstname = $(parentForm_ + "#e-firstname").val();
      const lastname = $(parentForm_ + "#e-lastname").val();
      const email = $(parentForm_ + "#e-email").val();
      const job = $(parentForm_ + "#e-job").val();
      selected_branches_ids = selected_branches.map(e => e.id);
     var data ={
        firstname: firstname,
        lastname: lastname,
        email: email,
        job: job,
        userId: userId,
        branch_ids: selected_branches_ids

      }

      _jsHelpers._blockUI();
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/customer/updateFromCustomer",
        headers: _jsHelpers._getHeaders(),
        data: {
          firstname: firstname,
          lastname: lastname,
          email: email,
          job: job,
          userId: userId,
          branch_ids: selected_branches_ids

        },
      })
        .done((res) => {
          if (!res.error) {

            $(parent + ".closeOptions").click();

            $parent.data("name", firstname);
            $parent.data("lastname", lastname);
            $parent.data("email", email);
            $parent.data("branches", JSON.stringify(selected_branches));
            $parent.data("job-title", job);

            $(parent + ".-complete-name").text(firstname + " " + lastname);
            $(parent + ".-email").text(email);
            $(parent + ".-job-title").text(job);

            _jsHelpers._handle_notify("success", res.message);
          } else {
            _jsHelpers._responseError(res);
          }
          modalEditUser.modal("hide");
        })
        .always((res) => {
          modalEditUser.modal("hide");
          _jsHelpers._unBlockUi();
        });
    });
  };

  const initModalDetail = function () {
    $(document).on("click", ".openModalDetail", function (e) {
      var userId = $(this).data("id-usuario");
      var $parent = $("#card-" + userId);

      var name = $parent.data("name");
      var lastName = $parent.data("lastname");
      var email = $parent.data("email");
      var jobTitle = $parent.data("job-title");
      var active = $parent.data("status");
      var photo = $parent.data("photo");
      var roles = $parent.data("roles");

      const status = active ? "active" : "inactive";

      if (roles.length) {
        $(".info-header-left").text(roles[0].name);
      } else {
        $(".info-header-left").text("Not assigned");
      }

      if (photo) {
        $(".img-detail").attr("src", photo);
      } else {
        $(".img-detail").attr("src", "/assets/admin/img/misc/user_active.png");
      }

      $(".detail-status").removeClass("active, inactive").addClass(status);

      $(".-name").text(name + " " + lastName);
      $(".-job").text(jobTitle);
      // $('.-email').text(email);

      modalDetail.modal("show");
    });
  };

  const initModalAddUserForm = function () {
    $("#form-add-user").on("submit", function (e) {
      e.preventDefault();

      const formParent_ = "#form-add-user ";

      var firstname = $(formParent_ + "#firstname").val();
      var lastname = $(formParent_ + "#lastname").val();
      var job = $(formParent_ + "#job").val();
      var role = $(formParent_ + "#role").val();
      var email = $(formParent_ + "#email").val();
      var password = $(formParent_ + "#password").val();
      selected_branches_ids = selected_branches.map(e => e.id);

      _jsHelpers._blockUI();
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/customer/createFromCustomer",
        headers: _jsHelpers._getHeaders(),
        data: {
          firstname: firstname,
          lastname: lastname,
          job: job,
          role: role,
          email: email,
          password: password,
          branch_ids: selected_branches_ids
        },
      })
        .done((res) => {
          if (!res.error) {
            getTpl(res, false);
            _jsHelpers._handle_notify("success", res.message);
            modalAddUser.modal("hide");
          } else {
            _jsHelpers._responseError(res);
          }
        })
        .always((res) => {
          _jsHelpers._unBlockUi();
        });
    });
  };

  var dataEmpty = function () {
    $(".container-no-data").hide();
    $(".container-cards .row").empty();
    $(".container-cards .row").append(
      `<p style="margin: auto;padding: 200px; font-size: 16px;">We can´t find anything like this</p>`
    );
  };

  var getTpl = function (res, fromFilters = false) {
    $(".container-no-data").hide();

    let tpl = ``;
    if (fromFilters) {
      $(".container-cards .row").empty();
      res.data.map((data) => {
        tpl += tplCard(data);
      });
    } else {
      tpl = tplCard(res.data);
    }

    $(".container-cards .row").append(tpl);
    $('[data-toggle="tooltip"]').tooltip();
    updateCantCards();
  };

  var tplCard = function (data) {
    var userId = data.id;
    var active = data.active;
    var firstname = data.name;
    var lastname = data.lastname;
    var completeName = data.name + " " + data.lastname;
    var created_at = data.created_at;
    var roles = data.roles;
    var email = data.email;
    var branches = data.branches;
    var email_verified_at = data.email_verified_at;
    var jobTitle = data.job_title;

    var photo = "/assets/admin/img/misc/user_active.png";

    var stringRol = "";
    if (roles.length > 0) {
      stringRol = roles[0].name;
    } else {
      stringRol = "Not assigned";
    }

    var bulletActive = "";
    var badge = "";
    if (active) {
      bulletActive = "bullet-success";
      badge = "badge-success";
    } else {
      bulletActive = "bullet-danger";
      badge = "badge-danger";
    }

    var footer = ``;

    var stringRoles = roles.toString();

    var tpl = `
            <div id="card-${userId}" class="col-xl-3 col-lg-3 col-md-4 col-sm-1 pb-1"
                data-name="${firstname}"
                data-lastname="${lastname}"
                data-email="${email}"
                data-job-title="${jobTitle}"
                data-details = ""
                data-status="${active}"
                data-branches='${JSON.stringify(branches)}'
                data-photo="${photo}"
                data-roles='${JSON.stringify(roles)}'
            >
                <div class="custom-card">
                    <div class="options">
                        <div class="badge ${badge}">
                            ${stringRol}
                        </div>
                        <i class="bx bx-dots-vertical-rounded openOptions"></i>
                    </div>
                    <div data-id-usuario="${userId}"  class="openModalDetail">
                        <div class="profile mb-1">
                                <img src="${photo}" class="mr-1" alt="" onerror="_jsHelpers._errorImage(this)">
                            <div class="data">
                                <p class="-info m-0 w-600 -complete-name">${completeName}
                                    <span class="bullet bullet-xs ${bulletActive} -bullet-status"></span>
                                </p>
                                <p class="-info m-0 -job-title" style="color: var(--fontColor); font-size: 12px">${jobTitle}</p>
                            </div>
                        </div>
                        <div class="more-data">
                            <p class="m-0 -email" style="color: var(--fontColor);">${email}</p>
                            ${footer}
                        </div>
                    </div>
                    <div class="more-options" style="display: none">
                        <div class="title">
                            <span>Options</span>
                            <i class="bx bx-x closeOptions"></i>
                        </div>
                        <p data-id-usuario="${userId}" class="text-success editUser">Edit teammate info</p>
                        <p data-id-usuario="${userId}" class="text-success updatePassword">Update password</p>
                        <p data-id-usuario="${userId}" class="text-success updateStatus" >Change status</p>
                        <p data-id-usuario="${userId}" class="text-danger deleteUser">Delete teammate</p>
                    </div>
                </div>
            </div>
       `;

    return tpl;
  };

  return {
    init: function () {
      init();
    },
  };
})();

$(document).ready(function () {
  Users.init();
});
