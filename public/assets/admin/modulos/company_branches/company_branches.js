const CompanyBranches = function () {

    var modalAddBranch = $("#modal-add-branch");
    var modalAddGroup = $("#modal-add-group");
    var modalEditBranch = $("#modal-edit-branch");
    var modalEditgroup = $("#modal-edit-group");
    var modalDeleteBranch = $("#modal-delete-branch");
    var modalDeleteGroup = $("#modal-delete-group");
    var modalDetails = $("#modal-detail");
    var modalDetailsGroup = $("#modal-detail-group");
    var modalFoodicsMatch = $("#modal-foodics-match");
    var COMPANY_USERS = []
    const init = async function () {
        var obj = searchToObject();
        if (obj.status == 'Success') {
            _jsHelpers._handle_notify('success', "Integration: connect to Foodics successfully.");
        }

        $(document).on('click', '.openOptions', function () {
            $(this).parents('.card-user').find('.more-options').show();
        });

        $(document).on('click', '.closeOptions', function () {
            $(this).parents('.more-options').hide();
        });

        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

        $(document).on('click', '#modal-add-branch .item-menu', function () {
            var _this = $(this)
            var target = _this.data('target')

            $('#modal-add-branch .item-menu').removeClass('active')
            _this.addClass('active')
            $('#modal-add-branch .contenido').hide()

            $('#modal-add-branch .' + target).show()

        });

        $(document).on('click', '#modal-edit-branch .item-menu', function () {
            var _this = $(this)
            var target = _this.data('target')

            $('#modal-edit-branch .item-menu').removeClass('active')
            _this.addClass('active')
            $('#modal-edit-branch .contenido').hide()

            $('#modal-edit-branch .' + target).show()

        });


        $(document).on('click', '#form-add-branch .btn-continue', function () {

            if (!validateAddBranch()) {
                return;
            }
            $('#form-add-branch .contenido').hide()
            $('#form-add-branch .container-users').show()
            $('#form-add-branch .item-menu').removeClass('active')
            $("#form-add-branch .item-menu[data-target='container-users']").addClass('active')
        });

        $(document).on('click', '#form-edit-branch .btn-continue', function () {
            if (!validateEditBranch()) {
                return;
            }
            $('#form-edit-branch .contenido').hide()
            $('#form-edit-branch .container-users').show()
            $('#form-edit-branch .item-menu').removeClass('active')
            $("#form-edit-branch .item-menu[data-target='container-users']").addClass('active')
        });

        modalFoodicsMatch.on("shown.bs.modal", function () {
            $('.select-foodics-branches').select2({
                dropdownParent: $("#modal-foodics-match"),
                dropdownAutoWidth: true,
                width: '100%'
            });
        });

        modalAddBranch.on("shown.bs.modal", function () {
            $('#form-add-branch .contenido').hide()
            $('#form-add-branch .container-branch').show()
            $('#form-add-branch .item-menu').removeClass('active')
            $("#form-add-branch .item-menu[data-target='container-branch']").addClass('active')

            $("#form-add-branch")[0].reset();

            $('.select-countries1').select2({
                dropdownParent: $("#modal-add-branch"),
                dropdownAutoWidth: true,
                width: '100%'
            });
            $('.select-timezone1').select2({
                dropdownParent: $("#modal-add-branch"),
                dropdownAutoWidth: true,
                width: '100%'
            });

            $('#select2-users').val(null).trigger('change');
        });

        _jsHelpers._blockUI();
        const company_id = $('#form-info-company #id-company').val();
        await $.ajax({
            type: "POST",
            url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/usersByCompany/",
            headers: _jsHelpers._getHeaders(),
            data: {
                company_id: company_id,
            }
        }).done((res) => {
            if (!res.error) {
                COMPANY_USERS = res.data;
                COMPANY_USERS.forEach(e => {
                    $('.select2-users').append(`
                  <option value="${e.id}">${e.email}</option>
                `);
                    $('.select2-group-users').append(`
                  <option value="${e.id}">${e.email}</option>
                `);
                })
            }
            _jsHelpers._unBlockUi();
        });

        initSelectUsers();
        initSelectGroupUsers();

        modalAddGroup.on("shown.bs.modal", function () {
            $('#form-add-group .contenido').hide()
            $('#form-add-group .container-group').show()
            $('#form-add-group .item-menu').removeClass('active')
            $("#form-add-group .item-menu[data-target='container-group']").addClass('active')

            $("#form-add-group")[0].reset();

            $('.select2-group-branch').select2({
                placeholder: `${_jsHelpers._trans('selectBranch')}`,
                dropdownParent: $("#modal-add-group"),
                dropdownAutoWidth: true,
                closeOnSelect: false,
                allowClear: true,
                width: '100%',
            });

            $('.select2-group-users').val(null).trigger('change');
        });

        $("#cb-selectAllGroupBranches").on('change', (e) => {
            if (e.target.checked) {
                $('.select2-group-branch').select2('destroy').find('option').prop('selected', 'selected').end().select2()
            
            } else {
                $('.select2-group-branch').select2('destroy').find('option').prop('selected', false).end().select2()
         
            }
        });
        $("#cb-selectAllGroupBranchesEdit").on('change', (e) => {
             if (e.target.checked) {
                $(".select2-group-branch-edit > option").prop("selected","selected");
                $(".select2-group-branch-edit").trigger("change");

             } else {
                $(".select2-group-branch-edit > option").prop("selected", false);
                $(".select2-group-branch-edit").trigger("change");
                
                          
             }
         });

        $("#cb-selectAllBranchesCompanies").on('change', (e) => {
            if (e.target.checked) {
                $(".select2-group-branch-edit > option").prop("selected", true);
                $(".select2-group-branch-edit").trigger("change");
            } else {
                $(".select2-group-branch-edit > option").prop("selected", false);
                $(".select2-group-branch-edit").trigger("change");
            }
        });
        

        modalEditBranch.on("shown.bs.modal", function () {
            $('#form-edit-branch .contenido').hide()
            $('#form-edit-branch .container-branch').show()
            $('#form-edit-branch .item-menu').removeClass('active')
            $("#form-edit-branch .item-menu[data-target='container-branch']").addClass('active')

            $('.select-countries').select2({
                dropdownParent: $("#modal-edit-branch"),
                dropdownAutoWidth: true,
                width: '100%'
            });
            $('.select-timezone').select2({
                dropdownParent: $("#modal-edit-branch"),
                dropdownAutoWidth: true,
                width: '100%'
            });
        });


        $(document).on('click', '.refresh-foodics', function () {
            _jsHelpers._blockUI();
            $.ajax({
                type: 'POST',
                url: _jsHelpers._PREFIX_ADMIN_API + '/foodics/',
                headers: _jsHelpers._getHeaders(),
            }).done(res => {
                if (!res.error) {
                    $('.select-foodics-branches').empty()
                    $('.select-foodics-branches').append(`<option value="0">none</option>`)
                    $.each($('.select-foodics-branches'), function () {
                        res.data.map(e => {
                            if (e.branch_id == $(this).attr('id')) {
                                $(this).append(`<option value="${e.id}" selected>${e.name}</option>`)
                            } else {
                                $(this).append(`<option value="${e.id}">${e.name}</option>`)
                            }
                        });
                    });

                    _jsHelpers._handle_notify("success", res.message);
                } else {
                    _jsHelpers._handle_notify('danger', res.message);
                }
            }).always(res => {
                _jsHelpers._unBlockUi();
            });

        })

        $(function () {
            $("ul.tabs li").click(function () {
                var tab_id = $(this).attr("data-tab");

                $("ul.tabs li").removeClass("current");
                $(".tab-content").removeClass("current");

                $(this).addClass("current");
                $("#" + tab_id).addClass("current");
            });
        });
        $('#btnExport').click(e=>{
            renderBranchesTable()
        })

        initSelectBranchesGroup();

        initEditInfoCompanyForm();
        initModalDetail();
        initModalDetailGroup();

        initModalAddBranchForm();
        initModalMatchFoodicsBranchForm();
        initModalAddGroupForm()

        initEditBranchForm();
        initEditGroupForm();
        initModalEditBranch();
        initModalEditGroup();

        initModalDeleteBranch();
        initModalDeleteGroup();
        initDeleteBranchForm();
        initDeleteGroupForm();


        initTimezoneSelect()
        initCountriesSelect()
        initFoodicsBranchSelect()
        handleFoodicsBranchSelect()
    };

    const searchToObject = function () {
        var pairs = window.location.search.substring(1).split("&"),
            obj = {},
            pair,
            i;

        for (i in pairs) {
            if (pairs[i] === "") continue;

            pair = pairs[i].split("=");
            obj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
        }

        return obj;
    };

    const validateAddBranch = function (isSave = false) {

        let status = true;
        let elements = []

        if (isSave) {
            elements = [
                'branch-name',
                'branch-address',
                'branch-box',
                'country',
                'district',
                'time-zone',
                'branch-email',
                'branch-phone',
                'branch-site-code',
                'select2-users',
            ]
        } else {
            elements = [
                'branch-name',
                'branch-address',
                'branch-box',
                'country',
                'district',
                'time-zone',
                'branch-email',
                'branch-phone',
                'branch-site-code',
                // 'select2-users',
            ]
        }


        for (let ele of elements) {
            let parent = $("#" + ele).parent(".controls");
            let value = $("#" + ele).val();
            if (_jsHelpers.undefined(value)) {
                status = false;
                parent.find(".help-block").text("This field required")
            } else {
                parent.find(".help-block").text("")
            }

        }

        if (!_jsHelpers.undefined($("#select2-users").val()) && status == false) {
            $("div[data-target='container-branch']").click()
        }

        return status


    };

    const validateAddGroup = function (isSave = false) {

        let status = true;
        let elements = []

        if (isSave) {
            elements = [
                'group-name',
                'group-tag',
                'group-code',
                'select2-group-branch',
                'select2-group-users',
            ]
        } else {
            elements = [
                'group-name',
                'group-tag',
                'group-code',
                'select2-group-branch',
                'select2-group-users',
            ]
        }


        for (let ele of elements) {
            let parent = $("#" + ele).parent(".controls");
            let value = $("#" + ele).val();

            if (_jsHelpers.undefined(value)) {
                status = false;
                parent.find(".help-block").text("This field required")
            } else {
                parent.find(".help-block").text("")
            }
        }
        return status
    };

    const initSelectGroupUsers = function () {
        function formatResult(data) {
            if (!data.id) {
                return data.text;
            }
            return $(`
                    <div class="wrapper-users">
                      <div class="">${data.email}</div>
                    </div>
                  `);
        }

        $(".select2-group-users").select2({
            dropdownAutoWidth: true,
            multiple: true,
            width: '100%',
            height: '30px',
            placeholder: `${_jsHelpers._trans("selectUsers")}`,
            allowClear: true,
            closeOnSelect: false,
            data: COMPANY_USERS,
            templateResult: formatResult,
            templateSelection: formatResult,
        });
    };

    const validateEditBranch = function (isSave = false) {

        let status = true;
        let elements = []

        if (isSave) {
            elements = [
                'e-branch-name',
                'e-branch-address',
                'e-branch-box',
                'e-country',
                'e-district',
                'e-time-zone',
                'e-branch-email',
                'e-branch-phone',
                'e-branch-site-code',
                'e-select2-users'
            ]
        } else {
            elements = [
                'e-branch-name',
                'e-branch-address',
                'e-branch-box',
                'e-country',
                'e-district',
                'e-time-zone',
                'e-branch-email',
                'e-branch-phone',
                'e-branch-site-code',
            ]
        }

        for (let ele of elements) {
            let parent = $("#" + ele).parent(".controls");
            let value = $("#" + ele).val();
            if (_jsHelpers.undefined(value)) {
                status = false;
                parent.find(".help-block").text("This field required")
            } else {
                parent.find(".help-block").text("")
            }

        }

        if (!_jsHelpers.undefined($("#e-select2-users").val()) && status == false) {
            $("div[data-target='container-branch']").click()
        }

        return status


    };

    const validateEditGroup = function (isSave = false) {

        let status = true;
        let elements = []

        if (isSave) {
            elements = [
                'e-group-name',
                'e-group-tag',
                'e-group-code',
                'e-select2-group-branch',
                'e-select2-group-users'
            ]
        } else {
            elements = [
                'e-group-name',
                'e-group-tag',
                'e-group-code',
                'e-select2-group-branch',
                'e-select2-group-users'
            ]
        }

        for (let ele of elements) {
            let parent = $("#" + ele).parent(".controls");
            let value = $("#" + ele).val();
            if (_jsHelpers.undefined(value)) {
                status = false;
                parent.find(".help-block").text("This field required")
            } else {
                parent.find(".help-block").text("")
            }

        }

        if (!_jsHelpers.undefined($("#e-select2-group-users").val()) && status == false) {
            $("div[data-target='container-branch']").click()
        }

        return status


    };

    const initTimezoneSelect = function () {
        $.ajax({
            type: 'POST',
            url: _jsHelpers._PREFIX_ADMIN_API + '/catalog/timezone/',
            headers: _jsHelpers._getHeaders(),
        }).done(res => {
            $('.select-timezone1').empty()
            if (!res.error) {
                $('.select-timezone1').append(`<option value="0">${_jsHelpers._trans("selectTimeZone")}</option>`)
                res.data.map(e => {
                    $('.select-timezone1').append(`<option value="${e.timezone}">${e.timezone}</option>`)
                });
            }
            $('.select-timezone').empty()
            if (!res.error) {
                $('.select-timezone').append(`<option value="0">${_jsHelpers._trans("selectTimeZone")}</option>`)
                res.data.map(e => {
                    $('.select-timezone').append(`<option value="${e.timezone}">${e.timezone}</option>`)
                });
            }
        })
    };

    const initCountriesSelect = function () {
        $.ajax({
            type: 'POST',
            url: _jsHelpers._PREFIX_ADMIN_API + '/catalog/countries',
            headers: _jsHelpers._getHeaders(),
        }).done(res => {
            $('.select-countries1').empty()
            if (!res.error) {
                $('.select-countries1').append(`<option value="0">${_jsHelpers._trans("selectCountry")}</option>`)
                res.data.map(e => {
                    $('.select-countries1').append(`<option value="${e.name}">${e.name}</option>`)
                });
            }
            $('.select-countries').empty()
            if (!res.error) {
                $('.select-countries').append(`<option value="0">${_jsHelpers._trans("selectCountry")}</option>`)
                res.data.map(e => {
                    $('.select-countries').append(`<option value="${e.name}">${e.name}</option>`)
                });
            }
        })
    };

    const initFoodicsBranchSelect = function () {
        $.ajax({
            type: 'GET',
            url: _jsHelpers._PREFIX_ADMIN_API + '/foodics',
            headers: _jsHelpers._getHeaders()
        }).done(res => {
            $('.select-foodics-branches').empty()
            if (!res.error) {
                $('.select-foodics-branches').append(`<option value="0">none</option>`)
                $.each($('.select-foodics-branches'), function () {
                    res.data.map(e => {
                        if (e.branch_id == $(this).attr('id')) {
                            $(this).append(`<option value="${e.id}" selected>${e.name}</option>`)
                        } else {
                            $(this).append(`<option value="${e.id}">${e.name}</option>`)
                        }
                    });
                });
            }
        })
    };

    const handleFoodicsBranchSelect = () => {
        $('.select-foodics-branches').on('change', function () {
            HandleDropdowns($(this));
        });
    }

    function HandleDropdowns(element) {
        var $element = element;
        var value = $element.val();
        $.each($('.select-foodics-branches').not($element), function () { //loop all remaining select elements
            var subValue = $(this).val();
            if (subValue === value) { // if value is same reset
                $(this).val('0');
            }
        });
    }

    const updateCantCards = function (type = "branches") {
        if(type == "branches"){
            var cantCards = $(".branch-card").length;
            cantCards = cantCards == 1 ? cantCards + ' branch' : cantCards + ' branches';
            $("#cant-branches").text(cantCards);
        }else{
            var cantCards = $(".groups-card").length;
            cantCards = cantCards == 1 ? cantCards + ' group' : cantCards + ' groups';
            $("#cant-groups").text(cantCards);
        }
    }


    const initSelectUsers = function () {
        function formatResult(data) {
            if (!data.id) {
                return data.text;
            }
            return $(`
              <div class="wrapper-users">
                <div class="">${data.email}</div>
              </div>
            `);
        }

        $(".select2-users").select2({
            dropdownAutoWidth: true,
            multiple: true,
            width: '100%',
            height: '30px',
            placeholder: `${_jsHelpers._trans("selectUsers")}`,
            allowClear: true,
            closeOnSelect: false,
            data: COMPANY_USERS,
            templateResult: formatResult,
            templateSelection: formatResult,
        });
    }

    const initSelectUsersEdit = function (users) {

        var data = [];
        if (!Array.isArray(users)) {
            users = JSON.parse(users);
        }
        let usersIds = users.map(e => e.id);
        COMPANY_USERS.forEach(e => {
            if (usersIds.includes(e.id)) {
                $('.select2-users-edit').append(`<option value="${e.id}" selected>${e.email}</option>`)
            }
            $('.select2-users-edit').append(`<option value="${e.id}">${e.email}</option>`)
        })

        if (users.length > 0) {
            for (let user of users) {
                var usrTmp = {
                    id: user.user_id,
                    email: user.email,
                    name: user.name + " " + user.lastname,
                    photo: user.photo,
                    // rol: user.roles[0].name,
                    selected: true,
                };
                data.push(usrTmp);
            }
        }

        const company_id = $(".more-data #id-company").val();

        $(".select2-users-edit").select2({
            placeholder: `${_jsHelpers._trans("selectUsers")}`,
            dropdownAutoWidth: true,
            closeOnSelect: false,
            width: "100%",
        });
    }

    const initSelectGroupUsersEdit = function (users) {
        $(".select2-users-group-edit").empty();
        var data = [];
        if (!Array.isArray(users)) {
            users = JSON.parse(users);
        }
        let usersIds = users.map(e => e.id);
        COMPANY_USERS.forEach(e => {
            if (usersIds.includes(e.id)) {
                $('.select2-users-group-edit').append(`<option value="${e.id}" selected>${e.email}</option>`)
            }
            $('.select2-users-group-edit').append(`<option value="${e.id}">${e.email}</option>`)
        })

        if (users.length > 0) {
            for (let user of users) {
                var usrTmp = {
                    id: user.user_id,
                    email: user.email,
                    name: user.name + " " + user.lastname,
                    photo: user.photo,
                    selected: true,
                };
                data.push(usrTmp);
            }
        }

        const company_id = $(".more-data #id-company").val();

        $(".select2-users-group-edit").select2({
            placeholder: `${_jsHelpers._trans("selectUsers")}`,
            dropdownAutoWidth: true,
            closeOnSelect: false,
            width: "100%",
        });
    }

    const initSelectBranchesGroup = function () {

        $.ajax({
            type: 'POST',
            url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branchesAndGroups/",
            headers: _jsHelpers._getHeaders(),
        }).done(res => {
            $('.select2-group-branch').empty()
            if (!res.error) {
                res.data.branches.map(e => {
                    $('.select2-group-branch').append(` <option value=${e.id} id="${e.id} class="">${e.name}</option>`)
                });
            }
        })
    };

    const initSelectBranchesEdit = function (branches) {
        function formatResult(data) {

            if (!data.id) {
                return data.text;
            }
            return $(`
                <div class="wrapper-users">
                    <div class="">${data.name}</div>
                </div>
            
        `);
        }

        var data = []
        if (!Array.isArray(branches)) {
            branches = JSON.parse(branches)
        }
        if (branches.length > 0) {
            for (let branch of branches) {

                var branchTmp = {
                    id: branch.id,
                    address: branch.address,
                    created_at: branch.created_at,
                    updated_at: branch.updated_at,
                    email: branch.email,
                    name: branch.name,
                    search: branch.name,
                    phone: branch.phone,
                    type: 'public',
                    selected: true
                }
                data.push(branchTmp)
            }
        }

        $(".select2-group-branch-edit").select2({
            placeholder: "Select branches",
            dropdownAutoWidth: true,
            closeOnSelect: false,
            allowClear: true,
            width: "100%",
            data: data,
            ajax: {
                type: "POST",
                url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branchesAndGroups/",
                headers: _jsHelpers._getHeaders(),
                dataType: "json",
                // we need to handle search from server side
                data: function (params) {
                    return {
                        search: params.term,
                        type: 'public',
                    }
                },
                processResults: function (data) {
                    return {
                        results: data.data.branches,
                    };
                },
                cache: true,
            },
            cache: true,
            templateResult: formatResult,
            templateSelection: formatResult,
        });


    };

    const initEditInfoCompanyForm = function () {
        $('#form-info-company').on('submit', function (e) {
            e.preventDefault();

            const parentForm_ = '#form-info-company ';

            const company_id = $('#form-info-company #id-company').val();

            const company = $(parentForm_ + '#company').val()
            const vatNumber = $(parentForm_ + '#vat-company').val()

            _jsHelpers._blockUI();
            $.ajax({
                type: 'PUT',
                url: _jsHelpers._PREFIX_ADMIN_API + '/company/' + company_id,
                headers: _jsHelpers._getHeaders(),
                data: {
                    company_name: company,
                    company_vat_number: vatNumber,
                },
            }).done(res => {
                if (!res.error) {
                    _jsHelpers._handle_notify("success", res.message);
                } else {
                    _jsHelpers._responseError(res);
                }
                _jsHelpers._unBlockUi();
            }).always(res => {
                modalEditBranch.modal('hide');
                _jsHelpers._unBlockUi();
            });
        });
    };

    const initModalDeleteBranch = function () {
        $(document).on('click', '.deleteBranch', function (e) {
            const branchId = $(this).data('id-branch');
            $('#form-delete-branch .branch-id').val(branchId)

            modalDeleteBranch.modal('show');
        });
    };

    const initModalDeleteGroup = function () {
        $(document).on('click', '.deleteGroup', function (e) {
            const groupId = $(this).data('id-group');
            $('#form-delete-group .group-id').val(groupId)

            modalDeleteGroup.modal('show');
        });
    };

    const initDeleteBranchForm = function () {
        $('#form-delete-branch').on('submit', function (e) {
            e.preventDefault();

            var branchId = $('#form-delete-branch .branch-id').val();
            _jsHelpers._blockUI();
            $.ajax({
                type: 'DELETE',
                url: _jsHelpers._PREFIX_ADMIN_API + '/branch/' + branchId,
                headers: _jsHelpers._getHeaders(),

            }).done(res => {
                if (!res.error) {
                    $('#card-' + branchId).remove()

                    if ($(".card-content .row .card-user").length == 0) {
                        $(".container-no-data").show()
                    }

                    updateCantCards();

                    _jsHelpers._handle_notify("success", res.message);
                } else {
                    _jsHelpers._responseError(res);
                }
                modalDeleteBranch.modal('hide');

            }).always(res => {
                modalDeleteBranch.modal('hide');
                _jsHelpers._unBlockUi();
            });
        });
    };

    const initDeleteGroupForm = function () {
        $('#form-delete-group').on('submit', function (e) {
            e.preventDefault();

            var groupId = $('#form-delete-group .group-id').val();
            _jsHelpers._blockUI();
            $.ajax({
                type: 'DELETE',
                url: _jsHelpers._PREFIX_ADMIN_API + '/groups/' + groupId,
                headers: _jsHelpers._getHeaders(),

            }).done(res => {
                if (!res.error) {
                    $('#card-' + groupId).remove()

                    if ($(".card-content .row .card-user").length == 0) {
                        $(".container-no-group").show()
                    }

                    updateCantCards('groups');
                    _jsHelpers._handle_notify("success", res.message);
                } else {
                    _jsHelpers._responseError(res);
                }
                modalDeleteGroup.modal('hide');

            }).always(res => {
                modalDeleteGroup.modal('hide');
                _jsHelpers._unBlockUi();
            });
        });
    };

    const initModalEditBranch = function () {
        $(document).on('click', '.editBranch', function (e) {

            var branchId = $(this).data('id-branch')

            var $parent = $('#card-' + branchId + ' ');

            const parentForm_ = '#form-edit-branch ';

            var name = $parent.data('name')
            var address = $parent.data('address')
            var box = $parent.data('box')
            var country = $parent.data('country')
            var district = $parent.data('district')
            var timezone = $parent.data('timezone')
            var email = $parent.data('email')
            var site_code = $parent.data('site-code')
            var phone = $parent.data('phone')
            var users = $parent.data('users')



            $(parentForm_ + '#e-branch-name').val(name);
            $(parentForm_ + '#e-branch-address').val(address);
            $(parentForm_ + '#e-branch-box').val(box);
            $(parentForm_ + '#e-country').val(country);
            $(parentForm_ + '#e-district').val(district);
            $(parentForm_ + '#e-time-zone').val(timezone);
            $(parentForm_ + '#e-branch-email').val(email);
            $(parentForm_ + '#e-branch-phone').val(phone);
            $(parentForm_ + '#e-branch-site-code').val(site_code);
            $(parentForm_ + '.select2-users-edit').val(users);
            $('.select2-users-edit').empty();

            initSelectUsersEdit(users)

            $(parentForm_ + '.branch-id').val(branchId)
            modalEditBranch.modal('show');
        });
    };

    const initModalEditGroup = function () {
        $(document).on('click', '.editGroup', function (e) {
            var groupId = $(this).data('id-group')
            var $parent = $('#card-' + groupId);
            const parentForm_ = '#form-edit-group ';

            var name = $parent.data('name');
            var tag = $parent.data('tag');
            var code = $parent.data('code');
            var branches = $parent.data('branches');
            var users = $parent.data('users');

            $(parentForm_ + '#e-group-name').val(name);
            $(parentForm_ + '#e-group-tag').val(tag);
            $(parentForm_ + '#e-group-code').val(code);
            $('.select2-group-branch-edit').empty();


            initSelectGroupUsersEdit(users)
            initSelectBranchesEdit(branches)

            $(parentForm_ + '.group-id').val(groupId)
            modalEditgroup.modal('show');
        });
    };

    const initEditBranchForm = function () {
        $('#form-edit-branch').on('submit', function (e) {
            e.preventDefault();

            if (!validateEditBranch(true)) {
                return;
            }


            const formParent_ = '#form-edit-branch ';
            const branchId = $(formParent_ + '.branch-id').val()
            const parent = '#card-' + branchId + ' ';
            const $parent = $('#card-' + branchId);

            var name = $(formParent_ + '#e-branch-name').val();
            var address = $(formParent_ + '#e-branch-address').val();
            var po_box = $(formParent_ + '#e-branch-box').val();
            var country = $(formParent_ + '#e-country').val();
            var district = $(formParent_ + '#e-district').val();
            var timezone = $(formParent_ + '#e-time-zone').val();
            var email = $(formParent_ + '#e-branch-email').val();
            var phone = $(formParent_ + '#e-branch-phone').val();
            var site_code = $(formParent_ + '#e-branch-site-code').val();

            var users = $(formParent_ + '.select2-users-edit').val();


            _jsHelpers._blockUI();
            $.ajax({
                type: 'PUT',
                url: _jsHelpers._PREFIX_ADMIN_API + '/branch',
                headers: _jsHelpers._getHeaders(),
                data: {
                    branch_id: branchId,
                    po_box: po_box,
                    name: name,
                    address: address,
                    country: country,
                    district: district,
                    timezone: timezone,
                    email: email,
                    phone: phone,
                    site_code: site_code,
                    users: users,
                },
            }).done(res => {
                if (!res.error) {
                    $(parent + '.closeOptions').click()
                    $parent.data('name', name)
                    $parent.data('email', email)
                    $parent.data('phone', phone)
                    $parent.data('address', address)
                    $parent.data('country', country)
                    $parent.data('district', district)
                    $parent.data('timezone', timezone)
                    $parent.data('site-code', site_code)
                    $parent.data('users', '' + JSON.stringify(res.data.users) + '')
                    $(parent + '.-branch-name').text(name)
                    $(parent + '.-branch-email').text(email)
                    $(parent + '.-branch-phone').text(phone)

                    _jsHelpers._handle_notify("success", res.message);
                } else {
                    _jsHelpers._responseError(res);
                }
                modalEditBranch.modal('hide');

            }).always(res => {
                modalEditBranch.modal('hide');
                _jsHelpers._unBlockUi();
            });
        });
    };

    const initEditGroupForm = function () {
        $('#form-edit-group').on('submit', function (e) {
            e.preventDefault();

            if (!validateEditGroup(true)) {
                return;
            }


            const formParent_ = '#form-edit-group ';
            const groupId = $(formParent_ + '.group-id').val()
            const parent = '#card-' + groupId + ' ';
            const $parent = $('#card-' + groupId);

            var name = $(formParent_ + '#e-group-name').val();
            var tag = $(formParent_ + '#e-group-tag').val();
            var code = $(formParent_ + '#e-group-code').val();
            var branches = $(formParent_ + '.select2-group-branch-edit').val();
            var users = $(formParent_ + '.select2-users-group-edit').val();

            data = {
                id: groupId,
                name: name,
                tag: tag,
                code: code,
                branches: branches,
                users: users,
            }


            _jsHelpers._blockUI();
            $.ajax({
                type: 'PUT',
                url: _jsHelpers._PREFIX_ADMIN_API + '/groups',
                headers: _jsHelpers._getHeaders(),
                data: {
                    id: groupId,
                    name: name,
                    tag: tag,
                    code: code,
                    branches: branches,
                    users: users,
                },
            }).done(res => {
                if (!res.error) {

                    $(parent + '.closeOptions').click()
                    $parent.data('name', name);
                    $parent.data('tag', tag);
                    $parent.data('code', code);

                    res.data.map((el, i) => {
                        $parent.data("users", "" + JSON.stringify(el.users) + "");
                        $parent.data("branches", "" + JSON.stringify(el.branches) + "");
                    });

                    $(parent + '#group-name').text(name)
                    $(parent + '#group-tag').text(tag)
                    $(parent + '#group-code').text(code)
                    $(parent + '#group-branches').text(branches.length + " branches")
                    $(parent + '#group-users').text(users.length + " users")

                    _jsHelpers._handle_notify("success", res.message);
                } else {
                    _jsHelpers._responseError(res);
                }
                modalEditgroup.modal('hide');

            }).always(res => {
                modalEditgroup.modal('hide');
                _jsHelpers._unBlockUi();
            });
        });
    };

    const initModalDetail = function () {
        $(document).on('click', '.openDetailModal', function (e) {

            const branchId = $(this).data('id-branch')
            const $parent = $('#card-' + branchId);
            const modalParent_ = '#modal-detail ';

            const branchName = $parent.data('name')
            const companyName = $parent.data('company-name')
            let nodes = $parent.data('nodes')
            const branchUserName = $parent.data('user-name')
            const branchUserEmail = $parent.data('user-email')
            const branchEmail = $parent.data('email')
            const branchPhone = $parent.data('phone')
            const branchAddress = $parent.data('address')
            const branchCountry = $parent.data('country')
            const branchTimezone = $parent.data('timezone')
            const branchSiteCode = $parent.data('site-code')
            const photo = $parent.data('photo')
            const status = 'active';

            let users = $parent.data('users')

            if (photo) {
                $('#img-detail').attr('src', photo);
            } else {
                $('#img-detail').attr('src', '/assets/admin/img/icons/icon-company.png');
            }

            nodes += nodes == 1 ? ' node' : ' nodes'

            $(modalParent_ + '.detail-status').removeClass('active, inactive').addClass(status);

            $(modalParent_ + '.-branch-name').text(branchName);
            $(modalParent_ + '.-company-name').text(companyName);
            $(modalParent_ + '.-nodes').text(nodes);
            $(modalParent_ + '.-branch-user-name').text(branchUserName);
            $(modalParent_ + '.-branch-user-email').text(branchUserEmail);
            $(modalParent_ + '.-branch-email').text(branchEmail);
            $(modalParent_ + '.-branch-phone').text(branchPhone);
            $(modalParent_ + '.-branch-address').text(branchAddress);
            $(modalParent_ + '.-branch-country').text(branchCountry);
            $(modalParent_ + '.-branch-timezone').text(branchTimezone);
            $(modalParent_ + '.-branch-sitecode').text(branchSiteCode);


            $(".lista-usuarios-branch").empty()

            if (!Array.isArray(users)) {
                users = JSON.parse(users)
            }

            if (users.length > 0) {
                var stringUser = ``;
                for (let user of users) {
                    let name = user.name
                    let photoUser = ''
                    if (user.photo) {
                        photoUser = user.photo
                    } else {
                        photoUser = "/assets/admin/img/misc/user_active.png"
                    }
                    stringUser += `
                 <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="${name}" class="avatar pull-up">
                <img class="media-object rounded-circle" src="${photoUser}" alt="Avatar" height="30" width="30">
                </li>
                `;
                }
                $(".lista-usuarios-branch").append(stringUser)
                $('[data-toggle="tooltip"]').tooltip({
                    container: "body"
                })
            } else {
                $(".lista-usuarios-branch").append("<p style='font-size: 12px; margin: 0px'>0 users linked</p>")
            }




            modalDetails.modal('show');
        });
    };

    const initModalDetailGroup = function () {
        $(document).on('click', '.openDetailModalGroup', function (e) {

            const groupId = $(this).data('id-group')
            const $parent = $('#card-' + groupId);
            const modalParent_ = '#modal-detail-group ';

            const name = $parent.data('name')
            const tag = $parent.data('tag')
            const code = $parent.data('code')
            let branches = $parent.data('branches')
            let users = $parent.data('users')

            $('#img-detail').attr('src', '/assets/admin/img/icons/icon-company.png');

            $(modalParent_ + '.-group-name-card').text(name);
            $(modalParent_ + '.-code-card').text(code);
            $(modalParent_ + '.-tag-card').text(tag);


            $(".lista-branches-group").empty()
            $(".lista-usuarios-group").empty()

            if (!Array.isArray(branches)) {
                branches = JSON.parse(branches)
            }

            if (branches.length > 0) {
                var stringBranch = ``;
                for (let branch of branches) {
                    let name = branch.name
                    let photoBranch = ''
                    if (branch.photo) {
                        photoBranch = branch.photo
                    } else {
                        photoBranch = "/assets/admin/img/icons/icon-company.png"
                    }
                    stringBranch += `
                 <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="${name}" class="avatar pull-up">
                <img class="media-object rounded-circle" src="${photoBranch}" alt="Avatar" height="30" width="30">
                </li>
                `;
                }
                $(".lista-branches-group").append(stringBranch)
                $('[data-toggle="tooltip"]').tooltip({
                    container: "body"
                })
            } else {
                $(".lista-branches-group").append("<p style='font-size: 12px; margin: 0px'>0 branches linked</p>")
            }

            if (!Array.isArray(users)) {
                users = JSON.parse(users)
            }

            if (users.length > 0) {
                var stringUser = ``;
                for (let user of users) {
                    let name = user.name
                    let photoUser = ''
                    if (user.photo) {
                        photoUser = user.photo
                    } else {
                        photoUser = "/assets/admin/img/misc/user_active.png"
                    }
                    stringUser += `
                 <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="${name}" class="avatar pull-up">
                <img class="media-object rounded-circle" src="${photoUser}" alt="Avatar" height="30" width="30">
                </li>
                `;
                }
                $(".lista-usuarios-group").append(stringUser)
                $('[data-toggle="tooltip"]').tooltip({
                    container: "body"
                })
            } else {
                $(".lista-usuarios-group").append("<p style='font-size: 12px; margin: 0px'>0 users linked</p>")
            }

            modalDetailsGroup.modal('show');
        });
    };

    const initModalAddBranchForm = function () {
        $('#form-add-branch').on('submit', function (e) {
            e.preventDefault();

            if (!validateAddBranch(true)) {
                return;
            }

            var formParent_ = '#form-add-branch ';

            var company_id = $('#form-info-company #id-company').val();
            var name = $(formParent_ + '#branch-name').val();
            var address = $(formParent_ + '#branch-address').val();
            var po_box = $(formParent_ + '#branch-box').val();
            var country = $(formParent_ + '#country').val();
            var district = $(formParent_ + '#district').val();
            var timezone = $(formParent_ + '#time-zone').val();
            var email = $(formParent_ + '#branch-email').val();
            var phone = $(formParent_ + '#branch-phone').val();
            var site_code = $(formParent_ + '#branch-site-code').val();
            var users = $(formParent_ + '.select2-users').val();




            _jsHelpers._blockUI();
            $.ajax({
                type: 'POST',
                url: _jsHelpers._PREFIX_ADMIN_API + '/branch',
                headers: _jsHelpers._getHeaders(),
                data: {
                    company_id: company_id,
                    po_box: po_box,
                    name: name,
                    address: address,
                    country: country,
                    district: district,
                    timezone: timezone,
                    email: email,
                    phone: phone,
                    site_code: site_code,
                    users: users,
                },
            }).done(res => {
                if (!res.error) {
                    getNewBranch(res)
                    _jsHelpers._handle_notify("success", res.message);
                    modalAddBranch.modal('hide')
                } else {
                    _jsHelpers._responseError(res);
                }
            }).always(res => {
                modalAddBranch.modal('hide')
                _jsHelpers._unBlockUi();
            });
        });
    };

    const initModalMatchFoodicsBranchForm = function () {
        $('#form-match-foodics-branch').on('submit', function (e) {
            e.preventDefault();

            var formParent_ = '#form-match-foodics-branch ';

            var data = [];
            var company_id = $('#form-info-company #id-company').val();
            $.each($('.select-foodics-branches'), function () {
                data.push({
                    branch_id: parseInt($(this).attr('id')),
                    foodics_branch_id: $(this).find(":selected").val() == 0 ? null : parseInt($(this).find(":selected").val())
                });
            });
            _jsHelpers._blockUI();
            $.ajax({
                type: 'POST',
                url: _jsHelpers._PREFIX_ADMIN_API + '/foodics/connectFoodicsBraches',
                headers: _jsHelpers._getHeaders(),
                data: {
                    data: data
                },
            }).done(res => {
                if (!res.error) {
                    _jsHelpers._handle_notify("success", res.message);
                    modalFoodicsMatch.modal('hide')
                } else {
                    _jsHelpers._responseError(res);
                }
            }).always(res => {
                modalFoodicsMatch.modal('hide')
                _jsHelpers._unBlockUi();
            });
        });
    };

    const initModalAddGroupForm = function () {
        $('#form-add-group').on('submit', function (e) {
            e.preventDefault();

            if (!validateAddGroup(true)) {
                return;
            }

            var formParent_ = '#form-add-group ';

            var company_id = $('#form-info-company #id-company').val();
            var name = $(formParent_ + '#group-name').val();
            var tag = $(formParent_ + '#group-tag').val();
            var code = $(formParent_ + '#group-code').val();
            var branches = $(formParent_ + '.select2-group-branch').val();
            var users = $(formParent_ + '.select2-group-users').val();
            const parent = '#card-' + company_id + ' ';

            const $parent = $('#card-' + company_id);

            _jsHelpers._blockUI();
            $.ajax({
                type: 'POST',
                url: _jsHelpers._PREFIX_ADMIN_API + '/groups',
                headers: _jsHelpers._getHeaders(),
                data: {
                    company_id: company_id,
                    name: name,
                    tag: tag,
                    code: code,
                    branches: branches,
                    users: users,
                },
            }).done(res => {
                if (!res.error) {
                    $(parent + '.closeOptions').click()

                    $parent.data("branches", "" + JSON.stringify(branches) + "");

                    getNewGroup(res)
                    _jsHelpers._handle_notify("success", res.message);
                    modalAddGroup.modal('hide')
                } else {
                    _jsHelpers._responseError(res);
                }
            }).always(res => {
                modalAddGroup.modal('hide')
                _jsHelpers._unBlockUi();
            });
        });
    };

    const getNewBranch = function (res) {
        $(".container-no-data").hide()

        var data = res.data

        var branchId = data.id;
        var companyName = $("#in-company").data("company")

        var name = data.name;
        var nodes = ''
        var email = data.email;
        var phone = data.phone;
        var po_box = data.po_box;
        var address = data.address;
        var country = data.country;
        var district = data.district;
        var timezone = data.timezone;
        var sitecode = data.site_code;
        var photo = '/assets/admin/img/icons/icon-company.png';
        var users = data.users;

        var footer = ''


        var tpl = `
            <div id="card-${branchId}" class="col-xl-3 col-lg-3 col-md-4 col-sm-1 pb-1"
                data-company-name="${companyName}"
                data-name="${name}"
                data-nodes="${nodes}"
                data-email="${email}"
                data-phone="${phone}"
                data-box="${po_box}"
                data-address="${address}"
                data-country="${country}"
                data-district="${district}"
                data-timezone = "${timezone}"
                data-site-code="${sitecode}"
                data-photo="${photo}"
                data-users='${JSON.stringify(users)}'
            >
                <div class="card-user branch-card">
                    <div class="options">
                        <i class="bx bx-dots-vertical-rounded openOptions"></i>
                    </div>
                    <div data-id-branch="${branchId}"  class="openDetailModal">
                        <div class="profile mb-1">
                            <div class="container-img-branch">
                                <img src="${photo}" class="mr-1" alt="" onerror="_jsHelpers._errorImage(this)">
                            </div>
                            <div class="data">
                                <p class="-info m-0 w-600 -branch-name">${name}</p>
                                <p class="-info m-0 -branch-company-name" style=" font-size: 12px">${companyName}</p>
                            </div>
                        </div>
                        <div class="more-data">
                            <p class="m-0 -branch-email" style="color: var(--fontColor);">${email}</p>
                            <p class="m-0 -branch-phone" style="color: var(--fontColor);">${phone}</p>
                            ${footer}
                        </div>
                    </div>
                    <div class="more-options" style="display: none">
                        <div class="title">
                            <span>Options</span>
                            <i class="bx bx-x closeOptions"></i>
                        </div>
                        <p data-id-branch="${branchId}" class="text-success editBranch" >Edit branch</p>
                        <p data-id-branch="${branchId}" class="text-danger deleteBranch">Delete branch</p>
                    </div>
                </div>
            </div>
           
       `
        $("#branchesContainer").append(tpl)
        updateCantCards();
    };

    const getNewGroup = function (res) {
        $(".container-no-group").hide()

        var data = res.data

        var groupId = data.id;
        var companyName = $("#in-company").data("company")

        var name = data.name;
        var tag = data.tag
        var code = data.code;
        var photo = '/assets/admin/img/icons/icon-company.png';
        var branches = data.branches;
        var users = data.users;
        var footer = ''


        var tpl = `
            <div id="card-${groupId}" class="col-xl-3 col-lg-3 col-md-4 col-sm-1 pb-1"
                data-company-name="${companyName}"
                data-name="${name}"
                data-tag="${tag}"
                data-code="${code}"
                data-users='${JSON.stringify(users)}'
                data-branches='${JSON.stringify(branches)}'
            >
                <div class="card-user groups-card">
                    <div class="options">
                        <i class="bx bx-dots-vertical-rounded openOptions"></i>
                    </div>
                    <div data-id-group="${groupId}"  class="openDetailModal">
                        <div class="profile mb-1">
                            <div class="container-img-branch">
                                <img src="${photo}" class="mr-1" alt="" onerror="_jsHelpers._errorImage(this)">
                            </div>
                            <div class="data">
                                <p class="-info m-0 w-600 -branch-name">${name}</p>
                                <p class="-info m-0 -branch-company-name" style=" font-size: 12px">${tag}</p>
                                <p class="-info m-0 -branch-company-name" style=" font-size: 12px">${code}</p>
                            </div>
                        </div>
                        <div class="more-data">
                            <p class="m-0 -branch-email" style="color: var(--fontColor);">${branches.length + (branches.length > 1 ? " branches" : " branch")}</p>
                            <p class="m-0 -branch-phone" style="color: var(--fontColor);">${users.length + (users.length > 1 ? " users" : " user")}</p>
                            ${footer}
                        </div>
                    </div>
                    <div class="more-options" style="display: none">
                        <div class="title">
                            <span>Options</span>
                            <i class="bx bx-x closeOptions"></i>
                        </div>
                        <p data-id-group="${groupId}" class="text-success editGroup" >Edit group</p>
                        <p data-id-group="${groupId}" class="text-danger deleteGroup">Delete group</p>
                    </div>
                </div>
            </div>
       `
        $("#groupsContainer").append(tpl)
        updateCantCards('groups');
    };

    function renderBranchesTable() {
        $.ajax({
            type: 'GET',
            url: _jsHelpers._PREFIX_ADMIN_API + '/counter/status',
            headers: _jsHelpers._getHeaders()
        }).done(res => {
           if(!res.error){
               $('#tbBranchesBody').empty();
               res.data.forEach(branch=>{
                let str = `
                <tr>
                <td>${branch.branch.site_code}</td>
                <td>${branch.branch.name}</td>
                <td>${branch.place}</td>
                <td>${branch.timezone}</td>
                <td>${branch.status}</td>
                <td>${branch.last_online}</td>
                <td>${branch.branch.address}</td>
                <td>${branch.branch.address}</td>
                <td>${branch.groups.map(item=>item.name).join(" | ")}</td>
                </tr>`;
                $('#tbBranchesBody').append(str)

               });
               exportData();
           }
        })
    }

    function exportData() {
        /* Get the HTML data using Element by Id */
        var table = document.getElementById("tbBranches");

        /* Declaring array variable */
        var rows = [];

        //iterate through rows of table
        for (var i = 0, row; row = table.rows[i]; i++) {
            //rows would be accessed using the "row" variable assigned in the for loop
            //Get each cell value/column from the row
            column1 = row.cells[0].innerText;
            column2 = row.cells[1].innerText;
            column3 = row.cells[2].innerText;
            column4 = row.cells[3].innerText;
            column5 = row.cells[4].innerText;
            column6 = row.cells[5].innerText;
            column7 = row.cells[6].innerText;
            column8 = row.cells[7].innerText;
            column9 = row.cells[8].innerText;

            /* add a new records in the array */
            rows.push(
                [
                    column1,
                    column2,
                    column3,
                    column4,
                    column5,
                    column6,
                    column7,
                    column8,
                    column9,
                ]
            );

        }
        csvContent = "data:text/csv;charset=utf-8,";
        /* add the column delimiter as comma(,) and each row splitted by new line character (\n) */
        rows.forEach(function (rowArray) {
            row = rowArray.join(",");
            csvContent += row + "\r\n";
        });

        /* create a hidden <a> DOM node and set its download attribute */
        var encodedUri = encodeURI(csvContent);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", "Branches Data.csv");
        document.body.appendChild(link);
        /* download the data file named "Stock_Price_Report.csv" */
        link.click();
    }

    return {
        init: function () {
            init()
        }
    }
}();

$(document).ready(function () {
    CompanyBranches.init();
})
