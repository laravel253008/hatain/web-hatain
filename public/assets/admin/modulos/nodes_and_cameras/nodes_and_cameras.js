const NodesAndCameras = (function () {
  firebase.initializeApp(_jsHelpers._FIREBASECONFIG);
  let db = firebase.firestore();

  var _deviceId = "";
  var nodeSerial = "";
  var nodeModel = "";
  var token = "";

  var containerLoading = $(".container-loading-node");

  var modalAddInstallationNode = $("#modal-add-installation-node");
  var modalDeleteNode = $("#modal-delete-node");
  var modalDownloadInfo = $("#modal-download-info");
  var modalPlugOutCameras = $("#modal-plug-out-cameras");
  var modalUpdateStatus = $("#modal-update-status");
  var modalUpdateSoftwareVersion = $("#modal-update-software-version");
  var modalUpdateBranch = $("#modal-update-branch");
  var modalDetail = $("#modal-detail");

  const init = function () {
    $(document).on("click", ".openOptions", function () {
      $(this).parents(".custom-card").find(".more-options").show();
    });

    $(document).on("click", ".closeOptions", function () {
      $(this).parents(".more-options").hide();
    });

    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

    initNewInstallation();

    initModalDetail();

    initModalDownloadInfo();

    initModalPlugOutCamera();

    initUpdateVersionForm();
    initModalUpdateVersion();

    initModalUpdateStatus();
    initUpdateStatusForm();

    initModalDeleteNode();
    initDeleteNodeForm();

    initUpdateBranchForm();
    initModalUpdateBranch();

    initBranchesSelect();

    getVersion();

    filtersControls();

    intervalConexion();

    listenNodes();
  };

  const listenNodes = function () {
    db.collection("devices").onSnapshot(function (querySnapshot) {
      querySnapshot.forEach(function (doc) {
        let deviceId = doc.id;
        //debugger
        var cardNode = $("div[data-device-id=" + deviceId + "]");

        var containerBall = cardNode.find(".container-ball");
        var status = cardNode.data("status");
        // var nodeTmp = nodes[deviceId]
        var nodeTmp = doc.data();

        var conexionNode = cardNode.data("conexion");

        // debugger
        if (status === 1) {
          if (conexionNode != nodeTmp.active) {
            cardNode.data("conexion", nodeTmp.active);
            if (nodeTmp.active) {

              containerBall.empty();
              containerBall.append(
                '<span class="-ball pulse-bg-success pulse-success"></span>'
              );
            } else {

              containerBall.empty();
              containerBall.append(
                '<span class="-ball pulse-bg-white pulse-white"></span>'
              );
            }
          }
        }
      });
    });
  };

  const intervalConexion = function () {
    setInterval(function () {
      $.each($(".card-content .row .tpl-card"), function (index, value) {
        let _this = $(this);

        let lastConnection = _this.data("last-connection");

        let dt = moment(lastConnection).fromNow();

        _this.find(".-last-connection").text(dt);

      });
    }, 60000);
  };

  const getVersion = function () {
    let containerVersion = $(".container-versions");
    let radioVersion = `
        <div class="radio radio-success mb-1">
            <input type="radio" name="radio-version" id="radio-version-0" value="0" checked>
            <label for="radio-version-0">All software versions</label>
        </div>
        `;
    $.ajax({
      type: "POST",
      url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/versiones",
      headers: _jsHelpers._getHeaders(),
    }).done((res) => {
      containerVersion.empty();
      if (!res.error) {
        res.data.map((e) => {
          if (e.id < 5) {
          
            radioVersion += `
            <div class="radio radio-success mb-1">
            <input type="radio" name="radio-version" id="radio-version-${e.id}" value="${e.version}">
            <label for="radio-version-${e.id}">${e.version}</label>
            </div>
            `;
          }
        });
        containerVersion.append(radioVersion);
      }
    });
  };

  const filtersControls = function () {
    $(document).on("keyup", ".filtros .search", function () {
      let _this = $(this);
      getFilter();
    });
    $(document).on("change", ".filtros .select-branch", function (e) {

      let _this = $(this);
      getFilter();
    });

    $(document).on("change", "input[name='radio-version']", function (e) {

      let _this = $(this).val();
      getFilter();
    });
    $(document).on("change", "input[name='radio-status']", function (e) {
      let _this = $(this).val();
      getFilter();
    });
  };

  const getFilter = function () {
    let companyId = $(".filtros .select-company").val();
    let branchId = $(".filtros .select-branch").val();
    let search = $(".filtros .search").val();
    let version = $("input:radio[name=radio-version]:checked").val();
    let status = $("input:radio[name=radio-status]:checked").val();
    branchId = _jsHelpers.undefined(branchId) ? 0 : branchId;
    $.ajax({
      type: "GET",
      url: _jsHelpers._PREFIX_ADMIN_API + "/nodes/getNodes",
      headers: _jsHelpers._getHeaders(),
      data: {
        company_id: companyId,
        branch_id: branchId,
        search: search,
        version: version,
        status: status,
      },
    }).done((res) => {
      if (!res.error) {
        getTpl(res, true);
      } else {
        dataEmpty();
      }
    });
  };

  const updateCantCards = function () {
    var cantCards = $(".custom-card").length;
    cantCards = cantCards == 1 ? cantCards + " node" : cantCards + " nodes";
    $(".-cant-card").text(cantCards);
  };

  const initBranchesSelect = function () {
    const companyId = $("#in-data").data("company-id");
    $.ajax({
      type: "GET",
      url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branchesByCompany",
      headers: _jsHelpers._getHeaders(),
    }).done((res) => {
      $(".select-branch").empty();
      if (!res.error) {
        $(".select-branch").append(
          `<option value="0">${_jsHelpers._trans("selectBranch")}</option>`
        );
        res.data.map((e) => {
          $(".select-branch").append(
            `<option value="${e.id}">${e.name}</option>`
          );
        });
      }

      $(".select-branch").select2({
        dropdownAutoWidth: true,
        width: "100%",
      });
    });
  };

  const initModalDownloadInfo = function () {
    $(document).on("click", ".downloadInfo", function (e) {
      const nodeId = $(this).data("id-node");

      modalDownloadInfo.modal("show");
    });
  };

  const initModalPlugOutCamera = function () {
    $(document).on("click", ".plugOutCameras", function (e) {
      const nodeId = $(this).data("id-node");

      modalPlugOutCameras.modal("show");
    });
  };

  const initNewInstallation = function () {
    var codeInstallation = $(".copy-to-clipboard");
    var containerDataNode = $(".container-data-node");

    $(document).on("click", ".btn-copy", function () {
      codeInstallation.select();
      document.execCommand("copy");
    });

    $(document).on("click", ".btn-add-new-installation", function () {
      containerDataNode.hide();
      $("#form-update-info-node")[0].reset();

      _jsHelpers._blockUI();
      $.ajax({
        type: "GET",
        url: _jsHelpers._PREFIX_ADMIN_API + "/nodes/requestInstallation",
        headers: _jsHelpers._getHeaders(),
      })
        .done((res) => {
          if (!res.error) {
            _deviceId = "";
            nodeSerial = "";
            nodeModel = "";
            token = "";
            containerLoading.hide();

            codeInstallation.val(res.data.command);
            token = res.data.token;
            containerLoading.show();

            modalAddInstallationNode.modal();
          } else {
            _jsHelpers._responseError(res);
          }
        })
        .always((res) => {
          _jsHelpers._unBlockUi();
        });
    });

    // checkToken (localstorage)
    // valido
    db.collection("installations").onSnapshot(function (querySnapshot) {
      querySnapshot.forEach(function (doc) {
        let id = doc.id;
        let data = doc.data();

        if (!_jsHelpers.undefined(token)) {
          if (token === id) {
            if (!_jsHelpers.undefined(data.serial)) {
              nodeSerial = data.serial;
              nodeModel = data.model;
              $(".-node-serial").val(nodeSerial);
              $(".-node-model").val(nodeModel);
              containerDataNode.show();
              containerLoading.hide();
            }
          }
        }
      });
    });

    // llamarApi refrescar el token

    // desloguear de firebase
    // loguear firebase

    // llamar funcion de lectura

    // db.collection("installations").onSnapshot(function (querySnapshot) {
    //
    //     let dataTmpNodes = [];
    //     querySnapshot.forEach(function (doc) {
    //         let id = doc.id;
    //         let data = doc.data()
    //         let merge = {[id]:data}
    //         dataTmpNodes.push(merge)
    //     });
    //
    //
    //     if(!_jsHelpers.undefined(token)){
    //         for(let data of dataTmpNodes){
    //             debugger

    //         }
    //         var node  = dataTmpNodes[token]
    //         debugger

    //         // _deviceId = node.device_id
    //         nodeSerial = node.serial
    //         nodeModel = node.modelo
    //         // $(".-device-id").val(_deviceId);
    //         $(".-node-serial").val(nodeSerial);
    //         $(".-node-model").val(nodeModel);
    //
    //         containerDataNode.show()
    //         containerLoading.hide()
    //     }
    //
    // })

    // return

    // db.collection("devices").onSnapshot(function (querySnapshot) {
    //
    //     // var cities = [];
    //     // querySnapshot.forEach(function(doc) {
    //     //     cities.push(doc.data().name);
    //     // });
    //
    //     if(!_jsHelpers.undefined(token)){
    //         var node  = snapshot.val()["devices"][token]
    //         _deviceId = node.device_id
    //         nodeSerial = node.serial
    //         nodeModel = node.modelo
    //         $(".-device-id").val(_deviceId);
    //         $(".-node-serial").val(nodeSerial);
    //         $(".-node-model").val(nodeModel);
    //
    //         containerDataNode.show()
    //         containerLoading.hide()
    //     }
    //
    //
    // });

    $("#form-update-info-node").on("submit", function (e) {
      e.preventDefault();

      var license = $(".-node-license").val();
      var branch_id = $("#form-update-info-node .select-branch").val();

      if (_jsHelpers.undefined(branch_id)) {
        _jsHelpers._handle_notify("danger", `${_jsHelpers._trans("selectBranch")}`);

        return;
      }

      _jsHelpers._blockUI();
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/device",
        headers: _jsHelpers._getHeaders(),
        data: {
          serial: nodeSerial,
          model: nodeModel,
          licencia: license,
          token: token,
          branch_id: branch_id,
        },
      })
        .done((res) => {
          if (!res.error) {
            modalAddInstallationNode.modal("hide");
            getTpl(res, false);

            _jsHelpers._handle_notify("success", res.message);
          } else {
            _jsHelpers._responseError(res);
          }
        })
        .always((res) => {
          _jsHelpers._unBlockUi();
        });
    });
  };

  const initModalUpdateVersion = function () {
    $(document).on("click", ".updateSoftwareVersion", function (e) {
      var nodeId = $(this).data("id-node");
      $("#form-update-software-version #version").val("");
      $("#form-update-software-version #password").val("");
      $("#form-update-software-version .node-id").val(nodeId);

      modalUpdateSoftwareVersion.modal("show");
    });
  };

  const initUpdateVersionForm = function () {
    $("#form-update-software-version").on("submit", function (e) {
      e.preventDefault();

      var version = $("#form-update-software-version #version").val();
      var password = $("#form-update-software-version #password").val();

      var nodeId = $("#form-update-software-version .node-id").val();
      var parent = "#card-" + nodeId + " ";

      _jsHelpers._blockUI();
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/user/updateSoftwareVersionXXX",
        headers: _jsHelpers._getHeaders(),
        data: {
          version: version,
          password: password,
          nodeId: nodeId,
        },
      })
        .done((res) => {
          $(parent + ".closeOptions").click();

          if (!res.error) {
            _jsHelpers._handle_notify("success", res.message);
          } else {
            _jsHelpers._responseError(res);
          }
          modalUpdateSoftwareVersion.modal("hide");
        })
        .always((res) => {
          _jsHelpers._unBlockUi();
          modalUpdateSoftwareVersion.modal("hide");
        });
    });
  };

  const initModalUpdateStatus = function () {
    $(document).on("click", ".updateStatus", function (e) {
      var nodeId = $(this).data("id-node");
      var $parent = $("#card-" + nodeId);

      var details = $parent.data("details");
      var status = $parent.data("status");
      var stringStatus = "";

      if (status == 1) {
        stringStatus = "Active";
      } else if (status == 2) {
        stringStatus = "Stand by";
      } else if (status == 3) {
        stringStatus = "Out of use";
      }
      var selectStatus = $("#form-update-status .select-status");
      $("#form-update-status .details").val(details);
      $("#form-update-status .current-status").val(stringStatus);
      selectStatus.val(status);
      $("#form-update-status .node-id").val(nodeId);

      modalUpdateStatus.modal("show");
    });
  };

  const initUpdateStatusForm = function () {
    $("#form-update-status").on("submit", function (e) {
      e.preventDefault();

      var details = $("#form-update-status .details").val();
      var status = $("#form-update-status .versions").val();
      var nodeId = $("#form-update-status .node-id").val();
      var parent = "#card-" + nodeId + " ";
      var $parent = $("#card-" + nodeId);

      status = parseInt(status);

      _jsHelpers._blockUI();
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/device/updateStatus/",
        headers: _jsHelpers._getHeaders(),
        data: {
          status: status,
          node_id: nodeId,
          details: details,
        },
      })
        .done((res) => {
          $(parent + ".closeOptions").click();

          if (!res.error) {
            $parent.data("details", details);
            $parent.data("status", status);

            $(parent + ".container-ball").empty();
            var ballStatus = "";
            if (status === 1) {
              ballStatus =
                '<span class="-ball pulse-bg-success pulse-success"></span>';
            } else if (status === 2) {
              ballStatus =
                '<span class="-ball pulse-bg-warning pulse-warning"></span>';
            } else if (status === 3) {
              ballStatus =
                '<span class="-ball pulse-bg-danger pulse-danger"></span>';
            } else {
              ballStatus =
                '<span class="-ball pulse-bg-danger pulse-danger"></span>';
            }
            $(parent + ".container-ball").append(ballStatus);

            modalUpdateStatus.modal("hide");

            _jsHelpers._handle_notify("success", res.message);
          } else {
            _jsHelpers._responseError(res);
          }
        })
        .always((res) => {
          _jsHelpers._unBlockUi();
        });
    });
  };

  const initModalDeleteNode = function () {
    $(document).on("click", ".deleteNode", function (e) {
      var nodeId = $(this).data("id-node");
      $("#form-delete-node .node-id").val(nodeId);

      modalDeleteNode.modal("show");
    });
  };

  const initDeleteNodeForm = function () {
    $("#form-delete-node").on("submit", function (e) {
      e.preventDefault();

      var nodeId = $("#form-delete-node .node-id").val();
      _jsHelpers._blockUI();
      $.ajax({
        type: "DELETE",
        url: _jsHelpers._PREFIX_ADMIN_API + "/device/" + nodeId,
        headers: _jsHelpers._getHeaders(),
      })
        .done((res) => {
          if (!res.error) {
            $("#card-" + nodeId).remove();
            updateCantCards();
            _jsHelpers._handle_notify("success", res.message);
          } else {
            _jsHelpers._responseError(res);
          }
          modalDeleteNode.modal("hide");
        })
        .always((res) => {
          modalDeleteNode.modal("hide");

          _jsHelpers._unBlockUi();
        });
    });
  };

  const initModalDetail = function () {
    $(document).on("click", ".openModalDetail", function (e) {
      var nodeId = $(this).data("id-node");
      var $parent = $("#card-" + nodeId);
      var modalParent_ = "#modal-detail ";

      var deviceId = $parent.data("device-id");
      var version = $parent.data("version");
      var company = $parent.data("company");
      var branch = $parent.data("branch");
      var conexion = $parent.data("conexion");
      var status = $parent.data("status");
      var created_at = $parent.data("created-at");
      let statusString = "outofuse";

      if (status == 1 && conexion == 1) {
        statusString = "active";
      } else if (status == 2) {
        statusString = "standby";
      } else if (status == 3) {
        statusString = "outofuse";
      } else {
        statusString = "outofuse";
      }

      if (!Array.isArray(company)) {
        company = JSON.parse(company);
      }
      if (!Array.isArray(branch)) {
        branch = JSON.parse(branch);
      }

      $(modalParent_ + ".detail-status")
        .removeClass(["active", "standby", "outofuse"])
        .addClass(statusString);
      $(modalParent_ + ".-device-id").text(deviceId);
      $(modalParent_ + ".-software-version").text("Version" + version);
      $(modalParent_ + ".-company-name").text(company.name);
      $(modalParent_ + ".-branch-name").text(branch.name);
      $(modalParent_ + ".-cams").text("0 cameras are connected");

      _jsHelpers._getSince(created_at);

      modalDetail.modal("show");
    });
  };

  const initModalUpdateBranch = function () {
    $(document).on("click", ".updateBranch", function (e) {
      var nodeId = $(this).data("id-node");
      var parent = $("#card-" + nodeId);

      var company = parent.data("company");
      var branch = parent.data("branch");

      if (!Array.isArray(company) && typeof company != "object") {
        company = JSON.parse(company);
      }
      if (!Array.isArray(branch) && typeof branch != "object") {
        branch = JSON.parse(branch);
      }

      $("#form-update-branch .node-id").val(nodeId);
      $("#form-update-branch .current-company").val(company.name);
      $("#form-update-branch .current-branch").val(branch.name);

      modalUpdateBranch.modal("show");
    });
  };

  const initUpdateBranchForm = function () {
    $("#form-update-branch").on("submit", function (e) {
      e.preventDefault();

      var branchId = $("#form-update-branch .select-branch").val();
      var nodeId = $("#form-update-branch .node-id").val();
      var password = $("#form-update-branch .password-to-change-branch").val();
      var $parent = $("#card-" + nodeId);

      var parent = "#card-" + nodeId + " ";

      if ($("#whitBranch").is(":checked")) {
        if (_jsHelpers.undefined(branchId)) {
          _jsHelpers._handle_notify("danger", "Select a new branch");
          return;
        }
      } else {
        branchId = 0;
      }

      if (_jsHelpers.undefined(password)) {
        _jsHelpers._handle_notify("danger", "Enter your password");
        return;
      }

      _jsHelpers._blockUI();
      $.ajax({
        type: "PUT",
        url: _jsHelpers._PREFIX_ADMIN_API + "/nodes/branch",
        headers: _jsHelpers._getHeaders(),
        data: {
          branch_id: branchId,
          node_id: nodeId,
          password: password,
        },
      })
        .done((res) => {
          $(parent + ".closeOptions").click();

          if (!res.error) {
            _jsHelpers._handle_notify("success", res.message);

            var company = res.data.company;
            var branch = res.data.branch;
            $parent.data("company", JSON.stringify(company));
            $parent.data("branch", JSON.stringify(branch));

            $parent
              .find(".-company-branch")
              .text(company.name + " - " + branch.name);
            modalUpdateBranch.modal("hide");
          } else {
            _jsHelpers._responseError(res);
          }
        })
        .always((res) => {
          _jsHelpers._unBlockUi();
        });
    });
  };

  var dataEmpty = function () {
    $(".container-no-data").hide();
    $(".container-cards .row").empty();
    $(".container-cards .row").append(
      `<p style="color:#525352;margin: auto;padding: 200px; font-size: 16px;">We can´t find anything like this</p>`
    );
  };

  var getTpl = function (res, fromFilters = false) {
    $(".container-no-data").hide();

    let tpl = ``;
    if (fromFilters) {
      $(".card-content .row").empty();
      res.data.map((data) => {
        tpl += tplCard(data);
      });
    } else {
      tpl = tplCard(res.data);
    }

    $(".card-content .row").append(tpl);
    $('[data-toggle="tooltip"]').tooltip();
    updateCantCards();
  };

  var tplCard = function (data) {
    var nodeId = data.id;
    var token = data.token;
    var deviceId = data.device_id;
    var serial = data.serial;
    var model = data.model;
    var version = data.firmwareversion;
    var status = data.estatus;
    var conexion = data.active;
    var createdAt = data.created_at;
    var company = data.company;
    var branch = data.branch;
    var ip = _jsHelpers.undefined(data.ip) ? "" : data.ip;
    var lastConnection = data.last_connection;
    var lastConnectionForHuman = data.last_connection_for_human;


    var bulletActive = "";
    if (status) {
      bulletActive = "bullet-success";
    } else {
      bulletActive = "bullet-danger";
    }

    var footer = "";

    var ballStatus = "";

    if (status === 1 && conexion) {
      ballStatus = '<span class="-ball pulse-bg-success pulse-success"></span>';
    } else if (status === 2) {
      ballStatus = '<span class="-ball pulse-bg-warning pulse-warning"></span>';
    } else if (status === 3) {
      ballStatus = '<span class="-ball pulse-bg-danger pulse-danger"></span>';
    } else {
      ballStatus = '<span class="-ball pulse-bg-danger pulse-danger"></span>';
    }

    var since = _jsHelpers._getSinceComplete(createdAt);

    var lastConnectionString = _jsHelpers._getLastConnection(
      lastConnection,
      lastConnectionForHuman
    );

    var tpl = `
            <div id="card-${nodeId}" class="col-xl-3 col-lg-3 col-md-4 col-sm-3 pb-1 tpl-card"
                data-token="${token}"
                data-device-id="${deviceId}"
                data-serial="${serial}"
                data-model="${model}"
                data-version="${version}"
                data-status="${status}"
                data-conexion="${conexion}"
                data-details = ""
                data-created-at = "${createdAt}"
                data-company = "${JSON.stringify(company)}"
                data-branch = "${JSON.stringify(branch)}"
                data-last-connection = "${lastConnection}"

            >
                <div class="custom-card" style="min-height: 130px">
                    <div class="options flex-end">
<!--                        <div class="btn-update-now">-->
<!--                            Update Now-->
<!--                        </div>-->
                        <i class="bx bx-dots-vertical-rounded openOptions"></i>
                    </div>
                    <div data-id-node="${nodeId}"  class="openModalDetail">
                        <div class="profile mb-1">
                           <div class="data">
                                <p class="-info m-0 w-600 -device-id">${deviceId}
                                    <span class="container-ball">
                                     ${ballStatus}
                                    </span>
                                </p>
                                <p class="-info m-0 -version" style="color: var(--fontColor); font-size: 12px">Version ${version}</p>
                                <p class="-info m-0 -num-cams" style="color: var(--fontColor); font-size: 12px">0 cameras connected</p>
                                <p class="-info m-0 -ip" style="color: var(--fontColor); font-size: 12px">${ip}</p>
                                ${lastConnectionString}
                            </div>
                        </div>
                         <div class="more-data">
                            <p class="m-0 -company-branch" style="color: var(--fontColor);">${
                              company.name
                            } - ${branch.name}</p>

                            <p class="m-0 date-active" style="color: var(--fontColor); font-size: 9px">${since}</p>

                        </div>
                    </div>
                    <div class="more-options" style="display: none">
                        <div class="title">
                            <span>Options</span>
                            <i class="bx bx-x closeOptions"></i>
                        </div>
                        <p data-id-node="${nodeId}" class="text-success updateBranch">Change branch</p>
                        <p data-id-node="${nodeId}" class="text-success updateSoftwareVersion">Update software version</p>
<!--                        <p data-id-node="${nodeId}" class="text-success downloadInfo" >Download node info</p>-->
                        <p data-id-node="${nodeId}" class="text-danger deleteNode">Delete node</p>
                    </div>
                </div>
            </div>
           
       `;

    return tpl;
  };

  return {
    init: function () {
      init();
    },
  };
})();

NodesAndCameras.init();
