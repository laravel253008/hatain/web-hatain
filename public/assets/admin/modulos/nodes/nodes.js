var _PREFIX_ADMIN_API = _jsHelpers._PREFIX_ADMIN_API;
var ALL_DATA = [];
var ALL_COMPANIES = [];
var DATA_GRID;

$(".btnradio").change(function (e) {
    $(".tableData").addClass("d-none");
    $(`#tableData${e.target.value}`).removeClass("d-none");
})

async function getDevicesData() {
    await $.ajax({
        type: "GET",
        url: _PREFIX_ADMIN_API + `/admin/company_data`,
        headers: _jsHelpers._getHeaders(),
    }).done((res) => {
        if (!res.error) {
            ALL_DATA = res.data;
            renderData();
        } else {
            _jsHelpers._handle_notify('danger', 'Wrong Data Input')
        }
        _jsHelpers._unBlockUi();
    }).fail((error) => {
        _jsHelpers._unBlockUi();
        _jsHelpers._handle_notify('danger', 'Server Error : Can\'t Fetch Data');
    });
}

async function renderData() {

    var dataCounters = ALL_DATA.counters.map((e, i) => {
        return {
            ID: i + 1,
            company: e.s_company.name,
            branch_code: e.branch_code,
            branch_id: e.branch_id,
            branch_name: e.s_branch?.name,
            status: e.status,
            timezone: e.timezone,
            created_at: e.created_at,
            updated_at: e.updated_at,
            branch_site_code: e.s_branch?.site_code,
        }
    });
    await renderTable(dataCounters, "Counters");
    var dataNodes = ALL_DATA.devices.map((e, i) => {
        return {
            ID: i + 1,
            company: e.s_company?.name,
            branch_name: e.s_branch?.name,
            serial: e.serial,
            model: e.model,
            branch_site_code: e.s_branch?.site_code,
            branch_id: e.branch_id,
            device_id: e.device_id,
        }
    });
    await renderTable(dataNodes, "Nodes");
    var dataCameras = ALL_DATA.cameras.map((e, i) => {
        return {
            ID: i + 1,
            company: e.s_company.name,
            camera_id: e.camera_id,
            name: e.name,
            camera_type: e.camera_type,
            string_connection: e.string_connection,
            plugged_in: e.plugged_in == 1 ? "YES" : "NO",
            node_device_id: e.node_id,
            node_active: e.status == 1 ? "YES" : "NO",
            branch_name: e.s_branch.name,
            branch_site_code: e.s_branch.site_code,
        }
    });
    await renderTable(dataCameras, "Cameras");
}

function renderTable(data, type) {

    $(`#tableData${type}`).dxDataGrid({
        dataSource: data || [],
        keyExpr: 'ID',
        columnsAutoWidth: true,
        allowColumnResizing: true,
        allowColumnReordering: true,
        filterRow: { visible: true },
        grouping: {
            autoExpandAll: false,
        },
        groupPanel: {
            visible: true,
        },
        headerFilter: { visible: true },
        filterBuilderPopup: {
            position: {
                of: window,
                at: 'top',
                my: 'top',
                offset: { y: 10 },
            },
        },
        showRowLines: true,
        showBorders: true,
        columnChooser: {
            enabled: true,
            mode: "select" // "dragAndDrop" or "select"
        },
        customizeColumns(columns) {
            columns[0].width = 70;
            columns[0].caption = '#';
            columns.forEach((c, i) => {
                if (i > 5) {
                    c.visible = false;
                    c.allowHiding = true;
                }
            });
        },
        paging: {
            pageSize: 10,
        },
        pager: {
            visible: true,
            allowedPageSizes: [10, 'all'],
            showPageSizeSelector: true,
            showInfo: true,
            showNavigationButtons: true,
        },
        sortByGroupSummaryInfo: [{
            summaryItem: 'count',
        }],
        summary: {
            totalItems: [],
        },
        export: {
            enabled: true,
        },
        onExporting(e) {
            const workbook = new ExcelJS.Workbook();
            const worksheet = workbook.addWorksheet('Linkers Analytics Center');

            // DevExpress.excelExporter.exportDataGrid({
            //     component: e.component,
            //     worksheet,
            //     autoFilterEnabled: true,
            // }).then(() => {
            //     workbook.xlsx.writeBuffer().then((buffer) => {
            //         saveAs(new Blob([buffer], { type: 'application/octet-stream' }), $(".btnradio:checked").val() + '.xlsx');
            //     });
            // });
            e.cancel = true;
        },
    });


};
getDevicesData();
