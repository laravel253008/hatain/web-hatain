

const handleDeleteMap = function (id, branch_id) {
    _jsHelpers._blockUI();
    $.ajax({
        type: "DELETE",
        url: _jsHelpers._PREFIX_ADMIN_API + "/heatmap/" + id,
        headers: _jsHelpers._getHeaders(),
    })
        .done(function (response) {
            if (!response.error) {
                $(`#map-${id}`).remove();
                location.reload();
            } else {
                _jsHelpers._responseError(response);
            }
            _jsHelpers._unBlockUi();
        });
}
$(document).on("click", ".openOptions", function () {
    $(this).parents(".card-user").find(".more-options").show();
  });

  $(document).on("click", ".closeOptions", function () {
    $(this).parents(".more-options").hide();
  });


document.getElementsByClassName("search")[0].addEventListener("keyup", function (e) {
    document.getElementById("heatMapsContainer").innerHTML = "";
    var str = ""
    MY_DATA.filter((item) => item.branches.name.toLowerCase().includes(e.target.value.toLowerCase())).forEach(item => {
        str = str + `
    <div id="map-${item.id}" class="card" style="max-width: 200px;padding: 10px; margin: 10px;">
                <div id="controlContainer" class="row justify-content-end">
                    <i class="bi bi-x heatmapCardDelete"
                        onclick="handleDeleteMap(${item.id},${item.branch_id})"
                        style="color: #ff8686;font-size: 18px;"></i>
                </div>
                <a href="/heatmaps/details/${item.id}">
                    <img class="card-img-top" height=150 width=180 src="${item.map_url}"
                        alt="Card image cap"
                        style="cursor: pointer;">
                </a>
                <div class="card-body">
                    <h5 class="card-title">${item.branches.name}</h5>
                    <p class="card-text"></p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
    `
    });
    document.getElementById("heatMapsContainer").innerHTML = str;
});