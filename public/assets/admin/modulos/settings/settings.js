var Settings = (function () {
  /**** CONSTANTES GLOBALES****/
  var _PREFIX_ADMIN_API = _jsHelpers._PREFIX_ADMIN_API;
  var CSV;
  var _SALES_DATA = {
    DateFormat: "Date",
    fields: [],
    Data: null,
  };
  const modalAdd = $("#modal-add-notification");
  const modalDelete = $("#modal-delete-notification");
  const modalEdit = $("#modal-update-notification");
  const modalUpdateStatus = $("#modal-update-status");
  let selected_branches_ids = [];
  var nonUsersEmails = []
  var COMPANY_USERS = [];
  var selected_branches = [];
  var selected_groubs = [];
  var select_branch_add = $(".select2-branch");
  var select_branch_update = $(".select2-branch1");

  $(document).on("click", ".btn-add-notification", function () {
    selected_branches = [];
    $('.select2-users').val(null).trigger('change');
    modalAdd.modal();
    nonUsersEmails = [];
    $('#nonUsersEmailsContainer').empty();

    $('#nonUserEmailInput').on('keyup', (e) => {
      $('#nonUserEmailInput').attr('style', 'border: none');
    });

    $('#btn-addNonUser').on('click', e => { handleNonUserEmails(e) })

    $('#nunUserEmailSearch').on('keyup', e => { searchNonUsers(e) });
  });

  var handleLogicModule = async function () {
    if ($(".container-cards .tpl-card").length == 0) {
      $(".container-no-data").show();
    }
    $(document).on("click", ".closeOptions", function () {
      $(this).parents(".more-options").hide();
    });

    $(document).on("click", ".btn-delnonUsrEmail", function () {
      var index = nonUsersEmails.indexOf($(this).data("name"));
      if (index !== -1) {
        nonUsersEmails.splice(index, 1);
      }
      $(this).parent().remove();
    });

    $(document).on("click", ".openOptions", function () {
      $(this).parents(".card-user").find(".more-options").show();
    });

    $(document).on("click", ".btnDeleteNotification", function (e) {
      e.preventDefault();
      let id = $("#form-delete .id").val();
      deleteCard(id);
    });

    $(document).on("click", ".btn-save-notification", function (e) {
      e.preventDefault();
      add();
    });

    $(document).on("click", ".btn-edit-status", function (e) {
      e.preventDefault();
      let id = $("#form-update-status #id").val();

      var $parent = $("#card-" + id);
      var name = $parent.data("name");
      var status = $parent.data("status");
      var users = $parent.data("users");
      var branches = $parent.data("branches");

      if (status == 0) {
        status = 1;
      } else {
        status = 0;
      }

      updateStatus(id, name, status, users, branches);
    });

    $(document).on("click", ".btn-edit-notification", function (e) {
      e.preventDefault();
      let id = $("#form-update-notification .branch-id").val();
      updateNotify(id);
    });

    _jsHelpers._blockUI();
    await $.ajax({
      type: "POST",
      url: _PREFIX_ADMIN_API + "/catalog/branchesWithGroups/",
      headers: _jsHelpers._getHeaders(),
    }).done((res) => {
      if (!res.error) {
        BRANCHES = res.data.branches;
        GROUPS = res.data.groups;
      }
    });
    const company_id = $("#in-data").data("company-id");
    await $.ajax({
      type: "POST",
      url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/usersByCompany/",
      headers: _jsHelpers._getHeaders(),
      data: {
        company_id: company_id,
      }
    }).done((res) => {
      if (!res.error) {
        COMPANY_USERS = res.data;
        COMPANY_USERS.forEach(e => {
          $('.select2-users').append(`
            <option value="${e.id}">${e.email}</option>
          `)
        })
      }
      _jsHelpers._unBlockUi();
    });

    initModalEdit();
    initModalDelete();
    initSelectUsers();
    initMatchRows();

    ///////////start branch modal //////

    if (selected_branches_ids == 0) {
      select_branch_add.empty();
      select_branch_add.append(`<option>${_jsHelpers._trans("selectBranch")}</option>`);
    }

    /**
     * this function initialize the branches modal and execute a call back function when the user click the submit button of the modal
     * the callback function returns the selected branches in the model
     */

    $(document).on("click", ".select2-branch", function () {
      _jsBranchesModalHelper._showMoreBranshesModal__A((data) => {
        selected_branches = data;
        renderSelectedBranches(selected_branches)
      }, selected_branches, selected_groubs);
    });

    $(document).on("click", ".select2-branch1", function () {
      _jsBranchesModalHelper._showMoreBranshesModal__A((data) => {
        selected_branches = data;
        renderSelectedBranchesUpdate(selected_branches)
      }, selected_branches, selected_groubs);
    });
    ///// end branch modal ///////
    $(".select-date-format").select2({
      dropdownAutoWidth: true,
      width: "100%",
    });
    $(".sales-file-match").select2({
      dropdownAutoWidth: true,
      width: "100%",
    });
    $("#browse").on("click", function () {
      $("#inputFile").click();
    });
    $("#downloadTemplate").on("click", function () {
      downloadCustomTemplate();
    });
    $("#inputFile").on("change", function (e) {
      if ($("#inputFile").val()) {
        $("#fileName").text(
          $("#inputFile")
            .val()
            .match(/[\/\\]([\w\d\s\.\-\(\)]+)$/)[1]
        );

        var selectedFile = e.target.files[0];
        let fileReader = new FileReader();
        fileReader.readAsBinaryString(selectedFile);

        fileReader.onload = (event) => {
          CSV = event.target.result
          convertCSVtoJSON(event.target.result);
        };
        fileReader.onerror = function (ex) {
          _jsHelpers._handle_notify("danger", "Couldn't read the file");
        };
      } else {
        $("#fileName").text("No file chosen, yet");
      }
    });

    $(document).on("keyup", ".search", function (e) {
      var filtered_notify = []
      NOTIFY.forEach(notify => {
        if (notify.name.toLowerCase().includes(e.target.value.toLowerCase()) ) {
          filtered_notify.push(notify);
        }
      });
      renderNotifyCards(filtered_notify);
    })
    initModalEditStatus();
    handleDateField();
    importSalesFile();
  };

  const renderNotifyCards = function (notify) {
    $(".container-cards").empty();
    if (notify.length == 0) {
      $(".container-cards").append(`
      <div class="container-no-data" style="">
          <p class="title">${_jsHelpers._trans('noOneHere')}</p>
          <p>${_jsHelpers._trans('noOneHereMsg')}</p>
          <button class="btn-dmg text-green" data-toggle="modal" data-target="#add-customer" class="">
          ${_jsHelpers._trans('addNotification')}
          </button>
      </div>
      `)
    }
    notify.forEach(data => {
      $(".container-cards").append(`
      <div id="card-${data.id}"  class="col-xl-3 col-lg-3 col-md-4 col-sm-3 pb-1 tpl-card">
      <div class="card-user" style="min-height: 175px">
          <div class="options flex-end">
              <i class="bx bx-dots-vertical-rounded openOptions"></i>
          </div>
          <div class="openModalDetail">
              <div class="profile mb-1">
                  <div class="data">
    
                      <p class="-info m-0 w-600" style="color: var(--fontColor); font-size: 12px">${_jsHelpers._trans('notificationName')}  ${data.name}</p>
                      <p class="-info m-0 w-600" style="color: var(--fontColor); font-size: 10px">${_jsHelpers._trans('createdDate')}${data.created_at}</p>
                      <p class="m-0 date-active" style="color: var(--fontColor); font-size: 10px">${_jsHelpers._trans('updatedAt')} ${data.updated_at}</p>
                      <span class="m-0 date-active" style="color: var(--fontColor); font-size: 12px">${_jsHelpers._trans('status')} </span>   
                  
                      <span class="container-ball">
                     <span class="-ball pulse-bg-success pulse-success"></span>  ${_jsHelpers._trans('active')}

                  </div>
              </div>
 
          </div>
          <div class="more-options" style="display: none">
          <div class="title">
              <span>${_jsHelpers._trans('options')}</span>
              <i class="bx bx-x closeOptions"></i>
          </div>
          <p  data-id="${data.id}" class="text-success editNotification">${_jsHelpers._trans('update')} </p>
          <p  data-id="${data.id}" class="text-danger deleteNotification">${_jsHelpers._trans('delete')}</p>




          <div class="item-options">
          <div class="custom-control custom-switch custom-control-inline mb-1">
              <p>${_jsHelpers._trans('status')}</p>
              <input type="checkbox" class="custom-control-input" id="vs-analysis-of">
              <label class="custom-control-label mr-1" for="vs-analysis-of">
              </label>
              <p>${_jsHelpers._trans('active')}</p>
          </div>

      </div>
      </div>
      </div>
      `);
    })
  }
  const deleteCard = function (id) {
    _jsHelpers._blockUI();
    $.ajax({
      type: "DELETE",
      url: _PREFIX_ADMIN_API + "/notify/" + id,
      headers: _jsHelpers._getHeaders(),
    })
      .done((res) => {
        if (!res.error) {
          $("#card-" + id).remove();
          updateCantCards();
          if ($(".container-cards .tpl-card").length == 0) {
            $(".container-no-data").show();
          }
          _jsHelpers._handle_notify("success", res.message);
        } else {
          _jsHelpers._responseError(res);
        }
        modalDelete.modal("hide");
        _jsHelpers._unBlockUi();
      })
      .fail((error) => {
        _jsHelpers._handle_notify(
          "danger",
          "Oops! There were found a problem."
        );
        _jsHelpers._unBlockUi();
      });
  };

  /////////// start branch select /////////

  /**
   * this function take the selected branches and analize it in the chart
   * @param {array} branches
   */
  const renderSelectedBranches = (branches) => {
    select_branch_add.empty();

    /** to render the selected branches names in the list_branch class */
    branches.map((branch) => {
      select_branch_add.append(`
        <option id="${branch.id}" class="item-branch1 ckeckboxes "selected"
        " data-id="${branch.id}" data-name="">
        <p>${branch.name}</p>
        </option>
        `);
    });

    if (branches.length == 0) {
      select_branch_add.empty();
      select_branch_add.append(`<option>${_jsHelpers._trans("selectBranch")}</option>`);
      $("#select2-branch").height("33px");
    } else {
      $("#select2-branch").height("100px");
    }
  };

  const renderSelectedBranchesUpdate = (branches) => {
    select_branch_update.empty();
    /** to render the selected branches names in the list_branch class */
    branches.map((branch) => {
      select_branch_update.append(`
        <option id="${branch.id}" class="item-branch1 ckeckboxes "selected"
        " data-id="${branch.id}" data-name="">
        <p>${branch.name}</p>
        </option>
        `);

      // }
    });

    if (branches == 0) {
      select_branch_add.empty();
      select_branch_add.append(`<option>${_jsHelpers._trans("selectBranch")}</option>`);
      $("#select2-branch1").height("33px");
    } else {
      $("#select2-branch1").height("100px");
    }
  };

  ///////// end select branch ///////

  const updateStatus = function (id, name, status, users, branches) {
    var usersId = [];
    var branchesId = [];

    users.map((el, i) => {
      usersId.push(el.user_id);
    });

    branches.map((el, i) => {
      branchesId.push(el.id);
    });

    const parent = "#card-" + id + " ";
    const $parent = $("#card-" + id);

    _jsHelpers._blockUI();
    $.ajax({
      url: _PREFIX_ADMIN_API + "/notify",
      type: "PUT",
      headers: _jsHelpers._getHeaders(),
      data: {
        id: id,
        name: name,
        branch_ids: branchesId,
        users: usersId,
        status: status,
      },
    })
      .done(function (response) {
        if (!response.error) {
          $(parent + ".closeOptions").click();
          $parent.data("status", status);
          location.reload();

          _jsHelpers._handle_notify("success", response.message);
          modalUpdateStatus.modal("hide");
        } else {
          _jsHelpers._responseError(response);
        }
        _jsHelpers._unBlockUi();
      })
      .fail(function (err) {
        _jsHelpers._handle_notify(
          "danger",
          "Oops! There were found a problem."
        );
        _jsHelpers._unBlockUi();
      });
  };

  const updateNotify = function (id) {
    const formParent_ = "#form-update-notification ";
    let selectExternalEmail = $(formParent_ + "#non-user-Email-Update").val();

    const branchId = $(formParent_ + ".branch-id").val();
    const parent = "#card-" + branchId + " ";
    var $parent = $("#card-" + branchId);

    let name = $("#form-update-notification #branch-id1").val();
    let users = $("#form-update-notification #select2-users1").val();
    let status = $("#form-update-notification .status").val();
    selected_branches_ids = selected_branches.map(e => e.id);
    _jsHelpers._blockUI();
    $.ajax({
      url: _PREFIX_ADMIN_API + "/notify",
      type: "PUT",
      headers: _jsHelpers._getHeaders(),
      data: {
        id: id,
        name: name,
        branch_ids: selected_branches_ids,
        users: users,
        status: status,
        non_user_email: nonUsersEmails,
      },
    })
      .done(function (response) {
        if (!response.error) {
          $(parent + ".closeOptions").click();

          $(parent + "#notifyName").text(name);
          $parent.data("name", name);
          $parent.data("status", status);

          _jsHelpers._handle_notify("success", response.message);
          modalEdit.modal("hide");

          location.reload();
        } else {
          _jsHelpers._responseError(response);
        }
        _jsHelpers._unBlockUi();
      })
      .fail(function (err) {
        _jsHelpers._handle_notify(
          "danger",
          "Oops! There were found a problem."
        );
        _jsHelpers._unBlockUi();
      });
  };

  const add = function () {
    let name = $("#form-add-notification #branch-id").val();
    let usersWithAccess = $("#form-add-notification #select2-users").val();
    let status = $("#form-add-notification .status").val();
    selected_branches_ids = selected_branches.map(e => e.id);
    _jsHelpers._blockUI();
    $.ajax({
      url: _PREFIX_ADMIN_API + "/notify",
      type: "POST",
      headers: _jsHelpers._getHeaders(),
      data: {
        name: name,
        branch_ids: selected_branches_ids,
        users: usersWithAccess,
        status: status,
        non_user_email: nonUsersEmails,

      },
    })
      .done(function (response) {
        nonUsersEmails = [];
        if (!response.error) {
          _jsHelpers._handle_notify("success", response.message);
          modalAdd.modal("hide");
          getTpl(response, false);
          location.reload();
        } else {
          _jsHelpers._responseError(response);
        }
        _jsHelpers._unBlockUi();
      })
      .fail(function (err) {
        _jsHelpers._handle_notify(
          "danger",
          "Oops! There were found a problem."
        );
        _jsHelpers._unBlockUi();
      });
  };

  const initSelectUsers = function () {

    function formatResult(data) {
      if (!data.id) {
        return data.text;
      }
      return $(`
              <div class="wrapper-users">
                <div class="">${data.email}</div>
              </div>
            `);
    }

    $(".select2-users").select2({
      dropdownAutoWidth: true,
      multiple: true,
      width: '100%',
      height: '30px',
      placeholder: `${_jsHelpers._trans("selectUsers")}`,
      allowClear: true,
      data: COMPANY_USERS,
      cache: true,
      closeOnSelect: false,
      templateResult: formatResult,
      templateSelection: formatResult,
    });
    // $('.select2-search__field').css('width', '100%');
  };

  const initModalEditStatus = function () {
    $(".activeRevoke").click(function () {
      let _this = $(this);
      let id = _this.data("id");
      $("#form-update-status #id").val(id);
      modalUpdateStatus.modal();
    });
  };

  const initModalDelete = function () {
    $(document).on("click", ".deleteNotification", function () {
      let _this = $(this);
      let id = _this.data("id");
      $("#form-delete .id").val(id);
      modalDelete.modal();
    });
  };

  const initModalEdit = function () {
    $(document).on("click", ".editNotification", function () {
      var branchId = $(this).data("id");
      var $parent = $("#card-" + branchId);
      branches = [];
      const parentForm_ = "#form-update-notification ";
      var name = $parent.data("name");
      var users = $parent.data("users");
      var branches = $parent.data("branches");
      var status = $parent.data("status");
      var non_users = [];
      if ($parent.data("non_users").length > 0) {
        $parent.data("non_users").forEach(item => {
          if (item.email) {
            non_users.push(item.email);
          }
        })
      }

      $(parentForm_ + "#branch-id1").val(name);
      $(parentForm_ + ".status").val(status);
      $(parentForm_ + "#select2-users1").val(users);

      $("#select2-users1").empty();

      initSelectUsersEdit(users);
      selected_branches = branches.map(item => {
        item.group_id = 0;
        return item;
      });
      initSelectBranchEdit(branches);

      $(parentForm_ + ".branch-id").val(branchId);

      modalEdit.modal();
      nonUsersEmails = non_users
      $('#nonUsersEmailsContainer-Edit').empty();
      // //////////////////////////////////////
      nonUsersEmails.forEach(email => {
        $('#nonUsersEmailsContainer-Edit').append(`
            <div class="nonUserElement">
                <p>${email}</p>
                <i class="bi bi-x btn-delnonUsrEmail" data-name="${email}"></i>
            </div>
            `);
      });

      $('#nonUserEmailInput-Edit').on('keyup', (e) => {
        $('#nonUserEmailInput-Edit').attr('style', 'border: none');
      });

      $('#btn-addNonUser-Edit').on('click', (e) => { handleNonUserEmails(e, true) });

      $('#nunUserEmailSearch-Edit').on('keyup', e => { searchNonUsers(e, true) });
    });
  };

  const initSelectBranchEdit = function (branches) {
    select_branch_update.empty();

    branches.map((branch, index) => {
      var exist = document.getElementById(branch.id);
      if (!exist || exist == undefined || exist == null) {
        select_branch_update.append(`
         <option id="${branch.id}" class="item-branch1 ckeckboxes "selected"
         " data-id="${branch.id}" data-name="">
         <p>${branch.name}</p>
         </option>
         `);
      }
    });
  };

  const initSelectUsersEdit = function (users) {

    var data = [];
    if (!Array.isArray(users)) {
      users = JSON.parse(users);
    }
    let usersIds = users.map(e => e.user_id);
    COMPANY_USERS.forEach(e => {
      if (usersIds.includes(e.id)) {
        $('#select2-users1').append(`<option value="${e.id}" selected>${e.email}</option>`)
      }
      $('#select2-users1').append(`<option value="${e.id}">${e.email}</option>`)
    })

    if (users.length > 0) {
      for (let user of users) {
        var usrTmp = {
          id: user.user_id,
          email: user.email,
          name: user.name + " " + user.lastname,
          photo: user.photo,
          selected: true,
        };
        data.push(usrTmp);
      }
    }

    const company_id = $(".more-data #id-company").val();

    $("#select2-users1").select2({
      placeholder: `${_jsHelpers._trans("selectUsers")}`,
      dropdownAutoWidth: true,
      width: "100%",
      closeOnSelect: false,
    });
  };

  var initMatchRows = () => {
    $(".sales-file-match").on("change", function () {
      HandleDropdowns($(this));
    });
  };

  var handleDateField = () => {
    dateFormat = {
      Slash: [
        "dd/MM/YY",
        "dd/MM/YYYY",
        "dd/MMM/YY",
        "dd/MMM/YYYY",
        "YY/dd/MM",
        "YYYY/dd/MM",
        "YY/dd/MMM",
        "YYYY/dd/MMM",
        "YY/MM/dd",
        "YYYY/MM/dd",
        "YY/MMM/dd",
        "YYYY/MMM/dd",
      ],
      Hyphen: [
        "dd-MM-YY",
        "dd-MM-YYYY",
        "dd-MMM-YY",
        "dd-MMM-YYYY",
        "YY-dd-MM",
        "YYYY-dd-MM",
        "YY-dd-MMM",
        "YYYY-dd-MMM",
        "YY-MM-dd",
        "YYYY-MM-dd",
        "YY-MMM-dd",
        "YYYY-MMM-dd",
      ],
      Dot: [
        "dd.MM.YY",
        "dd.MM.YYYY",
        "dd.MMM.YY",
        "dd.MMM.YYYY",
        "YY.dd.MM",
        "YYYY.dd.MM",
        "YY.dd.MMM",
        "YYYY.dd.MMM",
        "YY.MM.dd",
        "YYYY.MM.dd",
        "YY.MMM.dd",
        "YYYY.MMM.dd",
      ],
      Space: [
        "dd MM YY",
        "dd MM YYYY",
        "dd MMM YY",
        "dd MMM YYYY",
        "YY dd MM",
        "YYYY dd MM",
        "YY dd MMM",
        "YYYY dd MMM",
        "YY MM dd",
        "YYYY MM dd",
        "YY MMM dd",
        "YYYY MMM dd",
      ]
    };
    firstDayFormat = {
      Slash: [
        "01/MM/YY",
        "01/MM/YYYY",
        "01/MMM/YY",
        "01/MMM/YYYY",
        "YY/01/MM",
        "YYYY/01/MM",
        "YY/01/MMM",
        "YYYY/01/MMM",
        "YY/MM/01",
        "YYYY/MM/01",
        "YY/MMM/01",
        "YYYY/MMM/01",
      ],
      Hyphen: [
        "01-MM-YY",
        "01-MM-YYYYY",
        "01-MMM-YY",
        "01-MMM-YYYY",
        "YY-01-MM",
        "YYYY-01-MM",
        "YY-01-MMM",
        "YYYY-01-MMM",
        "YY-MM-01",
        "YYYY-MM-01",
        "YY-MMM-01",
        "YYYY-MMM-01",
      ],
      Dot: [
        "01.MM.YY",
        "01.MM.YYYYY",
        "01.MMM.YY",
        "01.MMM.YYYY",
        "YY.01.MM",
        "YYYY.01.MM",
        "YY.01.MMM",
        "YYYY.01.MMM",
        "YY.MM.01",
        "YYYY.MM.01",
        "YY.MMM.01",
        "YYYY.MMM.01",
      ],
      Space: [
        "01 MM YY",
        "01 MM YYYYY",
        "01 MMM YY",
        "01 MMM YYYY",
        "YY 01 MM",
        "YYYY 01 MM",
        "YY 01 MMM",
        "YYYY 01 MMM",
        "YY MM 01",
        "YYYY MM 01",
        "YY MMM 01",
        "YYYY MMM 01",
      ]
    };

    $(".select-date-format").select2("val", "");
    $(".select-date-format").append(
      "<option id='default-date-option' value='DateTime' selected>Date / Time</option>"
    );

    for (const index in dateFormat) {
      $(".select-date-format").append(
        "<optgroup id='" + index + "' label='" + index + "'></optgroup>"
      );
      dateFormat[index].forEach((element, i) => {
        $("#" + index).append(
          "<option value=" +
          element +
          " >" +
          moment().format(firstDayFormat[index][i]) +
          " (" +
          element +
          ")</option>"
        );
      });
    }

    $(".select-date-format").on("change", function () {
      _SALES_DATA.DateFormat = $(this).val();
    });

    $("body").on('click', '.select2-results__group', function () {
      $(this).siblings().toggle();
    })
  };

  var importSalesFile = () => {

    $("#import").on("click", function () {
      _jsHelpers._blockUI();
      if (!$("#inputFile").val()) {
        handleImportError("No file selected");
        _jsHelpers._unBlockUi();
        return;
      }
      if (
        $("#dateFormat").val() == "Date" ||
        $("#dateFormat").val() == "DateTime"
      ) {
        handleImportError("Please select date format");
        _jsHelpers._unBlockUi();
        return;
      }
      var selectedOptions = [];
      var dublicatedRow = false;
      _SALES_DATA.fields = [];
      $.each($(".sales-file-match"), function () {
        const selectedOption = $(this).find(":selected").val();
        const exist = selectedOptions.find((e) => e == selectedOption);
        if (exist) {
          dublicatedRow = true;
        }
        selectedOptions.push(selectedOption);
        _SALES_DATA.fields.push($(this).find(":selected").text());
      });
      var testArr = [];
      let finalData = [];
      let newKeys = ['StoreCode', 'DateTime', 'TotalTransactionQty', 'TotalTransactionValue'];
      _SALES_DATA.Data.forEach((item, idx) => {
        // map keys
        let obj = {}
        _SALES_DATA.fields.forEach(field => {
          obj[field] = item[field]
        });
        // rename keys
        let newObj = {}
        newKeys.forEach((key, idx) => {
          let oldKey = Object.keys(obj)[idx]
          newObj[key] = obj[oldKey];
        });
        testArr.push(newObj)
      });
      _SALES_DATA.Data = testArr;
      if (dublicatedRow) {
        handleImportError("Dublicated row");
        _SALES_DATA.fields = [];
        _jsHelpers._unBlockUi();
        return;
      }
      if(_SALES_DATA.Data.length == 0){
        _jsHelpers._handle_notify("danger", "No Data To Import");
        _jsHelpers._unBlockUi();
        return;
      }
      $.ajax({
        type: "POST",
        url: _PREFIX_ADMIN_API + "/sales/new_records",
        data: _SALES_DATA,
        headers: _jsHelpers._getHeaders(),
      })
        .done((res) => {
          if (!res.error) {
            $("#inputFile").val(null);
            $("#fileName").text("No file chosen, yet");
            handleDateField();
            $("#noFile").show();
            $("#sales-file-container").hide();
            $(".sales-file-match").empty();
            _jsHelpers._handle_notify("success", res.message);
          } else {
            _jsHelpers._handle_notify("danger", res.message);
          }
        })
        .always((res) => {
          _jsHelpers._unBlockUi();
        });
    });
  };

  const handleImportError = (message) => {
    _jsHelpers._handle_notify("danger", message);
    _jsHelpers._unBlockUi();
  };
  function HandleDropdowns(element) {
    var $element = element;
    var value = $element.val();
    $.each($(".sales-file-match").not($element), function () {
      //loop all remaining select elements
      var subValue = $(this).val();
      if (subValue === value) {
        // if value is same reset
        $(this).val("0");
      }
    });
  }

  var downloadCustomTemplate = () => {
    _jsHelpers._blockUI();
    companyName = $("#companyName").text();
    $.ajax({
      type: "POST",
      url: _PREFIX_ADMIN_API + "/catalog/branchesWithGroups/",
      headers: _jsHelpers._getHeaders(),
    }).done((res) => {
      if (!res.error && res.data.branches.length > 0) {
        dateTime = moment().subtract(1, 'hours').format("D/M/YYYY H:m:s");
        var rows = [
          [
            "StoreCode",
            "DateTime",
            "TotalTransactionQty",
            "TotalTransactionValue",
          ],
        ];

        for (var i = 0, row; (row = res.data.branches[i]); i++) {
          TotalTransactionQty = Math.floor(Math.random() * 50);
          TotalTransactionValue = Math.floor(Math.random() * 10001);

          let colomns = [
            res.data.branches[i].name,
            dateTime,
            TotalTransactionQty,
            TotalTransactionValue,
          ];
          rows.push(colomns);
        }
        csvContent = "data:text/csv;charset=utf-8,";
        /* add the column delimiter as comma(,) and each row splitted by new line character (\n) */
        rows.forEach(function (rowArray) {
          row = rowArray.join(",");
          csvContent += row + "\r\n";
        });

        /* create a hidden <a> DOM node and set its download attribute */
        var encodedUri = encodeURI(csvContent);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        var fileName = "_sales_records_template";
        link.setAttribute("download", fileName + ".csv");
        document.body.appendChild(link);
        /* download the data file named "data-report.csv" */
        link.click();
      } else {
        var link = document.createElement("a");
        link.setAttribute(
          "href",
          window.location.origin + "/download/SalesRecordsTemplate.csv"
        );
        link.setAttribute("target", "_blank");
        link.setAttribute("download", "w3logos");
        link.setAttribute("download", ".csv");
        document.body.appendChild(link);
        link.click();
      }
    })
      .always((res) => {
        _jsHelpers._unBlockUi();
      });
  };
  var convertCSVtoJSON = function (csv) {
    var lines = csv.split("\n");

    var result = [];
    var headers = lines[0].replace("\r", "").split(",");

    for (var i = 1; i < lines.length - 1; i++) {
      var obj = {};
      var currentline = lines[i].replace("\r", "").split(",");

      for (var j = 0; j < headers.length; j++) {
        obj[headers[j]] = currentline[j];
      }
      var valid = true
      Object.keys(obj).forEach(item => {
        if (item && (!obj[item] || obj[item] == "")) {
          valid = false
        }
      })
      if (valid) {
        result.push(obj);
      }
    }
    $("#noFile").hide();
    $(".sales-file-match").empty();
    $.each($(".sales-file-match"), function () {
      headers.map((e, index) => {
        if ($(this).attr("id") == index) {
          $(this).append(
            "<option value='" + index + "' selected>" + e + "</option>"
          );
        } else {
          $(this).append("<option value='" + index + "'>" + e + "</option>");
        }
      });
    });

    $("#sales-file-container").show();
    _SALES_DATA.Data = result;
  };
  const getTpl = function (res, fromFilters = false) {
    $(".container-no-data").hide();
    let tpl = ``;
    if (fromFilters) {
      $(".container-cards .row").empty();
      res.data.map((data) => {
        tpl += tplCard(data);
      });
    } else {
      tpl = tplCard(res.data);
    }
    $(".container-cards .row").append(tpl);
    updateCantCards();
  };

  const tplCard = function (data) {
    let id = data[0].id;
    let name = data[0].name;
    var createdAt = data[0].created_at;
    var updatedAt = data[0].updated_at;
    let tpl = ``;
    tpl += `
                <div id="card-${id}"  class="col-xl-3 col-lg-3 col-md-4 col-sm-3 pb-1 tpl-card">
                <div class="card-user" style="min-height: 175px">
                    <div class="options flex-end">
                        <i class="bx bx-dots-vertical-rounded openOptions"></i>
                    </div>
                    <div class="openModalDetail">
                        <div class="profile mb-1">
                            <div class="data">
                
                    

                          
                                <p class="-info m-0 w-600" style="color: var(--fontColor); font-size: 12px">Notification Name: ${name}</p>
                                <p class="-info m-0 w-600" style="color: var(--fontColor); font-size: 10px">created at:${createdAt}</p>
                                <p class="m-0 date-active" style="color: var(--fontColor); font-size: 10px">updated At: ${updatedAt}</p>
                                <span class="m-0 date-active" style="color: var(--fontColor); font-size: 12px">Status : </span>   
                            
                                <span class="container-ball">
                               <span class="-ball pulse-bg-success pulse-success"></span>  Active

                            </div>
                        </div>
           
                    </div>
                    <div class="more-options" style="display: none">
                    <div class="title">
                        <span>Options</span>
                        <i class="bx bx-x closeOptions"></i>
                    </div>
                    <p  data-id="${id}" class="text-success editNotification">Edit Notification </p>
                    <p  data-id="${id}" class="text-danger deleteNotification">Delete Notification</p>
        
          
        
        
                    <div class="item-options">
                    <div class="custom-control custom-switch custom-control-inline mb-1">
                        <p>Status</p>
                        <input type="checkbox" class="custom-control-input" id="vs-analysis-of">
                        <label class="custom-control-label mr-1" for="vs-analysis-of">
                        </label>
                        <p>Active</p>
                    </div>
        
                </div>
                </div>
                </div>
            `;
    return tpl;
  };
  const updateCantCards = function () {
    var cantCards = $(".tpl-card").length;
    cantCards =
      cantCards == 1
        ? cantCards + `${_jsHelpers._trans("notification")}`
        : cantCards +  `${_jsHelpers._trans("notifications")}`;
    $(".-cant-card").text(cantCards);
  };

  const handleNonUserEmails = (e, edit = false) => {
    e.preventDefault();
    var input = '#nonUserEmailInput';
    var container = '#nonUsersEmailsContainer';
    var search = '#nunUserEmailSearch';

    if (edit) {
      input += '-Edit';
      container += '-Edit';
      search += '-Edit';
    }

    let str = $(input).val()
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(str.trim())) {
      $(container).append(`<div class="nonUserElement">
                              <p>${str.trim()}</p>
                              <i class="bi bi-x btn-delnonUsrEmail" data-name="${str.trim()}"></i>
                          </div>`);
      nonUsersEmails.push(str.trim());
      $(input).val('')
      $(search).val('').trigger('keyup');
    } else {
      $(input).attr('style', 'border: 1px solid red !important');
    }
  }

  const searchNonUsers = (e, edit = false) => {
    e.preventDefault();
    var container = '#nonUsersEmailsContainer';
    var search = '#nunUserEmailSearch';

    if (edit) {
      container += '-Edit';
      search += '-Edit';
    }
    let str = $(search).val()
    $(container).empty();
    if (str.trim() == "") {
      nonUsersEmails.forEach(email => {
        $(container).append(`
            <div class="nonUserElement">
                <p>${email}</p>
                <i class="bi bi-x btn-delnonUsrEmail" data-name="${email}"></i>
            </div>
            `);
      });
    } else {
      nonUsersEmails.forEach(email => {
        if (email.includes(str)) {
          $(container).append(`
            <div class="nonUserElement">
                <p>${email}</p>
                <i class="bi bi-x btn-delnonUsrEmail" data-name="${email}"></i>
            </div>
            `);
        }
      });
    }
  }

  return {
    init: function () {
      handleLogicModule();
    },
  };
})();

$(document).ready(function () {
  Settings.init();
});
