const CompanyBranches = (function () {
  var _PREFIX_ADMIN_API = _jsHelpers._PREFIX_ADMIN_API;

  var modalAddToken = $("#modal-add-token");
  var modalToken = $("#modal-token");

  const modalEdit = $("#modal-update-token");
  const modalDelete = $("#modal-delete-token");
  const modalUpdateStatus = $("#modal-update-status");

  const init = function () {

    if ($(".container-cards .tpl-card").length == 0) {
            $(".container-no-data").show();
          }

    var obj = searchToObject();

    if (obj.status == "Success") {
      _jsHelpers._handle_notify(
        "success",
        "Integration: connect to Foodics successfully."
      );
    }

    $(document).on("click", ".modal-add-token", function () {
      modalAddToken.modal();
    });

    $(document).on("click", ".openOptions", function () {


      $(this).parents(".card-user").find(".more-options").show();
    });


    $(document).on("click", ".closeOptions", function () {
      $(this).parents(".more-options").hide();
    });

    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

    $(document).on("click", "#form-generate-token .btn-continue", function (e) {
      e.preventDefault();
      add();
    });

    $(document).on("click", ".btnDeleteToken", function (e) {
      e.preventDefault();
      let id = $("#form-delete .id").val();
      deleteCard(id);
    });

    $(document).on("click", ".btn-edit-token", function (e) {
      e.preventDefault();
      let id = $("#form-update-token .token-id").val();
      updateToken(id);
    });

    $(document).on("click", ".btn-edit-status", function (e) {
      e.preventDefault();
      let id = $("#form-update-status #id").val();
      var $parent = $("#card-" + id);
      var name = $parent.data("name");
      var revoked = $parent.data("revoked");

      if (revoked == 0) {
        revoked = 1;
      } else {
        revoked = 0;
      }

      updateStatus(id, name, revoked);
    });

    initModalDelete();
    initModalEdit();
    initModalEditStatus();
  };

  const updateStatus = function (id, name, revoked) {
    const parent = "#card-" + id + " ";
    const $parent = $("#card-" + id);

    if (revoked == 1) {
      _jsHelpers._blockUI();
      $.ajax({
        url: _PREFIX_ADMIN_API + "/token/revoke/" + id,
        type: "PUT",
        headers: _jsHelpers._getHeaders(),
        data: {
          name: name,
        },
      })
        .done(function (response) {
          if (!response.error) {
            $(parent + ".closeOptions").click();
            $parent.data("revoked", revoked);
            location.reload();

            _jsHelpers._handle_notify("success", response.message);
            modalUpdateStatus.modal("hide");
          } else {
            _jsHelpers._responseError(response);
          }
          _jsHelpers._unBlockUi();
        })
        .fail(function (err) {
          _jsHelpers._handle_notify(
            "danger",
            "Oops! There were found a problem."
          );
          _jsHelpers._unBlockUi();
        });
    } else {
      _jsHelpers._blockUI();
      $.ajax({
        url: _PREFIX_ADMIN_API + "/token/activate/" + id,
        type: "PUT",
        headers: _jsHelpers._getHeaders(),
        data: {
          name: name,
        },
      })
        .done(function (response) {
          if (!response.error) {
            $(parent + ".closeOptions").click();
            $parent.data("revoked", revoked);
            location.reload();

            _jsHelpers._handle_notify("success", response.message);
            modalUpdateStatus.modal("hide");
          } else {
            _jsHelpers._responseError(response);
          }
          _jsHelpers._unBlockUi();
        })
        .fail(function (err) {
          _jsHelpers._handle_notify(
            "danger",
            "Oops! There were found a problem."
          );
          _jsHelpers._unBlockUi();
        });
    }
  };
  const searchToObject = function () {
    var pairs = window.location.search.substring(1).split("&"),
      obj = {},
      pair,
      i;

    for (i in pairs) {
      if (pairs[i] === "") continue;

      pair = pairs[i].split("=");
      obj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
    }

    return obj;
  };

  const deleteCard = function (id) {
    _jsHelpers._blockUI();
    $.ajax({
      type: "DELETE",
      url: _PREFIX_ADMIN_API + "/token/" + id,
      headers: _jsHelpers._getHeaders(),
    })
      .done((res) => {
        if (!res.error) {
          $("#card-" + id).remove();
          updateCantCards();
          //   getTpl(res, false);

          if ($(".container-cards .tpl-card").length == 0) {
            $(".container-no-data").show();
          }

          _jsHelpers._handle_notify("success", res.message);
        } else {
          _jsHelpers._responseError(res);
        }
        modalDelete.modal("hide");
        _jsHelpers._unBlockUi();
      })
      .fail((error) => {
        _jsHelpers._handle_notify(
          "danger",
          "Oops! There were found a problem."
        );
        _jsHelpers._unBlockUi();
      });
  };

  const updateToken = function (id) {
    if (!validateEdit(true)) {
      return;
    }
    const formParent_ = "#form-update-token ";
    const tokenId = $(formParent_ + ".token-id").val();
    const parent = "#card-" + tokenId + " ";
    const $parent = $("#card-" + tokenId);

    let name = $("#form-update-token #token-id1").val();
    // let status = $("#form-update-notification .status").val();

    _jsHelpers._blockUI();
    $.ajax({
      url: _PREFIX_ADMIN_API + "/token/" + tokenId,
      type: "PUT",
      headers: _jsHelpers._getHeaders(),
      data: {
        id: id,
        name: name,
      },
    })
      .done(function (response) {
        if (!response.error) {
          $(parent + ".closeOptions").click();

          $(parent + "#token-id1").text(name);
          $(parent + "#tokenName").text(name);

          _jsHelpers._handle_notify("success", response.message);
          modalEdit.modal("hide");
          //   getTpl(response, false);
        } else {
          _jsHelpers._responseError(response);
        }
        _jsHelpers._unBlockUi();
      })
      .fail(function (err) {
        _jsHelpers._handle_notify(
          "danger",
          "Oops! There were found a problem."
        );
        _jsHelpers._unBlockUi();
      });
  };

  const initModalDelete = function () {
    $(document).on("click", ".deleteToken", function () {
      let _this = $(this);
      let id = _this.data("id");
      $("#form-delete .id").val(id);
      modalDelete.modal();
    });
  };

  const initModalEditStatus = function () {
    $(".activeRevoke").click(function(){
      let _this = $(this);
            let id = _this.data("id");
            $("#form-update-status #id").val(id);
            modalUpdateStatus.modal();
    })

  };

  const validateAdd = function (isSave = false) {
    let status = true;
    let elements = [];

    if (isSave) {
      elements = ["token-name"];
    } else {
      elements = ["token-name"];
    }

    for (let ele of elements) {
      let parent = $("#" + ele).parent(".controls");
      let value = $("#" + ele).val();
      if (_jsHelpers.undefined(value)) {
        status = false;
        parent.find(".help-block").text("This field required");
      } else {
        parent.find(".help-block").text("");
      }
    }

    return status;
  };

  const validateEdit = function (isSave = false) {
    let status = true;
    let elements = [];

    if (isSave) {
      elements = ["token-id1"];
    } else {
      elements = ["token-id1"];
    }

    for (let ele of elements) {
      let parent = $("#" + ele).parent(".controls");
      let value = $("#" + ele).val();
      if (_jsHelpers.undefined(value)) {
        status = false;
        parent.find(".help-block").text("This field required");
      } else {
        parent.find(".help-block").text("");
      }
    }

    return status;
  };

  const add = function () {
    if (!validateAdd(true)) {
      return;
    }

    let name = $("#form-generate-token #token-name").val();

    _jsHelpers._blockUI();
    $.ajax({
      url: _PREFIX_ADMIN_API + "/token",
      type: "POST",
      headers: _jsHelpers._getHeaders(),
      data: {
        name: name,
      },
    })
      .done(function (response) {
        if (!response.error) {
          _jsHelpers._handle_notify("success", response.message);
          modalAddToken.modal("hide");
          getTpl(response, false);
          showToken(response);
        } else {
          _jsHelpers._responseError(response);
        }
        _jsHelpers._unBlockUi();
      })
      .fail(function (err) {
        _jsHelpers._handle_notify(
          "danger",
          "Oops! There were found a problem."
        );
        _jsHelpers._unBlockUi();
      });
  };

  var showToken = function (res) {
    modalToken.modal();
    var accessToken = res.data.access_token;
    $("#token").val(accessToken);
  };



  $(document).on("click", ".btn-copy", function () {
    var codeInstallation = $(".copy-to-clipboard");
    codeInstallation.select();
    document.execCommand("copy");
  });

  const getTpl = function (res, fromFilters = false) {
    $(".container-no-data").hide();
    let tpl = ``;
    if (fromFilters) {
      $(".container-cards .row").empty();
      res.map((data) => {
        tpl += tplCard(data);
      });
    } else {
      tpl = tplCard(res);
    }
    $(".container-cards .row").append(tpl);
    updateCantCards();
  };

  const initModalEdit = function () {
  
    $(document).on("click", ".editToken", function () {
      var tokenId = $(this).data("id");
      var $parent = $("#card-" + tokenId);

      const parentForm_ = "#form-update-token ";
      var name = $parent.data("name");
      $(parentForm_ + "#token-id1").val(name);

      $(parentForm_ + ".token-id").val(tokenId);

      modalEdit.modal();
    });
  };

  


  const tplCard = function (data) {
    let id = data.data.id;
    let name = data.data.name;
    var createdAt = data.data.created_at;
    var updatedAt = data.data.updated_at;
    let tpl = ``;
    tpl += `
    <div data-id="${id}" id="card-${id}" class="col-xl-3 col-lg-3 col-md-4 col-sm-3 pb-1 tpl-card" data-company-name="{{$company['name']}}" data-name="{{$dato['name']}}" data-revoked="{{$dato['revoked']}}">
    <div class=" card-user" style="min-height: 175px">
        <div class="options">
            <i class="bx bx-dots-vertical-rounded openOptions"></i>
        </div>
        <div data-id="{{$dato['id']}}" class="openDetailModal">
            <div class="profile">

                <div class="data">
                    <p class="-info m-0 w-600 -token-name" id="tokenName">Token Name : ${name}</p>
                </div>
            </div>
            <div class="more-data">
            <span class="m-0 date-active" style="color: var(--fontColor); font-size: 12px">Status :</span>

            <span class="container-ball">
                <span class="-ball pulse-bg-success pulse-success"></span>
                <p class="m-0 date-active" style="color: var(--fontColor); font-size: 9px">Created Date : ${createdAt}</p>
                <p class="m-0 date-active" style="color: var(--fontColor); font-size: 9px">updated At : ${updatedAt}</p>
             
                 

            </div>
        </div>

        <div data-id="${id}" class="more-options" style="display: none">
            <div class="title">
                <span>Options</span>
                <i class="bx bx-x closeOptions"></i>
            </div>
            <p data-id="${id}" class="text-success editToken">Edit Token </p>
            <p data-id="${id}" class="text-danger deleteToken">Delete Token</p>

            <div class="item-options">
                <div class="custom-control custom-switch custom-control-inline mb-1">
                    <p>Status</p>
            

                    <input data-id="${id}" type="checkbox" class="custom-control-input activeRevoke" checked id="activeRevoke">

                    <label data-id="${id}" class="custom-control-label mr-1" for="activeRevoke">
                    </label>


                </div>

            </div>


        </div>
    </div>
</div>
            `;
    return tpl;
  };
  const updateCantCards = function () {
    var cantCards = $(".tpl-card").length;
    cantCards =
      cantCards == 1
        ? cantCards + " generate_token"
        : cantCards + " generate_tokens";
    $(".-cant-card").text(cantCards);
  };
  return {
    init: function () {
      init();
    },
  };
})();

$(document).ready(function () {
  CompanyBranches.init();
});
