var Integration = function () {

    /**** CONSTANTES GLOBALES****/
    var _PREFIX_ADMIN_API = _jsHelpers._PREFIX_ADMIN_API;


    var handleLogicModule = function(){
        var obj = searchToObject();
        if(obj.status == 'Failed')
        {
            _jsHelpers._handle_notify('danger', 'Integration: Failed connection to Foodics.');
        }
    }

       var searchToObject = function() {
        var pairs = window.location.search.substring(1).split("&"),
            obj = {},
            pair,
            i;

        for ( i in pairs ) {
            if ( pairs[i] === "" ) continue;

            pair = pairs[i].split("=");
            obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent( pair[1] );
        }

        return obj;
    }

    return {
        init: function () {
            handleLogicModule()
        }
    };
}();

$(document).ready(function () {
    Integration.init();
});
