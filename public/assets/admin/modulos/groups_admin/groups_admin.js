const CompanyGroups = function () {

    var modalAddGroup = $("#modal-add-group");
    var modalEditgroup = $("#modal-edit-group");
    var modalDeleteGroup = $("#modal-delete-group");
    var modalDetailsGroup = $("#modal-detail-group");
    var ALL_BRANCES = [];
    const init = async function () {


        $(document).on('click', '.openOptions', function () {
            $(this).parents('.card-user').find('.more-options').show();
        });

        $(document).on('click', '.closeOptions', function () {
            $(this).parents('.more-options').hide();
        });


        modalAddGroup.on("shown.bs.modal", function () {
            $('#form-add-group .contenido').hide()
            $('#form-add-group .container-group').show()
            $('#form-add-group .item-menu').removeClass('active')
            $("#form-add-group .item-menu[data-target='container-group']").addClass('active')

            $("#form-add-group")[0].reset();

            $('.select2_group_branch').select2({
                placeholder: `${_jsHelpers._trans('selectBranch')}`,
                dropdownParent: $("#modal-add-group"),
                dropdownAutoWidth: true,
                closeOnSelect: false,
                allowClear: true,
                width: '100%',
            });
            $('.select2-group-country').select2({
                placeholder: 'select a country',
                dropdownParent: $("#modal-add-group"),
                dropdownAutoWidth: true,
                closeOnSelect: false,
                allowClear: true,
                width: '100%',
            });
            $('.select2-group-city').select2({
                placeholder: 'select a city',
                dropdownParent: $("#modal-add-group"),
                dropdownAutoWidth: true,
                closeOnSelect: false,
                allowClear: true,
                width: '100%',
            });
            $('.select2-group-company').select2({
                placeholder: 'select a company',
                dropdownParent: $("#modal-add-group"),
                dropdownAutoWidth: true,
                closeOnSelect: false,
                allowClear: true,
                width: '100%',
            });
        });

        $("#cb-selectAllGroupBranches").on('change', (e) => {
            if (e.target.checked) {
                $('.select2_group_branch').select2('destroy').find('option').prop('selected', 'selected').end().select2()
            } else {
                $('.select2_group_branch').select2('destroy').find('option').prop('selected', false).end().select2()
            }
        });

        $("#cb-selectAllBranchesCompanies").on('change', (e) => {
            if (e.target.checked) {
                $(".select2_group_branch_edit > option").prop("selected", true);
                $(".select2_group_branch_edit").trigger("change");
            } else {
                $(".select2_group_branch_edit > option").prop("selected", false);
                $(".select2_group_branch_edit").trigger("change");
            }
        });




        initSelectBranchesGroup();
        initSelectBranchescountry();
        initSelectBranchescity();
        initSelectBranchescompany();
        initModalDetailGroup();
        initModalAddGroupForm()
        initEditGroupForm();
        initModalEditGroup();
        initModalDeleteGroup();
        initDeleteGroupForm();
    };



    const validateAddGroup = function (isSave = false) {

        let status = true;
        let elements = []

        if (isSave) {
            elements = [
                'group-name',
                'select2_group_branch',
            ]
        } else {
            elements = [
                'group-name',
                'select2_group_branch',
            ]
        }


        for (let ele of elements) {
            let parent = $("#" + ele).parent(".controls");
            let value = $("#" + ele).val();

            if (_jsHelpers.undefined(value)) {
                status = false;
                parent.find(".help-block").text("This field required")
            } else {
                parent.find(".help-block").text("")
            }
        }
        return status
    };

    const validateEditGroup = function (isSave = false) {

        let status = true;
        let elements = []

        if (isSave) {
            elements = [
                'e-group-name',
                'e-select2_group_branch',
            ]
        } else {
            elements = [
                'e-group-name',
                'e-select2_group_branch',
            ]
        }

        for (let ele of elements) {
            let parent = $("#" + ele).parent(".controls");
            let value = $("#" + ele).val();
            if (_jsHelpers.undefined(value)) {
                status = false;
                parent.find(".help-block").text("This field required")
            } else {
                parent.find(".help-block").text("")
            }

        }


        return status


    };


    const updateCantCards = function (type = "branches") {
        if (type == "branches") {
            var cantCards = $(".branch-card").length;
            cantCards = cantCards == 1 ? cantCards + ' branch' : cantCards + ' branches';
            $("#cant-branches").text(cantCards);
        } else {
            var cantCards = $(".groups-card").length;
            cantCards = cantCards == 1 ? cantCards + ' group' : cantCards + ' groups';
            $("#cant-groups").text(cantCards);
        }
    }


    const initSelectBranchesGroup = function () {

        $.ajax({
            type: 'POST',
            url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branches",
            headers: _jsHelpers._getHeaders(),
        }).done(res => {
            $('.select2_group_branch').empty()
            if (!res.error) {
                ALL_BRANCES = res.data;
                let branches_data = res.data.map(e => {
                    return {
                        id: e.id,
                        text: e.name
                    }
                });
                $('.select2_group_branch').select2({
                    data: branches_data
                })
            }
        })
    };

    const initSelectBranchescountry = function () {

        $.ajax({
            type: 'POST',
            url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branches",
            headers: _jsHelpers._getHeaders(),
        }).done(res => {

            $('.select2-group-country').empty();
            if (!res.error) {
                let country = [];
                res.data.forEach(elm => {
                    country.push(elm.country);
                });
                let countries = [...new Map(country.map((m) => [m, m])).values()];
                let country_data = countries.map((item, idx) => {
                    return {
                        id: item,
                        text: item
                    }
                })
                $('.select2-group-country').select2({
                    data: country_data
                })
                let selected_branches = [];
                let selected_branches_city = [];

                $('.select2-group-country').change(e => {
                    selected_branches = [];
                    selected_branches_city = [];

                    res.data.forEach(elm => {
                        if ($('.select2-group-country').val().includes(elm.country)) {
                            selected_branches.push(elm.id);
                            if (!selected_branches_city.includes(elm.district)) {
                                selected_branches_city.push(elm.district);
                            }
                        }
                        if ($('.select2-group-country').val().length == 0) {
                            selected_branches = []
                            selected_branches_city = [];
                        }
                    });
                    $('.select2-group-city').val(selected_branches_city).trigger('change');
                    $('.select2_group_branch').val(selected_branches).trigger('change');

   
                });


            }
        })
    };

    const initSelectBranchescity = function () {

        $.ajax({
            type: 'POST',
            url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branches",
            headers: _jsHelpers._getHeaders(),
        }).done(res => {
            $('.select2-group-city').empty();

            if (!res.error) {
                let city = [];
                res.data.forEach(elm => {
                    city.push(elm.district);
                });
                let cities = [...new Map(city.map((m) => [m, m])).values()];

                let city_data = cities.map((item, idx) => {
                    return {
                        id: item,
                        text: item
                    }
                })
                $('.select2-group-city').select2({
                    data: city_data
                })

                let selected_branches = [];
                $('.select2-group-city').change(e => {
                    selected_branches = []
                    res.data.forEach(elm => {
                        if ($('.select2-group-city').val().includes(elm.district)) {
                            selected_branches.push(elm.id);
                        }
                        if ($('.select2-group-city').val().length == 0) {
                            selected_branches = []
                        }
                    });
                    $('.select2_group_branch').val(selected_branches).trigger('change');
                });

            }
        })
    };

    const initSelectBranchescompany = function () {

        $.ajax({
            type: 'GET',
            url: _jsHelpers._PREFIX_ADMIN_API + "/users/customers",
            headers: _jsHelpers._getHeaders(),
        }).done(res => {
            $('.select2-group-company').empty();

            if (!res.error) {
                let companies_data = res.data.map(e => {
                return {
                    id: e.company_id,
                    text: `${e.company.name}`
                }
                });

                 companies_data = companies_data.filter((item) => item.id !== 40);

                $('.select2-group-company').select2({
                    data: companies_data
                })
                let selected_branches = [];
                let selected_branches_city = [];
                let selected_branches_country = [];

                $('.select2-group-company').change(e => {
                    selected_branches = [];
                    selected_branches_city = [];
                    selected_branches_country = [];
                    ALL_BRANCES.forEach((elm, idx1) => {
                        if ($('.select2-group-company').val().includes(elm.company_id.toString())) {
                            selected_branches.push(elm.id);
                            if (!selected_branches_city.includes(elm.district)) {
                                selected_branches_city.push(elm.district);
                            }
                            if (!selected_branches_country.includes(elm.country)) {
                                selected_branches_country.push(elm.country);
                            }
                        }
                        if ($('.select2-group-company').val().length == 0) {
                            selected_branches = []
                            selected_branches_city = [];
                        }

                    });

                    $('.select2-group-country').val(selected_branches_country).trigger('change');
                    $('.select2-group-city').val(selected_branches_city).trigger('change');
                    $('.select2_group_branch').val(selected_branches).trigger('change');


                });
            }
        })
    };
    const initModalDeleteGroup = function () {
        $(document).on('click', '.deleteGroup', function (e) {
            const groupId = $(this).data('id-group');
            $('#form-delete-group .group-id').val(groupId)

            modalDeleteGroup.modal('show');
        });
    };

    const initDeleteGroupForm = function () {
        $('#form-delete-group').on('submit', function (e) {
            e.preventDefault();
            var groupId = $('#form-delete-group .group-id').val();
            _jsHelpers._blockUI();
            $.ajax({
                type: 'DELETE',
                url: _jsHelpers._PREFIX_ADMIN_API + '/groups/admin/' + groupId,
                headers: _jsHelpers._getHeaders(),

            }).done(res => {
                if (!res.error) {

                    $('#card-' + groupId).remove()

                    if ($(".card-content .row .card-user").length == 0) {
                        $(".container-no-group").show()
                    }

                    updateCantCards('groups');
                    _jsHelpers._handle_notify("success", res.message);
                } else {
                    _jsHelpers._responseError(res);
                }
                modalDeleteGroup.modal('hide');

            }).always(res => {
                modalDeleteGroup.modal('hide');
                _jsHelpers._unBlockUi();
            });
        });
    };

    const initModalEditGroup = function () {
        $(document).on('click', '.editGroup', function (e) {
            var groupId = $(this).data('id-group')
            var $parent = $('#card-' + groupId);
            const parentForm_ = '#form-edit-group ';
            var name = $parent.data('name');
            var branches = $parent.data('branches');

            $(parentForm_ + '#e-group-name').val(name);
            $('.select2_group_branch_edit').empty();


            initSelectBranchesEdit(branches)

            $(parentForm_ + '.group-id').val(groupId)
            modalEditgroup.modal('show');
        });
    };
    const initSelectBranchesEdit = function (branches) {
        function formatResult(data) {

            if (!data.id) {
                return data.text;
            }
            return $(`
                <div class="wrapper-users">
                    <div class="">${data.name}</div>
                </div>
            
        `);
        }

        var data = []
        if (!Array.isArray(branches)) {
            branches = JSON.parse(branches)
        }
        if (branches.length > 0) {
            for (let branch of branches) {

                var branchTmp = {
                    id: branch.id,
                    address: branch.address,
                    created_at: branch.created_at,
                    updated_at: branch.updated_at,
                    email: branch.email,
                    name: branch.name,
                    search: branch.name,
                    phone: branch.phone,
                    type: 'public',
                    selected: true
                }
                data.push(branchTmp)
            }
        }

        $(".e-select2_group_branch").select2({
            placeholder: "Select a branches",
            dropdownAutoWidth: true,
            closeOnSelect: false,
            allowClear: true,
            width: "100%",
            data: data,
            ajax: {
                type: "POST",
                url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branches",
                headers: _jsHelpers._getHeaders(),
                dataType: "json",
                // we need to handle search from server side
                data: function (params) {
                    return {
                        search: params.term,
                        type: 'public',
                    }
                },
                processResults: function (data) {
                    return {
                        results: data.data.branches,
                    };
                },
                cache: true,
            },
            cache: true,
            templateResult: formatResult,
            templateSelection: formatResult,
        });


    };

    const initEditGroupForm = function () {
        $('#form-edit-group').on('submit', function (e) {
            e.preventDefault();

            if (!validateEditGroup(true)) {
                return;
            }


            const formParent_ = '#form-edit-group ';
            const groupId = $(formParent_ + '.group-id').val()
            const parent = '#card-' + groupId + ' ';
            const $parent = $('#card-' + groupId);

            var name = $(formParent_ + '#e-group-name').val();
            var branches = $(formParent_ + '.e-select2_group_branch').val();


                var   data = {
                    name: name,
                    branches: branches,
                }

            _jsHelpers._blockUI();
            $.ajax({
                type: 'PUT',
                url: _jsHelpers._PREFIX_ADMIN_API + '/groups/admin',
                headers: _jsHelpers._getHeaders(),
                data: data
            }).done(res => {
                if (!res.error) {

                    $(parent + '.closeOptions').click()
                    $parent.data('name', name);


                    res.data.map((el, i) => {
                        $parent.data("branches", "" + JSON.stringify(el.branches) + "");
                    });
                    $(parent + '#group-name').text(name)
                    $(parent + '#group-branches').text(branches.length + " branches")

                    _jsHelpers._handle_notify("success", res.message);
                } else {
                    _jsHelpers._responseError(res);
                }
                modalEditgroup.modal('hide');

            }).always(res => {
                modalEditgroup.modal('hide');
                _jsHelpers._unBlockUi();
            });
        });
    };

    const initModalDetailGroup = function () {
        $(document).on('click', '.openDetailModalGroup', function (e) {

            const groupId = $(this).data('id-group')
            const $parent = $('#card-' + groupId);
            const modalParent_ = '#modal-detail-group ';

            const name = $parent.data('name')
            let branches = $parent.data('branches')
            let users = $parent.data('users')

            $('#img-detail').attr('src', '/assets/admin/img/icons/icon-company.png');

            $(modalParent_ + '.-group-name-card').text(name);


            $(".lista-branches-group").empty()
            $(".lista-usuarios-group").empty()

            if (!Array.isArray(branches)) {
                branches = JSON.parse(branches)
            }

            if (branches.length > 0) {
                var stringBranch = ``;
                for (let branch of branches) {
                    let name = branch.name
                    let photoBranch = ''
                    if (branch.photo) {
                        photoBranch = branch.photo
                    } else {
                        photoBranch = "/assets/admin/img/icons/icon-company.png"
                    }
                    stringBranch += `
                 <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="${name}" class="avatar pull-up">
                <img class="media-object rounded-circle" src="${photoBranch}" alt="Avatar" height="30" width="30">
                </li>
                `;
                }
                $(".lista-branches-group").append(stringBranch)
                $('[data-toggle="tooltip"]').tooltip({
                    container: "body"
                })
            } else {
                $(".lista-branches-group").append("<p style='font-size: 12px; margin: 0px'>0 branches linked</p>")
            }

            if (!Array.isArray(users)) {
                users = JSON.parse(users)
            }

            if (users.length > 0) {
                var stringUser = ``;
                for (let user of users) {
                    let name = user.name
                    let photoUser = ''
                    if (user.photo) {
                        photoUser = user.photo
                    } else {
                        photoUser = "/assets/admin/img/misc/user_active.png"
                    }
                    stringUser += `
                 <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="${name}" class="avatar pull-up">
                <img class="media-object rounded-circle" src="${photoUser}" alt="Avatar" height="30" width="30">
                </li>
                `;
                }
                $(".lista-usuarios-group").append(stringUser)
                $('[data-toggle="tooltip"]').tooltip({
                    container: "body"
                })
            } else {
                $(".lista-usuarios-group").append("<p style='font-size: 12px; margin: 0px'>0 users linked</p>")
            }

            modalDetailsGroup.modal('show');
        });
    };

    const initModalAddGroupForm = function () {
        $('#form-add-group').on('submit', function (e) {
            e.preventDefault();

            if (!validateAddGroup(true)) {
                return;
            }

            var formParent_ = '#form-add-group ';
            var company_id = $('#form-info-company #id-company').val();
            var name = $(formParent_ + '#group-name').val();
            var branches = $(formParent_ + '.select2_group_branch').val();
            const parent = '#card-' + company_id + ' ';
            const $parent = $('#card-' + company_id);

                var   data = {
                    name: name,
                    branches: branches,
                }

            _jsHelpers._blockUI();
            $.ajax({
                type: 'POST',
                url: _jsHelpers._PREFIX_ADMIN_API + '/groups/admin',
                headers: _jsHelpers._getHeaders(),
                data: data
            }).done(res => {
                if (!res.error) {
                    $(parent + '.closeOptions').click()

                    $parent.data("branches", "" + JSON.stringify(branches) + "");

                    getNewGroup(res)
                    _jsHelpers._handle_notify("success", res.message);
                    modalAddGroup.modal('hide')
                } else {
                    _jsHelpers._responseError(res);
                }
            }).always(res => {
                modalAddGroup.modal('hide')
                _jsHelpers._unBlockUi();
            });
        });
    };

    const getNewGroup = function (res) {
        $(".container-no-group").hide()

        var data = res.data

        var groupId = data.id;
        var companyName = $("#in-company").data("company")

        var name = data.name;
        var photo = '/assets/admin/img/icons/icon-company.png';
        var branches = data.branches;
        var users = data.users;
        var footer = ''


        var tpl = `
            <div id="card-${groupId}" class="col-xl-3 col-lg-3 col-md-4 col-sm-1 pb-1"
                data-company-name="${companyName}"
                data-name="${name}"
                data-branches='${JSON.stringify(branches)}'
            >
                <div class="card-user groups-card">
                    <div class="options">
                        <i class="bx bx-dots-vertical-rounded openOptions"></i>
                    </div>
                    <div data-id-group="${groupId}"  class="openDetailModal">
                        <div class="profile mb-1">
                            <div class="container-img-branch">
                                <img src="${photo}" class="mr-1" alt="" onerror="_jsHelpers._errorImage(this)">
                            </div>
                            <div class="data">
                                <p class="-info m-0 w-600 -branch-name">${name}</p>
                            </div>
                        </div>
                        <div class="more-data">
                            <p class="m-0 -branch-email" style="color: var(--fontColor);">${branches.length + (branches.length > 1 ? " branches" : " branch")}</p>
                            ${footer}
                        </div>
                    </div>
                    <div class="more-options" style="display: none">
                        <div class="title">
                            <span>Options</span>
                            <i class="bx bx-x closeOptions"></i>
                        </div>
                        <p data-id-group="${groupId}" class="text-success editGroup" >Edit group</p>
                        <p data-id-group="${groupId}" class="text-danger deleteGroup">Delete group</p>
                    </div>
                </div>
            </div>
       `
        $("#groupsContainer").append(tpl)
        updateCantCards('groups');
    };

    return {
        init: function () {
            init()
        }
    }
}();

$(document).ready(function () {
    CompanyGroups.init();
})