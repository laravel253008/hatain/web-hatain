const DetailCamera = function () {

    const modalDeleteCamera = $("#modal-delete-camera")

    var canvasLine = document.getElementById("canvas-line");
    var ctxLine;


    const init = function () {
        controls()

        configLayoutLine()
    }

    const controls = function () {
        $(document).on("click", ".step", function () {
            let _this = $(this);
            let target = _this.data('target')

            $(".container-steps .step").removeClass("active");
            _this.addClass('active')

            $(".content-step").addClass('ds-none')
            $("." + target).removeClass('ds-none')
        });


        $(document).on("click", ".delete-cam", function () {
            modalDeleteCamera.modal()
        })

        $(document).on("click", ".delete-cam", function () {
            let cameraId = $(this).data("id")
            deleteCamera(cameraId)
        });

        $(document).on("click", ".edit-cam", function () {

        });
    }

    const configLayoutLine = function () {
        var wt = $(".container-config-line .s-left").width()
        var ht = parseInt($("#in-data").data("ht"));
        var percentage = $("#in-data").data("percentage")
        var start = percentage * ht ;
        canvasLine.width = wt;
        // canvasLine.height = ht;
        ctxLine = canvasLine.getContext("2d")

        if(!_jsHelpers.undefined(percentage)){
            drawLine(wt,ht, start)
        }
    }

    const drawLine = function (wt, ht, value) {
        ctxLine.clearRect(0, 0, wt, ht);
        ctxLine.lineWidth = 2;
        ctxLine.strokeStyle = "#A1C523";
        ctxLine.beginPath();
        ctxLine.moveTo(0, value);
        ctxLine.lineTo(wt, value);
        ctxLine.closePath();
        ctxLine.stroke();
    }

    const deleteCamera = function (cameraId) {
        _jsHelpers._blockUI()
        $.ajax({
            type: 'DELETE',
            url: _jsHelpers._PREFIX_ADMIN_API + '/cameras/' + cameraId,
            headers: _jsHelpers._getHeaders(),
        }).done(res => {
            if (!res.error) {
                _jsHelpers._handle_notify("success", res.message);
                modalDeleteCamera.modal('hide');
                location.href="/cameras"
            } else {
                _jsHelpers._unBlockUi()
                _jsHelpers._responseError(res);
            }

        }).fail(error => {
            _jsHelpers._handle_notify("danger", "Oops! There were found a problem.")
            _jsHelpers._unBlockUi()
        })
    }

    return {
        init: function () {
            init()
        }
    }
}();

DetailCamera.init();
