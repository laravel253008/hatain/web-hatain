const Cameras = function () {


    const modalDeleteCamera = $("#modal-delete-camera")
    const modalPlugOutcamera = $("#modal-plug-out-camera")



    const init = function () {
        
        $(document).on('click', '.add_zone', function () {
            location.href = "/new-zone";

        });

        $(document).on('click', '.openOptions', function () {
            $(this).parents('.card-user').find('.more-options').show();
        });

        $(document).on('click', '.closeOptions', function () {
            $(this).parents('.more-options').hide();
        });

        initModalDeleteCamera()
        initModalPlugOutCamera()

        $(document).on("click",".btnDeleteCamera",function(e){
            e.preventDefault();
            let deviceId = $("#form-delete-camera .device-id").val()
            let cameraId = $("#form-delete-camera .camera-id").val()
            deleteCamera(deviceId,cameraId)
        });

        initBranchesSelect()

        filtersControls()
    };


    const filtersControls = function () {
        $(document).on("keyup", ".filtros .search", function () {
            let _this = $(this)
            getFilter()
        });
        $(document).on("change", ".filtros .select-branch", function (e) {
            let _this = $(this)
            getFilter()
        });

    }

    const getFilter = function (){
        let branchId = $(".filtros .select-branch").val()
        let search = $(".filtros .search").val()
        branchId = _jsHelpers.undefined(branchId) ? 0 : branchId
        $.ajax({
            type: 'GET',
            url: _jsHelpers._PREFIX_ADMIN_API + '/cameras',
            headers: _jsHelpers._getHeaders(),
            data:{
                branch_id: branchId,
                search: search,
            }
        }).done(res => {
            if (!res.error) {
                getTpl(res,true)
            }else{
                dataEmpty()
            }
        })
    }

    const initBranchesSelect = function () {

        const companyId = $("#in-data").data('company-id')
        $.ajax({
            type: 'GET',
            url: _jsHelpers._PREFIX_ADMIN_API + '/branch/' + companyId,
            headers: _jsHelpers._getHeaders(),
        }).done(res => {
            $('.select-branch').empty()
            if (!res.error) {
                $('.select-branch').append(`<option value="0">${_jsHelpers._trans("selectBranch")}</option>`)
                res.data.branches.map(e => {
                    $('.select-branch').append(`<option value="${e.id}">${e.name}</option>`)
                });
            }

            $('.select-branch').select2({
                dropdownAutoWidth: true,
                width: '100%'
            });

        })
    };

    const deleteCamera = function (deviceId, cameraId) {
        _jsHelpers._blockUI()
        $.ajax({
            type: 'DELETE',
            url: _jsHelpers._PREFIX_ADMIN_API + '/cameras/' + cameraId,
            headers: _jsHelpers._getHeaders(),
        }).done(res => {
            if (!res.error) {
                $('#card-' + cameraId).remove()
                updateCantCards();
                if ($(".container-cards .tpl-card").length == 0) {
                    $(".container-no-data").show()
                }
                _jsHelpers._handle_notify("success", res.message);
            } else {
                _jsHelpers._responseError(res);
            }
            modalDeleteCamera.modal('hide');

            _jsHelpers._unBlockUi();
        }).fail(error => {
            _jsHelpers._handle_notify("danger", "Oops! There were found a problem.")
            _jsHelpers._unBlockUi();
        })
    }


    const initModalDeleteCamera = function(){

        $(document).on("click",".deleteCamera",function(){
            let _this = $(this)
            let deviceId = _this.data("device-id");
            let cameraId = _this.data("camera-id");

            $("#form-delete-camera .device-id").val(deviceId)
            $("#form-delete-camera .camera-id").val(cameraId)

            modalDeleteCamera.modal()
        })

    }

    const initModalPlugOutCamera = function(){
        $(document).on("click",".plugOutCamera",function(){

            modalPlugOutcamera.modal()
        })
    }

    var dataEmpty = function(){
        $(".container-no-data").hide()
        $(".container-cards .row").empty()
        $(".container-cards .row").append(`<p style="margin: auto;padding: 200px; font-size: 16px;">We can´t find anything like this</p>`)
    }

    const getTpl = function (res, fromFilters = false) {
        $(".container-no-data").hide()

        let tpl = ``;
        if (fromFilters) {
            $(".container-cards .row").empty()
            res.data.map(data => {
                tpl += tplCard(data)
            })
        } else {
            tpl = tplCard(res.data)
        }

        $(".container-cards .row").append(tpl)
        updateCantCards();
    }

    const tplCard = function (data) {

        var createdAt = data.created_at

        var since = _jsHelpers._getSinceComplete(createdAt)

        let tpl = ``;
        tpl += `
            <div id="card-${data.device_id + '-'}${data.camera_id}"  class="col-xl-3 col-lg-3 col-md-4 col-sm-3 pb-1 tpl-card">
                <div class="card-user" style="min-height: 175px">
                    <div class="options flex-end">
                        <i class="bx bx-dots-vertical-rounded openOptions"></i>
                    </div>
                <div data-id-camera="" class="openModalDetail">
                <div class="profile mb-1">
                <img class="img" src="/assets/admin/img/misc/img-camera-fija.png"/>
                <div class="data">
                    <p class="-info m-0 w-600" style="color: var(--fontColor); font-size: 12px">${data.camera_id}</p>
                    <p class="-info m-0" style="color: var(--fontColor); font-size: 12px">${data.ip}</p>
                    <p class="-info m-0" style="color: var(--fontColor); font-size: 12px">${data.device_id}</p>
                </div>
                </div>
                
                 <div class="more-data">
                    <p class="m-0 date-active" style="color: var(--fontColor); font-size: 12px">${since}</p>
                </div>
            </div>
            <div class="more-options" style="display: none">
                <div class="title">
                <span>Options</span>
                <i class="bx bx-x closeOptions"></i>
                </div>
                <a class="text-success detailCamera" href="/detail-camera">Detail camera</a>
            <p data-device-id="${data.device_id}" data-camera-id="${data.camera_id}" class="text-success plugOutCamera">Plug out camera</p>
            <p data-device-id="${data.device_id}" data-camera-id="${data.camera_id}" class="text-danger deleteCamera">Delete camera</p>
            </div>
            </div>
            </div>
        `;

        return tpl;
    }

    const updateCantCards = function(){
        var cantCards = $(".tpl-card").length
        cantCards = cantCards==1 ? cantCards+' camera' : cantCards+' cameras';
        $(".-cant-card").text(cantCards)
    }

    return {
        init: function () {
            init()
        }
    }
}();

Cameras.init();



