var ALL_COMPANY_BRANCHES = [];
var BRANCHES_WHIT_HEAT_MAP = [];
var BRANCHES_WITHOUT_HEAT_MAP = [];
var BRANCH_CAMERAS = [];
const mapData = {
    name:"",
    cameras: [],
    map_url: "",
};
var THIS_CAMERA = {
    camera_id: 0,
    domain: [],
};
var url_update_photo = "/heatmap/uploadHeatImage";

function restDots() {
    $('.cell-dot').css('backgroundColor', 'transparent');
    $('.cell-dot').css("boxShadow", 'none');
    $('.cell-dot').css("borderRadius", 'none');
    $('.cell-dot').removeClass("red-area");
    $('.cell-dot').data("value", 0);
    $('.cell-dot').text("");
}


async function init(params) {
    $('#dotsContainer').css('backgroundColor', 'transparent');
    $('.cell-dot').css('backgroundColor', 'transparent');
    if (ALL_COMPANY_BRANCHES.length == 0) {
        await $.ajax({
            type: "POST",
            url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branchesWithGroups/",
            headers: _jsHelpers._getHeaders(),
        })
            .done((res) => {
                if (!res.error) {
                    ALL_COMPANY_BRANCHES = res.data.branches;
                }
                _jsHelpers._unBlockUi();
            })
            .fail((error) => {
                _jsHelpers._unBlockUi();
                _jsHelpers._handle_notify(
                    "danger",
                    "Server Error : Can't Fetch Company Branches"
                );
            });
    }

    $("#branchList").empty();
    $("#branchList").append(`<option value="">${_jsHelpers._trans("selectBranch")}</option>`);
    ALL_COMPANY_BRANCHES.forEach((item) => {
        $("#branchList").append(`<option value="${item.id}">${item.name}</option>`);
    });
}

$("#branchList").on("change", function (e) {
    SELECTED_BRANCH_ID = e.target.value;
    mapData.branch_id = SELECTED_BRANCH_ID;
    getCamerasByBranchID(mapData.branch_id);
    $('.upload-img-span').removeClass("d-none");
    $('#btnUploadImg').removeClass("d-none");
    $("._controls_1").css("backgroundColor", "#a1c523");
    $('.uploadDiv').removeClass("d-none");
    $('#check-1').empty();
    $('#check-1').append(`<i class="fa fa-check" aria-hidden="true" style=" margin: 0px; float: right; padding-top: 5px;"></i> `);
});

function getCamerasByBranchID(branchId) {
    $.ajax({
        type: "GET",
        url: _jsHelpers._PREFIX_ADMIN_API + "/nodes/getDevicesByBranch",
        headers: _jsHelpers._getHeaders(),
        data: {
            "branch_id": branchId
        }
    })
        .then((res) => {
            // $("#branche_cameras").empty();
            if (!res.error) {
                BRANCH_CAMERAS = res.data[0].cameras;
                if (BRANCH_CAMERAS.length == 0) {
                    _jsHelpers._handle_notify("danger", "No Cameras For This Branch");
                }

                $(".btnCamera").on("click", handleCameraClick);
                THIS_CAMERA.camera_id = BRANCH_CAMERAS[0].id;
                THIS_CAMERA.domain = [];
                $(".btnCamera").css("backgroundColor", "transparent");
                $(`#${BRANCH_CAMERAS[0].id}`).css("backgroundColor", "darkslategray");
            } else {
                _jsHelpers._handle_notify("danger", "No Cameras For This Branch");
            }
            _jsHelpers._unBlockUi();
        })
        .fail((error) => {
            _jsHelpers._unBlockUi();
            _jsHelpers._handle_notify(
                "danger",
                "Server Error : Can't Fetch Branche's Cameras"
            );
        });
}

function handleCameraClick(e) {
    THIS_CAMERA.camera_id = $(this).attr("id");
    THIS_CAMERA.domain = [];
    $(".btnCamera").css("backgroundColor", "transparent");
    $(this).css("backgroundColor", "#a1c523");
}

$(".cell-dot").on("click", handelDotClick);
$(".cell-dot").mousedown(() => {
    mapData.cameras.forEach(camera => {
        if (camera.camera_id == THIS_CAMERA.camera_id) {
            restDots();
        }
    });

    THIS_CAMERA.domain.forEach(cam => {
        $(`#${cam}`).css('backgroundColor', 'transparent');
        $(`#${cam}`).css("boxShadow", 'none');
        $(`#${cam}`).css("borderRadius", 'none');
        $(`#${cam}`).removeClass("red-area");
        $(`#${cam}`).data("value", 0);
        $(`#${cam}`).text("");
    });
    THIS_CAMERA.domain = [];
    $(".cell-dot").on("mouseover", handelDotClick);
});

$(".cell-dot").mouseup(() => {
    $('#btnSave').removeClass('d-none');
    $("._controls_3").css("backgroundColor", "#a1c523");

    $('#check-3').empty();
    $('#check-3').append(`<i class="fa fa-check" aria-hidden="true" style=" margin: 0px; float: right; padding-top: 5px;"></i> `);
    var domain = {};
    THIS_CAMERA.domain.forEach(dot => {
        let row = Math.ceil((dot - 1) / 79);
        if (!domain[row]) {
            domain[row] = [];
        }
        domain[row] = diminsions(domain[row], dot);
    })
    THIS_CAMERA.domain = [];
    Object.keys(domain).forEach(row => {
        if (domain[row].length > 1) {
            let min = Math.min(...domain[row]);
            let max = Math.max(...domain[row])
            for (let i = min; i < max; i++) {
                $(`#${i}`).css("backgroundColor", "red");
                $(`#${i}`).data("value", 1);
                THIS_CAMERA.domain.push(i)
            }
        }
    });

    if (THIS_CAMERA.camera_id != 0) {
        var isExist = false;
        mapData.cameras.forEach(item => {
            if (item.camera_id == THIS_CAMERA.camera_id) {
                item.camera_id = THIS_CAMERA.camera_id;
                item.domain = THIS_CAMERA.domain.map(e => e);
                isExist = true;
            }
        });
        if (!isExist) {
            mapData.cameras.push({ camera_id: THIS_CAMERA.camera_id, domain: THIS_CAMERA.domain.map(e => e) });
        }
    }
    $(".cell-dot").off("mouseover", handelDotClick);
});

function diminsions(diminsion, dotId) {
    dotId = Number(dotId);
    if (diminsion.length <= 1) {
        diminsion.push(dotId);
    } else {
        if (Math.min(...diminsion) > dotId) {
            let index = diminsion.indexOf(Math.min(...diminsion))
            diminsion[index] = dotId;
        } else if (Math.max(...diminsion) < dotId) {
            let index = diminsion.indexOf(Math.max(...diminsion))
            diminsion[index] = dotId;
        }
    }
    return diminsion;
}

function handelDotClick(e) {
    if (THIS_CAMERA.camera_id == 0 || mapData.map_url == "") {
        return false;
    }
    changeCameraDomain($(this).attr("id"));
    if ($(this).data("value") == 0) {
        $(this).css("backgroundColor", "red");
        $(this).data("value", 1);
    } else {
        $(this).css("backgroundColor", "white");
        $(this).data("value", 0);
    }
}

function changeCameraDomain(dotID) {
    const index = THIS_CAMERA.domain.indexOf(dotID);
    if (index > -1) {
        THIS_CAMERA.domain.splice(index, 1);
    } else {
        THIS_CAMERA.domain.push(dotID);
    }
}

$("#btnUploadImg").dropzone({
    url: "/",
    autoProcessQueue: false,
    dictDefaultMessage: "",
    acceptedFiles: ".jpeg,.jpg,.png",
    maxFilesize: 2, // Maximo 15Mb
    maxThumbnailFilesize: 2,
    maxFiles: 1,
    init: function () {
        this.on("maxfilessizeexceeded", function (file) {
            this.removeFile(file);
            _jsHelpers._handle_notify("danger", "Max size 2MB.");
        });
        this.on("thumbnail", function (file) {
            this.removeFile(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (event) {
                var imagen_base64 = event.target.result;
                handleAJAXFoto(imagen_base64);
            };
            reader.onerror = function (error) {
                _jsHelpers._handle_notify("danger", error);
            };
        }); // FIN ADDED FILE
    },
});

var handleAJAXFoto = function (imagen_base64) {
    _jsHelpers._blockUI();
    $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + url_update_photo,
        headers: _jsHelpers._getHeaders(),
        data: {
            file: imagen_base64,
            branch_id: mapData.branch_id,
        },
    })
        .done(function (response) {
            if (!response.error) {
                $("#map-img").attr("src", response.data);
                mapData.map_url = response.data;
                _jsHelpers._handle_notify("success", response.message);
                $('#devices').removeClass('d-none');
                BRANCH_CAMERAS.forEach((item) => {
                    $("#branche_cameras").append(`<div class="btnCamera" id="${item.id}" style="border: 1px solid; display: flex; margin: 3px; justify-content: center;align-items: baseline;border-radius: 8px;cursor: pointer;">
                    <i class="bi bi-camera" style="font-size: 20px; margin-right: 10px;"></i>
                    <p>${item.name}</p>
                </div>`);
                });
                
                $(".btnCamera").on("click", handleCameraClick);
                THIS_CAMERA.camera_id = BRANCH_CAMERAS[0].id;
                THIS_CAMERA.domain = [];
                $(".btnCamera").css("backgroundColor", "transparent");
                $(`#${BRANCH_CAMERAS[0].id}`).css("backgroundColor", "#a1c523");
                 $("._controls_2").css("backgroundColor", "#a1c523");
                $('#check-2').empty();
                $('#check-2').append(`<i class="fa fa-check" aria-hidden="true" style=" margin: 0px; float: right; padding-top: 5px;"></i>  `);
            } else {
                _jsHelpers._responseError(response);
            }

            _jsHelpers._unBlockUi();
        })
        .fail(function (err) {
            _jsHelpers._unBlockUi();
        });
};

$('#btnSave').on('click', function (e) {
    mapData.name = $("#mapName").val();
    if(mapData.name == ""){
        _jsHelpers._handle_notify("danger", "Please Add Heat Map Name!");
        return false;
    }
    if (mapData.branch_id == 0 || mapData.map_url == "" || mapData.cameras.length == 0) {
        _jsHelpers._handle_notify("danger", "Wrong Data Input");
        return false;
    }
    $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/heatmap",
        data: mapData,
        headers: _jsHelpers._getHeaders(),
    }).done((res) => {
        if (!res.error) {
            $("._controls_4").css("backgroundColor", "#a1c523");
            $('#check-4').empty();
            $('#check-4').append(`<i class="fa fa-check" aria-hidden="true" style=" margin: 0px; float: right; padding-top: 5px;"></i> `);
            window.location.replace(`/heatmaps`)
        }
    });
});
init();