var _PREFIX_ADMIN_API = _jsHelpers._PREFIX_ADMIN_API;
var urlChart = _PREFIX_ADMIN_API + "/reports/company_daily";
var chart;
var SELECTED_VARIABLES = [];
var ALL_COMPANY_BRANCHES = [];
var ALL_COMPANY_GROUPS = [];
var SELECTED_BRANCHES = [];
var allData;
var FILTERS = {
  branch_ids: [],
  time_lapse: "this_month",
  type: "branches",
  from_date: '17/02/2022',//new Date().toLocaleDateString(),
  to_date: '17/02/2022',//new Date().toLocaleDateString(),
};
var linkersGraphColors = ['#186BA0', '#EA7268', '#CC549F', '#EE9A3A', '#5EC3AD', '#17A9DD'];

function Num2Time(stay_time) {
  min = stay_time - Math.floor(stay_time / 60) * 60
  sec = (Number(min - Math.floor(min)).toFixed(2)) * 60;
  return `${String(Math.floor(stay_time / 60)).padStart(2, "0")}:${String(Math.floor(min)).padStart(2, "0")}:${String(Math.floor(sec)).padStart(2, "0")}`
}

var sparkelOptions = {
  series: [{
    data: [],//[0, 20, 30, 0, 50, 60, 70, 80]
  }],
  chart: {
    type: 'area',
    height: 100,
    // width:200,
    sparkline: {
      enabled: true
    },
  },
  stroke: {
    curve: 'smooth'
  },
  fill: {
    opacity: 0.3,
  },
  yaxis: {
    min: 0
  },
  colors: ['#17A9DD'],
  title: {
    text: '0',//'$424,652',
    offsetX: 0,
    style: {
      fontSize: '12px',
      fontFamily: 'Maven pro family',
    }
  },
  subtitle: {
    text: '',//'Sales',
    offsetX: 0,
    style: {
      fontSize: '18px',
      fontFamily: 'Maven pro family',
    }
  }
};

var barChartOptions = {
  series: [{
    data: []
  }],
  chart: {
    type: 'bar',
    height: 350,
    zoom: {
      enabled: false,
    },
    toolbar: {
      show: false
    }
  },
  tooltip: {
    enabled: false
  },
  colors: linkersGraphColors,
  plotOptions: {
    bar: {
      borderRadius: 4,
      horizontal: false,
    }
  },
  dataLabels: {
    enabled: true
  },
  xaxis: {
    categories: ['Visitors', 'Lost Customers', 'Customers', 'Sales Conversioin', 'Outside Traffic', 'New Customers', 'Returning Customers', 'Stay Time', 'Turn In Rate']
  }
};

var comparisionOptions = {
  series: [
    {
      name: "Website Blog",
      type: "column",
      data: []
    },
    {
      name: "Social Media",
      type: "line",
      data: []
    }
  ],
  chart: {
    height: 350,
    type: "line",
    zoom: {
      enabled: false,
    },
    toolbar: {
      show: false
    }
  },
  tooltip: {
    enabled: false
  },
  colors: linkersGraphColors,
  stroke: {
    curve: "smooth",
    width: [2, 2]
  },
  title: {
    text: "Traffic Sources",
    style: {
      fontSize: '16px',
      fontWeight: 'bold',
      fontFamily: 'Maven pro family',
      color: '#263238'
    }
  },
  dataLabels: {
    enabled: true,
  },
  xaxis: {
    categories: [
      "00",
      "01",
      "02",
      "03",
      "04",
      "05",
      "06",
      "07",
      "08",
      "09",
      "10",
      "11",
      "12",
      "13",
      "14",
      "15",
      "16",
      "17",
      "18",
      "19",
      "20",
      "21",
      "22",
      "23"
    ]
  },
};

var donutOptions = {
  series: [],
  chart: {
    type: 'donut',
    toolbar: {
      show: false
    },
  },
  tootip: {
    show: false,
    enabled: false,
    enabledOnSeries: false,
  },
  title: {
    style: {
      fontSize: '12px',
      fontFamily: 'Maven pro family',
      color: '#263238'
    }
  },
  plotOptions: {
    pie: {

      donut: {
        size: '70%',
      },
      labels: {
        show: false,
      }
    }
  },
  colors: linkersGraphColors
};

var bubbleOptions = {
  series: [
    {
      name: "SAMPLE A",
      data: []
    },
    {
      name: "SAMPLE B",
      data: []
    },
    {
      name: "SAMPLE C",
      data: []
    }
  ],
  chart: {
    height: 350,
    type: "scatter",
    zoom: {
      enabled: false
    },
    toolbar: {
      show: false
    }
  },
  colors: linkersGraphColors,
  tootip: {
    show: false,
    enabled: false,
    enabledOnSeries: false,
  },
  dataLabels: {
    enabled: true,
    formatter: function (val) {
      return parseFloat(val).toFixed(0);
    },
  },
  tootip: {
    enabled: false
  },
  xaxis: {
  },
  yaxis: {
    labels: {
      formatter: function (val) {
        return parseFloat(val).toFixed(0);
      }
    }
  }
};
companyName = $("#companyName")[0].innerText;
$(".cx-report-title").text(companyName + `${_jsHelpers._trans("sitesDailyReport")}`);
handleDateSelection();

var totVisitors = new ApexCharts(document.querySelector("#total-visitors"), sparkelOptions);
var totOutsideTrffic = new ApexCharts(document.querySelector("#total-outside-traffic"), sparkelOptions);
var totReturningCustomers = new ApexCharts(document.querySelector("#total-returning-customers"), sparkelOptions);
var totNewCustomers = new ApexCharts(document.querySelector("#total-new-customers"), sparkelOptions);
var totStayTime = new ApexCharts(document.querySelector("#total-stay-time"), sparkelOptions);
var totTurnInRate = new ApexCharts(document.querySelector("#total-turn-in-rate"), sparkelOptions);
var totCustomers = new ApexCharts(document.querySelector("#total-customers"), sparkelOptions);
var totLostCustomers = new ApexCharts(document.querySelector("#total-lost-customers"), sparkelOptions);
totVisitors.render();
totOutsideTrffic.render();
totReturningCustomers.render();
totNewCustomers.render();
totStayTime.render();
totTurnInRate.render();
totCustomers.render();
totLostCustomers.render();

var visitors_vs_outsideTraffic = new ApexCharts(document.querySelector("#visitor-outsideTraffic"), comparisionOptions);
var turnInRate = new ApexCharts(document.querySelector("#turnInRate"), comparisionOptions);
var returning_vs_new = new ApexCharts(document.querySelector("#returning-new"), donutOptions);
var customers_vs_lost = new ApexCharts(document.querySelector("#customers-lost"), donutOptions);
var bubbleChart = new ApexCharts(document.querySelector("#general-all"), bubbleOptions);
var thisWeek_general_allBar = new ApexCharts(document.querySelector("#thisWeek_general_allBar"), barChartOptions);
var lastWeek_general_allBar = new ApexCharts(document.querySelector("#lastWeek_general_allBar"), barChartOptions);
var thisWeek_general_allBar_view = new ApexCharts(document.querySelector("#thisWeek_general_allBar_view"), barChartOptions);
var lastWeek_general_allBar_view = new ApexCharts(document.querySelector("#lastWeek_general_allBar_view"), barChartOptions);

visitors_vs_outsideTraffic.render();
turnInRate.render();
returning_vs_new.render();
customers_vs_lost.render();
bubbleChart.render();
thisWeek_general_allBar.render();
lastWeek_general_allBar.render();
thisWeek_general_allBar_view.render();
lastWeek_general_allBar_view.render();




var visitor_VS_visitors = new ApexCharts(document.querySelector("#visitor_VS_visitors"), comparisionOptions);
var lastWeek_outSideTraffci_VS_visitors = new ApexCharts(document.querySelector("#lastWeek_outSideTraffci_VS_visitors"), comparisionOptions);
visitor_VS_visitors.render();
lastWeek_outSideTraffci_VS_visitors.render();

var branchComparision = new ApexCharts(document.querySelector("#branchComparision"), comparisionOptions);
var branchComparision_pie = new ApexCharts(document.querySelector("#branchComparision_pie"), donutOptions);
var branchComparision_stayTime = new ApexCharts(document.querySelector("#branchComparision_stayTime"), comparisionOptions);
branchComparision_stayTime.render();
branchComparision.render();
branchComparision_pie.render();

/****************************************View *******************************************/
var totVisitors_view = new ApexCharts(document.querySelector("#total-visitors_view"), sparkelOptions);
var totOutsideTrffic_view = new ApexCharts(document.querySelector("#total-outside-traffic_view"), sparkelOptions);
var totReturningCustomers_view = new ApexCharts(document.querySelector("#total-returning-customers_view"), sparkelOptions);
var totNewCustomers_view = new ApexCharts(document.querySelector("#total-new-customers_view"), sparkelOptions);
var totStayTime_view = new ApexCharts(document.querySelector("#total-stay-time_view"), sparkelOptions);
var totTurnInRate_view = new ApexCharts(document.querySelector("#total-turn-in-rate_view"), sparkelOptions);
var totCustomers_view = new ApexCharts(document.querySelector("#total-customers_view"), sparkelOptions);
var totLostCustomers_view = new ApexCharts(document.querySelector("#total-lost-customers_view"), sparkelOptions);
totVisitors_view.render();
totOutsideTrffic_view.render();
totReturningCustomers_view.render();
totNewCustomers_view.render();
totStayTime_view.render();
totTurnInRate_view.render();
totCustomers_view.render();
totLostCustomers_view.render();

var visitors_vs_outsideTraffic_view = new ApexCharts(document.querySelector("#visitor-outsideTraffic_view"), comparisionOptions);
var turnInRate_view = new ApexCharts(document.querySelector("#turnInRate_view"), comparisionOptions);
var returning_vs_new_view = new ApexCharts(document.querySelector("#returning-new_view"), donutOptions);
var customers_vs_lost_view = new ApexCharts(document.querySelector("#customers-lost_view"), donutOptions);
var bubbleChart_view = new ApexCharts(document.querySelector("#general-all_view"), bubbleOptions);
visitors_vs_outsideTraffic_view.render();
turnInRate_view.render();
returning_vs_new_view.render();
customers_vs_lost_view.render();
bubbleChart_view.render();

var visitor_VS_visitors_view = new ApexCharts(document.querySelector("#visitor_VS_visitors_view"), comparisionOptions);
var lastWeek_outSideTraffci_VS_visitors_view = new ApexCharts(document.querySelector("#lastWeek_outSideTraffci_VS_visitors_view"), comparisionOptions);
visitor_VS_visitors_view.render();
lastWeek_outSideTraffci_VS_visitors_view.render();

var branchComparision_view = new ApexCharts(document.querySelector("#branchComparision_view"), comparisionOptions);
var branchComparision_pie_view = new ApexCharts(document.querySelector("#branchComparision_pie_view"), donutOptions);
var branchComparision_stayTime_view = new ApexCharts(document.querySelector("#branchComparision_stayTime_view"), comparisionOptions);
branchComparision_stayTime_view.render();
branchComparision_view.render();
branchComparision_pie_view.render();

function handleDateSelection() {
  var date_element = document.getElementById(`dateInput`);
  var today_date = new Date(new Date().setDate(new Date().getDate() - 1));
  date_element.valueAsDate = today_date;
  date_element.max = today_date.getFullYear() + "-" + ("0" + (today_date.getMonth() + 1)).slice(-2) +
    "-" + ("0" + (today_date.getDate())).slice(-2);
}
async function getData() {
  try {
    var x = FILTERS.from_date.split("/")
    let thisWeekDate = new Date(`${Number(x[1])}-${Number(x[0])}-${x[2]}`);
    let lastWeekDate = new Date(thisWeekDate.getFullYear(), thisWeekDate.getMonth(), thisWeekDate.getDate() - 7);

    _jsHelpers._blockUI();
    if (ALL_COMPANY_BRANCHES.length == 0) {
      await $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branchesWithGroups/",
        headers: _jsHelpers._getHeaders(),
      }).done((res) => {
        if (!res.error) {
          ALL_COMPANY_BRANCHES = res.data.branches;
          ALL_COMPANY_GROUPS = res.data.groups;
          SELECTED_BRANCHES = ALL_COMPANY_BRANCHES.map(item => {
            item.group_id = 0;
            return item;
          });
        }
      }).fail((error) => {
        _jsHelpers._handle_notify('danger', 'Server Error : Can\'t Fetch Company Branches : ' + error);
      });
    }


    if (ALL_COMPANY_BRANCHES.length !== SELECTED_BRANCHES.length) {
      $('.branches').text(`(${SELECTED_BRANCHES.map(e => e.name).join(',')})`);
    }
    $('#dateInput').trigger('change')

    $('.rpt-date').text(FILTERS.from_date);

    FILTERS.branch_ids = SELECTED_BRANCHES.map(e => e.id);
    var error = false;
    await $.ajax({
      type: "GET",
      url: urlChart,
      headers: _jsHelpers._getHeaders(),
      data: FILTERS,
    }).done((res) => {
      allData = res.data
      if (res.error) {
        _jsHelpers._handle_notify('danger', res.message);
        _jsHelpers._unBlockUi();
        error = true
      }
    }).fail((error) => {
      _jsHelpers._handle_notify('danger', error);
    });
    _jsHelpers._unBlockUi();
    if (error) return;

    function RenderSparkelsCharts(chart, variable, title) {
      var thisWeekData = allData.hourly.this_week;
      let count = thisWeekData.data.map(item => {
        return item.count[variable] == 0 ? 0.1 : item.count[variable];
      });
      if (['stay_time', 'turn_in_rate', 'returning_customer', 'new_customer'].includes(variable)) {
        chart.updateOptions({
          series: [{
            data: count
          }],
          title: {
            text: title,
          },
          subtitle: {
            text: variable == 'stay_time' ?
              Num2Time(Number(allData.daily.this_week.data[0].count[variable]).toFixed(1)) :
              Number(allData.daily.this_week.data[0].count[variable]).toFixed(1) + "%"
            // text: Number(allData.daily.this_week.data[0].count[variable]).toFixed(1) + (variable == 'stay_time' ? " min" : "%"),
          }
        });
      } else {
        chart.updateOptions({
          series: [{
            data: count
          }],
          title: {
            text: title,
          },
          subtitle: {
            text: variable == 'customers' ? allData.daily.this_week.data[0].count[variable] + " | " + Number(allData.daily.this_week.data[0].count.conversion).toFixed(1) + "%" : allData.daily.this_week.data[0].count[variable],
          }
        });
      }
    }

    RenderSparkelsCharts(totVisitors, 'visitors', 'Visitors');
    RenderSparkelsCharts(totOutsideTrffic, 'outside_traffic', 'Outside Traffic');
    RenderSparkelsCharts(totReturningCustomers, 'returning_customer', 'Returning Customers');
    RenderSparkelsCharts(totNewCustomers, 'new_customer', 'New Customers');
    RenderSparkelsCharts(totStayTime, 'stay_time', 'Stay Time');
    RenderSparkelsCharts(totTurnInRate, 'turn_in_rate', 'Turn In Rate');
    RenderSparkelsCharts(totCustomers, 'customers', 'Customers | Conversion');
    RenderSparkelsCharts(totLostCustomers, 'lost', 'Lost Customers');

    RenderSparkelsCharts(totVisitors_view, 'visitors', 'Visitors');
    RenderSparkelsCharts(totOutsideTrffic_view, 'outside_traffic', 'Outside Traffic');
    RenderSparkelsCharts(totReturningCustomers_view, 'returning_customer', 'Returning Customers');
    RenderSparkelsCharts(totNewCustomers_view, 'new_customer', 'New Customers');
    RenderSparkelsCharts(totStayTime_view, 'stay_time', 'Stay Time');
    RenderSparkelsCharts(totTurnInRate_view, 'turn_in_rate', 'Turn In Rate');
    RenderSparkelsCharts(totCustomers_view, 'customers', 'Customers | Conversion');
    RenderSparkelsCharts(totLostCustomers_view, 'lost', 'Lost Customers');

    let visitorsHourlyData = [];
    let outSideTrafficHourlyData = [];
    let customersHourlyData = [];
    let lostCustomersHourlyData = [];
    let lastWeek_visitorsHourlyData = [];
    let lastWeek_outSideTrafficHourlyData = [];
    let turnInRateHourlyData = [];

    allData.hourly.this_week.data.forEach(item => {
      visitorsHourlyData.push(item.count.visitors);
      customersHourlyData.push(item.count.customers);
      lostCustomersHourlyData.push(item.count.lost);
      outSideTrafficHourlyData.push(item.count.outside_traffic);
      turnInRateHourlyData.push(item.count.turn_in_rate);
    });

    allData.hourly.last_week.data.forEach(item => {
      lastWeek_visitorsHourlyData.push(item.count.visitors);
      lastWeek_outSideTrafficHourlyData.push(item.count.outside_traffic);
    });

    thisWeek_general_allBar.updateOptions({
      series: [{
        data: [
          allData.daily.this_week.data[0].count['visitors'],
          allData.daily.this_week.data[0].count['lost'],
          allData.daily.this_week.data[0].count['customers'],
          Number(allData.daily.this_week.data[0].count['conversion']).toFixed(2),
          allData.daily.this_week.data[0].count['outside_traffic'],
          Number(allData.daily.this_week.data[0].count['new_customer']).toFixed(2),
          Number(allData.daily.this_week.data[0].count['returning_customer']).toFixed(2),
          Number(allData.daily.this_week.data[0].count['stay_time']).toFixed(2),
          Number(allData.daily.this_week.data[0].count['turn_in_rate']).toFixed(2),
        ]
      }],
      title: {
        text: "General Chart"
      },
    });

    thisWeek_general_allBar_view.updateOptions({
      series: [{
        data: [
          allData.daily.this_week.data[0].count['visitors'],
          allData.daily.this_week.data[0].count['lost'],
          allData.daily.this_week.data[0].count['customers'],
          Number(allData.daily.this_week.data[0].count['conversion']).toFixed(2),
          allData.daily.this_week.data[0].count['outside_traffic'],
          Number(allData.daily.this_week.data[0].count['new_customer']).toFixed(2),
          Number(allData.daily.this_week.data[0].count['returning_customer']).toFixed(2),
          Number(allData.daily.this_week.data[0].count['stay_time']).toFixed(2),
          Number(allData.daily.this_week.data[0].count['turn_in_rate']).toFixed(2),
        ]
      }],
      title: {
        text: "General Chart"
      },
    });

    lastWeek_general_allBar.updateOptions({
      series: [{
        data: [
          allData.daily.last_week.data[0].count['visitors'],
          allData.daily.last_week.data[0].count['lost'],
          allData.daily.last_week.data[0].count['customers'],
          Number(allData.daily.last_week.data[0].count['conversion']).toFixed(2),
          allData.daily.last_week.data[0].count['outside_traffic'],
          Number(allData.daily.last_week.data[0].count['new_customer']).toFixed(2),
          Number(allData.daily.last_week.data[0].count['returning_customer']).toFixed(2),
          Number(allData.daily.last_week.data[0].count['stay_time']).toFixed(2),
          Number(allData.daily.last_week.data[0].count['turn_in_rate']).toFixed(2),
        ]
      }],
      title: {
        text: lastWeekDate.toLocaleDateString() + " General Chart"
      },
    });

    lastWeek_general_allBar_view.updateOptions({
      series: [{
        data: [
          allData.daily.last_week.data[0].count['visitors'],
          allData.daily.last_week.data[0].count['lost'],
          allData.daily.last_week.data[0].count['customers'],
          Number(allData.daily.last_week.data[0].count['conversion']).toFixed(2),
          allData.daily.last_week.data[0].count['outside_traffic'],
          Number(allData.daily.last_week.data[0].count['new_customer']).toFixed(2),
          Number(allData.daily.last_week.data[0].count['returning_customer']).toFixed(2),
          Number(allData.daily.last_week.data[0].count['stay_time']).toFixed(2),
          Number(allData.daily.last_week.data[0].count['turn_in_rate']).toFixed(2),
        ]
      }],
      title: {
        text: lastWeekDate.toLocaleDateString() + " General Chart"
      },
    });

    visitors_vs_outsideTraffic.updateOptions({
      series: [
        {
          name: "Outside Traffic",
          type: "column",
          data: outSideTrafficHourlyData
        },
        {
          name: "Visitors",
          type: "line",
          data: visitorsHourlyData
        }
      ],
      title: {
        text: "Visitors VS Outside Traffic"
      },
    });

    turnInRate.updateOptions({
      title: {
        text: "Turn In Rate"
      },
      series: [
        {
          name: "Turn In Rate",
          type: "line",
          data: turnInRateHourlyData

        }
      ],
      yaxis: {
        // tickAmount: 10,
        labels: {
          formatter: function (val) {
            return parseFloat(val).toFixed(1);
          }
        }
      },
      dataLabels: {
        // enabled: false,
        formatter: function (val, opt) {
          return parseFloat(val).toFixed(1);
        }
      },
    });

    lastWeek_outSideTraffci_VS_visitors.updateOptions({
      series: [
        {
          name: "Outside Traffic",
          type: "column",
          data: lastWeek_outSideTrafficHourlyData
        },
        {
          name: "Visitors",
          type: "line",
          data: lastWeek_visitorsHourlyData
        }
      ],
      title: {
        text: lastWeekDate.toLocaleDateString() + " : Visitors VS Outside Traffic"
      },
    });

    visitor_VS_visitors.updateOptions({
      series: [
        {
          name: "Today Visitors",
          type: "column",
          data: visitorsHourlyData
        },
        {
          name: "Last Week Visitors",
          type: "line",
          data: lastWeek_visitorsHourlyData
        }
      ],
      title: {
        text: thisWeekDate.toLocaleDateString() + " Visitors VS " + lastWeekDate.toLocaleDateString() + " Visitors"
      },
    });


    returning_vs_new.updateOptions({
      series: [allData.daily.this_week.total.returning_customer, allData.daily.this_week.total.new_customer],
      labels: ['Returning', 'New'],
      title: {
        text: "Customers : Returning VS New"
      },
    });

    customers_vs_lost.updateOptions({
      series: [allData.hourly.this_week.total.customers, allData.hourly.this_week.total.lost],
      labels: ['Customers', 'Lost'],
      title: {
        text: "Customers VS Lost Customers"
      },
    });

    /***************************VIEW****************************************************** */
    visitors_vs_outsideTraffic_view.updateOptions({
      series: [
        {
          name: "Outside Traffic",
          type: "column",
          data: outSideTrafficHourlyData
        },
        {
          name: "Visitors",
          type: "line",
          data: visitorsHourlyData
        }
      ],
      title: {
        text: "Visitors VS Outside Traffic"
      },
    });

    turnInRate_view.updateOptions({
      title: {
        text: "Turn In Rate"
      },
      series: [
        {
          name: "Turn In Rate",
          type: "column",
          data: turnInRateHourlyData

        }
      ],
      yaxis: {
        // tickAmount: 10,
        labels: {
          formatter: function (val) {
            return parseFloat(val).toFixed(1);
          }
        }
      },
      dataLabels: {
        // enabled: false,
        formatter: function (val, opt) {
          return parseFloat(val).toFixed(1);
        }
      },
    });

    lastWeek_outSideTraffci_VS_visitors_view.updateOptions({
      series: [
        {
          name: "Outside Traffic",
          type: "column",
          data: lastWeek_outSideTrafficHourlyData
        },
        {
          name: "Visitors",
          type: "line",
          data: lastWeek_visitorsHourlyData
        }
      ],
      title: {
        text: lastWeekDate.toLocaleDateString() + " : Visitors VS Outside Traffic"
      },
    });

    visitor_VS_visitors_view.updateOptions({
      series: [
        {
          name: "Today Visitors",
          type: "column",
          data: visitorsHourlyData
        },
        {
          name: "Last Week Visitors",
          type: "line",
          data: lastWeek_visitorsHourlyData
        }
      ],
      title: {
        text: thisWeekDate.toLocaleDateString() + " Visitors VS " + lastWeekDate.toLocaleDateString() + " Visitors"
      },
    });


    returning_vs_new_view.updateOptions({
      series: [allData.daily.this_week.total.returning_customer, allData.daily.this_week.total.new_customer],
      labels: ['Returning', 'New'],
      title: {
        text: "Customers : Returning VS New"
      },
    });

    customers_vs_lost_view.updateOptions({
      series: [allData.hourly.this_week.total.customers, allData.hourly.this_week.total.lost],
      labels: ['Customers', 'Lost'],
      title: {
        text: "Customers VS Lost Customers"
      },
    });



    let branchesVisitors = [];
    let branchesStayTime = [];
    let branchNames = [];
    let branchesSeries = [];
    allData.sammury.forEach(item => {
      branchesStayTime.push(item.stay_time.value)
      branchesVisitors.push(item.visitors.value);
      branchNames.push(item.branchName);
      branchesSeries.push({ name: item.branchName, data: [item.visitors.value] });
    });

    branchComparision_stayTime.updateOptions({
      series: [{ name: 'branches', data: branchesStayTime }],
      chart: {
        type: 'bar'
      },
      xaxis: {
        categories: branchNames
      },
      title: {
        text: "Branch's Stay Time"
      },
    });

    branchComparision_pie.updateOptions({
      series: branchesVisitors,
      labels: branchNames,
      title: {
        text: "Branches's Visitors"
      },
    });

    branchComparision.updateOptions({
      series: [{ name: 'branches', data: branchesVisitors }],
      xaxis: {
        categories: branchNames
      },
      title: {
        text: "Branch's Visitors"
      },
    });

    /********************************VIEW**************************** */
    branchComparision_stayTime_view.updateOptions({
      series: [{ name: 'branches', data: branchesStayTime }],
      chart: {
        type: 'bar'
      },
      xaxis: {
        categories: branchNames
      },
      title: {
        text: "Branch's Stay Time"
      },
    });

    branchComparision_pie_view.updateOptions({
      series: branchesVisitors,
      labels: branchNames,
      title: {
        text: "Branches's Visitors",
        style: {
          fontSize: '18px',
          fontFamily: 'Maven pro family',
          color: '#263238'
        }
      },
    });

    branchComparision_view.updateOptions({
      series: [{ name: 'branches', data: branchesVisitors }],
      xaxis: {
        categories: branchNames
      },
      title: {
        text: "Branch's Visitors"
      },
    });

    // render Bubble Chart Data :
    let hoursArr = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];
    var visitorsSeries = [];
    var lostSeries = [];
    var CustomersSeries = [];
    var OutsideTrafficSeries = [];

    hoursArr.forEach((item, idx) => {
      let data = allData.hourly.this_week.data
      visitorsSeries.push([Number(item), data[idx].count.visitors]);
      lostSeries.push([Number(item), data[idx].count.lost]);
      CustomersSeries.push([Number(item), data[idx].count.customers]);
      OutsideTrafficSeries.push([Number(item), data[idx].count.outside_traffic]);
    });

    bubbleChart.updateOptions({
      series: [
        {
          name: "Visitors",
          data: visitorsSeries
        },
        // {
        //   name: "Lost Customers",
        //   data: lostSeries
        // },
        {
          name: "Customers",
          data: CustomersSeries
        },
        {
          name: "Outside Traffic",
          data: OutsideTrafficSeries
        },
      ],
    });
    /***************************VEIEW**************************************** */
    bubbleChart_view.updateOptions({
      series: [
        {
          name: "Visitors",
          data: visitorsSeries
        },
        {
          name: "Customers",
          data: CustomersSeries
        },
        {
          name: "Outside Traffic",
          data: OutsideTrafficSeries
        },
      ],
    })
  } catch (error) {
    _jsHelpers._handle_notify('danger', 'Data Not Available');
    console.error(error);
  }

}

$('#brachSelection').on('click', (e) => {
  e.preventDefault();
  _jsBranchesModalHelper._showMoreBranshesModal__A(
    (selectedBranches) => handleBranchSelection_CB(selectedBranches),
    SELECTED_BRANCHES
  );
});

const handleBranchSelection_CB = (selectedBranches) => {
  SELECTED_BRANCHES = selectedBranches //list-branches
};

$('#dateInput').on('change', e => {
  let arr = e.target.value.split("-");
  let date = `${arr[2]}/${arr[1]}/${arr[0]}`;
  FILTERS.from_date = date;
  FILTERS.to_date = date;
});

$('#btn-apply').on('click', e => {
  getData();
});

$('#nextIcon').on('click', e => {
  _jsHelpers._blockUI();
  $('#test').removeAttr('hidden');


  new Promise(function (resolve, reject) {
    try {
      setTimeout(function () {
        var element = document.getElementById('test');
        var opt = {
          margin: 0,
          filename: `Report-${new Date().toDateString()}.pdf`,
          image: { type: "jpeg", quality: 0.98 },
          pagebreak: { mode: "css", before: ".breackMode" },
          html2canvas: { scale: 1 },
          jsPDF: { unit: "in", format: "letter", orientation: "portrait" },
        };

        html2pdf(element, opt);

        resolve("anything");
      }, 500);
    } catch (error) {
      reject(error);
    }
  }).then(() => {
    $('#test').attr('hidden', true);
    _jsHelpers._unBlockUi();
  })
});

getData();