var FoodicsNew = function () {

    /**** CONSTANTES GLOBALES****/
    var _PREFIX_ADMIN_API = _jsHelpers._PREFIX_ADMIN_API;


    var handleLogicModule = function(){
        var obj = searchToObject();
        getFoodicsToken(obj);
    }

    var searchToObject = function() {
        var pairs = window.location.search.substring(1).split("&"),
            obj = {},
            pair,
            i;

        for ( i in pairs ) {
            if ( pairs[i] === "" ) continue;

            pair = pairs[i].split("=");
            obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent( pair[1] );
        }

        return obj;
    }

    var getFoodicsToken = function(obj){
        const company_id = $('#id-company').val();
        _jsHelpers._blockUI();
        $.ajax({
                type: 'GET',
                url: _jsHelpers._PREFIX_ADMIN_API + '/foodics/connection/?code=' + obj.code + '&state=' + obj.state  + '&id=' + company_id,
                headers: _jsHelpers._getHeaders(),
            }).done(res => {
                if (!res.error) {
                    window.location.href = "/company-branches?status=Success";
                }else{
                    window.location.href = "/integration/foodics?status=Failed";
                }
            });
    }

    return {
        init: function () {
            handleLogicModule();
        }
    };
}();

$(document).ready(function () {
    FoodicsNew.init();
});
