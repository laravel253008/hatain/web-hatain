'use strict';
var THIS_CAMERA = {
    camera_id: 0,
    domain: [],
};

var FILTER = {
    "node_ids": [],
    "camera_ids": [],
    "from_date": "20/06/2022",
    "to_date": "24/06/2022",
    "resolution": 0
}
var DATA = {};
var SELECTED_DAYS = [];
var SELECTED_HOURS = [];
var ALL_DATA = {};
var realTime = true;
var url_update_photo = "/heatmap/uploadHeatImage";
firebase.initializeApp(_jsHelpers._FIREBASECONFIG);
let db = firebase.firestore();

$('#dotsContainer').css('backgroundColor', 'transparent');
// init 
$('#map-img').attr('src', mapData.map_url);

function restDots() {
    $('.cell-dot').css('backgroundColor', 'transparent');
    $('.cell-dot').css("boxShadow", 'none');
    $('.cell-dot').css("borderRadius", 'none');
    $('.cell-dot').removeClass("red-area");
    $('.cell-dot').data("value", 0);
    $('.cell-dot').text("");
}

function init() {
    DATA.id = mapData.id;
    DATA.name = mapData.name;
    DATA.branch_id = mapData.branch_id;
    DATA.cameras = mapData.devices[0].cameras;
    DATA.map_url = mapData.map_url;

    $("#mapName").val(mapData.name);
    $('.cell-dot').css('backgroundColor', 'transparent');
    document.getElementById('fromDate').valueAsDate = new Date();
    document.getElementById('tomDate').valueAsDate = new Date();

    function getDevicesIDs() {
        return mapData.devices.map(device => {
            return device.device_id;
        });
    }

    function getCamerasIDs() {
        var cameraIDs = [];
        mapData.devices.forEach(device => {
            device.cameras.forEach(camera => {
                cameraIDs.push(camera.camera_id);
            });
        });
        return cameraIDs;
    }

    function getColor(value) {
        //value from 0 to 1
        var hue = ((1 - value) * 120).toString(10);
        return ["hsl(", hue, ",100%,50%)"].join("");
    }

    $('#fromDate').change((e) => {
        FILTER.from_date = moment(e.target.value).format('DD/MM/YYYY');
    });
    $('#toDate').change((e) => {
        FILTER.to_date = moment(e.target.value).format('DD/MM/YYYY');
    });

    function fillHoursGaps(data) {
        var arr = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"];
        let firstDay = data[Object.keys(data)[0]];
        if (!firstDay) {
            return;
        }
        let firstHourofFirstDay = firstDay[Object.keys(firstDay)[0]];
        var cameras = Object.keys(firstHourofFirstDay);
        Object.keys(data).forEach(day => {
            arr.forEach(hour => {
                if (!data[day][hour]) {
                    data[day][hour] = {};
                    cameras.forEach(cam => {
                        data[day][hour][cam] = 0
                    })
                }
            })
        });
    }

    function handleData(data) {
        var result = {};
        var days = [];
        Object.keys(data).forEach(item => {
            let day = item.split(" ")[0].split("-")[2];
            let hour = item.split(" ")[1].split("-")[0].split(":")[0];
            if (!days.includes(day)) {
                result[day] = {};
                days.push(day);
            }
            result[day][hour] = {}
            Object.keys(data[item]).forEach(camera => {
                result[day][hour][camera] = data[item][camera].total.entry
            })
        });
        fillHoursGaps(result);
        return result;
    }

    const historicalHeatMap = function () {
        restDots();
        realTime = false;
        FILTER.node_ids = []
        FILTER.camera_ids = [];
        var camerasDomains = {};
        mapData.devices.forEach(device => {
            FILTER.node_ids.push(device.id)
            device.cameras.forEach(camera => {
                FILTER.camera_ids.push(camera.id);
                camerasDomains[camera.id] = camera.heatmaps[0].domain;
            });
        });

        $.ajax({
            type: "GET",
            url: _jsHelpers._PREFIX_ADMIN_API + "/counter/devices_activity",
            data: FILTER,
            headers: _jsHelpers._getHeaders(),
        }).done((res) => {
            if (!res.error) {
                $('#historicalHeatMapControls').removeClass('d-none')
                ALL_DATA = handleData(res.data);

                $('#daysContainer').empty();
                Object.keys(ALL_DATA).sort().forEach(day => {
                    $('#daysContainer').append(`
                    <div id="${day}" class="btnDayContainer">
                        <p class="btnDay">${day}</p>
                    </div>`)
                });
                $('#daysContainer').append(`
                    <div id="allDaysContainer">
                        <p id="allDays" class="btnDay">All</p>
                    </div>`);

                $('.btnHourContainer').css('opacity', '1');
                $('.btnHourContainer').addClass('active');
                $('#AllHoursContainer').css('backgroundColor', '#a2a2a1');
                $('#AllHoursContainer').addClass('active');

                $('.btnDayContainer').css('opacity', '1');
                $('.btnDayContainer').addClass('active');
                $('#allDaysContainer').css('backgroundColor', '#a5c939');
                $('#allDaysContainer').addClass('active');
                handleDaySelection();
                handleHourSelection();
                // renderHistoricalMapData();

                $('.btnDayContainer').on('click', function (e) {
                    if ($(this).hasClass('active')) {
                        $(this).css('opacity', '0.3');
                        $(this).removeClass('active');

                        $('#allDaysContainer').css('backgroundColor', 'transparent');
                        $('#allDaysContainer').removeClass('active');
                    } else {
                        $(this).css('opacity', '1');
                        $(this).addClass('active');
                    }
                    handleDaySelection()
                });

                $('#allDaysContainer').on('click', function (e) {
                    if ($(this).hasClass('active')) {
                        $(this).css('backgroundColor', 'transparent');
                        $('.btnDayContainer').css('opacity', '0.3');
                        $(this).removeClass('active');
                        $('.btnDayContainer').removeClass('active');

                    } else {

                        $(this).css('backgroundColor', '#a5c939');
                        $(this).addClass('active');
                        $('.btnDayContainer').css('opacity', '1');
                        $('.btnDayContainer').addClass('active');
                    }
                    handleDaySelection()
                });
            }
        });
    }

    const realTimeHeatMap = function () {
        restDots();
        $('#historicalHeatMapControls').addClass('d-none')
        const postIDs = getDevicesIDs();
        var today = moment().format("YYYY-MM-DD");
        var camera_id = getCamerasIDs();
        var query = db
            .collection("devices")
            .where(firebase.firestore.FieldPath.documentId(), "in", postIDs);
        query.get().then((querySnapshot) => {
            querySnapshot.forEach((document) => {
                document.ref
                    .collection("cameras/counting/" + today)
                    .where(firebase.firestore.FieldPath.documentId(), "in", camera_id)
                    .onSnapshot(qs => {
                        if (realTime == true) {
                            var count = []
                            var cameras = []
                            qs.forEach(doc => {
                                count.push(doc.data().entry);
                                cameras.push(doc.id);
                            });

                            var maxCount = Math.max(...count);
                            var sumCount = count.reduce((partialSum, a) => partialSum + a, 0);
                            // cameras.forEach((item, idx) => {
                            mapData.devices.forEach(device => {
                                device.cameras.forEach(camera => {
                                    let idx = cameras.indexOf(camera.camera_id)
                                    if (idx != -1) {
                                        // if (camera.camera_id == item) {
                                        let map = camera.heatmaps.find(x => x.heatmap_id == mapData.id);
                                        map.domain.forEach((dot) => {
                                            // let x_max = count[idx];
                                            // let x_min = count[idx] - (count[idx] * (count[idx] / sumCount));
                                            let val = count[idx];//Math.floor(Math.random() * (x_max - x_min + 1)) + x_min;
                                            $(`#${dot}`).css("backgroundColor", getColor(val / maxCount));
                                            // $(`#${dot}`).css("boxShadow", `0 0 25px 20px ${getColor(val / maxCount)}`);
                                            // $(`#${dot}`).css("borderRadius", '50%');
                                            // $(`#${dot}`).addClass("tooltip");
                                            $(`#${dot}`).append(`<span class="tooltiptext">${map.name} : ${val}</span>`);
                                        });

                                    } else {
                                        let map = camera.heatmaps.find(x => x.heatmap_id == mapData.id);
                                        map.domain.forEach((dot) => {
                                            let val = count[idx];
                                            $(`#${dot}`).css("backgroundColor", 'gray');
                                            $(`#${dot}`).append(`<span class="tooltiptext">${map.name} : 0</span>`);
                                        });
                                    }
                                });
                            });
                            // })
                        }
                    });
            });
        });
    }

    $('#btnApplyFilter').on('click', function (e) {
        if ($("#selectDate").val() == "Live") {
            realTime = true;
            realTimeHeatMap();
        } else {
            historicalHeatMap();
        }
    });

    $("#btnCancel").on("click", function (e) {
        $('#camerasList').addClass("d-none");
        $('.cards-div').css('width', '75%');
        $('#btnUploadImg').addClass("d-none");
        $('#btnCancel').addClass("d-none");
        restDots();
        if ($("#selectDate").val() == "Live") {
            realTime = true;
            realTimeHeatMap();
        } else {
            historicalHeatMap();
        }
    });

    $('.btnHourContainer').on('click', function (e) {
        if ($(this).hasClass('active')) {
            $(this).css('opacity', '0.3');
            $(this).removeClass('active');

            $('#AllHoursContainer').css('backgroundColor', 'transparent');
            $('#AllHoursContainer').removeClass('active');
        } else {
            $(this).css('opacity', '1');
            $(this).addClass('active');
        }
        handleHourSelection();
    });

    $('#AllHoursContainer').on('click', function (e) {
        if ($(this).hasClass('active')) {
            $(this).css('backgroundColor', 'transparent');
            $(this).removeClass('active');
            $('.btnHourContainer').css('opacity', '0.3');
            $('.btnHourContainer').removeClass('active');

        } else {
            $(this).css('backgroundColor', '#a2a2a1');
            $(this).addClass('active');
            $('.btnHourContainer').css('opacity', '1');
            $('.btnHourContainer').addClass('active');
        }
        handleHourSelection();
    });

    function handleDaySelection() {
        SELECTED_DAYS = [];
        $(".btnDayContainer").each(function () {
            if ($(this).hasClass("active")) {
                SELECTED_DAYS.push($(this).attr("id"));
            }
        });
        renderHistoricalMapData();
    }

    function handleHourSelection() {
        SELECTED_HOURS = [];
        $(".btnHourContainer").each(function () {
            if ($(this).hasClass("active")) {
                SELECTED_HOURS.push($(this).attr("id"));
            }
        });
        renderHistoricalMapData();
    }

    function renderHistoricalMapData() {
        restDots();
        if (SELECTED_DAYS.length < 1 || SELECTED_HOURS.length < 1) {
            // _jsHelpers._handle_notify("danger", "No Data Provided For This Time Period!")
            return;
        }
        var selectedData = {};
        mapData.devices.forEach(device => {
            device.cameras.forEach(camera => {
                var tot = 0;
                SELECTED_DAYS.forEach(day => {
                    SELECTED_HOURS.forEach(hour => {
                        if (ALL_DATA[day][hour]) {
                            tot = tot + ALL_DATA[day][hour][camera.id];
                        }
                    });
                });
                selectedData[camera.id] = tot;
            });
            var count = Object.keys(selectedData).map(e => selectedData[e])
            var maxCount = Math.max(...count);
            var sumCount = count.reduce((partialSum, a) => partialSum + a, 0);
            Object.keys(selectedData).forEach((item, idx) => {
                mapData.devices.forEach(device => {
                    device.cameras.forEach(camera => {
                        if (camera.id == item) {
                            let map = camera.heatmaps.find(x => x.heatmap_id == mapData.id);
                            map.domain.forEach((dot) => {
                                // let x_max = count[idx];
                                // let x_min = count[idx] - (count[idx] * (count[idx] / sumCount));
                                let val = count[idx];//Math.floor(Math.random() * (x_max - x_min + 1)) + x_min;
                                $(`#${dot}`).css("backgroundColor", getColor(val / maxCount));
                                // $(`#${dot}`).css("boxShadow", `0 0 25px 20px ${getColor(val / maxCount)}`);
                                // $(`#${dot}`).css("borderRadius", '50%');
                                $(`#${dot}`).append(`<span class="tooltiptext">${map.name} : ${val}</span>`);
                            });
                        }
                    });
                });
            })
        });
    }



    realTimeHeatMap();

    const buttonRight = document.getElementById('slideRight');
    const buttonLeft = document.getElementById('slideLeft');

    buttonRight.onclick = function () {
        document.getElementById('daysContainer').scrollLeft += 20;
    };
    buttonLeft.onclick = function () {
        document.getElementById('daysContainer').scrollLeft -= 20;
    };
}

const handleDateValues = (date) => {
    var firstDay, lastDay
    let todayDate = new Date();
    switch (date) {
        case 'this-month':
            firstDay = new Date(todayDate.getFullYear(), todayDate.getMonth(), 2);
            lastDay = new Date(new Date().setDate(new Date().getDate()));
            return { firstDay, lastDay };
        case 'last-month':
            firstDay = new Date(todayDate.getFullYear(), todayDate.getMonth() - 1, 2);
            lastDay = new Date(todayDate.getFullYear(), todayDate.getMonth(), 1);
            return { firstDay, lastDay };
        case 'this-week':
            firstDay = getStartOfWeek();
            lastDay = getEndOfWeek();
            return { firstDay, lastDay };
        case 'last-week':
            let mydate = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
            firstDay = getStartOfWeek(mydate);
            lastDay = getEndOfWeek(mydate);
            return { firstDay, lastDay };
        case 'today':
            firstDay = todayDate;
            lastDay = todayDate;
            return { firstDay, lastDay };
        case 'yesterday':
            firstDay = new Date(new Date().setDate(new Date().getDate() - 1));
            lastDay = firstDay;
            return { firstDay, lastDay };
    }
    function getStartOfWeek(date) {
        // Copy date if provided, or use current date if not
        date = date ? new Date(+date) : new Date();
        date.setHours(25, 0, 0, 0);
        // Set date to previous Sunday
        date.setDate(date.getDate() - date.getDay());
        return date;
    }
    function getEndOfWeek(date) {
        date = getStartOfWeek(date);
        date.setDate(date.getDate() + 6);
        return date;
    }
};

$('#btnEdit').on('click', function (e) {
    realTime = false;
    animate();
    DATA.cameras = [];
    $('#camerasList').removeClass("d-none");
    $('.cards-div').css('width', '97%');
    $('#btnUploadImg').removeClass("d-none");
    $('.upload-image').removeClass("d-none");

    $('.uploadDiv').removeClass("d-none");
    
    $('#btnCancel').removeClass("d-none");
    $('#dotsContainer').css('backgroundColor', 'transparent');
    restDots();

    $(".cell-dot").off("mousedown", handleMoseDown);
    $(".cell-dot").on("mousedown", handleMoseDown);
    $(".cell-dot").off("mouseup", handelMouseUp);
    $(".cell-dot").on("mouseup", handelMouseUp);
    getCamerasByBranchID();
});

function handleMoseDown(e) {
    // if the user us editing the same camera domain
    let camera = DATA.cameras.find(e => e.camera_id == THIS_CAMERA.camera_id)
    if (camera) {
        camera.domain.forEach(cam => {
            $(`#${cam}`).css('backgroundColor', 'transparent');
            $(`#${cam}`).css("boxShadow", 'none');
            $(`#${cam}`).css("borderRadius", 'none');
            $(`#${cam}`).removeClass("red-area");
            $(`#${cam}`).data("value", 0);
            $(`#${cam}`).text("");
        });
    }
    THIS_CAMERA.domain = [];
    $(".cell-dot").on("mouseover", handelDotClick);
}

function handelMouseUp(e) {
    if (THIS_CAMERA.domain.length <= 1) {
        THIS_CAMERA.domain = [];
        $(".cell-dot").off("mouseover", handelDotClick);
        return;
    }
    var domain = {};
    THIS_CAMERA.domain.forEach(dot => {
        let row = Math.ceil((dot - 1) / 79);
        if (!domain[row]) {
            domain[row] = [];
        }
        domain[row] = diminsions(domain[row], dot);
    })
    THIS_CAMERA.domain = [];
    Object.keys(domain).forEach(row => {
        if (domain[row].length > 1) {
            let min = Math.min(...domain[row]);
            let max = Math.max(...domain[row])
            for (let i = min; i <= max; i++) {
                $(`#${i}`).css("backgroundColor", "green");
                $(`#${i}`).data("value", 1);
                THIS_CAMERA.domain.push(i)
            }
        }
    });

    if (THIS_CAMERA.camera_id != 0) {
        var isExist = false;
        DATA.cameras.forEach(item => {
            if (item.camera_id == THIS_CAMERA.camera_id) {
                item.camera_id = THIS_CAMERA.camera_id;
                item.domain = THIS_CAMERA.domain.map(e => e);
                isExist = true;
            }
        });
        if (!isExist) {
            DATA.cameras.push({ camera_id: THIS_CAMERA.camera_id, domain: THIS_CAMERA.domain.map(e => e) });
        }
    }
    $(".cell-dot").off("mouseover", handelDotClick);
}

function diminsions(diminsion, dotId) {
    dotId = Number(dotId)
    if (diminsion.length <= 1) {
        diminsion.push(dotId);
    } else {
        if (Math.min(...diminsion) > dotId) {
            let index = diminsion.indexOf(Math.min(...diminsion))
            diminsion[index] = dotId;
        } else if (Math.max(...diminsion) < dotId) {
            let index = diminsion.indexOf(Math.max(...diminsion))
            diminsion[index] = dotId;
        }
    }
    return diminsion;
}

$('#selectDate').on('change', function (e) {
    if (e.target.value == 5) {
        $('#customDateContainer').removeClass('d-none');
    } else {
        $('#customDateContainer').addClass('d-none');
        let chosenDate = handleDateValues(e.target.value);
        FILTER.from_date = moment(chosenDate.firstDay).format('DD/MM/YYYY');
        FILTER.to_date = moment(chosenDate.lastDay).format('DD/MM/YYYY');
        $(`#fromDate`).val(moment(chosenDate.firstDay).format('YYYY-MM-DD'));
        $(`#toDate`).val(moment(chosenDate.lastDay).format('YYYY-MM-DD'));
    }
});

$('#resolution').on('change', function (e) {
    FILTER.resolution = e.target.value;
});

function handelDotClick(e) {
    if (THIS_CAMERA.camera_id == 0) {
        alert("No Camera Selected");
        return false;
    }
    changeCameraDomain($(this).attr("id"));
    if ($(this).data("value") == 0) {
        $(this).css("backgroundColor", "gray");
        $(this).data("value", 1);
    } else {
        $(this).css("backgroundColor", "white");
        $(this).data("value", 0);
    }
}

function changeCameraDomain(dotID) {
    const index = THIS_CAMERA.domain.indexOf(dotID);
    if (index > -1) {
        THIS_CAMERA.domain.splice(index, 1);
    } else {
        THIS_CAMERA.domain.push(dotID);
    }
}

$('#btnSave').on('click', function (e) {
    DATA.name = $("#mapName").val();
    if (mapData.name == "") {
        _jsHelpers._handle_notify("danger", "Please Add Heat Map Name!");
        return false;
    }
    if (DATA.branch_id == 0 || DATA.map_url == "" || DATA.cameras.length == 0) {
        _jsHelpers._handle_notify("danger", "Wrong Data Input");
        return false;
    }
    $.ajax({
        type: "PUT",
        url: _jsHelpers._PREFIX_ADMIN_API + "/heatmap/" + DATA.id,
        data: DATA,
        headers: _jsHelpers._getHeaders(),
    }).done((res) => {
        // history.back();
        window.location.pathname = '/heatmaps'
    });
});


$("#btnUploadImg").dropzone({
    url: "/",
    autoProcessQueue: false,
    dictDefaultMessage: "",
    acceptedFiles: ".jpeg,.jpg,.png",
    maxFilesize: 2,
    maxThumbnailFilesize: 2,
    maxFiles: 1,
    init: function () {
        this.on("maxfilessizeexceeded", function (file) {
            this.removeFile(file);
            _jsHelpers._handle_notify("danger", "Max size 2MB.");
        });
        this.on("thumbnail", function (file) {
            this.removeFile(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (event) {
                var imagen_base64 = event.target.result;
                handleAJAXFoto(imagen_base64);
            };
            reader.onerror = function (error) {
                _jsHelpers._handle_notify("danger", error);
            };
        }); // FIN ADDED FILE
    },
});

var handleAJAXFoto = function (imagen_base64) {
    _jsHelpers._blockUI();
    $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + url_update_photo,
        headers: _jsHelpers._getHeaders(),
        data: {
            file: imagen_base64,
            branch_id: mapData.branch_id,
        },
    })
        .done(function (response) {
            if (!response.error) {
                $("#map-img").attr("src", response.data);
                mapData.map_url = response.data;
                _jsHelpers._handle_notify("success", response.message);
            } else {
                _jsHelpers._responseError(response);
            }

            _jsHelpers._unBlockUi();
        })
        .fail(function (err) {
            _jsHelpers._unBlockUi();
        });
};

function handleCameraClick(e) {
    THIS_CAMERA.camera_id = $(this).attr("id");
    THIS_CAMERA.domain = [];
    $(".btnCamera").css("backgroundColor", "transparent");
    $(this).css("backgroundColor", "rgb(161, 197, 35)");
}

$(".btnCamera").on("click", handleCameraClick);

function getCamerasByBranchID() {
    $.ajax({
        type: "GET",
        url: _jsHelpers._PREFIX_ADMIN_API + "/nodes/getDevicesByBranch",
        headers: _jsHelpers._getHeaders(),
        data: {
            "branch_id": mapData.branch_id
        }
    }).then((res) => {
        $("#branche_cameras").empty();
        if (!res.error) {
            if (res.data[0].cameras.length > 0) {
                 $('#devices').removeClass('d-none');

                res.data[0].cameras.forEach((item) => {
                    $("#branche_cameras").append(`<div class="btnCamera" id="${item.id}" style="border: 1px solid; display: flex; justify-content: center; margin: 3px;align-items: baseline;border-radius: 8px;cursor: pointer;">
                        <i class="bi bi-camera" style="font-size: 20px;margin-right: 10px;"></i>
                        <p>${item.name}</p>
                    </div>`);
                });
                THIS_CAMERA.camera_id = res.data[0].cameras[0].id
                THIS_CAMERA.domain = [];
                $(".btnCamera").css("backgroundColor", "transparent");
                $(`#${THIS_CAMERA.camera_id}`).css("backgroundColor", "rgb(161, 197, 35)");
                $(".btnCamera").on("click", handleCameraClick);
            } else {
                _jsHelpers._handle_notify(
                    "danger",
                    "No Cameras On This Branch"
                );
            }

        }
        _jsHelpers._unBlockUi();
    }).fail((error) => {
        _jsHelpers._unBlockUi();
        _jsHelpers._handle_notify(
            "danger",
            "Server Error : Can't Fetch Branche's Cameras"
        );
    });
}

function animate() {
    $(".cell-dot").map(function (idx) {
        setTimeout(() => {
            $(this).css('backgroundColor', 'red');
        }, 300)
    });

    $(".cell-dot").map(function (idx) {
        setTimeout(() => {
            $(this).css('backgroundColor', 'transparent');
        }, 300)
    });
}

init();
