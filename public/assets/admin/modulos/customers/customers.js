const CUSTOMER = (function () {
  const modalUpdate = $("#modal-update-counter");
  var company = [];
  var LOGGER_DATA;
  var USERS_DATA;

  const init = function () {
    renderCustomersCards(CUSTOMERS)
    $(document).on("change", "#add-user-customer #account-type", function () {
      var _this = $(this);
      if (_this.val() == "Premium") {
        $(".nodes").show();
      } else {
        $(".nodes").hide();
      }
    });

    $(document).on("click", ".openOptions", function () {
      $(this).parents(".card-user").find(".more-options").show();
    });

    $(document).on("click", ".closeOptions", function () {
      $(this).parents(".more-options").hide();
    });

    $('#update-all-company-counter').on('click', function (e) {
      company = [];
    });

    async function getCustomersLoggerData(data) {
      _jsHelpers._blockUI();
      await $.ajax({
        type: 'GET',
        url: _jsHelpers._PREFIX_ADMIN_API + '/user/loginHistory',
        headers: _jsHelpers._getHeaders(),
        data
      }).then(res => {
        _jsHelpers._unBlockUi();
        if (res.data.length > 0) {
          LOGGER_DATA = res.data.map((e, idx) => {
            return {
              "ID": idx + 1,
              // "User ID": e.user_id,
              "User Name": e.user_name ? e.user_name : "No Name",
              // "Company ID": e.company_id,
              "Company Name": e.company_name ? e.company_name : "Admin",
              "By": e.by,
              "Login At": e.login_at,
              "Logout At": e.logout_at,
              "Session Duration": e.session_duration,
            }
          });
        } else {
          LOGGER_DATA = [];
        }
        // LOGGER_DATA = res.data;
      });
    }

    async function initCustomerLogger() {
      _jsHelpers._blockUI();
      let today = new Date().toLocaleDateString();
      let todayFormate = `${today.split("/")[1]}/${today.split("/")[0]}/${today.split("/")[2]}`
      let data = {
        "user_ids": [], // optional
        "company_ids": [], // optional
        "by": "user", //either linkers or user (optional) 
        "resolution": "all", // login_at or logout_at
        "from_date": todayFormate,//"19/10/2022",
        "to_date": todayFormate,//"20/10/2022"
      }
      await getCustomersLoggerData(data)
      await $.ajax({
        type: "GET",
        url: _jsHelpers._PREFIX_ADMIN_API + "/users",
        headers: _jsHelpers._getHeaders(),
      }).done((res) => {
        if (!res.error) {
          USERS_DATA = res
        }
        _jsHelpers._unBlockUi();
      });

      $("#companies").select2({
        data: CUSTOMERS.map(e => { return { id: e.company_id, text: e.name } }),
        dropdownAutoWidth: true,
        multiple: true,
        width: '50%',
        height: '30px',
        allowClear: true,
        cache: true,
        closeOnSelect: false,
      });
      $("#users").select2({
        data: USERS_DATA.map(e => { return { id: e.id, text: e.name } }),
        dropdownAutoWidth: true,
        multiple: true,
        width: '50%',
        height: '30px',
        placeholder: `${_jsHelpers._trans("selectUsers")}`,
        allowClear: true,
        cache: true,
        closeOnSelect: false,
      });

      document.getElementById('from_date').valueAsDate = new Date();
      document.getElementById('to_date').valueAsDate = new Date();

      $("#companies").val(CUSTOMERS.map(e => e.company_id)).trigger("change");
      $("#users").val(USERS_DATA.map(e => e.id)).trigger("change");
      $("#allCompanies").prop("checked", true);
      $("#allUsers").prop("checked", true);
    }

    $(".btnApply").on("click", function (e) {
      let from_date = $("#from_date").val();
      let fromDateFormate = `${from_date.split("-")[2]}/${from_date.split("-")[1]}/${from_date.split("-")[0]}`
      let to_date = $("#to_date").val();
      let toDateFormate = `${to_date.split("-")[2]}/${to_date.split("-")[1]}/${to_date.split("-")[0]}`

      var data = {
        "user_ids": $("#users").val(), // optional
        "company_ids": $("#companies").val(), // optional
        "by": $("#by").val(), //either linkers or user (optional) 
        "resolution": $("#resolution").val(), // login_at or logout_at
        "from_date": fromDateFormate,//"19/10/2022",
        "to_date": toDateFormate,//"20/10/2022"
      }
      getCustomersLoggerData(data).then(() => {
        renderTable(LOGGER_DATA);
      })
    });

    $("#allCompanies").change(function (e) {
      if (this.checked) {
        $("#companies").val(CUSTOMERS.map(e => e.company_id)).trigger("change");
      } else {
        $("#companies").val([]).trigger("change");
      }
    })
    $("#allUsers").change(function (e) {
      if (this.checked) {
        $("#users").val(USERS_DATA.map(e => e.id)).trigger("change");
      } else {
        $("#users").val([]).trigger("change");
      }
    })

    $('.ShowUsersLogger').on('click', async function (e) {
      if ($("#customersLoggerContainer").css("display") == 'none') {
        console.log('none');
        await initCustomerLogger().then(async ()=>{
          await renderTable(LOGGER_DATA);
        });
        $(".ShowUsersLogger").text(`${_jsHelpers._trans("customers")}`);
        $("#customersLoggerContainer").show();
        $("#customersContainer").hide();
      } else {
        console.log('not none');

        $(".ShowUsersLogger").text(`${_jsHelpers._trans("customerlogger")}`);
        $("#customersContainer").show();
        $("#customersLoggerContainer").hide();
       
      }

    })

    $(document).on("click", ".updateCompanyCounters", function () {
      var userId = $(this).data("id-usuario");
      var parent = $("#card-" + userId);
      company = [];
      company.push(parent.data("company-id"));
      modalUpdate.modal();
    });

    modalUpdate.on('submit', function (e) {
      e.preventDefault();
      if ($('#from-date').val() == "" || $('#from-date').val().length != 8) {
        _jsHelpers._handle_notify("danger", "Wrong Data Input !");
        return;
      }
      if ($('#to-date').val() == "" || $('#to-date').val().length != 8) {
        _jsHelpers._handle_notify("danger", "Wrong Data Input !");
        return;
      }
      let data = company.length > 0 ? {
        "company_ids": company,
        "from": $('#from-date').val(),
        "to": $('#to-date').val(),
      } : {
        "from": $('#from-date').val(),
        "to": $('#to-date').val(),
      }
      $.ajax({
        type: 'POST',
        url: _jsHelpers._PREFIX_ADMIN_API + '/updateCompanyData',
        headers: _jsHelpers._getHeaders(),
        data,
      }).then(res => { });
      _jsHelpers._handle_notify("success", "Counters Will be Updated");
      modalUpdate.modal("hide");
    })

    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

    $(document).on("click", "#add-user-customer .item-menu", function () {
      var _this = $(this);
      var target = _this.data("target");

      $("#add-user-customer .item-menu").removeClass("active");
      _this.addClass("active");
      $("#add-user-customer .contenido").hide();

      $("#add-user-customer ." + target).show();
    });

    $(document).on("click", "#add-user-customer .btn-continue", function () {
      $("#add-user-customer .contenido").hide();
      $("#add-user-customer .container-company").show();
      $("#add-user-customer .item-menu").removeClass("active");
      $(
        "#add-user-customer .item-menu[data-target='container-company']"
      ).addClass("active");
    });

    $(document).on("click", "#edit-user-customer .item-menu", function () {
      var _this = $(this);
      var target = _this.data("target");

      $("#edit-user-customer .item-menu").removeClass("active");
      _this.addClass("active");
      $("#edit-user-customer .contenido").hide();

      $("#edit-user-customer ." + target).show();
    });

    $(document).on("click", "#edit-user-customer .btn-continue", function () {
      $("#edit-user-customer .contenido").hide();
      $("#edit-user-customer .container-company").show();
      $("#edit-user-customer .item-menu").removeClass("active");
      $(
        "#edit-user-customer .item-menu[data-target='container-company']"
      ).addClass("active");
    });

    $(document).on("click", ".btn-filter-all-status", function () {
    });
    $(document).on("click", ".btn-filter-active", function () {
    });
    $(document).on("click", ".btn-filter-inactive", function () { });

    $("#add-customer").on("shown.bs.modal", function () {
      $("#add-user-customer")[0].reset();
    });

    $(document).on("keyup", ".search", function (e) {
      var filtereCustomers = []
      CUSTOMERS.forEach(customer => {
        if (customer.name.toLowerCase().includes(e.target.value.toLowerCase()) ||
          customer.lastname.toLowerCase().includes(e.target.value.toLowerCase()) ||
          customer.company.name.toLowerCase().includes(e.target.value.toLowerCase())) {
          filtereCustomers.push(customer);
        }
      });

      renderCustomersCards(filtereCustomers)
    })

    initModalAddUserForm();
    initModalProfile();

    initEditInfoForm();
    initModalEditUser();

    initUpdatePasswordForm();
    initAddFFcAccount();

    initModalUpdatePassword();
    initModalAddFFcEmail();

    initModalUpdateStatus();
    initUpdateStatusForm();

    initModalDeleteUser();
    initDeleteUserForm();

    initImpersonate();

    initModalUpdatePremium();
    initUpdatePremiumForm();
  };

  const renderCustomersCards = function (CUSTOMERS_Array) {
    $("#customersContainer").empty();
    if (CUSTOMERS_Array.length == 0) {
      $("#customersContainer").append(`
      <div class="container-no-data" style="">
          <p class="title">${_jsHelpers._trans('noOneHere')}</p>
          <p>${_jsHelpers._trans('noOneHereMsg')}</p>
          <button class="btn-dmg text-green" data-toggle="modal" data-target="#add-customer" class="">
          ${_jsHelpers._trans('addCustomer')}
          </button>
      </div>
      `)
    }
    CUSTOMERS_Array.forEach(user => {
      $("#customersContainer").append(`
      <div id="card-${user.id}" style="min-height: 190px;padding-right: 0px ;" class="col-xl-3 col-lg-3 col-md-4 col-sm-1 pb-1" 
  data-name="${user.name}" 
  data-lastname="${user.lastname}" 
  data-email="${user.email}" 
  data-job-title="${user.job_title}" 
  data-status="${user.active}" 
  data-photo="${user.photo}" 
  data-roles="{{json_encode($user.roles'])}}" 
  data-company-name="${user.company.name}" 
  data-company-id="${user.company.id}" 
  data-company-number="${user.company.vat}" 
  data-company-username="${user.company.ffc_username}" 
  data-company-password="${user.company.ffc_password}" 
  data-account="{{json_encode($user.account'])}}">
      <div class="card-user" style="">
          <div class="options">
              <div class="badge badge-success">
                  ${user.company.name}
              </div>
              <i class="bx bx-dots-vertical-rounded openOptions"></i>
          </div>
          <div data-id-usuario="${user.id}" class="openProfileModal">
              <div class="profile mb-1">
                  ${user.photo ? `<img src="${user.photo}" class="mr-1" alt=""  >` : '<img src="/assets/admin/img/misc/user_active.png" class="mr-1" alt="">'}
                  <div class="data">
                      <p class="-info m-0 w-600 -complete-name">${user.name} ${user.lastname}
                          ${user.active ? '<span class="bullet bullet-xs bullet-success -bullet-status"></span>' : '<span class="bullet bullet-xs bullet-danger -bullet-status"></span>'}
                      </p>
                      <p class="-info m-0 -job-title" style=" font-size: 10px">${user.job_title}</p>
                  </div>
              </div>
              <div class="more-data">
                  <p class="m-0 -email" style="">${user.email}</p>
              </div>
          </div>
          <div class="more-options" style="display: none">
              <div class="title">
                  <span>${_jsHelpers._trans('options')}</span>
                  <i class="bx bx-x closeOptions"></i>
              </div>
              <p data-id-usuario="${user.id}" class="text-success loginImpersonate">${_jsHelpers._trans('accessToThisUser')}</p>
              <p data-id-usuario="${user.id}" class="text-success updateCompanyCounters">${_jsHelpers._trans('updateCustomer')}</p>
              <p data-id-usuario="${user.id}" data-id-company="${user.company.id}" class="text-success editCustomer">${_jsHelpers._trans('editCustomerInfo')}</p>
              <p data-id-usuario="${user.id}" data-id-company="${user.company.id}" class="text-success addfootfallAccount">${_jsHelpers._trans('addFootfallCredential')}</p>
              <p data-id-usuario="${user.id}" class="text-success updatePassword">${_jsHelpers._trans('updatePassword')}</p>
              <p data-id-usuario="${user.id}" class="text-success updatePremium">${_jsHelpers._trans('updateToPremium')}</p>
              <p data-id-usuario="${user.id}" class="text-success updateStatus">${_jsHelpers._trans('changeStatus')}</p>
              <p data-id-usuario="${user.id}" class="text-danger deleteUser">${_jsHelpers._trans('deleteCustomer')}</p>
          </div>
      </div>
  </div>
      `);
    })
  }

  const initModalUpdatePremium = function () {
    $(document).on("click", ".updatePremium", function (e) {
      var userId = $(this).data("id-usuario");
      $("#form-update-premium .user-id").val(userId);

      $("#modal-update-premium").modal("show");
    });
  };

  const initUpdatePremiumForm = function () {
    $("#form-update-premium").on("submit", function (e) {
      e.preventDefault();

      var userId = $("#form-update-premium .user-id").val();
      var parent = "#card-" + userId + " ";
      var $parent = $("#card-" + userId);

      _jsHelpers._blockUI();
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/account/validate",
        headers: _jsHelpers._getHeaders(),
        data: {
          user_id: userId,
        },
      })
        .done((res) => {
          $(parent + ".closeOptions").click();

          if (!res.error) {
            _jsHelpers._handle_notify("success", res.message);
          } else {
            _jsHelpers._responseError(res);
          }
          $("#modal-update-premium").modal("hide");
        })
        .always((res) => {
          $("#modal-update-premium").modal("hide");
          _jsHelpers._unBlockUi();
        });
    });
  };

  const updateCantCards = function () {
    var cantCards = $(".card-user").length;
    cantCards =
      cantCards == 1 ? cantCards + `${_jsHelpers._trans('customer')}` : cantCards + `${_jsHelpers._trans('customers')}`;
    $(".-cant-card").text(cantCards);
  };

  const initAddFFcAccount = function () {
    $("#add_FFc_email_form").on("submit", function (e) {
      e.preventDefault();

      var password = $("#add_FFc_email_form #password").val();
      var username = $("#add_FFc_email_form #username").val();
      var userId = $("#add_FFc_email_form #p-user-id").val();
      var parent = "#card-" + userId + " ";


      var companyId = $("#add_FFc_email_form #id-company").val();

      _jsHelpers._blockUI();
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/company/" + companyId,
        headers: _jsHelpers._getHeaders(),
        data: {
          ffc_username: username,
          ffc_password: password,
        },
      })
        .done((res) => {

          $(parent + ".closeOptions").click();

          if (!res.error) {
            _jsHelpers._handle_notify("success", res.message);
          } else {

            _jsHelpers._responseError(res);
          }
          $("#add_FFc_account").modal("hide");
        })
        .always((res) => {
          _jsHelpers._unBlockUi();
          $("#add_FFc_account").modal("hide");
        });
    });
  };

  const initUpdatePasswordForm = function () {
    $("#update-password-customer").on("submit", function (e) {
      e.preventDefault();

      var password = $("#update-password-customer #new_password").val();
      var password_confirmation = $(
        "#update-password-customer #confirm_password"
      ).val();
      var userId = $("#update-password-customer #p-user-id").val();
      var parent = "#card-" + userId + " ";

      _jsHelpers._blockUI();
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/user/updatePassword",
        headers: _jsHelpers._getHeaders(),
        data: {
          password: password,
          password_confirmation: password_confirmation,
          userId: userId,
        },
      })
        .done((res) => {
          $(parent + ".closeOptions").click();

          if (!res.error) {
            _jsHelpers._handle_notify("success", res.message);
          } else {
            _jsHelpers._responseError(res);
          }
          $("#update-password").modal("hide");
        })
        .always((res) => {
          _jsHelpers._unBlockUi();
          $("#update-password").modal("hide");
        });
    });
  };

  const initModalUpdatePassword = function () {
    $(document).on("click", ".updatePassword", function (e) {
      var userId = $(this).data("id-usuario");
      $("#update-password-customer #new_password").val("");
      $("#update-password-customer #confirm_password").val("");
      $("#update-password-customer #p-user-id").val(userId);

      $("#update-password").modal("show");
    });
  };

  const initModalAddFFcEmail = function () {
    $(document).on("click", ".addfootfallAccount", function (e) {
      var userId = $(this).data("id-usuario");
      var companyId = $(this).data("id-company");
      var $parent = $("#card-" + userId);
      var companyUsername = $parent.data("company-username");

      $("#add_FFc_email_form #username").val(companyUsername);
      $("#add_FFc_email_form #p-user-id").val(userId);
      $("#add_FFc_email_form #id-company").val(companyId);

      $("#add_FFc_account").modal("show");
    });
  };

  const renderTable = function (data, columns = []) {
    $('#customersLoggerTable').dxDataGrid({
      dataSource: data ? data : orders,
      keyExpr: 'ID',
      columnsAutoWidth: true,
      allowColumnResizing: true,
      allowColumnReordering: true,
      filterRow: { visible: true },
      grouping: {
        autoExpandAll: false,
      },
      groupPanel: {
        visible: true,
      },
      headerFilter: { visible: true },
      filterBuilderPopup: {
        position: {
          of: window,
          at: 'top',
          my: 'top',
          offset: { y: 10 },
        },
      },
      showRowLines: true,
      showBorders: true,
      customizeColumns(columns) {
        columns[0].width = 70;
        columns[0].caption = '#';
      },
      // scrolling: {
      //   rowRenderingMode: 'virtual',
      // },
      paging: {
        pageSize: 10,
      },
      pager: {
        visible: true,
        allowedPageSizes: [10, 'all'],
        showPageSizeSelector: true,
        showInfo: true,
        showNavigationButtons: true,
      },
      sortByGroupSummaryInfo: [{
        summaryItem: 'count',
      }],
      summary: {
      },
      export: {
        enabled: true,
      },
      onExporting(e) {
        const workbook = new ExcelJS.Workbook();
        const worksheet = workbook.addWorksheet('Linkers Analytics Center');

        DevExpress.excelExporter.exportDataGrid({
          component: e.component,
          worksheet,
          autoFilterEnabled: true,
        }).then(() => {
          workbook.xlsx.writeBuffer().then((buffer) => {
            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `${$("#companyName")[0].innerText + new Date().toLocaleDateString().split("/").join("")}` + '.xlsx');
          });
        });
        e.cancel = true;
      },
    });
  };


  /**
   * showpass text
   */
  const passwordShow = document.querySelector('.showPass');

  $("#togglePassword").on("click", function (e) {
    const type = passwordShow.getAttribute('type') === 'password' ? 'text' : 'password';
    passwordShow.setAttribute('type', type);
    this.classList.toggle('bi-eye');
  });


  const passwordShow1 = document.querySelector('.showPass1');
  $("#togglePassword1").on("click", function (e) {
    const type = passwordShow1.getAttribute('type') === 'password' ? 'text' : 'password';
    passwordShow1.setAttribute('type', type);
    this.classList.toggle('bi-eye');
  });


  const passwordShow2 = document.querySelector('.showPass2');
  $("#togglePassword2").on("click", function (e) {
    const type = passwordShow2.getAttribute('type') === 'password' ? 'text' : 'password';
    passwordShow2.setAttribute('type', type);
    this.classList.toggle('bi-eye');
  });


  ///////////////////// end show pass click   


  const initImpersonate = function () {
    $(document).on("click", ".loginImpersonate", function (e) {
      var userId = $(this).data("id-usuario");
      _jsHelpers._blockUI();
      $.ajax({
        type: "POST",
        url: "/admin/api/loginImpersonate",
        // url: _jsHelpers._PREFIX_ADMIN_API + '/user/impersonate',
        // headers: _jsHelpers._getHeaders(),
        data: {
          _token: _jsHelpers._token(),
          user_id: userId,
        },
      })
        .done((res) => {
          if (!res.error) {
            localStorage.setItem("_myToken", res.access_token);
            _jsHelpers._handle_notify("success", res.message);

            location.href = "/";
          } else {
            _jsHelpers._responseError(res);
          }
        })
        .always((res) => {
          _jsHelpers._unBlockUi();
        });
    });
  };

  const initModalUpdateStatus = function () {
    $(document).on("click", ".updateStatus", function (e) {
      var userId = $(this).data("id-usuario");
      var $parent = $("#card-" + userId);

      var details = $parent.data("details");
      var status = $parent.data("status");
      var stringStatus = status ? "Active" : "Inactive";
      $("#update-status-customer .details").val(details);
      $("#update-status-customer #current_status").val(stringStatus);
      $("#update-status-customer .select-user-state").val(status);
      $("#update-status-customer .p-user-id").val(userId);

      $("#update-status").modal("show");
    });
  };

  const initUpdateStatusForm = function () {
    $("#update-status-customer").on("submit", function (e) {
      e.preventDefault();

      var details = $("#update-status-customer .details").val();
      var status = $("#update-status-customer .select-user-state").val();
      var userId = $("#update-status-customer .p-user-id").val();
      var parent = "#card-" + userId + " ";
      var $parent = $("#card-" + userId);

      _jsHelpers._blockUI();
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/user/updateStatus",
        headers: _jsHelpers._getHeaders(),
        data: {
          status: status,
          user_id: userId,
          details: details,
        },
      })
        .done((res) => {
          $(parent + ".closeOptions").click();

          if (!res.error) {
            $parent.data("details", details);
            $parent.data("status", status);

            if (parseInt(status)) {
              $(parent + ".-bullet-status").removeClass("bullet-danger");
              $(parent + ".-bullet-status").addClass("bullet-success");
            } else {
              $(parent + ".-bullet-status").removeClass("bullet-success");
              $(parent + ".-bullet-status").addClass("bullet-danger");
            }

            _jsHelpers._handle_notify("success", res.message);
          } else {
            _jsHelpers._responseError(res);
          }
          $("#update-status").modal("hide");
        })
        .always((res) => {
          $("#update-status").modal("hide");
          _jsHelpers._unBlockUi();
        });
    });
  };

  const initModalDeleteUser = function () {
    $(document).on("click", ".deleteUser", function (e) {
      var userId = $(this).data("id-usuario");
      $("#delete-user-customer .p-user-id").val(userId);

      $("#delete-user").modal("show");
    });
  };

  const initDeleteUserForm = function () {
    $("#delete-user-customer").on("submit", function (e) {
      e.preventDefault();

      var userId = $("#delete-user-customer .p-user-id").val();
      _jsHelpers._blockUI();
      $.ajax({
        type: "DELETE",
        url: _jsHelpers._PREFIX_ADMIN_API + "/user/" + userId,
        headers: _jsHelpers._getHeaders(),
      })
        .done((res) => {
          if (!res.error) {
            $("#card-" + userId).remove();

            if ($(".card-content .row .card-user").length == 0) {
              $(".container-no-data").show();
            }
            updateCantCards();

            _jsHelpers._handle_notify("success", res.message);
          } else {
            _jsHelpers._responseError(res);
          }
          $("#delete-user").modal("hide");
        })
        .always((res) => {
          $("#delete-user").modal("hide");
          _jsHelpers._unBlockUi();
        });
    });
  };

  const initModalEditUser = function () {
    $(document).on("click", ".editCustomer", function (e) {
      var userId = $(this).data("id-usuario");
      var companyId = $(this).data("id-company");
      var $parent = $("#card-" + userId);

      var name = $parent.data("name");
      var lastName = $parent.data("lastname");
      var email = $parent.data("email");
      var jobTitle = $parent.data("job-title");
      var companyName = $parent.data("company-name");
      var companyNumber = $parent.data("company-number");

      $("#edit-user-customer #e-firstname").val(name);
      $("#edit-user-customer #e-lastname").val(lastName);
      $("#edit-user-customer #e-email").val(email);
      $("#edit-user-customer #e-job").val(jobTitle);
      $("#edit-user-customer #e-company-name").val(companyName);
      $("#edit-user-customer #e-company-number").val(companyNumber);

      $("#edit-user-customer #e-user-id").val(userId);
      $("#edit-user-customer #e-company-id").val(companyId);

      $("#edit-customer").modal("show");
    });
  };

  const initEditInfoForm = function () {
    $("#edit-user-customer").on("submit", function (e) {
      e.preventDefault();

      var firstname = $("#edit-user-customer #e-firstname").val();
      var lastname = $("#edit-user-customer #e-lastname").val();
      var email = $("#edit-user-customer #e-email").val();
      var job = $("#edit-user-customer #e-job").val();
      var companyName = $("#edit-user-customer #e-company-name").val();
      var companyNumber = $("#edit-user-customer #e-company-number").val();
      var userId = $("#edit-user-customer #e-user-id").val();
      var companyId = $("#edit-user-customer #e-company-id").val();
      var parent = "#card-" + userId + " ";
      var $parent = $("#card-" + userId);
      _jsHelpers._blockUI();
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/customer/update",
        headers: _jsHelpers._getHeaders(),
        data: {
          firstname: firstname,
          lastname: lastname,
          email: email,
          job: job,
          userId: userId,
          company_name: companyName,
          company_vat_number: companyNumber,
          company_id: companyId,
        },
      })
        .done((res) => {
          if (!res.error) {
            $(parent + ".closeOptions").click();

            $parent.data("name", firstname);
            $parent.data("lastname", lastname);
            $parent.data("email", email);
            $parent.data("job-title", job);

            $(parent + ".-complete-name").text(firstname + " " + lastname);
            $(parent + ".-email").text(email);
            $(parent + ".-job-title").text(job);

            _jsHelpers._handle_notify("success", res.message);
          } else {
            _jsHelpers._responseError(res);
          }
          $("#edit-customer").modal("hide");
        })
        .always((res) => {
          $("#edit-customer").modal("hide");
          _jsHelpers._unBlockUi();
        });
    });
  };

  const initModalProfile = function () {
    $(document).on("click", ".openProfileModal", function (e) {
      var userId = $(this).data("id-usuario");
      var $parent = $("#card-" + userId);

      var name = $parent.data("name");
      var lastName = $parent.data("lastname");
      var email = $parent.data("email");
      var jobTitle = $parent.data("job-title");
      var active = $parent.data("status");
      var photo = $parent.data("photo");
      var roles = $parent.data("roles");
      var account = $parent.data("account");

      const status = active ? "active" : "inactive";

      if (roles.length) {
        $("#profile-team-role").text(roles[0].name);
      } else {
        $("#profile-team-role").text("Not assigned");
      }

      if (photo) {
        $("#profile-team-img").attr("src", photo);
      } else {
        $("#profile-team-img").attr(
          "src",
          "/assets/admin/img/misc/user_active.png"
        );
      }

      $(".account-type").text(account.account_type);
      $("#profile-team-job").text(jobTitle);
      $("#profile-team-status")
        .removeClass("active, inactive")
        .addClass(status);
      $("#profile-team-name").text(name + " " + lastName);
      $("#profile-team-email").text(email);

      if (active) {
      } else {
      }

      $(".profile-team").modal("show");
    });
  };

  const initModalAddUserForm = function () {
    $("#add-user-customer").on("submit", function (e) {
      e.preventDefault();

      var firstname = $("#add-user-customer #firstname").val();
      var lastname = $("#add-user-customer #lastname").val();
      var job = $("#add-user-customer #job").val();
      var role = "Company manager";
      var email = $("#add-user-customer #email").val();
      var password = $("#add-user-customer #password").val();
      var company = $("#add-user-customer #company-name").val();
      var companyNumber = $("#add-user-customer #company-number").val();

      var account_type = $("#add-user-customer #account-type").val();
      var industry_id = $("#add-user-customer #industry").val();
      var nodes = $("#add-user-customer #nodes").val();


      _jsHelpers._blockUI();
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/customer/create",
        headers: _jsHelpers._getHeaders(),
        data: {
          firstname: firstname,
          lastname: lastname,
          job: job,
          role: role,
          email: email,
          password: password,
          company_name: company,
          company_vat_number: companyNumber,
          account_type: account_type,
          industry_id: industry_id,
          cant_nodes: nodes,
        },
      })
        .done((res) => {
          if (!res.error) {
            getNewUser(res);
            _jsHelpers._handle_notify("success", res.message);
            $("#add-customer").modal("hide");
          } else {
            _jsHelpers._responseError(res);
          }
        })
        .always((res) => {
          _jsHelpers._unBlockUi();
        });
    });
  };

  var getNewUser = function (res) {
    $(".container-no-data").hide();

    var data = res.data;

    var userId = data.id;
    var active = data.active;
    var firstname = data.name;
    var lastname = data.lastname;
    var completeName = data.name + " " + data.lastname;
    var created_at = data.created_at;
    var roles = data.roles;
    var email = data.email;
    var email_verified_at = data.email_verified_at;
    var jobTitle = data.job_title;

    var companyId = data.company.id;
    var companyName = data.company.company_name;
    var companyNumber = data.company.company_vat_number;

    var account = data.account;

    var photo = "/assets/admin/img/misc/user_active.png";

    var stringRol = "";
    if (roles.length > 0) {
      stringRol = roles[0].name;
    } else {
      stringRol = "Not assigned";
    }

    var bulletActive = "";
    if (active) {
      bulletActive = "bullet-success";
    } else {
      bulletActive = "bullet-danger";
    }


    var footer = ``;
    if (active && email_verified_at) {
      footer = `
<!--            <p class="m-0 date-active" style="color: var(--fontColor); font-size: 9px">Active since {{$date->format('M')}}, {{$date->format('d')}} <sup>th</sup>, {{$date->format('Y')}} at {{$date->format('H:i a')}} </p>-->
            `;
    }

    var stringRoles = roles.toString();

    var tpl = `
            <div id="card-${userId}" class="col-xl-3 col-lg-3 col-md-4 col-sm-1 pb-1"
                data-name="${firstname}"
                data-lastname="${lastname}"
                data-email="${email}"
                data-job-title="${jobTitle}"
                data-details = ""
                data-status="${active}"
                
                data-photo="${photo}"
                data-roles='${JSON.stringify(roles)}'
                data-company-name="${companyName}"
                data-company-number="${companyNumber}"
                
                data-account='${JSON.stringify(account)}'


            >
                <div class="card-user" style="">
                    <div class="options">
                        <div class="badge badge-success">
                            ${stringRol}
                        </div>
                        <i class="bx bx-dots-vertical-rounded openOptions"></i>
                    </div>
                    <div data-id-usuario="${userId}"  class="openProfileModal">
                        <div class="profile mb-1">
                                <img src="${photo}" class="mr-1" alt="" >
                            <div class="data">
                                <p class="-info m-0 w-600 -complete-name">${completeName}
                                    <span class="bullet bullet-xs ${bulletActive} -bullet-status"></span>
                                </p>
                                <p class="-info m-0 -job-title" style="color: var(--fontColor); font-size: 10px">${jobTitle}</p>
                            </div>
                        </div>
                        <div class="more-data">
                            <p class="m-0 -email" style="color: var(--fontColor);">${email}</p>
                            ${footer}
                        </div>
                    </div>
                    <div class="more-options" style="display: none">
                        <div class="title">
                            <span>Options</span>
                            <i class="bx bx-x closeOptions"></i>
                        </div>
                        <p data-id-usuario="${userId}" class="text-success loginImpersonate">Login in as this user</p>
                        <p data-id-usuario="${userId}" data-id-company="${companyId}" class="text-success editcustomer">Edit customer info</p>
                        <p data-id-usuario="${userId}" class="text-success updatePassword">Update password</p>
                        <p data-id-usuario="${userId}" class="text-success updateStatus" >Change status</p>
                        <p data-id-usuario="${userId}" class="text-danger deleteUser">Delete customer</p>
                    </div>
                </div>
            </div>
           
       `;
    $(".card-content .row").append(tpl);
    updateCantCards();
  };

  return {
    init: function () {
      init();
    },
  };
})();

$(document).ready(function () {
  CUSTOMER.init();
});
