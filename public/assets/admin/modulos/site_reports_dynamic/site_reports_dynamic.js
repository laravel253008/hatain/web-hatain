DYANMIC_TYPE = "report";

$(document).on("mouseup", handleMouseUpToHideWidgetFilter);

$('.btnDownloadPDF').on('click', e => {
    _jsHelpers._blockUI();

    new Promise(function (resolve, reject) {
        try {
            $("#header").removeClass("d-none");
            setTimeout(function () {
                var element = document.getElementById('report');
                $('.apexcharts-legend-text').css({'color':'white '});
                $('.apexcharts-text').css({'fill':'white '});
                $('.apexcharts-toolbar').css({'display':'none'});
                
                var opt = {
                    margin: 0,
                    filename: `Report-${new Date().toDateString()}.pdf`,
                    image: {
                        type: "jpeg",
                        quality: 0.98
                    },
                    pagebreak: {
                        mode: "css",
                        before: ".breackMode"
                    },
                    html2canvas: {
                        scale: 1
                    },
                    jsPDF: {
                        unit: "cm",
                        format: "A2",
                        orientation: "portrait"
                    },
                };

                html2pdf(element, opt);

                resolve("anything");
            }, 500)
        } catch (error) {
            reject(error);
        }
    }).then(() => {
        $("#header").addClass("d-none");
        _jsHelpers._unBlockUi();
    })
});
$('#templetsList').on('change', function (e) {
    getTempletData(e.target.value);
});
$("#templetsList").select2({
    dropdownAutoWidth: true,
    width: "100%",
    placeholder: `${_jsHelpers._trans("selectReport")}`,

});

getTempletData().then(() => {
    init().then(() => {
        handleModalsLogic();
        handleDashboardLogic();
    });
})