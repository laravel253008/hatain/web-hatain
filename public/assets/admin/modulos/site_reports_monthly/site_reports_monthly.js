var _PREFIX_ADMIN_API = _jsHelpers._PREFIX_ADMIN_API;
var urlChart = _PREFIX_ADMIN_API + "/reports/company_monthly";
var chart;
var SELECTED_VARIABLES = [];
var ALL_COMPANY_BRANCHES = [];
var ALL_COMPANY_GROUPS = [];
var SELECTED_BRANCHES = [];
var allData;
var FILTERS = {
  branch_ids: [],
  time_lapse: "this_month",
  type: "branches",
  from_date: '01/02/2022',
  to_date: '28/02/2022',
};
var linkersGraphColors = ['#a1c523', '#EA7268', '#CC549F', '#EE9A3A', '#5EC3AD', '#17A9DD']

function Num2Time(stay_time) {
  min = stay_time - Math.floor(stay_time / 60) * 60
  sec = (Number(min - Math.floor(min)).toFixed(2)) * 60;
  return `${String(Math.floor(stay_time / 60)).padStart(2, "0")}:${String(Math.floor(min)).padStart(2, "0")}:${String(Math.floor(sec)).padStart(2, "0")}`
}

function getMonthDate(m,y) {
  m = m - 1;
  var date = new Date();
  var firstDay = new Date(y, m, 1);
  var lastDay = new Date(y, m + 1, 0);
  let arr1 = firstDay.toDateString().split(" ");
  let arr2 = lastDay.toDateString().split(" ");
  return {
    firstDay: `${arr1[2]}/${getMonthFromString(arr1[1])}/${arr1[3]}`,
    lastDay: `${arr2[2]}/${getMonthFromString(arr2[1])}/${arr2[3]}`
  };
}

function getMonthFromString(mon) {

  var d = Date.parse(mon + "1, 2012");
  if (!isNaN(d)) {
    return new Date(d).getMonth() + 1;
  }
  return -1;
}

var sparkelOptions = {
  series: [{
    data: [],
  }],
  chart: {
    type: 'area',
    height: 150,
    // width:150,
    sparkline: {
      enabled: true
    },
  },
  stroke: {
    curve: 'smooth'
  },
  fill: {
    opacity: 0.3,
  },
  yaxis: {
    min: 0
  },
  colors: ['#a1c523'],
  title: {
    text: '0',//'$424,652',
    offsetX: 0,
    style: {
      fontSize: '12px',
      fontFamily: 'tahoma',
      color: '#525352'
    }
  },
  subtitle: {
    text: '',//'Sales',
    offsetX: 0,
    style: {
      fontSize: '18px',
      fontFamily: 'tahoma',
      color: '#525352'
    }
  }
};

var barChartOptions = {
  series: [{
    data: []
  }],
  chart: {
    type: 'bar',
    height: 350,
    zoom: {
      enabled: false,
    },
    toolbar: {
      show: false
    }
  },
  tooltip: {
    enabled: false
  },
  plotOptions: {
    bar: {
      borderRadius: 4,
      horizontal: false,
    }
  },
  dataLabels: {
    enabled: true
  },
  colors: linkersGraphColors,
  xaxis: {
    categories: ['Visitors', 'Lost Customers', 'Customers', 'Outside Traffic', 'New Customers', 'Returning Customers', 'Stay Time', 'Turn In Rate', 'Sales Conversion'],
  },
};

var bubbleOptions = {
  series: [
    {
      name: "SAMPLE A",
      data: []
    },
    {
      name: "SAMPLE B",
      data: []
    },
    {
      name: "SAMPLE C",
      data: []
    }
  ],
  colors: linkersGraphColors,
  chart: {
    height: 350,
    type: "scatter",
    zoom: {
      enabled: false
    },
    toolbar: {
      show: false
    }
  },
  dataLabels: {
    enabled: true,
    formatter: function (val) {
      return parseFloat(val).toFixed(0);
    },
  },
  tootip: {
    enabled: false
  },
  xaxis: {
    categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '']
  },
  yaxis: {
    labels: {
      formatter: function (val) {
        return parseFloat(val).toFixed(0);
      }
    }
  }
};

companyName = $("#companyName")[0].innerText;
$(".cx-report-title").text(companyName + `${_jsHelpers._trans("sitesMonthlyReport")}`);
handleDateSelection();

var totVisitors = new ApexCharts(document.querySelector("#total-visitors"), sparkelOptions);
var totOutsideTrffic = new ApexCharts(document.querySelector("#total-outside-traffic"), sparkelOptions);
var totReturningCustomers = new ApexCharts(document.querySelector("#total-returning-customers"), sparkelOptions);
var totNewCustomers = new ApexCharts(document.querySelector("#total-new-customers"), sparkelOptions);
var totStayTime = new ApexCharts(document.querySelector("#total-stay-time"), sparkelOptions);
var totTurnInRate = new ApexCharts(document.querySelector("#total-turn-in-rate"), sparkelOptions);
var totCustomers = new ApexCharts(document.querySelector("#total-customers"), sparkelOptions);
var totLostCustomers = new ApexCharts(document.querySelector("#total-lost-customers"), sparkelOptions);
var totConversion = new ApexCharts(document.querySelector("#total-conversion"), sparkelOptions);
totVisitors.render();
totOutsideTrffic.render();
totReturningCustomers.render();
totNewCustomers.render();
totStayTime.render();
totTurnInRate.render();
totCustomers.render();
totLostCustomers.render();
totConversion.render();

var bubbleChartPercent = new ApexCharts(document.querySelector("#general-allPercent"), bubbleOptions);
var bubbleChartCount = new ApexCharts(document.querySelector("#general-allCount"), bubbleOptions);
bubbleChartPercent.render();
bubbleChartCount.render();

var thisWeek_general_allBar = new ApexCharts(document.querySelector("#thisWeek_general_allBar"), barChartOptions);
var lastWeek_general_allBar = new ApexCharts(document.querySelector("#lastWeek_general_allBar"), barChartOptions);
var WeekDays = new ApexCharts(document.querySelector("#WeekDays"), barChartOptions);
var WeekEnds = new ApexCharts(document.querySelector("#WeekEnds"), barChartOptions);
var bestDay = new ApexCharts(document.querySelector("#bestDay"), barChartOptions);
var otherDays = new ApexCharts(document.querySelector("#otherDays"), barChartOptions);
thisWeek_general_allBar.render();
lastWeek_general_allBar.render();
WeekDays.render();
WeekEnds.render();
bestDay.render();
otherDays.render();

/****************************************View *******************************************/
var totVisitors_view = new ApexCharts(document.querySelector("#total-visitors_view"), sparkelOptions);
var totOutsideTrffic_view = new ApexCharts(document.querySelector("#total-outside-traffic_view"), sparkelOptions);
var totReturningCustomers_view = new ApexCharts(document.querySelector("#total-returning-customers_view"), sparkelOptions);
var totNewCustomers_view = new ApexCharts(document.querySelector("#total-new-customers_view"), sparkelOptions);
var totStayTime_view = new ApexCharts(document.querySelector("#total-stay-time_view"), sparkelOptions);
var totTurnInRate_view = new ApexCharts(document.querySelector("#total-turn-in-rate_view"), sparkelOptions);
var totCustomers_view = new ApexCharts(document.querySelector("#total-customers_view"), sparkelOptions);
var totLostCustomers_view = new ApexCharts(document.querySelector("#total-lost-customers_view"), sparkelOptions);
var totConversion_view = new ApexCharts(document.querySelector("#total-conversion_view"), sparkelOptions);
totVisitors_view.render();
totOutsideTrffic_view.render();
totReturningCustomers_view.render();
totNewCustomers_view.render();
totStayTime_view.render();
totTurnInRate_view.render();
totCustomers_view.render();
totLostCustomers_view.render();
totConversion_view.render();

var bubbleChartPercent_view = new ApexCharts(document.querySelector("#general-allPercent_view"), bubbleOptions);
var bubbleChartCount_view = new ApexCharts(document.querySelector("#general-allCount_view"), bubbleOptions);
bubbleChartPercent_view.render();
bubbleChartCount_view.render();

var thisWeek_general_allBar_view = new ApexCharts(document.querySelector("#thisWeek_general_allBar_view"), barChartOptions);
var lastWeek_general_allBar_view = new ApexCharts(document.querySelector("#lastWeek_general_allBar_view"), barChartOptions);
var WeekDays_View = new ApexCharts(document.querySelector("#WeekDays_View"), barChartOptions);
var WeekEnds_View = new ApexCharts(document.querySelector("#WeekEnds_View"), barChartOptions);
var bestDay_View = new ApexCharts(document.querySelector("#bestDay_View"), barChartOptions);
var otherDays_View = new ApexCharts(document.querySelector("#otherDays_View"), barChartOptions);
thisWeek_general_allBar_view.render();
lastWeek_general_allBar_view.render();
WeekDays_View.render();
WeekEnds_View.render();
bestDay_View.render();
otherDays_View.render();

function handleDateSelection() {
  var date_element = document.getElementById(`dateInput`);
  var today_date = new Date(new Date().setDate(new Date().getDate() - 1));
  date_element.valueAsDate = today_date;
  date_element.max = today_date.getFullYear() + "-" + ("0" + (today_date.getMonth() + 1)).slice(-2);
}
async function getData() {
  var x = FILTERS.from_date.split("/")
  let thisWeekDate = new Date(`${x[1]}-${x[0]}-${x[2]}`);

  _jsHelpers._blockUI();
  if (ALL_COMPANY_BRANCHES.length == 0) {
    await $.ajax({
      type: "POST",
      url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branchesWithGroups/",
      headers: _jsHelpers._getHeaders(),
    }).done((res) => {
      if (!res.error) {
        ALL_COMPANY_BRANCHES = res.data.branches;
        ALL_COMPANY_GROUPS = res.data.groups;
        SELECTED_BRANCHES = ALL_COMPANY_BRANCHES.map(item => {
          item.group_id = 0;
          return item;
        })
      }
    }).fail((error) => {
      _jsHelpers._handle_notify('danger', 'Server Error : Can\'t Fetch Company Branches : ' + error);
    });
  }


  if (ALL_COMPANY_BRANCHES.length !== SELECTED_BRANCHES.length) {
    $('.branches').text(`(${SELECTED_BRANCHES.map(e => e.name).join(',')})`);
  }


  $('.rpt-date').text(`${_jsHelpers._trans("from")} (${FILTERS.from_date}) ${_jsHelpers._trans("to")} (${FILTERS.to_date})`);

  FILTERS.branch_ids = SELECTED_BRANCHES.map(e => e.id);
  var error = false;
  await $.ajax({
    type: "GET",
    url: urlChart,
    headers: _jsHelpers._getHeaders(),
    data: FILTERS,
  }).done((res) => {
    allData = res.data
    if (res.error) {
      _jsHelpers._handle_notify('danger', res.message);
      _jsHelpers._unBlockUi();
      error = true
    }
  }).fail((error) => {
    _jsHelpers._handle_notify('danger', error);
  });
  _jsHelpers._unBlockUi();
  if (error) return;
  fillBranchesTable(allData.sammury);

  function RenderSparkelsCharts(chart, variable, title) {
    var thisWeekData = allData.daily;
    let count = thisWeekData.data.map(item => {
      return item.count[variable] == 0 ? 0.1 : item.count[variable];
    });

    if (['stay_time', 'turn_in_rate', 'returning_customer', 'new_customer', 'conversion'].includes(variable)) {
      chart.updateOptions({
        series: [{
          data: count
        }],
        title: {
          text: title,
        },
        subtitle: {
          text: variable == 'stay_time' ?
            Num2Time(Number(allData.daily.total[variable]).toFixed(1)) :
            Number(allData.daily.total[variable]).toFixed(1) + "%"
          // text: Number(allData.daily.total[variable]).toFixed(1) + (variable == "stay_time" ? " min" : "%"),
        }
      });
    } else {
      chart.updateOptions({
        series: [{
          data: count
        }],
        title: {
          text: title,
        },
        subtitle: {
          text: variable == 'customers' ? allData.daily.total[variable] : allData.daily.total[variable],
        }
      });
    }
  }

  RenderSparkelsCharts(totVisitors, 'visitors', 'Visitors');
  RenderSparkelsCharts(totOutsideTrffic, 'outside_traffic', 'Outside Traffic');
  RenderSparkelsCharts(totReturningCustomers, 'returning_customer', 'Returning Customers');
  RenderSparkelsCharts(totNewCustomers, 'new_customer', 'New Customers');
  RenderSparkelsCharts(totStayTime, 'stay_time', 'Stay Time');
  RenderSparkelsCharts(totTurnInRate, 'turn_in_rate', 'Turn In Rate');
  RenderSparkelsCharts(totCustomers, 'customers', 'Customers');
  RenderSparkelsCharts(totLostCustomers, 'lost', 'Lost Customers');
  RenderSparkelsCharts(totConversion, 'conversion', 'Conversion');

  RenderSparkelsCharts(totVisitors_view, 'visitors', 'Visitors');
  RenderSparkelsCharts(totOutsideTrffic_view, 'outside_traffic', 'Outside Traffic');
  RenderSparkelsCharts(totReturningCustomers_view, 'returning_customer', 'Returning Customers');
  RenderSparkelsCharts(totNewCustomers_view, 'new_customer', 'New Customers');
  RenderSparkelsCharts(totStayTime_view, 'stay_time', 'Stay Time');
  RenderSparkelsCharts(totTurnInRate_view, 'turn_in_rate', 'Turn In Rate');
  RenderSparkelsCharts(totCustomers_view, 'customers', 'Customers');
  RenderSparkelsCharts(totLostCustomers_view, 'lost', 'Lost Customers');
  RenderSparkelsCharts(totConversion_view, 'conversion', 'Conversion');



  let returningCustomer = [];
  let newCustomer = [];
  let stayTime = [];
  let conversion = [];
  let turnInRate = [];

  let visitors = [];
  let customers = [];
  let lostCustomers = [];
  let outsideTrffic = [];
  let dates = [];
  allData.daily.data.forEach(item => {
    dates.push(item.date.split("/")[0]);
    returningCustomer.push(item.count.returning_customer);
    newCustomer.push(item.count.new_customer);
    stayTime.push(item.count.stay_time);
    conversion.push(item.count.conversion);
    turnInRate.push(item.count.turn_in_rate);

    visitors.push(item.count.visitors);
    customers.push(item.count.customers);
    lostCustomers.push(item.count.lost);
    outsideTrffic.push(item.count.outside_traffic);
  });
  bubbleChartPercent.updateOptions({
    series: [
      {
        name: "Returning Customers",
        data: returningCustomer
      },
      {
        name: "New Customers",
        data: newCustomer
      },
      {
        name: "Stay Time",
        data: stayTime
      },
      {
        name: "Sales Conversion",
        data: conversion
      },
      {
        name: "Turn In Rate",
        data: turnInRate
      },
    ],
    xaxis: {
      categories: dates
    },
    title: {
      text: "General Data Chart"
    },
  });

  bubbleChartCount.updateOptions({
    series: [
      {
        name: "Visitors",
        data: visitors
      },
      {
        name: "Customers",
        data: customers
      },
      // {
      //   name: "Lost Customers",
      //   data: lostCustomers
      // },
      {
        name: "Outside Traffic",
        data: outsideTrffic
      },
    ],
    xaxis: {
      categories: dates
    },
    title: {
      text: "General Data Chart"
    },
  });

  thisWeek_general_allBar.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.month.this_month.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For The Month"
    },
  });

  lastWeek_general_allBar.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.month.last_days.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For Other Months"
    },
  });

  WeekDays.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.weekdays.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For WeekDays"
    },
  });

  WeekEnds.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.weekends.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For The WeekEnds"
    },
  });
  /***************************VEIEW******************************** */
  bubbleChartPercent_view.updateOptions({
    series: [
      {
        name: "Returning Customers",
        data: returningCustomer
      },
      {
        name: "New Customers",
        data: newCustomer
      },
      {
        name: "Stay Time",
        data: stayTime
      },
      {
        name: "Sales Conversion",
        data: conversion
      },
      {
        name: "Turn In Rate",
        data: turnInRate
      },
    ],
    title: {
      text: "General Data Chart"
    },
    xaxis: {
      categories: dates
    },
  });

  bubbleChartCount_view.updateOptions({
    series: [
      {
        name: "Visitors",
        data: visitors
      },
      {
        name: "Customers",
        data: customers
      },
      {
        name: "Outside Traffic",
        data: outsideTrffic
      },
    ],
    title: {
      text: "General Data Chart"
    },
    xaxis: {
      categories: dates
    },
  });

  thisWeek_general_allBar_view.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.month.this_month.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.this_month.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For The Month"
    },
  });

  lastWeek_general_allBar_view.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.month.last_days.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.month.last_days.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For Other Months"
    },
  });

  WeekDays_View.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.weekdays.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekdays.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For WeekDays"
    },
  });

  WeekEnds_View.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(allData.weekends.total.visitors)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.lost)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.customers)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(allData.weekends.total.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For The WeekEnds"
    },
  });

  findBestDay();

}

function fillBranchesTable(sumaryData) {
  var tableBody = document.getElementById("tableBody");
  var tableBody_view = document.getElementById("tableBody_view");
  tableBody.innerHTML = ""
  tableBody_view.innerHTML = ""
  Object.keys(sumaryData.total_week).forEach(branch => {
    var newRow_view = document.createElement("tr");
    var newRow = document.createElement("tr");

    var tdBranchName = document.createElement("td")
    var tdBranchName_view = document.createElement("td")

    tdBranchName.textContent = branch;
    tdBranchName_view.textContent = branch;

    tdBranchName.className = "branch-table-cell"
    tdBranchName_view.className = "branch-table-cell_view"

    newRow.append(tdBranchName)
    newRow_view.append(tdBranchName_view)
    Object.keys(sumaryData.total_week[branch][0]).forEach(item => {
      let td = document.createElement("td");
      let td_view = document.createElement("td");

      let div = document.createElement("div");
      let div_view = document.createElement("div");

      div.style.display = "flex"
      div_view.style.display = "flex"

      td.className = "branch-table-cell"
      td_view.className = "branch-table-cell_view"

      if (['stay_time', 'turn_in_rate', 'returning_customer', 'new_customer', 'conversion'].includes(item)) {
        div.innerHTML = item == 'stay_time' ? '<p>00:' + Number(sumaryData.total_week[branch][0][item]).toFixed(0) + " Min" + '</p>' : '<p>' + Number(sumaryData.total_week[branch][0][item]).toFixed(2) + "%" + '</p>';
        div_view.innerHTML = item == 'stay_time' ? '<p>00:' + Number(sumaryData.total_week[branch][0][item]).toFixed(0) + " Min" + '</p>' : '<p>' + Number(sumaryData.total_week[branch][0][item]).toFixed(2) + "%" + '</p>';
      } else {
        div.innerHTML = '<p>' + sumaryData.total_week[branch][0][item] + '</p>';
        div_view.innerHTML = '<p>' + sumaryData.total_week[branch][0][item] + '</p>';
      }
      let indecator = document.createElement("i");
      let indecator_view = document.createElement("i");

      if (sumaryData.avg_week[branch] && sumaryData.avg_last_days[branch]) {
        if (sumaryData.avg_week[branch][0][item] == sumaryData.avg_last_days[branch][0][item]) {
          indecator.className = "bi bi-caret-right-fill"
          indecator_view.className = "bi bi-caret-right-fill"
        } else {
          if (sumaryData.avg_week[branch][0][item] > sumaryData.avg_last_days[branch][0][item]) {
            indecator.className = "bi bi-caret-up-fill"
            indecator_view.className = "bi bi-caret-up-fill"
          } else {
            indecator.className = "bi bi-caret-down-fill"
            indecator_view.className = "bi bi-caret-down-fill"
          }
        }
      } else {
        indecator.className = "bi bi-caret-right-fill"
        indecator_view.className = "bi bi-caret-right-fill"
      }
      div.append(indecator)
      div_view.append(indecator_view)
      td.append(div)
      td_view.append(div_view)


      newRow.append(td);
      newRow_view.append(td_view);
    });
    tableBody.append(newRow);
    tableBody_view.append(newRow_view);
  })
}

function findBestDay() {
  let visitors = 0;
  let bestDayDate = '';
  let bestDayarr = [];
  let otherDaysarr = [];

  allData.daily.data.forEach(item => {
    if (item.count.visitors >= visitors) {
      visitors = item.count.visitors;
      bestDayDate = item.date;
    }
  });

  allData.daily.data.forEach(item => {
    if (item.date == bestDayDate) {
      bestDayarr.push(item);
    } else {
      otherDaysarr.push(item);
    }
  });

  var bestDayVisitors = 0;
  var bestDayCustomers = 0;
  var bestDayOutsideTraffic = 0;
  var bestDayLostCustomers = 0;
  var bestDayReturningCustomers = 0;
  var bestDayNewCustomers = 0;
  var bestDayStayTime = 0;
  var bestDayTurnInRate = 0;
  var bestDayConversion = 0;
  bestDayarr.forEach(item => {
    bestDayVisitors = bestDayVisitors + item.count.visitors
    bestDayCustomers = bestDayCustomers + item.count.customers
    bestDayOutsideTraffic = bestDayOutsideTraffic + item.count.outside_traffic
    bestDayLostCustomers = bestDayLostCustomers + item.count.lost
    bestDayReturningCustomers = bestDayReturningCustomers + item.count.returning_customer
    bestDayNewCustomers = bestDayNewCustomers + item.count.new_customer
    bestDayStayTime = bestDayStayTime + item.count.stay_time
    bestDayTurnInRate = bestDayTurnInRate + item.count.turn_in_rate
    bestDayConversion = bestDayConversion + item.count.conversion
  })
  let bestDayAvg = {
    visitors: bestDayVisitors,
    customers: bestDayCustomers,
    outside_traffic: bestDayOutsideTraffic,
    lost: bestDayLostCustomers,
    returning_customer: bestDayReturningCustomers,
    new_customer: bestDayNewCustomers,
    turn_in_rate: bestDayTurnInRate,
    stay_time: bestDayStayTime,
    conversion: bestDayConversion,
  }

  var otherDaysVisitors = 0;
  var otherDaysCustomers = 0;
  var otherDaysOutsideTraffic = 0;
  var otherDaysLostCustomers = 0;
  var otherDaysReturningCustomers = 0;
  var otherDaysNewCustomers = 0;
  var otherDaysStayTime = 0;
  var otherDaysTurnInRate = 0;
  var otherDaysConversion = 0;
  otherDaysarr.forEach(item => {
    otherDaysVisitors = otherDaysVisitors + item.count.visitors
    otherDaysCustomers = otherDaysCustomers + item.count.customers
    otherDaysOutsideTraffic = otherDaysOutsideTraffic + item.count.outside_traffic
    otherDaysLostCustomers = otherDaysLostCustomers + item.count.lost
    otherDaysReturningCustomers = otherDaysReturningCustomers + item.count.returning_customer
    otherDaysNewCustomers = otherDaysNewCustomers + item.count.new_customer
    otherDaysStayTime = otherDaysStayTime + item.count.stay_time
    otherDaysTurnInRate = otherDaysTurnInRate + item.count.turn_in_rate
    otherDaysConversion = otherDaysConversion + item.count.conversion
  })
  let thisMonth = $('#dateInput').val().split("-");
  let otherDaysAvg = {
    visitors: (otherDaysVisitors) / (Number(new Date(thisMonth[0], thisMonth[1], 0).getDate()) - 1),
    customers: (otherDaysCustomers) / (Number(new Date(thisMonth[0], thisMonth[1], 0).getDate()) - 1),
    outside_traffic: (otherDaysOutsideTraffic) / (Number(new Date(thisMonth[0], thisMonth[1], 0).getDate()) - 1),
    lost: (otherDaysLostCustomers) / (Number(new Date(thisMonth[0], thisMonth[1], 0).getDate()) - 1),
    returning_customer: otherDaysReturningCustomers / (Number(new Date(thisMonth[0], thisMonth[1], 0).getDate()) - 1),
    new_customer: otherDaysNewCustomers / (Number(new Date(thisMonth[0], thisMonth[1], 0).getDate()) - 1),
    turn_in_rate: otherDaysTurnInRate / (Number(new Date(thisMonth[0], thisMonth[1], 0).getDate()) - 1),
    stay_time: otherDaysStayTime / (Number(new Date(thisMonth[0], thisMonth[1], 0).getDate()) - 1),
    conversion: otherDaysConversion / (Number(new Date(thisMonth[0], thisMonth[1], 0).getDate()) - 1),
  }

  bestDay_View.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(bestDayAvg.visitors)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.lost)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.customers)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "The BestDay : " + bestDayDate
    },
  });


  otherDays_View.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(otherDaysAvg.visitors)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.lost)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.customers)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For The Other Days"
    },
  });

  bestDay.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(bestDayAvg.visitors)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.lost)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.customers)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(bestDayAvg.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "The BestDay : " + bestDayDate
    },
  });


  otherDays.updateOptions({
    series: [{
      data: [
        Number(handleNigativeNumbers(otherDaysAvg.visitors)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.lost)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.customers)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.outside_traffic)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.new_customer)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.returning_customer)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.stay_time)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.turn_in_rate)).toFixed(2),
        Number(handleNigativeNumbers(otherDaysAvg.conversion)).toFixed(2),
      ]
    }],
    title: {
      text: "AVG For The Other Days"
    },
  });

}

function handleNigativeNumbers(num) {
  return num < 0 ? 0 : num
}

$('#brachSelection').on('click', (e) => {
  e.preventDefault();
  _jsBranchesModalHelper._showMoreBranshesModal__A(
    (selectedBranches) => handleBranchSelection_CB(selectedBranches),
    SELECTED_BRANCHES
  );
});

const handleBranchSelection_CB = (selectedBranches) => {
  SELECTED_BRANCHES = selectedBranches //list-branches
};

$('#dateInput').on('change', e => {
  m = e.target.value.split("-")[1];
  y = e.target.value.split("-")[0];
  var from = getMonthDate(m,y).firstDay;
  var to = getMonthDate(m,y).lastDay;
  
  FILTERS.from_date = from;
  FILTERS.to_date = to;
});

$('#btn-apply').on('click', e => {
  getData();
});

$('#nextIcon').on('click', e => {
  _jsHelpers._blockUI();
  $('#test').removeAttr('hidden');


  new Promise(function (resolve, reject) {
    try {
      setTimeout(function () {
        var element = document.getElementById('test');
        var opt = {
          margin: 0,
          filename: `Report-${new Date().toDateString()}.pdf`,
          image: { type: "jpeg", quality: 0.98 },
          pagebreak: { mode: "css", before: ".breackMode" },
          html2canvas: { scale: 1 },
          jsPDF: { unit: "in", format: "letter", orientation: "portrait" },
        };

        html2pdf(element, opt);

        resolve("anything");
      }, 500);
    } catch (error) {
      reject(error);
    }
  }).then(() => {
    $('#test').attr('hidden', true);
    _jsHelpers._unBlockUi();
  })
});

var now = new Date();

var day = ("0" + now.getDate()).slice(-2);
var month = ("0" + (now.getMonth() + 1)).slice(-2);

var today = now.getFullYear() + "-" + (month);

$('#dateInput').val(today).trigger('change');

getData();