const _PREFIX_ADMIN_API = _jsHelpers._PREFIX_ADMIN_API;
var company_modules = [];
var COMPANY_USERS = [];

var METHODE = "POST";
var SELECTED_ROLE_ID = 0;
const modalAdd = $("#modal-add-role");
const assignUsers = $("#modal-assign-users");
async function init() {

    if (company_modules.length == 0) {
        await $.ajax({
            type: "GET",
            url: _PREFIX_ADMIN_API + "/role/modules",
            headers: _jsHelpers._getHeaders(),
        }).done((res) => {
            company_modules = res.data
        });
    }
}

function handleLogic() {

    function initializeControles() {
        function handleAddRoleClick(e) {
            METHODE = "POST";
            modalAdd.modal('show');
            $('#roleName').val("");
            $('#permissionsList').empty();
            company_modules['modules'].forEach(module => {
                $('#permissionsList').append(`
                <div style="display: flex;justify-content: space-between;">
                    <p>${module.name}</p>
                    <div>
                        <input id="${module.id}-Viewer" name="${module.id}" type="radio" value="1" ${module.permission_code >= 1 ? "" : "disabled"}>
                        <label for="${module.id}-Viewer">
                            <i class="bi bi-book"> ${_jsHelpers._trans('viewer')}</i>
                        </label>
                        <input id="${module.id}-Author" name="${module.id}" type="radio" value="2" ${module.permission_code >= 2 ? "" : "disabled"}>
                        <label for="${module.id}-Author">
                            <i class="bi bi-pencil"> ${_jsHelpers._trans('author')}</i>
                        </label>
                        <input id="${module.id}-Admin" name="${module.id}" type="radio" value="3" ${module.permission_code >= 3 ? "" : "disabled"}>
                        <label for="${module.id}-Admin">
                            <i class="bi bi-shield-check"> ${_jsHelpers._trans('admin')}</i>
                        </label>
                        <input id="${module.id}-None" name="${module.id}" type="radio" value="0" checked>
                        <label for="${module.id}-None">
                        ${_jsHelpers._trans('none')}
                        </label>
                    </div>
                </div>
                `);
                // module.permission_code = 0;
            });
        }

        function handleEditRoleClick(e) {
            SELECTED_ROLE_ID = $(this).data("id")
            METHODE = "PUT";
            $.ajax({
                type: "GET",
                url: _PREFIX_ADMIN_API + "/role/" + SELECTED_ROLE_ID,
                headers: _jsHelpers._getHeaders(),
            }).done((res) => {
                var RoleData = res.data[0]

                modalAdd.modal('show');

                $('#roleName').val(RoleData.name)
                $('#permissionsList').empty();

                company_modules.modules.forEach(item => {
                    permission_code = null;
                    RoleData.modules.forEach(module => {
                        if (item.id == module.id) {
                            permission_code = module.permission_code;
                        }
                    })
                    item.role_permission_code = permission_code ?? 0;
                });

                company_modules.modules.forEach(module => {
                    $('#permissionsList').append(`
                <div style="display: flex;justify-content: space-between;">
                    <p>${module.name}</p>
                    <div>
                        <input id="${module.id}-Viewer" name="${module.id}" type="radio" value="1" ${module.role_permission_code == 1 ? 'checked' : ''} ${module.permission_code >= 1 ? "" : "disabled"}>
                        <label for="${module.id}-Viewer">
                            <i class="bi bi-book">  ${_jsHelpers._trans('viewer')}</i>
                        </label>
                        <input id="${module.id}-Author" name="${module.id}" type="radio" value="2" ${module.role_permission_code == 2 ? 'checked' : ''} ${module.permission_code >= 2 ? "" : "disabled"}>
                        <label for="${module.id}-Author">
                            <i class="bi bi-pencil"> ${_jsHelpers._trans('author')}</i>
                        </label>
                        <input id="${module.id}-Admin" name="${module.id}" type="radio" value="3" ${module.role_permission_code == 3 ? 'checked' : ''} ${module.permission_code >= 3 ? "" : "disabled"}>
                        <label for="${module.id}-Admin">
                            <i class="bi bi-shield-check"> ${_jsHelpers._trans('admin')}</i>
                        </label>
                        <input id="${module.id}-None" name="${module.id}" type="radio" value="0" ${module.role_permission_code == 0 ? 'checked' : ''} >
                        <label for="${module.id}-None">
                        ${_jsHelpers._trans('none')}
                        </label>
                    </div>
                </div>
                `);
                });


            });
        }

        function handleDeleteRoleClick(e) {
            $.ajax({
                type: "Delete",
                url: _PREFIX_ADMIN_API + "/role/" + $(this).data("id"),
                headers: _jsHelpers._getHeaders(),
            }).done((res) => {
                var RoleData = res.data
                $(`#${$(this).data("id")}`).remove();
                _jsHelpers._handle_notify("success", "Deleted Successfully");
            });
        }

        function handleAssignUsersClick(e) {
            SELECTED_ROLE_ID = $(this).data("id");
            assignUsers.modal('show');
            const company_id = $("#in-data").data("company-id");
            $("#roleID").val($(this).data("id"))
            $.ajax({
                type: "GET",
                url: _jsHelpers._PREFIX_ADMIN_API + "/customer/CompanyUsers",
                headers: _jsHelpers._getHeaders(),
            }).done((res) => {
                if (!res.error) {
                    COMPANY_USERS = res.data;
                    $(".select2-users").empty();
                    COMPANY_USERS.forEach(e => {
                        $('.select2-users').append(`
                          <option value="${e.id}">${e.email} <span style="color:#a5c939;"> ${e.roles[0] ? "| " + e.roles[0].name : ''}</span></option>
                        `)
              
                      })
              
                }else{
                    _jsHelpers._handle_notify("danger", res.message);
                }
                _jsHelpers._unBlockUi();

                $.ajax({
                    type: "GET",
                    url: _PREFIX_ADMIN_API + "/role/" + SELECTED_ROLE_ID,
                    headers: _jsHelpers._getHeaders(),
                }).done((res) => {
                    var RoleData = res.data[0].users.map(user => user.id);
                    $(".select2-users").val(RoleData).trigger('change')
                });
            });

             initSelectPackage();

        }

        const initSelectPackage = function () {

            function formatResult(data) {
      
              if (!data.id) {
                return data.text;
              }
              return $(` <div class="">${data.text}</div>`);
            }
        
                             $(".select2-users").select2({
                            dropdownAutoWidth: true,
                            multiple: true,
                            width: '100%',
                            height: '30px',
                            placeholder: "Select Users",
                            allowClear: false,
                            data: COMPANY_USERS,
                            cache: true,
                            closeOnSelect: false,
                            templateResult: formatResult,
                            templateSelection: formatResult,
                        });
          };
        $("#btnAddRole").off('click')
        $("#btnAddRole").click(handleAddRoleClick);
        
        $(".btnEditRole").off('click')
        $(".btnEditRole").click(handleEditRoleClick);

        $('.btnDeleteRole').off('click')
        $('.btnDeleteRole').click(handleDeleteRoleClick)

        $(".btnAssignUsers").off('click')
        $(".btnAssignUsers").click(handleAssignUsersClick);
    }

    $("#btnSaveAssignedUsers").on("click", function (e) {
        e.preventDefault();
        data = {
            "user_ids": $("#select2-users").val(),
            "role_id": $("#roleID").val()
        }

        $.ajax({
            type: "POST",
            url: _jsHelpers._PREFIX_ADMIN_API + "/role/assignRoleToUser",
            headers: _jsHelpers._getHeaders(),
            data
        }).done((res) => {
            if (res.error) {
                _jsHelpers._handle_notify("danger", res.message);
            } else {
                _jsHelpers._handle_notify("success", "Users Assigned Successfully");
                assignUsers.modal('hide');
                $(".modal-backdrop").addClass("d-none")
            }
        });

    })

    $("#btnConferm").on("click", function (e) {
        e.preventDefault();
        $("#modal-confermation").hide();
        $(".modal-backdrop").addClass("d-none")
        assignUsers.modal('show');
        const company_id = $("#in-data").data("company-id");
        // const company_id = $("#in-data").data("role");

        $("#roleID").val(SELECTED_ROLE_ID)

        function formatResult(data) {
            if (!data.id) {
                return data.text;
            }
            return $(`
                    <div class="wrapper-users">
                      <p class="">${data.email} <span style="color:#a5c939;"> ${data.roles[0] ? "| " + data.roles[0].name : ''}</span></p>
                    </div>
                  `);
        }

        $.ajax({
            type: "GET",
            url: _jsHelpers._PREFIX_ADMIN_API + "/customer/CompanyUsers",
            headers: _jsHelpers._getHeaders(),
        }).done((res) => {
            if (!res.error) {
                COMPANY_USERS = res.data;

                $(".select2-users").select2({
                    dropdownAutoWidth: true,
                    multiple: true,
                    width: '100%',
                    height: '30px',
                    placeholder: "Select Users",
                    allowClear: true,
                    data: COMPANY_USERS,
                    cache: true,
                    closeOnSelect: false,
                    templateResult: formatResult,
                    templateSelection: formatResult,
                });
            }else{
                _jsHelpers._handle_notify("danger", res.message);
            }
            _jsHelpers._unBlockUi();

            $.ajax({
                type: "GET",
                url: _PREFIX_ADMIN_API + "/role/" + SELECTED_ROLE_ID,
                headers: _jsHelpers._getHeaders(),
            }).done((res) => {

                var RoleData = res.data[0].users.map(user => user.id);
                $(".select2-users").val(RoleData).trigger('change')
            });
        });
    })

    $('#form-add-role').on('submit', (e) => {
        e.preventDefault();
        var results = [];
        company_modules.modules.forEach(function (el) {
            if (document.querySelector("input[name='" + el.id + "']:checked")) {
                if (document.querySelector("input[name='" + el.id + "']:checked").value != 0) {
                    results.push({ id: el.id, permission_code: document.querySelector("input[name='" + el.id + "']:checked").value })
                }
            }
        });

        var data = {
            "name": $('#roleName').val(),
            "modules": results
        }

        if (METHODE == "POST") {
         _jsHelpers._blockUI();
            $.ajax({
                type: "POST",
                url: _jsHelpers._PREFIX_ADMIN_API + "/role",
                headers: _jsHelpers._getHeaders(),
                data
            }).done((res) => {
                if (!res.error) {
                    _jsHelpers._handle_notify("success", "Role Created Successfully");
                    data = res.data
                    renderNewRow(data)
                    modalAdd.modal('hide');
                    $(".modal-backdrop").addClass("d-none")
                    SELECTED_ROLE_ID = data.id
                    $('#modal-confermation').modal();
                 _jsHelpers._unBlockUi();
                } else {
                    _jsHelpers._handle_notify("danger", res.message);
                _jsHelpers._unBlockUi();
                }
            }).fail((error) => {
                _jsHelpers._handle_notify(
                  "danger",
                  "Oops! There were found a problem."
                );
                _jsHelpers._unBlockUi();
              });
        } else {
            $.ajax({
                type: "PUT",
                url: _jsHelpers._PREFIX_ADMIN_API + "/role/" + SELECTED_ROLE_ID,
                headers: _jsHelpers._getHeaders(),
                data
            }).done((res) => {
                if (res.error) {
                    _jsHelpers._handle_notify("danger", res.message);
                } else {
                    _jsHelpers._handle_notify("success", "Role Updated Successfully");
                    $(`#${SELECTED_ROLE_ID}`).remove();
                    data = res.data
                    renderNewRow(data)
                    modalAdd.modal('hide');
                    $(".modal-backdrop").addClass("d-none")
                }
            });
        }

    });

    function renderNewRow(role) {
        $('#tableBody').append(`
        <tr id="${role.id}">
            <td>${role.name}</td>
            <td>${role.created_at}</td>
            <td class="edit-options">
            <i style="padding:0px 10px;cursor: pointer;"  data-id="${role.id}" class="fa fa-plus btnAssignUsers"></i>
            <i style="cursor: pointer;padding:0px 10px;" data-id="${role.id}" class="fa-solid fa-pencil btnEditRole"></i>
            <i style="cursor: pointer;padding:0px 10px" data-id="${role.id}" class="btn btn-danger btnDeleteRole">delete</i>
            </td>   
        </tr>
        `);
        initializeControles();
    }

    initializeControles();
}

init().then(() => {
    handleLogic();
});