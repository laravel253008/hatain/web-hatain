const LOC = function () {

    const init = function () {
        initTable();
        selectCamera()
        showHideEvents();
    }

    const initTable = function () {
        $('#table-extended-success').DataTable({
            "responsive": true,
            "searching": false,
            "lengthChange": false,
            "paging": false,
            "bInfo": false,
            "columnDefs": [
                {
                    "orderable": false,
                    "targets": [0,5,6,7,8,9]
                },
                {
                    "targets": [0],
                    "visible": false
                }
            ]
        });
    }

    const selectCamera = function () {
        const table = $('#table-extended-success').DataTable();
        $('.selectCamera').on('click', function () {
            table.column(0).visible(true);
            $('.controls .buttons').addClass('hidden');
            $('.buttons[data-section="select-camera"]').removeClass('hidden').addClass('display-flex');
            eventCheck();
        });

        $('.closeSelectCamera').on('click', function () {
            table.column(0).visible(false);
            $('.controls .buttons').addClass('hidden').removeClass('display-flex');
            $('.buttons[data-section="actions"]').removeClass('hidden');
        });

        $('.selectAll').on('click', function () {
            selectAll();
            $("input[type=checkbox].dt").prop('checked', true);
        });

        $('.unSelectAll').on('click', function () {
            unSelectAll();
            $("input[type=checkbox].dt").prop('checked', false);
        });
    }

    const eventCheck = function () {
        $("input[type=checkbox].dt").change(function () {
            if ($(this).prop("checked")) {
                selectAll();
            } else {
                unSelectAll();
            }
        });
    };

    const selectAll = function () {
        $('.controls .buttons').addClass('hidden');
        $('.buttons[data-section="select-camera"]').removeClass('display-flex');
        $('.buttons[data-section="unselect-camera"]').removeClass('hidden').addClass('display-flex');
    }

    const unSelectAll = function () {
        $('.controls .buttons').addClass('hidden').removeClass('display-flex');
        $('.buttons[data-section="select-camera"]').removeClass('hidden').addClass('display-flex');
    }

    const showHideEvents = function () {
        $('.left-show, .right-hide').on('click', function () {
            $('.left-show').addClass('hidden');
            $('.left-hide').removeClass('hidden');

            $('.right-show').removeClass('hidden');
            $('.right-hide').addClass('hidden');

            $('#left').addClass('col-xl-11 col-md-11').removeClass('col-xl-1 col-md-1');
            $('#right').removeClass('col-xl-11 col-md-11').addClass('col-xl-1 col-md-1');

            $('#content-left').removeClass('hidden');
            $('#content-right').addClass('hidden');

            $('#title-left').addClass('hidden');
            $('#title-right').removeClass('hidden');
        });

        $('.right-show, .left-hide').on('click', function () {
            $('.left-show, .left-hide').removeClass('hidden');
            $('.left-hide').addClass('hidden');

            $('.right-show').addClass('hidden');
            $('.right-hide').removeClass('hidden');

            $('#left').removeClass('col-xl-11 col-md-11').addClass('col-xl-1 col-md-1');
            $('#right').addClass('col-xl-11 col-md-11').removeClass('col-xl-1 col-md-1');

            $('#content-left').addClass('hidden');
            $('#content-right').removeClass('hidden');

            $('#title-left').removeClass('hidden');
            $('#title-right').addClass('hidden');
        });
    }

    return {
        init: function () {
            init();
        }
    }
}();

LOC.init();