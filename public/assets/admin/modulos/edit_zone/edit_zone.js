const EditCamera = (function () {
  const modalConfigurationLayout = $("#modal-configuration-new-layout");
  const containerConfigLayout = $(".container-config-layout");
  const containerConfigLine = $(".container-config-line");
  const containerConfigPolygons = $(".container-config-polygons");

  var perimeter = new Array();
  var complete = false;
  var canvas = document.getElementById("jPolygon");
  var canvasLine = document.getElementById("canvas-line");
  var ctx;
  var ctxLine;
  var poligonos = [];
  var plugged = 1;
  let CAMERA = {
    type: "",
    percentage: "",
    orientation: 1,
    polygons: "",
  };

  //CONFIG LAYOTU POLYGONS

  //CONFIG LAYOUT LINE

  const init = function () {

    let step = _jsHelpers._getParam("step");
    controls();
    if (step == "2") {
      $(".step[data-target='step-2']").trigger("click");
    }

    $(document).on("click", ".btn-show-config-layout", function () {
      containerConfigLine.addClass("ds-none");
      containerConfigPolygons.addClass("ds-none");
      containerConfigLayout.removeClass("ds-none");
      modalConfigurationLayout.modal({
        backdrop: "static",
        keyboard: false,
      });
    });

    //Save config layout
    $(document).on("click", ".btn-si-new-layout", function (e) {
      e.preventDefault();
      containerConfigLayout.addClass("ds-none");
      let type = $("input:radio[name=radio-type-camera]:checked").val();
      if (type == "fixed") {
        containerConfigLine.removeClass("ds-none");
      } else {
        containerConfigPolygons.removeClass("ds-none");
        configLayoutPolygons();
      }
    });

    $(document).on("click", ".btn-no-new-layout", function () {
      modalConfigurationLayout.modal("hide");
    });

    $('#btnPlgged').on('click', (e) => {
      if (plugged == 1) {
        $('#btnPlgged').addClass('btn-secondary');
        $('#btnPlgged').removeClass('btn-outline-success');
        $('#btnPlgged').text('Plugged Out')
        plugged = 0;
      } else {
        $('#btnPlgged').removeClass('btn-secondary');
        $('#btnPlgged').addClass('btn-outline-success');
        $('#btnPlgged').text('Plugged In')
        plugged = 1;
      }
    })

    // Keep everything in anonymous function, called on window load.
    if (window.addEventListener) {
      window.addEventListener(
        "load",
        function () {
          var canvas, context, canvaso, contexto;
          let img_width = "800"//thisCamera.cam_width
          let img_height = "600"//thisCamera.cam_height
          var detectionData = {
            display_width: img_width,
            display_height: img_height,
            detection_zone: `1;1;1;${img_width};${img_width};${img_height};1;${img_height}`,
            line_crossing: []
          };
          // The active tool instance.
          var tool;
          var recPoints = {
            topLeft: "",
            topRight: "",
            bottomRight: "",
            bottomLeft: "",
          };
          // var tool_default = "rectangle";
          var tool_default = "line";
          function init() {
            // Find the canvas element.
            canvaso = document.getElementById("imageView");
            canvaso.width = document.querySelector('.img-preview-camera').width;
            canvaso.height = document.querySelector('.img-preview-camera').height;
            if (!canvaso) {
              alert("Error: I cannot find the canvas element!");
              return;
            }

            if (!canvaso.getContext) {
              alert("Error: no canvas.getContext!");
              return;
            }

            // Get the 2D canvas context.
            contexto = canvaso.getContext("2d");
            if (!contexto) {
              alert("Error: failed to getContext!");
              return;
            }


            // Add the temporary canvas.
            var container = canvaso.parentNode;
            canvas = document.createElement("canvas");
            if (!canvas) {
              alert("Error: I cannot create a new canvas element!");
              return;
            }

            canvas.id = "imageTemp";
            canvas.width = canvaso.width;
            canvas.height = canvaso.height;
            container.appendChild(canvas);

            context = canvas.getContext("2d");

            // Get the tool select input.
            var tool_select = document.getElementById("dtool");
            if (!tool_select) {
              alert("Error: failed to get the dtool element!");
              return;
            }
            tool_select.addEventListener("change", ev_tool_change, false);

            // Activate the default tool.
            if (tools[tool_default]) {
              tool = new tools[tool_default]();
              tool_select.value = tool_default;
            }

            $('#btnSaveDetectionDrawings').on('click', function () {
              if (detectionData?.line_crossing.length == 0) {
                _jsHelpers._handle_notify('danger', 'Something went wrong');
                return;
              }
              // return;
              let type = $("input:radio[name=radio-type-camera]:checked").val();
              detectionData.type = type
              detectionData.plugged_in = plugged
              detectionData.name = $("#cameraName").val()
              CAMERA.type = type;
              CAMERA.polygons = "";
              let cameraId = $("#in-data").data("camera-id");
              // return
              _jsHelpers._blockUI();
              $.ajax({
                type: "PUT",
                url: _jsHelpers._PREFIX_ADMIN_API + "/cameras/" + cameraId,
                data: detectionData,
                headers: _jsHelpers._getHeaders(),
              })
                .done((res) => {
                  if (!res.error) {
                    _jsHelpers._handle_notify(
                      "success",
                      "Camera edited successfully."
                    );

                  } else {
                    _jsHelpers._responseError(res);
                  }
                  _jsHelpers._unBlockUi();
                })
                .fail((error) => {
                  _jsHelpers._handle_notify(
                    "danger",
                    "Oops! There were found a problem."
                  );
                  _jsHelpers._unBlockUi();
                });
            })

            // Attach the mousedown, mousemove and mouseup event listeners.
            canvas.addEventListener("mousedown", ev_canvas, false);
            canvas.addEventListener("mousemove", ev_canvas, false);
            canvas.addEventListener("mouseup", ev_canvas, false);
            // canvas.addEventListener("mouseout", ev_canvas, false);

            //*/************************************************ */
            try {

              if (thisCamera.line_crossing.length > 0) {
                let testRecArr = thisCamera.detection_zone.split(";");
                let reqWidth = Math.abs(testRecArr[0] - testRecArr[2]);
                let reqHight = Math.abs(testRecArr[1] - testRecArr[5]);
                contexto.strokeStyle = "blue";
                contexto.lineWidth = 3;
                contexto.strokeRect(testRecArr[0], testRecArr[1], reqWidth, reqHight);
                let linePoints = thisCamera.line_crossing[0].entry.split(";")//testLineCrossing[0].entry.split(";")
                tool_default = "line";
                if (tools[tool_default]) {
                  tool = new tools[tool_default]();
                  $('#dtool').value = tool_default;
                  $("#detectionZoneStage").css("border", "1px solid #A1C523")
                }


                context.clearRect(0, 0, canvas.width, canvas.height);
                context.beginPath();
                context.moveTo(linePoints[4], linePoints[5]);
                context.lineTo(linePoints[6], linePoints[7]);
                context.strokeStyle = '#000000';
                context.stroke();
                context.closePath();
                img_update();
                drawInOutLine({ x0: Number(linePoints[4]), y0: Number(linePoints[5]) }, { _x: Number(linePoints[6]), _y: Number(linePoints[7]) });
                tool_default = "rectangle"
                $("#crossLineStage").css("border", "1px solid #A1C523")
              }
            } catch (error) {
              _jsHelpers._handle_notify('danger', 'Something went wrong');
            }

            //**************************************************** */
          }
          var $canvas = $("#imageView");
          var canvasOffset = $canvas.offset();
          var offsetX = canvasOffset.left;
          var offsetY = canvasOffset.top;
          var ctx = $canvas[0].getContext("2d");

          // The general-purpose event handler. This function just determines the mouse
          // position relative to the canvas element.
          function ev_canvas(ev) {
            if (ev.layerX || ev.layerX == 0) {
              // Firefox
              ev._x = ev.layerX;
              ev._y = ev.layerY;
            } else if (ev.offsetX || ev.offsetX == 0) {
              // Opera
              ev._x = ev.offsetX;
              ev._y = ev.offsetY;
            }
            // Call the event handler of the tool.
            var func = tool[ev.type];
            if (func) {
              func(ev);
            }
          }
          // The event handler for any changes made to the tool selector.
          function ev_tool_change(ev) {
            if (tools[this.value]) {
              tool = new tools[this.value]();
            }
          }

          // This function draws the #imageTemp canvas on top of #imageView, after which
          // #imageTemp is cleared. This function is called each time when the user
          // completes a drawing operation.
          function img_update() {
            contexto.drawImage(canvas, 0, 0);
            context.clearRect(0, 0, canvas.width, canvas.height);
          }

          function reduceLength(cX, cY, pX, pY, l) {
            length = Math.hypot(pX - cX, pY - cY);
            while (length > l) {
              pX = (cX + pX) / 2;
              pY = (cY + pY) / 2;
              length = Math.hypot(pX - cX, pY - cY);
            }
            return [pX, pY];
          }
          /**
           * to drow the in_out crossLine
           * @param {*} start 
           * @param {*} end 
           */
          function drawInOutLine(start, end) {
            const length = 60;
            const startX = start.x0;
            const startY = start.y0;
            const endX = end._x;
            const endY = end._y;

            const centerAX = (startX + endX) / 2;
            const centerAY = (startY + endY) / 2;

            slopAX = endX - startX;
            slopAY = endY - startY;

            enteryX = centerAX - slopAY;
            enteryY = centerAY + slopAX;

            exitX = centerAX + slopAY;
            exitY = centerAY - slopAX;
            var [shortEntryX, shortEntryY] = reduceLength(centerAX, centerAY, enteryX, enteryY, length);
            var [shortExitX, shortExitY] = reduceLength(centerAX, centerAY, exitX, exitY, length);

            context.beginPath();
            context.fillText("Out", shortExitX - 20, shortExitY - 20);
            context.moveTo(centerAX, centerAY);
            context.lineTo(shortEntryX, shortEntryY);
            context.strokeStyle = '#00ff00';
            context.stroke();
            context.closePath();

            context.beginPath();
            context.fillText("In", shortEntryX + 20, shortEntryY + 20)
            context.moveTo(centerAX, centerAY);
            context.lineTo(shortExitX, shortExitY);
            context.strokeStyle = '#ff0000';
            context.stroke();
            context.closePath();

            detectionData.line_crossing.push({
              "entry": `${shortEntryX};${shortEntryY};${centerAX};${centerAY};${startX};${startY};${endX};${endY};`,//"288;45 ;299;88; 342;77; 256;99",
              "exit": `${shortExitX};${shortExitY};${centerAX};${centerAY};${startX};${startY};${endX};${endY};`,//"288;45 ;299;88; 342;77; 256;99",
            })
          }
          /**
           * get rectangle coordinate based on first and last drwing points
           * @param {string} initPoint initial point({x:1, y:1})
           * @param {string} finalPint final point({x:4, y:5})
           */
          function getRecCoordinates(initPoint, finalPoint) {
            let x0 = initPoint.x;
            let y0 = initPoint.y;
            let xf = finalPoint.x;
            let yf = finalPoint.y;
            if (initPoint.y > finalPoint.y) { // bottom
              if (initPoint.x > finalPoint.x) { //right
                // start point is bottomRight
                recPoints.bottomRight = `${x0};${y0}`;
                recPoints.topLeft = `${xf};${yf}`;
                recPoints.topRight = `${x0};${yf}`;
                recPoints.bottomLeft = `${xf};${y0}`;
              }
              if (initPoint.x < finalPoint.x) { //left
                // start point is bottomLeft
                recPoints.bottomRight = `${xf};${y0}`;
                recPoints.topLeft = `${x0};${yf}`;
                recPoints.topRight = `${xf};${yf}`;
                recPoints.bottomLeft = `${x0};${y0}`;
              }
            }
            if (initPoint.y < finalPoint.y) { //top
              if (initPoint.x > finalPoint.x) { //right
                // start point is topRight
                recPoints.bottomRight = `${x0};${yf}`;
                recPoints.topLeft = `${xf};${y0}`;
                recPoints.topRight = `${x0};${y0}`;
                recPoints.bottomLeft = `${xf};${yf}`;
              }
              if (initPoint.x < finalPoint.x) { //left
                // start point is topLeft
                recPoints.bottomRight = `${xf};${yf}`;
                recPoints.topLeft = `${x0};${y0}`;
                recPoints.topRight = `${xf};${y0}`;
                recPoints.bottomLeft = `${x0};${yf}`;
              }
            }
            detectionData.detection_zone = `${recPoints.topLeft};${recPoints.topRight};${recPoints.bottomRight};${recPoints.bottomLeft}`;
          }
          // This object holds the implementation of each drawing tool.
          var tools = {};

          // The line tool.
          tools.rectangle = function () {
            contexto.strokeStyle = "blue";
            contexto.lineWidth = 3;
            var thisTool = this;
            this.isDown = false;

            this.mousedown = function (e) {
              e.preventDefault();
              e.stopPropagation();

              /// clear any drawing first
              c_width = $canvas.width();
              c_height = $canvas.height();
              ctx.clearRect(0, 0, c_width, c_height);
              context.clearRect(0, 0, canvas.width, canvas.height);

              // save the starting x/y of the rectangle tool_default = "line";
              thisTool.x0 = e._x;
              thisTool.y0 = e._y;
              // set a flag indicating the drag has begun
              thisTool.isDown = true;
            };

            this.mousemove = function (e) {
              e.preventDefault();
              e.stopPropagation();
              if (!thisTool.isDown) {
                return;
              }
              mouseX = e._x;
              mouseY = e._y;
              var width = mouseX - thisTool.x0;
              var height = mouseY - thisTool.y0;
              contexto.clearRect(0, 0, canvas.width, canvas.height);
              contexto.strokeRect(thisTool.x0, thisTool.y0, width, height);
              thisTool.prevWidth = width;
              thisTool.prevHeight = height;
            };

            this.mouseup = function (e) {
              e.preventDefault();
              e.stopPropagation();

              // the drag is over, clear the dragging flag
              getRecCoordinates({ x: thisTool.x0, y: thisTool.y0 }, { x: e._x, y: e._y })
              thisTool.isDown = false;
              tool_default = "line";
              if (tools[tool_default]) {
                tool = new tools[tool_default]();
                $('#dtool').value = tool_default;
                $("#detectionZoneStage").css("border", "1px solid #A1C523")
              }
            };
            this.mouseout = function (e) {
              e.preventDefault();
              e.stopPropagation();
              // clear
              c_width = $canvas.width();
              c_height = $canvas.height();
              ctx.clearRect(0, 0, c_width, c_height);
              context.clearRect(0, 0, canvas.width, canvas.height);
              thisTool.isDown = false;
            };
          };
          //*********************line*************** */
          tools.line = function () {
            var tool = this;
            this.started = false;

            this.mousedown = function (ev) {
              tool.started = true;
              tool.x0 = ev._x;
              tool.y0 = ev._y;
            };

            var newToolx0 = 0;
            var newTooly0 = 0;

            this.mousemove = function (ev) {
              if (!tool.started) {
                return;
              }
              context.clearRect(0, 0, canvas.width, canvas.height);
              context.beginPath();
              context.moveTo(tool.x0, tool.y0);
              context.lineTo(ev._x, ev._y);
              context.strokeStyle = '#000000';
              context.stroke();
              context.closePath();
            };

            this.mouseup = function (ev) {
              if (tool.started) {
                tool.mousemove(ev);
                tool.started = false;
                img_update();
                drawInOutLine(tool, ev);
                $("#crossLineStage").css("border", "1px solid #A1C523")
              }
            };
          };

          $(".canvas-clear").on("click", function (e) {
            // set the tool to rectangle

            // tool_default = "rectangle";
            tool_default = "line";
            if (tools[tool_default]) {
              tool = new tools[tool_default]();
              $('#dtool').value = tool_default;
            }
            // reset stags colors
            $("#detectionZoneStage").css("border", "1px solid white")
            $("#crossLineStage").css("border", "1px solid white")
            // clear
            c_width = $canvas.width();
            c_height = $canvas.height();
            ctx.clearRect(0, 0, c_width, c_height);
            context.clearRect(0, 0, canvas.width, canvas.height);
            detectionData.line_crossing = [];
          });

          init();
        },
        false
      );
      window.addEventListener('resize', () => {
        document.getElementById('imageView').width = $('.img-preview-camera').width();
        document.getElementById('imageView').height = $('.img-preview-camera').height();
        document.getElementById('imageTemp').width = $('.img-preview-camera').width();
        document.getElementById('imageTemp').height = $('.img-preview-camera').height();
      });
    }
  };

  const configLayoutLine = function () {
    $(document).on("click", ".direction", function () {
      $(".c-arrow").addClass("ds-none");
      $(".direction").removeClass("active");
      $(this).addClass("active");
      if ($(this).data("target") == "up") {
        $(".long-arrow-up").removeClass("ds-none");
        CAMERA.orientation = 1;
      } else {
        $(".long-arrow-down").removeClass("ds-none");
        CAMERA.orientation = 0;
      }
    });

    var verticalSlider = document.getElementById("slider-vertical");
    if (!_jsHelpers.undefined($("#slider-vertical").html())) {
      verticalSlider.noUiSlider.destroy();
    }
    var wt = $(".container-config-line .s-left").width();
    var ht = parseInt($("#in-data").data("ht"));
    var percentage = $("#in-data").data("percentage");
    percentage = _jsHelpers.undefined(percentage) ? "0.50" : percentage;
    $(".percentage").text(percentage);
    percentage = parseFloat(percentage);

    var start = percentage * ht;
    canvasLine.width = wt;
    canvasLine.height = ht;
    ctxLine = canvasLine.getContext("2d");
    noUiSlider.create(verticalSlider, {
      start: percentage,
      step: 0.01,
      orientation: "vertical",
      range: {
        min: 0,
        max: 1,
      },
    });

    drawLine(wt, ht, start);

    verticalSlider.noUiSlider.on("slide", function (values, handle) {
      var value = values[handle];

      let px = value * ht;
      CAMERA.percentage = value;

      $(".percentage").text(value);
      drawLine(wt, ht, px);
    });

    $(document).on("click", ".container-config-line .btn-back", function () {
      containerConfigLayout.removeClass("ds-none");
      containerConfigLine.addClass("ds-none");
      containerConfigPolygons.addClass("ds-none");
    });

    $(document).on("click", ".container-config-line .btn-no-save", function () {
      modalConfigurationLayout.modal("hide");
    });
  };

  const drawLine = function (wt, ht, value) {
    ctxLine.clearRect(0, 0, wt, ht);
    ctxLine.lineWidth = 2;
    ctxLine.strokeStyle = "#A1C523";
    ctxLine.beginPath();
    ctxLine.moveTo(0, value);
    ctxLine.lineTo(wt, value);
    ctxLine.closePath();
    ctxLine.stroke();
  };

  const controls = function () {
    $(document).on("click", ".step", function () {
      let _this = $(this);
      let target = _this.data("target");

      $(".container-steps .step").removeClass("active");
      _this.addClass("active");

      $(".content-step").addClass("ds-none");
      $("." + target).removeClass("ds-none");

      if (target == "step-1") {
        $(".btn-save").show();
      }
      if (target == "step-2") {
        $(".btn-save").hide();
        configLayoutLine();
      }
    });

    $(document).on("click", "#radio-camera-fixed", function () {
      $(".note-camera-fixed").show();
      $(".note-camera-360").hide();
      CAMERA_TYPE = "FIXED";
    });
    $(document).on("click", "#radio-camera-360", function () {
      $(".note-camera-fixed").hide();
      $(".note-camera-360").show();
      CAMERA_TYPE = "360";
    });
  };

  return {
    init: function () {
      init();
    },
  };
})();

EditCamera.init();
