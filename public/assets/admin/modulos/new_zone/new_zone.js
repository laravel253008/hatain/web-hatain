
const NewCamera = function () {

    const modalConnectCamera = $("#modal-connect-camera")
    const modalConnectManualCamera = $("#modal-connect-manual-camera")
    const modalScanning = $("#modal-scanning");
    const modalConnecting = $("#modal-connecting");
    const modalConfirmationCamera = $("#modal-confirmation-camera");
    const selectBranch = $(".select-branch")
    const selectNode = $(".select-node")

    let DEVICE_ID = '';
    let BRANCH = '';

    let CAMERA = {
        node_id: '',
        name: '',
        img_width : '',
        img_height : '',
        camera_id: '',
        port: '',
        ip: '',
        type: '',
        camera_type: '',
        status: '1',
        connection_status: '1',
        image_url: '',
        placeholder: '',
        username: '',
        password: '',
        string_connection: ''
    }

    const init = function () {

        controls()

        initBranchesNodesSelect()
        $(document).on("click", ".btn-cancel", function () {
            location.href = '/zones';

        });

        // SCAN CAMERAS
        $(document).on("click", ".btn-scan-camera", function () {
            scanCameras()
        });

        // CONNECT CAMERA
        initModalConnectCamera()
        $(document).on("click", ".btn-connect-camera", function (e) {
            e.preventDefault()
            let parent_ = "#form-connect-camera ";
            let username = $(parent_ + "#user").val()
            let password = $(parent_ + "#password").val()
            CAMERA.username = username;
            CAMERA.password = password;
            modalConnectCamera.modal('hide')
            connectCamera();
        });

        $(document).on("click", ".btn-add-manual-camera", function () {
            modalConnectManualCamera.modal()
            CAMERA.node_id = DEVICE_ID
        })

        $(document).on("click", ".btn-connect-manual-camera", function (e) {
            e.preventDefault()
            let parent_ = "#form-connect-manual-camera ";
            let stringConnection = $(parent_ + "#string-connection").val()
            let cameraId = $(parent_ + "#camera-id").val()
            let cameraType = $(parent_ + "#camera-type").val()
            // let username = $(parent_ + "#user").val()
            // let password = $(parent_ + "#password").val()
            // CAMERA.username = username;
            // CAMERA.password = password;
            CAMERA.camera_type = cameraType;
            CAMERA.camera_id = cameraId
            CAMERA.string_connection = stringConnection
            modalConnectManualCamera.modal('hide')
            connectManualCamera();
        });

        //NEW CAMERA
        $(document).on("click", ".btn-save", function (e) {

            if (CAMERA.ip) {
                // by scan
                newCamera();
            } else {
                // by user
                newCameraManual();
            }

        });




    }


    const connectManualCamera = function () {
        modalConnecting.find(".modal-body").addClass("ds-none")
        modalConnecting.find(".initAction").removeClass("ds-none")
        modalConnecting.modal({
            backdrop: 'static',
            keyboard: false
        });

        CAMERA.image_url = ''
        $.ajax({
            type: 'POST',
            url: _jsHelpers._PREFIX_ADMIN_API + '/device/cameras/manual_image',
            data: {
                device_id: CAMERA.node_id,
                camera_id: CAMERA.camera_id,
                // ip: CAMERA.port,
                camera_type: CAMERA.camera_type,
                // user: CAMERA.username,
                // password: CAMERA.password
                camera_added_by: "user",
                string_connection: CAMERA.string_connection
            },
            headers: _jsHelpers._getHeaders(),
        }).done(res => {
            if (!res.error) {
                CAMERA.img_width = res.data.width;
                CAMERA.img_height = res.data.height;
                let img = _jsHelpers._PREFIX_ADMIN_API;
                img += '/' + res.data.image + '?v=' + Math.floor((Math.random() * 1000000) + 1);
                $("#form-confirmation-camera img").attr("src", img)
                $(".-camera-ip").text(CAMERA.ip)
                CAMERA.image_url = res.data.image
                modalConnecting.modal("hide");
                modalConfirmationCamera.modal()
            } else {
                modalConnecting.modal("hide");
                _jsHelpers._handle_notify("danger", "Oops! There were found a problem.")
            }

        }).fail(error => {
            setTimeout(function () {
                modalConnecting.modal("hide");
            }, 1500)
            _jsHelpers._handle_notify("danger", "Oops! There were found a problem.")
        })
    }


    const cleanDataCamera = function () {
        CAMERA.node_id = ''
        CAMERA.camera_id = ''
        CAMERA.port = ''
        CAMERA.ip = ''
        CAMERA.type = ''
        CAMERA.camera_type = ''
        CAMERA.image_url = ''
        CAMERA.placeholder = ''
        CAMERA.username = ''
        CAMERA.password = ''
        CAMERA.string_connection = ''
    }


    const scanCameras = function () {

        modalScanning.find(".modal-body").addClass("ds-none")
        modalScanning.find(".initAction").removeClass("ds-none")
        modalScanning.modal({
            backdrop: 'static',
            keyboard: false
        });
        $.ajax({
            type: 'GET',
            url: _jsHelpers._PREFIX_ADMIN_API + '/device/cameras?device_id=' + DEVICE_ID,
            headers: _jsHelpers._getHeaders(),
        }).done(res => {
            if (!res.error) {
                let data = res.data.cameras
                tplScanCamera(data)
            } else {
                _jsHelpers._handle_notify("danger", "Oops! There were found no cameras.")
            }
            modalScanning.modal("hide");

        }).fail(error => {
            setTimeout(function () {
                modalScanning.modal("hide");
            }, 1500)
            _jsHelpers._handle_notify("danger", "Oops! There were found no cameras.")
        });
    }

    const connectCamera = function () {
        modalConnecting.find(".modal-body").addClass("ds-none")
        modalConnecting.find(".initAction").removeClass("ds-none")
        modalConnecting.modal({
            backdrop: 'static',
            keyboard: false
        });
        CAMERA.image_url = ''
        $.ajax({
            type: 'POST',
            url: _jsHelpers._PREFIX_ADMIN_API + '/device/cameras/image',
            data: {
                device_id: CAMERA.node_id,
                camera_id: CAMERA.camera_id,
                ip: CAMERA.port,
                camera_type: CAMERA.camera_type,
                user: CAMERA.username,
                password: CAMERA.password,
                camera_added_by: "scan"
            },
            headers: _jsHelpers._getHeaders(),
        }).done(res => {
            if (!res.error) {
                let img = _jsHelpers._PREFIX_ADMIN_API;
                img += '/' + res.data.image + '?v=' + Math.floor((Math.random() * 1000000) + 1);
                $("#form-confirmation-camera img").attr("src", img)
                $(".-camera-ip").text(CAMERA.ip)
                CAMERA.image_url = res.data.image
                modalConnecting.modal("hide");
                modalConfirmationCamera.modal()
            } else {
                modalConnecting.modal("hide");
                _jsHelpers._handle_notify("danger", "Oops! There were found a problem.")
            }

        }).fail(error => {
            setTimeout(function () {
                modalConnecting.modal("hide");
            }, 1500)
            _jsHelpers._handle_notify("danger", "Oops! There were found a problem.")
        })
    }

    const newCamera = function () {
        _jsHelpers._blockUI()
        $.ajax({
            url: _jsHelpers._PREFIX_ADMIN_API + "/cameras",
            type: "POST",
            headers: _jsHelpers._getHeaders(),
            data: CAMERA
        }).done(function (response) {
            if (!response.error) {
                _jsHelpers._handle_notify("success", response.message)
                // location.href="/edit-camera?deviceId="+DEVICE_ID+"&cameraId="+response.data.id+"&step=2";
            } else {
                _jsHelpers._responseError(response);
            }
            _jsHelpers._unBlockUi();
        }).fail(function (err) {
            _jsHelpers._handle_notify("danger", "Oops! There were found a problem.")
            _jsHelpers._unBlockUi();
        });
    }

    const newCameraManual = function () {
        _jsHelpers._blockUI()
        CAMERA.camera_type = "enternce";
        CAMERA.name = $("#cameraName").val();
        $.ajax({
            url: _jsHelpers._PREFIX_ADMIN_API + "/cameras/manual",
            type: "POST",
            headers: _jsHelpers._getHeaders(),
            data: CAMERA
        }).done(function (response) {
            if (!response.error) {
                _jsHelpers._handle_notify("success", response.message)
                location.href = "/edit-camera?deviceId=" + DEVICE_ID + "&cameraId=" + response.data.id + "&step=1";
            } else {
                _jsHelpers._responseError(response);
            }
            _jsHelpers._unBlockUi();
        }).fail(function (err) {
            _jsHelpers._handle_notify("danger", "Oops! There were found a problem.")
            _jsHelpers._unBlockUi();
        });
    }

    const controls = function () {
        $(document).on("click", ".step", function () {
            let _this = $(this);
            let target = _this.data('target')
            $(".container-steps .step").removeClass("active");
            _this.addClass('active')

            $(".content-step").addClass('ds-none')
            $("." + target).removeClass('ds-none')
        });


        $(document).on("click", ".btn-confirmation-camera", function (e) {
            e.preventDefault()
            modalConfirmationCamera.modal('hide')

            let img = _jsHelpers._PREFIX_ADMIN_API + "/" + CAMERA.image_url
            img += '?v=' + Math.floor((Math.random() * 1000000) + 1);
            $(".step-2 .img-preview-camera").attr("src", img)

            $(".step-2 #camera-ip").val(CAMERA.ip)
            $(".step-2 #camera-type").val(CAMERA.camera_type)
            $(".step-2 #linked-to-node").val(CAMERA.node_id)

            $(".container-steps .step").removeClass("active");
            $(".step[data-target='step-2']").addClass('active');
            $(".content-step").addClass('ds-none');
            $(".step-2").removeClass('ds-none');

            $(".btn-back-to-1").removeClass("ds-none")
            $(".btn-save").removeClass("ds-none")
        });

        $(document).on("click", ".btn-back-to-1", function (e) {
            $(".container-steps .step").removeClass("active");
            $(".step[data-target='step-1']").addClass('active');
            $(".content-step").addClass('ds-none');
            $(".step-1").removeClass('ds-none');

            $(".btn-back-to-1").addClass("ds-none")
            $(".btn-back-to-2").removeClass("ds-none")
            $(".btn-save").addClass("ds-none")
        });

        $(document).on("click", ".btn-back-to-2", function (e) {
            $(".container-steps .step").removeClass("active");
            $(".step[data-target='step-2']").addClass('active');
            $(".content-step").addClass('ds-none');
            $(".step-2").removeClass('ds-none');

            $(".btn-back-to-2").addClass("ds-none")
            $(".btn-save").removeClass("ds-none")
            $(".btn-back-to-1").removeClass("ds-none")
        });

        $(document).on("click", "#radio-camera-fixed", function () {
            $(".note-camera-fixed").show()
            $(".note-camera-360").hide()
            CAMERA.type = "fixed"
        })
        $(document).on("click", "#radio-camera-360", function () {
            $(".note-camera-fixed").hide()
            $(".note-camera-360").show()
            CAMERA.type = "360"
        })
    }

    const initBranchesNodesSelect = function () {

        const companyId = $("#in-data").data('company-id')
        selectBranch.attr("disabled", true)

        $.ajax({
            type: 'GET',
            url: _jsHelpers._PREFIX_ADMIN_API + '/branch/' + companyId,
            headers: _jsHelpers._getHeaders(),
        }).done(res => {
            selectBranch.empty()
            if (!res.error) {
                selectBranch.attr("disabled", false)
                selectBranch.append(`<option value="0">${_jsHelpers._trans("selectBranch")}</option>`)
                res.data.branches.map(e => {
                    selectBranch.append(`<option value="${e.id}">${e.name}</option>`)
                });
            }
        })

        selectBranch.select2({
            dropdownAutoWidth: true,
            width: '100%'
        });

        selectBranch.on("change", function (e) {
            var _this = $(this)
            var idBranch = _this.val()

            if (idBranch == 0) {
                $(".container-select-nodes").addClass('ds-none')
                $(".btn-scan-camera").addClass('ds-none')
                $(".btn-add-manual-camera").addClass('ds-none')
                return
            }

            BRANCH = $('.select-branch option:selected').text();

            _jsHelpers._blockUI()
            $.ajax({
                type: 'GET',
                url: _jsHelpers._PREFIX_ADMIN_API + '/nodes/getNodesByBranch',
                data: {
                    branch_id: idBranch
                },
                headers: _jsHelpers._getHeaders(),
            }).done(res => {
                selectNode.empty()
                if (!res.error) {
                    selectNode.append(`<option value="0">Select a node</option>`)
                    res.data.map(e => {
                        selectNode.append(`<option value="${e.device_id}">${e.device_id}</option>`)
                    });

                    $(".container-select-nodes").removeClass('ds-none')
                }
                _jsHelpers._unBlockUi()
            }).fail(error => {
                _jsHelpers._unBlockUi()
            })
        });

        selectNode.select2({
            dropdownAutoWidth: true,
            width: '100%'
        });

        selectNode.on("change", function (e) {
            var _this = $(this)
            var deviceId = _this.val()

            if (_jsHelpers.undefined(deviceId)) {
                $(".btn-scan-camera").addClass('ds-none')
                $(".btn-add-manual-camera").addClass('ds-none')
            } else {
                DEVICE_ID = deviceId;
                $(".btn-scan-camera").removeClass('ds-none')
                $(".btn-add-manual-camera").removeClass('ds-none')
            }

        })


    };

    const initModalConnectCamera = function () {

        $(document).on("click", ".card-camera", function () {
            cleanDataCamera();
            let _this = $(this)
            let cameraId = _this.data("camera-id")
            let cameraName = _this.data("camera-name")
            let cameraIp = _this.data("camera-ip")
            let cameraType = _this.data("camera-type")
            let devicePort = _this.data("device-port")

            CAMERA.node_id = DEVICE_ID
            CAMERA.camera_id = cameraId;
            CAMERA.ip = cameraIp;
            CAMERA.camera_type = cameraType;
            CAMERA.port = devicePort;

            if (cameraType == "USB") {
                connectCamera();
            } else {
                $("#form-connect-camera .camera-id").val(cameraId)
                $("#form-connect-camera #ip").val(cameraIp)
                modalConnectCamera.modal()
            }
        });
    }

    const tplScanCamera = function (data) {
        var stringTpl = ``;
        data.map(data => {
            stringTpl += `
        
         <div class="col-xl-3 col-lg-3 col-md-4 col-sm-1 pb-1">
                <div class="card-camera" 
                data-camera-id="${data._id}" 
                data-camera-name="${data.CameraName}" 
                data-camera-ip="${data.DevicePort}" 
                data-camera-type="${data.CameraType}"
                data-device-port="${data.DevicePort}">
                    <div class="content-camera">
                        <div>
                            <p class="f600">${data.CameraName}</p>
                            <p>${data.CameraType}</p>
                        </div>

                        <div>
                            <p>${BRANCH}</p>
                            <p>${DEVICE_ID}</p>
                        </div>
                    </div>


                    <span class="btn-select-camara">
                            Select this camera
                    </span>
                </div>
            </div>
        `;
        });
        $(".container-empty").addClass("ds-none");
        $(".container-scan-cameras").removeClass("ds-none");
        $(".container-scan-cameras").empty();
        $(".container-scan-cameras").append(stringTpl);
    };

    return {
        init: function () {
            init()
        }
    }
}();

NewCamera.init();