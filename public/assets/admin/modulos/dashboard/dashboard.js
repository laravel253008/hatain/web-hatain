DYANMIC_TYPE = "dashboard";
$(document).on("mouseup", handleMouseUpToHideWidgetFilter);

$('#templetsList').on('change', function (e) {
    getTempletData(e.target.value);
});

$("#templetsList").select2({
    dropdownAutoWidth: true,
    width: "100%",
    placeholder: `${_jsHelpers._trans("selectDashcoard")}`,

});

getTempletData().then(() => {
    init().then(() => {
        handleModalsLogic();
        handleDashboardLogic();
    });
})

