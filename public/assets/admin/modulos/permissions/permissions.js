var available_modules = [];
var COMPANY_USERS = [];
var METHODE = "POST";
var SELECTED_PACKAGE_ID = 0;
const modalAdd = $("#modal-add-package");
const assignCompany = $("#modal-assign-company");
async function init() {
  if (available_modules.length == 0) {
    await $.ajax({
      type: "GET",
      url: _jsHelpers._PREFIX_ADMIN_API + "/available_module",
      headers: _jsHelpers._getHeaders(),
    }).done((res) => {
      available_modules = res.data;
    });
  }
}

function handleLogic() {
  function handleControles() {
    function handleAddPackage(e) {
      METHODE = "POST";
      modalAdd.modal("show");
      $("#roleName").val("");
      $("#permissionsList").empty();
      available_modules.forEach((module) => {
        if (module.applied > 0) {
          module.permission_code = 0;
          $("#permissionsList").append(`
                    <div style="display: flex;justify-content: space-between;">
                        <p>${module.name}</p>
                        <div>
                            <input id="${module.id}-Viewer" name="${module.id}" type="radio" value="1">
                            <label for="${module.id}-Viewer">
                                <i class="bi bi-book"> ${_jsHelpers._trans('viewer')}</i>
                            </label>
                            <input id="${module.id}-Author" name="${module.id}" type="radio" value="2">
                            <label for="${module.id}-Author">
                                <i class="bi bi-pencil"> ${_jsHelpers._trans('author')}</i>
                            </label>
                            <input id="${module.id}-Admin" name="${module.id}" type="radio" value="3">
                            <label for="${module.id}-Admin">
                                <i class="bi bi-shield-check"> ${_jsHelpers._trans('admin')}</i>
                            </label>
                            <input id="${module.id}-None" name="${module.id}" type="radio" value="0" checked>
                            <label for="${module.id}-None">
                            ${_jsHelpers._trans('none')}
                            </label>
                        </div>
                    </div>
                `);
        }else {
          $("#permissionsList").append(`
                    <div class="disabled" style="display: flex;justify-content: space-between; disabled">
                        <p style="color: gray;" class"disabled" disabled>${module.name}</p>
                        <div>
                            <input disabled id="${module.id}-Viewer" name="${module.id}" type="radio" value="1">
                            <label class"disabled" for="${module.id}-Viewer">
                                <i class="bi bi-book"> ${_jsHelpers._trans('viewer')}</i>
                            </label>
                            <input disabled id="${module.id}-Author" name="${module.id}" type="radio" value="2">
                            <label class"disabled" for="${module.id}-Author">
                                <i class="bi bi-pencil"> ${_jsHelpers._trans('author')}</i>
                            </label>
                            <input disabled id="${module.id}-Admin" name="${module.id}" type="radio" value="3">
                            <label class"disabled" for="${module.id}-Admin">
                                <i class="bi bi-shield-check"> ${_jsHelpers._trans('admin')}</i>
                            </label>
                            <input disabled id="${module.id}-None" name="${module.id}" type="radio" value="0" checked>
                            <label class"disabled" for="${module.id}-None">
                            ${_jsHelpers._trans('none')}
                            </label>
                        </div>
                    </div>
                `);

        }
      });
    }

    function handleEditPackage(e) {
      SELECTED_PACKAGE_ID = $(this).data("id");
      METHODE = "PUT";
      $.ajax({
        type: "GET",
        url: _jsHelpers._PREFIX_ADMIN_API + "/company_package/" + SELECTED_PACKAGE_ID,
        headers: _jsHelpers._getHeaders(),
      }).done((res) => {
        var PackageData = res.data;

        modalAdd.modal("show");

        $("#packageName").val(PackageData.name);
        $("#permissionsList").empty();
        available_modules.forEach((item) => {
          item.permission_code = 0;
          PackageData.modules.forEach((module) => {
            if (item.slug == module.slug) {
              item.permission_code = module.permission_code;
            }
          });
        });
        available_modules.forEach((module) => {
          if (module.applied > 0) {
            $("#permissionsList").append(`
            <div style="display: flex;justify-content: space-between;">
                <p>${module.name}</p>
                <div>
                    <input id="${module.id}-Viewer" name="${
              module.id
            }" type="radio" value="1" ${
              module.permission_code == 1 ? "checked" : ""
            }>
                    <label for="${module.id}-Viewer">
                        <i class="bi bi-book"> ${_jsHelpers._trans('viewer')}</i>
                    </label>
                    <input id="${module.id}-Author" name="${
              module.id
            }" type="radio" value="2" ${
              module.permission_code == 2 ? "checked" : ""
            }>
                    <label for="${module.id}-Author">
                        <i class="bi bi-pencil"> ${_jsHelpers._trans('author')}</i>
                    </label>
                    <input id="${module.id}-Admin" name="${
              module.id
            }" type="radio" value="3" ${
              module.permission_code == 3 ? "checked" : ""
            }>
                    <label for="${module.id}-Admin">
                        <i class="bi bi-shield-check"> ${_jsHelpers._trans('admin')}</i>
                    </label>
                    <input id="${module.id}-None" name="${
              module.id
            }" type="radio" value="0" ${
              module.permission_code == 0 ? "checked" : ""
            }>
                    <label for="${module.id}-None">
                    ${_jsHelpers._trans('none')}
                    </label>
                </div>
            </div>
            `);
          }
          else {
            $("#permissionsList").append(`
            <div class"disabled" style="display: flex;justify-content: space-between;">
                <p style="color: gray;" class"disabled" disabled>${module.name}</p>
                <div>
                    <input disabled id="${module.id}-Viewer" name="${
              module.id
            }" type="radio" value="1" ${
              module.permission_code == 1 ? "checked" : ""
            }>
                    <label class"disabled" for="${module.id}-Viewer">
                        <i class="bi bi-book"> ${_jsHelpers._trans('viewer')}</i>
                    </label>
                    <input disabled id="${module.id}-Author" name="${
              module.id
            }" type="radio" value="2" ${
              module.permission_code == 2 ? "checked" : ""
            }>
                    <label class"disabled" for="${module.id}-Author">
                        <i class="bi bi-pencil"> ${_jsHelpers._trans('author')}</i>
                    </label>
                    <input disabled id="${module.id}-Admin" name="${
              module.id
            }" type="radio" value="3" ${
              module.permission_code == 3 ? "checked" : ""
            }>
                    <label class"disabled" for="${module.id}-Admin">
                        <i class="bi bi-shield-check"> ${_jsHelpers._trans('admin')}</i>
                    </label>
                    <input disabled id="${module.id}-None" name="${
              module.id
            }" type="radio" value="0" ${
              module.permission_code == 0 ? "checked" : ""
            }>
                    <label class"disabled" for="${module.id}-None">
                    ${_jsHelpers._trans('none')}
                    </label>
                </div>
            </div>
            `);
  
          }
        });
      });
    }

    function handleDeletePackage(e) {
      _jsHelpers._isConfirmed(`${_jsHelpers._trans("deletePackage")}`, () => {
        _jsHelpers._blockUI();
      $.ajax({
        type: "Delete",
        url: _jsHelpers._PREFIX_ADMIN_API + "/available_package/" + $(this).data("id"),
        headers: _jsHelpers._getHeaders(),
      }).done((res) => {
        if (!res.error) {
          var PackageData = res.data;
          $(`#${$(this).data("id")}`).remove();
          _jsHelpers._handle_notify("success", "Deleted Successfully");
        }else {
          _jsHelpers._handle_notify('danger', res.message);
        }
        _jsHelpers._unBlockUi();
       
      }).fail((error) => {
        _jsHelpers._unBlockUi();
        _jsHelpers._handle_notify('danger', error.responseJSON.message);
    });
    });

    }

    function handleAssignCompany(e) {
      SELECTED_PACKAGE_ID = $(this).data("id");
      assignCompany.modal("show");
      $("#roleID").val($(this).data("id"));
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/companies",
        headers: _jsHelpers._getHeaders(),
 
      }).done((res) => {
        if (!res.error) {
          COMPANY_USERS = res.data;
          $(".select2-users").empty();
          COMPANY_USERS.forEach(e => {
            $('.select2-users').append(`
              <option locked="locked" value="${e.id}">${e.name} | <strong style="color:#a5c939;">${  e.package ? e.package.name : "Not Assigned"
              }</strong></option>
            `)
  
          })
   
        }
        _jsHelpers._unBlockUi();

        $.ajax({
          type: "GET",
          url: _jsHelpers._PREFIX_ADMIN_API + "/company_package/" + SELECTED_PACKAGE_ID,
          headers: _jsHelpers._getHeaders(),
        }).done((res) => {
          var PackageData = res.data.companies.map((user) => user.id);
          $(".select2-users").val(PackageData).trigger("change");
        });
      });

      initSelectPackage();
    }


    const initSelectPackage = function () {

      function formatResult(data) {

        if (!data.id) {
          return data.text;
        }
        return $(` <div class="">${data.text}</div>`);
      }
  
       $(".select2-users").select2({
            dropdownAutoWidth: true,
            multiple: true,
            width: "100%",
            height: "30px",
            placeholder: `${_jsHelpers._trans('selectUsers')}`,
            allowClear: false,
            data: COMPANY_USERS,
            cache: true,
            closeOnSelect: false,
            templateResult: formatResult,
            templateSelection: formatResult,
          });
    };


    function handleMakeTrial(e) {
      SELECTED_PACKAGE_ID = $(this).data("id");
      let trial = 1;
      var data = {
        trial: trial,
      };

      _jsHelpers._blockUI();
      $.ajax({
        type: "PUT",
        url:  _jsHelpers._PREFIX_ADMIN_API + "/available_package/trial/" +  SELECTED_PACKAGE_ID,
         headers: _jsHelpers._getHeaders(),
        data ,
      })
        .done((res) => {
            if (res.error) {
                _jsHelpers._handle_notify("danger", res.message);
              } else {
                _jsHelpers._handle_notify("success", "Package Updated Successfully");
                $(`#${SELECTED_PACKAGE_ID}`).remove();
                data = res.data;
                renderNewRow(data);
                modalAdd.modal("hide");
                $(".modal-backdrop").removeClass("show");
                location.reload();
              }
          _jsHelpers._unBlockUi();
        })
        .fail((error) => {
          _jsHelpers._handle_notify(
            "danger",
            "Oops! There were found a problem."
          );
          _jsHelpers._unBlockUi();
        });
    }
    $("#btnAddPackage").off();
    $("#btnAddPackage").on("click", handleAddPackage);

    $(".btnEditPackage").off();
    $(".btnEditPackage").on("click", handleEditPackage);

    $(".btnDeletePackage").off();
    $(".btnDeletePackage").on("click", handleDeletePackage);

    $(".btnAssignCompany").off();
    $(".btnAssignCompany").on("click", handleAssignCompany);

    $(".btnMakeTrial").off();
    $(".btnMakeTrial").on("click", handleMakeTrial);
  }

  $("#btnSaveAssignedUsers").on("click", function (e) {

    e.preventDefault();
    data = {
      company_ids: $("#select2-users").val(),
      package_id: $("#roleID").val(),
    };
    $.ajax({
      type: "POST",
      url: _jsHelpers._PREFIX_ADMIN_API + "/company_package",
      headers: _jsHelpers._getHeaders(),
      data,
    }).done((res) => {

      if (res.error) {
        _jsHelpers._handle_notify("danger", res.message);
      } else {
        assignCompany.modal("hide");
        _jsHelpers._handle_notify("success", res.message);

      }
    });
  });

  $("#form-add-package").on("submit", (e) => {
    e.preventDefault();
    var results = [];
    available_modules.forEach(function (el) {
      if (document.querySelector("input[name='" + el.id + "']:checked")) {
        if (
          document.querySelector("input[name='" + el.id + "']:checked").value !=
          0
        ) {
          results.push({
            id: el.id,
            permission_code: document.querySelector(
              "input[name='" + el.id + "']:checked"
            ).value,
          });
        }
      }
    });

    var data = {
      name: $("#packageName").val(),
      modules: results,
    };
    if (data.name.trim() == "") {
      _jsHelpers._handle_notify("danger", "Name field is required");
      return;
    }

    if (data.modules.length == 0) {
      _jsHelpers._handle_notify("danger", "No Modules Assigned");
      return;
    }

    if (METHODE == "POST") {
      $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/available_package",
        headers: _jsHelpers._getHeaders(),
        data,
      })
        .done((res) => {
          if (!res.error) {
            _jsHelpers._handle_notify(
              "success",
              "Package Created Successfully"
            );
            data = res.data;
            renderNewRow(data);
            modalAdd.modal("hide");
            $(".modal-backdrop").removeClass("show");
            _jsHelpers._unBlockUi();
          } else {
            _jsHelpers._handle_notify("danger", res.message);
          }
          _jsHelpers._unBlockUi();
        })
        .fail((error) => {
          _jsHelpers._handle_notify(
            "danger",
            "Oops! There were found a problem."
          );
          _jsHelpers._unBlockUi();
        });
    } else {
      $.ajax({
        type: "PUT",
        url:
          _jsHelpers._PREFIX_ADMIN_API +
          "/available_package/" +
          SELECTED_PACKAGE_ID,
        headers: _jsHelpers._getHeaders(),
        data,
      }).done((res) => {
        if (res.error) {
          _jsHelpers._handle_notify("danger", res.message);
        } else {
          _jsHelpers._handle_notify("success", "Package Updated Successfully");
          $(`#${SELECTED_PACKAGE_ID}`).remove();
          data = res.data;
          renderNewRow(data);
          modalAdd.modal("hide");
          $(".modal-backdrop").removeClass("show");
        }
      });
    }
  });



  function renderNewRow(package) {
    if (package.trial == 1) {
        $("#tableBody").append(`
        <tr id="${package.id}">
        <td>${package.name}</td>
        <td class="edit-options">
        <div class="trial">
        <i data-id="${package.id}" class="fa fa-tasks  disabled" aria-hidden="true"></i>
        <span id="make-trial">${_jsHelpers._trans('makeTrial')}</span>
    </div>
        <div class="assignUser">
            <i data-id="${package.id}" class="fa fa-plus btnAssignCompany"></i>
            <span id="assign-user">${_jsHelpers._trans('assignUser')}</span>

        </div>
        <div class="editPackage">

            <i data-id="${package.id}" class="fa-solid fa-pencil btnEditPackage"></i>
            <span id="edit-package">${_jsHelpers._trans('editPackage')}</span>
        </div>
        <div class="deletePackage">
        <i data-id="${package.id}" class="btn btn-danger btnDeletePackageDisabled  disabled">delete</i>
        <span id="delete-package">${_jsHelpers._trans('delete')}</span>
    </div>

    </td>
        </tr>
        `);
    }else {
        $("#tableBody").append(`
            <tr id="${package.id}">
            <td>${package.name}</td>
            <td class="edit-options">
            <div class="trial">
                <i data-id="${package.id}" class="fa fa-tasks btnMakeTrial" aria-hidden="true"></i>
                <span id="make-trial">${_jsHelpers._trans('makeTrial')}</span>
            </div>
            <div class="assignUser">
    
                <i data-id="${package.id}" class="fa fa-plus btnAssignCompany"></i>
                <span id="assign-user">${_jsHelpers._trans('assignUser')}</span>
    
            </div>
            <div class="editPackage">
    
                <i data-id="${package.id}" class="fa-solid fa-pencil btnEditPackage"></i>
                <span id="edit-package">${_jsHelpers._trans('editPackage')}</span>
            </div>
            <div class="deletePackage">
    
                <i data-id="${package.id}" class="btn btn-danger btnDeletePackage">delete</i>
                <span id="delete-package">${_jsHelpers._trans('delete')}</span>
            </div>
    
        </td>
            </tr>
            `);

    }

    handleControles();
  }
  handleControles();
}

init().then(() => {
  handleLogic();
});
