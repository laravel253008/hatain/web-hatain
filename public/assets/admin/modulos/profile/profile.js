
var Profile = function () {
    var url_update_photo = "/profile/photo";

    var handleLogicModule = function () {
        handleUpdatePhoto();
    }

    var handleUpdatePhoto = function () {

        $("#profile_photo").dropzone({
            url: "/",
            autoProcessQueue: false,
            dictDefaultMessage: "",
            acceptedFiles: ".jpeg,.jpg,.png",
            maxFilesize: 2, // Maximo 15Mb
            maxThumbnailFilesize: 2,
            maxFiles: 1,
            init: function () {
                this.on('maxfilessizeexceeded', function (file) {
                    this.removeFile(file);
                    _jsHelpers._handle_notify("danger", "Max size 2MB.");
                });
                this.on("thumbnail", function (file) {
                    this.removeFile(file);
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function (event) {
                        var imagen_base64 = event.target.result
                        handleAJAXPhoto(imagen_base64)
                    };
                    reader.onerror = function (error) {
                        _jsHelpers._handle_notify('danger', error);
                    };

                });
            },
        });


        var handleAJAXPhoto = function (imagen_base64) {
            _jsHelpers._blockUI();
            $.ajax({
                type: 'POST',
                url: url_update_photo,
                data: {
                    _token: _jsHelpers._token(),
                    file: imagen_base64,
                },
            }).done(function (response) {
                console.log(response);

            }).fail(function (err) {
                console.log(err);
            });
        }
    }

    return {
        init: function () {
            handleLogicModule();
        }
    };
}();

jQuery(document).ready(function () {
    Profile.init();
});
