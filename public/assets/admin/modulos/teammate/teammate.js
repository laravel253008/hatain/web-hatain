const TEAMMATE = function () {

    const init = function () {
        $(document).on('click', '.openOptions', function () {
            $(this).parents('.card-user').find('.more-options').show();
        });

        $(document).on('click', '.closeOptions', function () {
            $(this).parents('.more-options').hide();
        });

        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

        initModalAddUserForm();
        initModalProfile();

        initEditInfoForm();
        initModalEditUser();

        initUpdatePasswordForm();
        initModalUpdatePassword();

        initModalUpdateStatus()
        initUpdateStatusForm()

        initModalDeleteUser()
        initDeleteUserForm();
    };


    const updateCantCards = function(){
        var cantCards = $(".card-user").length
        cantCards = cantCards==1 ? cantCards+' teammate' : cantCards+' teammates';
        $(".-cant-card").text(cantCards)
    }

    const initModalUpdatePassword = function () {
        $(document).on('click', '.updatePassword', function (e) {
            var userId = $(this).data('id-usuario');
            $('#update-password-teammate #new_password').val('')
            $('#update-password-teammate #confirm_password').val('')
            $('#update-password-teammate #p-user-id').val(userId)

            $('#update-password').modal('show');
        });
    }

    const initUpdatePasswordForm = function () {
        $('#update-password-teammate').on('submit', function (e) {
            e.preventDefault();

            var password = $('#update-password-teammate #new_password').val()
            var password_confirmation = $('#update-password-teammate #confirm_password').val()
            var userId = $('#update-password-teammate #p-user-id').val()
            var parent = '#card-' + userId + ' ';


            _jsHelpers._blockUI();
            $.ajax({
                type: 'POST',
                url: _jsHelpers._PREFIX_ADMIN_API + '/user/updatePassword',
                headers: _jsHelpers._getHeaders(),
                data: {
                    password: password,
                    password_confirmation: password_confirmation,
                    userId: userId
                },
            }).done(res => {
                $(parent + '.closeOptions').click()

                if (!res.error) {
                    _jsHelpers._handle_notify("success", res.message);
                } else {
                    _jsHelpers._responseError(res);
                }
                $('#update-password').modal('hide');

            }).always(res => {
                _jsHelpers._unBlockUi();
                $('#update-password').modal('hide');
            });
        });
    };

    const initModalUpdateStatus = function () {
        $(document).on('click', '.updateStatus', function (e) {
            var userId = $(this).data('id-usuario');
            var $parent = $('#card-' + userId);

            var details = $parent.data('details');
            var status = $parent.data('status');
            var stringStatus = status ? 'Active' : 'Inactive'
            $('#update-status-teammate .details').val(details)
            $('#update-status-teammate #current_status').val(stringStatus)
            $('#update-status-teammate .select-user-state').val(status)
            $('#update-status-teammate .p-user-id').val(userId)

            $('#update-status').modal('show');
        });
    }

    const initUpdateStatusForm = function () {
        $('#update-status-teammate').on('submit', function (e) {
            e.preventDefault();

            var details = $('#update-status-teammate .details').val()
            var status = $('#update-status-teammate .select-user-state').val()
            var userId = $('#update-status-teammate .p-user-id').val()
            var parent = '#card-' + userId + ' ';
            var $parent = $('#card-' + userId);

            _jsHelpers._blockUI();
            $.ajax({
                type: 'POST',
                url: _jsHelpers._PREFIX_ADMIN_API + '/user/updateStatus',
                headers: _jsHelpers._getHeaders(),
                data: {
                    status: status,
                    user_id: userId,
                    details: details
                },
            }).done(res => {
                $(parent + '.closeOptions').click()

                if (!res.error) {

                    $parent.data('details', details);
                    $parent.data('status', status);

                    if (parseInt(status)) {
                        $(parent + ".-bullet-status").removeClass('bullet-danger')
                        $(parent + ".-bullet-status").addClass('bullet-success')
                    } else {
                        $(parent + ".-bullet-status").removeClass('bullet-success')
                        $(parent + ".-bullet-status").addClass('bullet-danger')
                    }

                    _jsHelpers._handle_notify("success", res.message);
                } else {
                    _jsHelpers._responseError(res);
                }
                $('#update-status').modal('hide');

            }).always(res => {
                $('#update-status').modal('hide');
                _jsHelpers._unBlockUi();

            });
        });
    };

    const initModalDeleteUser = function () {
        $(document).on('click', '.deleteUser', function (e) {
            var userId = $(this).data('id-usuario');
            $('#delete-user-teammate .p-user-id').val(userId)

            $('#delete-user').modal('show');
        });
    }

    const initDeleteUserForm = function () {
        $('#delete-user-teammate').on('submit', function (e) {
            e.preventDefault();

            var userId = $('#delete-user-teammate .p-user-id').val();
            _jsHelpers._blockUI();
            $.ajax({
                type: 'DELETE',
                url: _jsHelpers._PREFIX_ADMIN_API + '/user/' + userId,
                headers: _jsHelpers._getHeaders(),

            }).done(res => {
                if (!res.error) {
                    $('#card-' + userId).remove()
                    if ($(".card-content .row .card-user").length == 0) {
                        $(".container-no-data").show()
                    }
                    updateCantCards()
                    _jsHelpers._handle_notify("success", res.message);
                } else {
                    _jsHelpers._responseError(res);
                }
                $('#delete-user').modal('hide');

            }).always(res => {
                $('#delete-user').modal('hide');
                _jsHelpers._unBlockUi();
            });
        });
    };

    const initModalEditUser = function () {
        $(document).on('click', '.editTeammate', function (e) {

            var userId = $(this).data('id-usuario')
            var $parent = $('#card-' + userId);

            var name = $parent.data('name')
            var lastName = $parent.data('lastname')
            var email = $parent.data('email')
            var jobTitle = $parent.data('job-title')

            $('#edit-user-teammate #e-firstname').val(name);
            $('#edit-user-teammate #e-lastname').val(lastName);
            $('#edit-user-teammate #e-email').val(email);
            $('#edit-user-teammate #e-job').val(jobTitle);
            $('#edit-user-teammate #e-user-id').val(userId)

            $('#edit-user').modal('show');
        });
    };

    const initEditInfoForm = function () {
        $('#edit-user-teammate').on('submit', function (e) {
            e.preventDefault();

            var firstname = $('#edit-user-teammate #e-firstname').val()
            var lastname = $('#edit-user-teammate #e-lastname').val()
            var email = $('#edit-user-teammate #e-email').val()
            var job = $('#edit-user-teammate #e-job').val()
            var userId = $('#edit-user-teammate #e-user-id').val()
            var parent = '#card-' + userId + ' ';
            var $parent = $('#card-' + userId);
            _jsHelpers._blockUI();
            $.ajax({
                type: 'POST',
                url: _jsHelpers._PREFIX_ADMIN_API + '/user/update',
                headers: _jsHelpers._getHeaders(),
                data: {
                    firstname: firstname,
                    lastname: lastname,
                    email: email,
                    job: job,
                    userId: userId
                },
            }).done(res => {
                if (!res.error) {
                    $(parent + '.closeOptions').click()

                    $parent.data('name', firstname)
                    $parent.data('lastname', lastname)
                    $parent.data('email', email)
                    $parent.data('job-title', job)

                    $(parent + '.-complete-name').text(firstname + ' ' + lastname)
                    $(parent + '.-email').text(email)
                    $(parent + '.-job-title').text(job)

                    _jsHelpers._handle_notify("success", res.message);
                } else {
                    _jsHelpers._responseError(res);
                }
                $('#edit-user').modal('hide');

            }).always(res => {
                $('#edit-user').modal('hide');
                _jsHelpers._unBlockUi();
            });
        });
    };

    const initRolSelect = function () {
        $.ajax({
            type: 'POST',
            url: _jsHelpers._PREFIX_ADMIN_API + '/catalog/roles',
            headers: _jsHelpers._getHeaders(),
        }).done(res => {
            if (!res.error) {
                res.data.map(e => {
                    $('#add-user-teammate #role').append(`<option value="${e.name}">${e.name}</option>`)
                });
            }
        })
    };

    const initModalProfile = function () {
        $(document).on('click', '.openProfileModal', function (e) {

            var userId = $(this).data('id-usuario')
            var $parent = $('#card-' + userId);

            var name = $parent.data('name')
            var lastName = $parent.data('lastname')
            var email = $parent.data('email')
            var jobTitle = $parent.data('job-title')
            var active = $parent.data('status')
            var photo = $parent.data('photo')
            var roles = $parent.data('roles')

            const status = active ? 'active' : 'inactive';

            if (roles.length) {
                $('#profile-team-role').text(roles[0].name);
            } else {
                $('#profile-team-role').text('Not assigned');
            }

            if (photo) {
                $('#profile-team-img').attr('src', photo);
            } else {
                $('#profile-team-img').attr('src', '/assets/admin/img/misc/user_active.png');
            }

            $('#profile-team-job').text(jobTitle)
            $('#profile-team-status').removeClass('active, inactive').addClass(status);
            $('#profile-team-name').text(name + ' ' + lastName);
            $('#profile-team-email').text(email);

            if (active) {

            } else {

            }

            $('.profile-team').modal('show');
        });
    };

    const initModalAddUserForm = function () {
        $('#add-user-teammate').on('submit', function (e) {
            e.preventDefault();
            var firstname = $('#add-user-teammate #firstname').val();
            var lastname = $('#add-user-teammate #lastname').val()
            var job = $('#add-user-teammate #job').val()
            var email = $('#add-user-teammate #email').val()
            var password = $('#add-user-teammate #password').val()

            _jsHelpers._blockUI();
            $.ajax({
                type: 'POST',
                url: _jsHelpers._PREFIX_ADMIN_API + '/user/saveUser',
                headers: _jsHelpers._getHeaders(),
                data: {
                    firstname: firstname,
                    lastname: lastname,
                    job: job,
                    role: 'Super Admin',
                    email: email,
                    password: password
                },
            }).done(res => {
                if (!res.error) {
                    getNewUser(res)
                    $('#xlarge').modal('hide')
                    _jsHelpers._handle_notify("success", res.message);
                } else {
                    _jsHelpers._responseError(res);
                }
            }).always(res => {
                _jsHelpers._unBlockUi();
            });
        });
    };

    var getNewUser = function (res) {

        $(".container-no-data").hide()

        var data = res.data

        var userId = data.id
        var active = data.active
        var firstname = data.name
        var lastname = data.lastname
        var completeName = data.name + ' ' + data.lastname
        var roles = data.roles
        var email = data.email
        var email_verified_at = data.email_verified_at
        var jobTitle = data.job_title

        var photo = '/assets/admin/img/misc/user_active.png';

        var stringRol = '';
        if (roles.length > 0) {
            stringRol = roles[0].name
        } else {
            stringRol = 'Not assigned';
        }

        var bulletActive = '';
        if (active) {
            bulletActive = 'bullet-success'
        } else {
            bulletActive = 'bullet-danger'
        }

        var footer = ``;
        if (active && email_verified_at) {
            footer = `
<!--            <p class="m-0 date-active" style="color: var(--fontColor); font-size: 12px">Active since {{$date->format('M')}}, {{$date->format('d')}} <sup>th</sup>, {{$date->format('Y')}} at {{$date->format('H:i a')}} </p>-->
            `;
        }


        var stringRoles = roles.toString()

        var tpl = `
            <div id="card-${userId}" class="col-xl-3 col-lg-3 col-md-4 col-sm-1 pb-1"
                data-name="${firstname}"
                data-lastname="${lastname}"
                data-email="${email}"
                data-job-title="${jobTitle}"
                data-details = ""
                data-status="${active}"
                
                data-photo="${photo}"
                data-roles='${JSON.stringify(roles)}'
            >
                <div class="card-user">
                    <div class="options">
                        <div class="badge badge-success">
                            ${stringRol}
                        </div>
                        <i class="bx bx-dots-vertical-rounded openOptions"></i>
                    </div>
                    <div data-id-usuario="${userId}"  class="openProfileModal">
                        <div class="profile mb-1">
                                <img src="${photo}" class="mr-1" alt="" onerror="_jsHelpers._errorImage(this)">
                            <div class="data">
                                <p class="-info m-0 w-600 -complete-name">${completeName}
                                    <span class="bullet bullet-xs ${bulletActive} -bullet-status"></span>
                                </p>
                                <p class="-info m-0 -job-title" style="color: var(--fontColor); font-size: 12px">${jobTitle}</p>
                            </div>
                        </div>
                        <div class="more-data">
                            <p class="m-0 -email" style="color: var(--fontColor);">${email}</p>
                            ${footer}
                        </div>
                    </div>
                    <div class="more-options" style="display: none">
                        <div class="title">
                            <span>Options</span>
                            <i class="bx bx-x closeOptions"></i>
                        </div>
                        <p class="text-success editTeammate" data-id-usuario="${userId}">Edit teammate info</p>
                        <p data-id-usuario="${userId}" class="text-success updatePassword">Update password</p>
                        <p data-id-usuario="${userId}" class="text-success updateStatus" >Change status</p>
                        <p data-id-usuario="${userId}" class="text-danger deleteUser">Delete teammate</p>
                    </div>
                </div>
            </div>
           
       `
        $(".card-content .row").append(tpl)
        updateCantCards()
    }


    return {
        init: function () {
            init()
        }
    }
}();

TEAMMATE.init();

