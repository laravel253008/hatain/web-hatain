var DYANMIC_TYPE = "dashboard";
var WidgetChartType = "line"
var series = [];
var ALL_COMPANY_BRANCHES = [];
var SELECTED_BRANCHES = [];
var SELECTED_BRANCHES_FILTER = [];
var selected_groubs = [];
var CHARTS = {};
var WIDGETS = [];
var IS_EDITING_MODE = false;
var IS_GRID_EDITED = false;
var IS_DATE_FILTER_USED = false;
var IS_CHARTS_WITH_NUMBERS = false;
var select_branch_add = $(".select2-branch");
var select_branch_add_companies = $(".addCompany");

var screen_width_size = window.screen.width;
var ALL_VARIABLES = ['visitors', 'customers', 'lost', 'outside_traffic', 'returning_customer', 'stay_time', 'new_customer', 'turn_in_rate', 'sales_conversion', "sales_amount", "avg_ticket_size"];

var percentVariables = ["returning_customer", "stay_time", "new_customer", "turn_in_rate", 'sales_conversion', "avg_ticket_size"];

var linkersGraphColors = ['#BA3425', '#ACC73A', '#D6685C', '#4785D6', '#3785A8', '#34598A', '#175EC2', '#a1c523', '#8a99b5', '#F56D11', '#7A83EB'];

document.getElementById("from-date").valueAsDate = new Date();
document.getElementById("to-date").valueAsDate = new Date();
document.getElementById("from-date").max = moment().format("YYYY-MM-DD");
document.getElementById("to-date").min = moment().format("YYYY-MM-DD");
document.getElementById("to-date").max = moment().format("YYYY-MM-DD");


const formateSeriesName = (seriesName) => {
    seriesName = seriesName.split("_");
    for (let i = 0; i < seriesName.length; i++) {
        seriesName[i] = seriesName[i].charAt(0).toUpperCase() + seriesName[i].slice(1);;
    }
    return seriesName.join(" ");
}

var SERIES_DATA = {
    analysis_of: "",
    chart_type: "line",
    color: "red",
    branch_ids: []
}

var WIDGET_DATA = {
    title: "",
    chart_type: "line",
    resolution: 1,
    time_period: "last_month",
    series: [

    ],
    position: {
        x: 0,
        y: 0,
        h: 3,
        w: 4
    },
    dashboard_template_id: 0
}

var TEMPLET_DATA = {
    name: "",
    widgets: []
}


var mixedChartOptions = {
    series: [],
    chart: {
        // type: "line",
    },
    tooltip: {
        enabled: true
    },
    legend: {
        position: "bottom",
        formatter: function (seriesName, opts) {
            var series_name = WIDGETS.find(x => Number(x.id) == Number(opts.w.globals.dom.baseEl.id.split("-")[1])).data[opts.seriesIndex].analysis_of;
            if (percentVariables.includes(series_name)) {
                if (series_name == "stay_time") {
                    return [formateSeriesName(series_name), " : ", Num2Time(Number(opts.w.globals.seriesTotals[opts.seriesIndex] / opts.w.globals.categoryLabels.length))];
                }
                if (series_name == "avg_ticket_size") {
                    return [formateSeriesName(series_name), " : ", Number(opts.w.globals.seriesTotals[opts.seriesIndex] / opts.w.globals.categoryLabels.length).toFixed(2)];
                }
                return [formateSeriesName(series_name), " : ", Number(opts.w.globals.seriesTotals[opts.seriesIndex] / opts.w.globals.categoryLabels.length).toFixed(2) + "%"];
            } else {
                return [formateSeriesName(series_name), " : ", opts.w.globals.seriesTotals[opts.seriesIndex]]
            }
        }
    },
    colors: linkersGraphColors,
    stroke: {
        curve: "smooth",
        width: [2, 2, 2]
    },
    plotOptions: {
        bar: {
            columnWidth: '50%'
        }
    },
    fill: {
        gradient: {
            inverseColors: false,
            shade: 'light',
            type: "vertical",
            opacityFrom: 0.85,
            opacityTo: 0.55,
            stops: [0, 100, 100, 100]
        }
    },
    title: {
        text: "",
        style: {
            fontWeight: 'bold',
            fontFamily: 'Maven pro family',
            color: '#263238'
        },
    },
    tooltip: {
        followCursor: true,
        y: {
            formatter: function (value, opt) {
                var series_name = WIDGETS.find(x => x.id == opt.w.globals.dom.baseEl.id.split("-")[1]).data[opt.seriesIndex].analysis_of;

                if (percentVariables.includes(series_name)) {
                    if (series_name == "stay_time") {
                        return Num2Time(value);
                    }
                    if (series_name == "avg_ticket_size") {
                        return Number(value).toFixed(2);
                    }
                    return Number(value).toFixed(1) + "%";
                } else {
                    return value
                }
            },
            title: {
                formatter: (seriesName) => formateSeriesName(seriesName)
            },
        },
    },
    dataLabels: {
        enabled: false,
        style: {
            fontFamily: 'Helvetica, Arial, sans-serif',
            fontWeight: 'bold',
            colors: ['#202121']
        },
        formatter: function (val, opts) {
            return Number(val).toFixed(1)
        },
        background: {
            enabled: false,
        }
    },
    xaxis: {
        tickAmount:7,
        categories: []
    },
    yaxis: {
        labels: {
            formatter: (val) => {
                return Number(val).toFixed(1)
            }
        }
    },
    responsive: [{
        breakpoint: 1000,
        options: {
            plotOptions: {
                bar: {
                    horizontal: false
                }
            },
            legend: {
                position: "bottom"
            }
        }
    }]
};

var pieChartOptions = {
    series: [],
    chart: {
        type: "pie",
    },
    tooltip: {
        enabled: true,
        // colors: '#F44336'
    },
    colors: linkersGraphColors,
    title: {
        text: "",
        style: {
            // fontSize: '16px',
            fontWeight: 'bold',
            fontFamily: 'Maven pro family',
        },
    },
    dataLabels: {
        enabled: false,
        style: {
            fontFamily: 'Helvetica, Arial, sans-serif',
            fontWeight: 'bold',
        },
        background: {
            enabled: false,
        }
    },
    xaxis: {
        categories: []
    },
};

var sparkelOptions = {
    chart: {
        group: 'sparks',
        type: 'area',
        height: 80,
        sparkline: {
            enabled: true
        },
    },
    series: [{
        data: [15, 75, 47, 65, 14, 32, 19, 54, 44, 61]
    }],
    stroke: {
        curve: 'smooth',
        width: 1
    },
    colors: ['#a5c939'],
    xaxis: {
        crosshairs: {
            width: 1
        },
    },
    tooltip: {
        x: {
            show: false
        },
        y: {
            title: {
                formatter: function formatter(val) {
                    return '';
                }
            }
        }
    }
}

var gridOptions = {
    disableOneColumnMode: true,
    float: true
};


var grid = GridStack.init(gridOptions);

grid.enableMove(false);
grid.enableResize(false);

addNewWidget = function (node) {
    var node = node ? node : {
        // x: 0,
        // y: 0,
        w: 2,
        h: 2,
        content: `<div id="">New Wedgit</div>`
    };
    grid.addWidget(node);
    return false;
};

$(document).ready(function () {
    $(window).bind("beforeunload", function (e) {
        if (IS_EDITING_MODE && IS_GRID_EDITED) {
            $.ajax({
                type: "GET",
                url: _jsHelpers._PREFIX_ADMIN_API + "/dashboard/leave_edit/" + $("#templetsList").val(),
                headers: _jsHelpers._getHeaders(),
            }).done((res) => {
                if (res.error) {
                    _jsHelpers._handle_notify('danger', res.message);
                    $("#temolatePanel").removeClass("d-none");
                    $("#editModePanel").removeClass("d-none");
                    $(".widgetPanel").show();
                    $("#btnSettings").addClass("d-none");
                    grid.enableMove(true);
                    grid.enableResize(true);
                    IS_EDITING_MODE = true;
                } else {
                }
                _jsHelpers._unBlockUi();
            }).fail((error) => {
                _jsHelpers._unBlockUi();
                _jsHelpers._handle_notify('danger', error.responseJSON.message);
            });
        }
    });
});
grid.on('resizestop', function (event, el) {
    let node = el.gridstackNode;

    switch (node.chart_type) {
        case "mixed":
            el.setAttribute('gs-w', Number(el.getAttribute('gs-h')) + 1);
            CHARTS[node.id].updateOptions({}, true, true, true);
            break;
        case "pie":
            el.setAttribute('gs-w', Number(el.getAttribute('gs-h')) + 1);
            CHARTS[node.id].updateOptions({}, true, true, true);
            break;
        case "table":
            break;
        case "label":
            let w = el.getAttribute('gs-w');
            el.setAttribute('gs-h', Number(1));
            if (w < 2 || w > 4) {
                el.setAttribute('gs-w', Number(1));
            }
            break;
        case "text":
            break;
    }
    IS_GRID_EDITED = true;
});

grid.on('dragstop', function (event, el) {
    IS_GRID_EDITED = true;
});


$("#from-date").on("change", function () {
    document.getElementById("to-date").min = $("#from-date").val();
})

$("#btnApplyDateFilter").on("click", function (e) {
    let dateFilter = {
        "from_date": formateDateToString($("#from-date").val()), // "10/10/2022",
        "to_date": formateDateToString($("#to-date").val()), //"15/10/2022"
    }
    getTempletData($('#templetsList').val(), dateFilter);
    IS_DATE_FILTER_USED = true;
});

function handleMouseUpToHideWidgetFilter(e) {
    if (e.target.classList.contains("collapsible")) return;
    if (e.target.classList.contains("widgetFilteritem")) return;
    if (e.target.classList.contains("widgetFilteritem_label")) return;
    $(".filterList").each(function () {
        $(this).addClass("d-none");
    });
}

async function getTempletData(templet_id, dateFilter) {
    SELECTED_BRANCHES_FILTER = SELECTED_BRANCHES;
    grid.removeAll();
    _jsHelpers._blockUI();
    await $.ajax({
        type: "GET",
        url: _jsHelpers._PREFIX_ADMIN_API + "/dashboard/template_data?type=" + DYANMIC_TYPE + "&template_id=" + (templet_id ? templet_id : ""),
        headers: _jsHelpers._getHeaders(),
        data: dateFilter
    }).done((res) => {
        if (!res.error) {
            TEMPLET_DATA = res;
            $("#NoWidgets").addClass("d-none");
            if (TEMPLET_DATA.data.length == 0) {
                $("#NoWidgets").removeClass("d-none")
            } else {
                if (TEMPLET_DATA.data.template.widgets.length == 0) {
                    $("#dateFilteContainer").hide();

                    $("#NoWidgets").removeClass("d-none")
                } else {
                    $("#dateFilteContainer").show();
                }
            }
        } else {
            _jsHelpers._handle_notify('danger', res.message);
        }
        _jsHelpers._unBlockUi();
    }).fail((error) => {
        _jsHelpers._unBlockUi();
        _jsHelpers._handle_notify('danger', error.responseJSON.message);
    });
    if (TEMPLET_DATA.data.length == 0) {
        return;
    }
    if (TEMPLET_DATA?.data?.users_Templates?.length > 0) {
        $('#templetsList').empty();
        $('#templetsList').append(`<optgroup label="${_jsHelpers._trans("yourDashboard")}">`);
        TEMPLET_DATA?.data?.users_Templates?.forEach(temp => {
            $('#templetsList').append(`<option value="${temp.id}">${temp.name}</option>`);
        });
    };
    if (TEMPLET_DATA?.data?.company_templates?.length > 0) {
        $('#templetsList').append(`<optgroup label="${_jsHelpers._trans("globalDashboard")}">`);
        TEMPLET_DATA?.data?.company_templates.forEach(temp => {
            $('#templetsList').append(`<option value="${temp.id}">${temp.name}</option>`);
        });
    }
    $("#templetsList").val(TEMPLET_DATA?.data?.template?.id);
    convertResponseToWidgets();
};

function renderSeries(data) {
    $('#seriesList').empty();
    data.forEach(item => {
        $('#seriesList').append(
            `
        <div id="${item.id}" class="seriesItem">
            <div style="display: flex;justify-content: space-between;">
                <div style="width:20%">
                  <P>${item.name}</P>
                </div>
                <div style="width:20%">
                  <P>${item.chart_type}</P>
                </div>
                <div class="row color_dev"  style="width:20%">
                <span style="
                background: ${item.color} !important;
                color: ${item.color};
                height: 12px;
                width: 12px;
                border-width: 0px;
                border-color: ${item.color};
                border-radius: 12px;
                margin-right: 4px;
                margin-top: 4px;
                "></span><br/>
                  <p>${item.color}</p>
                </div>
                <div style="width:20%">
                  <i data-id="${item.id}" class="bi bi-x deleteSeries"></i>
                </div>
            </div>
        </div>
        `);
    });

    if (data.length > 0 && data[0].chart_type != "line") {
        $(".color_dev").addClass("d-none");
    }else {
        $(".color_dev").removeClass("d-none");
    }

    $(".deleteSeries").off("click");
    $(".deleteSeries").on("click", function (e) {
        $("#btnAddSeries").removeClass("d-none")
        WIDGET_DATA.series.splice(WIDGET_DATA.series.findIndex(x => x.id == $(this).data("id")), 1);
        renderSeries(WIDGET_DATA.series);
    });
    if (data.length == 1 && data[0].chart_type == "label") {
        $("#btnAddSeries").addClass("d-none")
    } else {
        $("#btnAddSeries").removeClass("d-none")

    }
    autoFillWidgetTitle();

};

async function init() {
    _jsHelpers._blockUI();
    await $.ajax({
        type: "POST",
        url: _jsHelpers._PREFIX_ADMIN_API + "/catalog/branchesWithGroups/",
        headers: _jsHelpers._getHeaders(),
    }).done((res) => {
        if (!res.error) {
            ALL_COMPANY_BRANCHES = res.data.branches;
            SELECTED_BRANCHES = ALL_COMPANY_BRANCHES.map(e => {
                e.group_id = 0;
                return e
            });
            SELECTED_BRANCHES_FILTER = SELECTED_BRANCHES;
            SERIES_DATA.branch_ids = SELECTED_BRANCHES.map(e => e.id);
        }
        _jsHelpers._unBlockUi();
    }).fail((error) => {
        _jsHelpers._unBlockUi();
        _jsHelpers._handle_notify('danger', error.responseJSON.message);
    });






    $('#btnSelectBranches').on('click', (e) => {
        e.preventDefault();
        _jsBranchesModalHelper._showMoreBranshesModal__A(
            (selectedBranches) => handleBranchSelection_CB(selectedBranches),
            SELECTED_BRANCHES_FILTER)
    });

    const handleBranchSelection_CB = (selectedBranches) => {
        SELECTED_BRANCHES_FILTER = selectedBranches
        renderWidgetAfterBranchFilter();
    };

};

function generateUniqueColor(count) {
    while (count > linkersGraphColors.length) {
        var c = "#";
        c1 = linkersGraphColors[linkersGraphColors.length - 11];
        c2 = linkersGraphColors[linkersGraphColors.length - 10];
        for (var i = 0; i < 3; i++) {
            var sub1 = c1.substring(1 + 2 * i, 3 + 2 * i);
            var sub2 = c2.substring(1 + 2 * i, 3 + 2 * i);
            var v1 = parseInt(sub1, 16);
            var v2 = parseInt(sub2, 16);
            var v = Math.floor((v1 + v2) / 2);
            var sub = v.toString(16).toUpperCase();
            var padsub = ('0' + sub).slice(-2);
            c += padsub;
        }
        linkersGraphColors.push(c);
    }
}
function handleModalsLogic() {
    $('.carousel').carousel('pause');

    function getLastPlaceCoordinates() {
        let maxX = 0;
        let maxY = 0;
        WIDGETS.forEach(widget => {
            if (widget.x > maxX) {
                maxX = widget.x
            }
            if (widget.y > maxY) {
                maxY = widget.y
            }
        });
        maxX += 2;
        maxY += 2;
        return {
            x: maxX,
            y: maxY
        };
    }

    function handelSaveWidget(e, type) {
        WIDGET_DATA.title = $("#widgetTitle").val();
        switch (true) {
            case WIDGET_DATA.title == "":
                _jsHelpers._handle_notify('danger', 'Title Field Is Required!!');
                break;
            case WIDGET_DATA.series.length == 0:
                if (WIDGET_DATA.chart_type != "text") {
                    _jsHelpers._handle_notify('danger', 'No Searies Has Been Added!!');
                }
                break;
        }


        e.preventDefault();
        if (WIDGET_DATA.position.h) {
            WIDGET_DATA.position = JSON.stringify(WIDGET_DATA.position)
        }
        if (type == "add") {
            let oldPosition = JSON.parse(WIDGET_DATA.position)
            var position = getLastPlaceCoordinates();
            oldPosition.x = position.x;
            oldPosition.y = position.y;
            WIDGET_DATA.position = JSON.stringify(oldPosition)
        }
        WIDGET_DATA.dashboard_template_id = TEMPLET_DATA.data.template.id
        WIDGET_DATA.series.forEach(variable => {
            delete variable.data;
        });
        WIDGET_DATA.text_title = "";
        if (WIDGET_DATA.chart_type == "text") {
            WIDGET_DATA.series = [];
            WIDGET_DATA.text_title = $("#widgetText").val();
        }

        WIDGET_DATA.results = $("#results").val() ? $("#results").val() : "global";
        WIDGET_DATA.time_period = $("#timeLape").val();

        WIDGET_DATA.resolution = $("#resolution").val();

        if (WIDGET_DATA.time_period == "custom") {
            let from = $("#time-lape-from").val();
            let to = $("#time-lape-to").val();
            WIDGET_DATA.from_date = formateDateToString(from);
            WIDGET_DATA.to_date = formateDateToString(to);
        }
        _jsHelpers._blockUI();
        $.ajax({
            type: type == "add" ? "POST" : "PUT",
            url: _jsHelpers._PREFIX_ADMIN_API + "/dashboard/widget/" + (type == "add" ? "" : WIDGET_DATA.id),
            headers: _jsHelpers._getHeaders(),
            data: WIDGET_DATA
        }).done((res) => {
            if (!res.error) {
                $("#widgetModal").modal("hide");
                TEMPLET_DATA.data.template.widgets = res.data.widgets;
                WIDGET_DATA.series = [];
                grid.removeAll();
                $("#NoWidgets").addClass("d-none");
                $("#dateFilteContainer").removeClass("d-none");
                convertResponseToWidgets();
                if (res.data.widgets.length == 0) {
                    $("#dateFilteContainer").hide();
                } else {
                    $("#dateFilteContainer").show();
                }
            }
            _jsHelpers._unBlockUi();
        }).fail((error) => {
            _jsHelpers._unBlockUi();
            _jsHelpers._handle_notify('danger', error.responseJSON.message);
        });
    }
    $('.btnNext').on('click', e => handelSaveWidget(e, "add"));

    $(".btnSaveWidgetChanges").on("click", e => handelSaveWidget(e, "put"));

    $(".deleteBranch").on("click", function (e) {
        var id = $(this).data("id");
        const indexOfObject = SELECTED_BRANCHES.findIndex(object => {
            return object.id === id;
        });
        SELECTED_BRANCHES.splice(indexOfObject, 1);
    });

    $('.backWidgetType').on('click', function (e) {

        $('#seriesList').empty();
        WIDGET_DATA.series = [];
        renderSeries(WIDGET_DATA.series)
        $(".carousel-item").removeClass("active");
        $(".carousel-item").eq(0).addClass("active");
        $('#widgetWizardFooter').addClass("d-none");
    });

    $('.widgetType').on('click', function (e) {
        WidgetChartType = $(this).data("value");
        WIDGET_DATA.chart_type = $(this).data("value");
        handelWidgetModalLogic(WIDGET_DATA.chart_type);
        $(".carousel-item").removeClass("active");
        $(".carousel-item").eq(1).addClass("active");
        $('#widgetWizardFooter').removeClass("d-none");
        $('#timeLape').trigger("change");
    });

    $("#timeLapeContainer").change(function (e) {
        if (e.target.value == "custom") {
            $("#custom-date").show();
        } else {
            $("#custom-date").hide();
        }
    })

    $('#btnAddSeries').on('click', function (e) {
        $('#seriesModal').modal("show");
        switch (WIDGET_DATA.chart_type) {
            case "bar":
                $("#chartType").empty();
                $("#chartType").append('<option value="bar">Bar</option>');
                $("#colorContainer").addClass("d-none");
                break;
            case "mixed":
                $("#chartType").empty();
                $("#chartType").append('<option value="line">Line</option>');
                $("#chartType").append('<option value="column">Column</option>');
                $("#chartType").append('<option value="area">Area</option>');
                $("#colorContainer").removeClass("d-none");
                break;
            case "pie":
                $("#chartType").empty();
                $("#chartType").append('<option value="pie">Pie</option>');
                $("#colorContainer").addClass("d-none");
                break;
            case "table":
                $("#chartType").empty();
                $("#chartType").append('<option value="table">Table</option>');
                $("#colorContainer").addClass("d-none");
                break;
            case "label":
                $("#chartType").empty();
                $("#chartType").append('<option value="label">label</option>');
                $("#colorContainer").addClass("d-none");
                break;
        }
        $("#name").val(formateSeriesName($("#seriesSelection").val()));
        renderSelectedBranches(SELECTED_BRANCHES);
    });

    $('#resolution').change(function (e) {
        WIDGET_DATA.resolution = e.target.value
        autoFillWidgetTitle();
    })
    $('#results').change(function (e) {
        $("#resolutionContainer").removeClass("d-none");

        if ($('#results').val() == "comparative") {
            $("#resolutionContainer").addClass("d-none");   
            return;
        }
        if (WIDGET_DATA.chart_type != 'mixed' && WIDGET_DATA.chart_type != 'table') {
            $("#resolutionContainer").addClass("d-none");
            return;   
        }
    })

    $('#timeLape').change(function (e) {
        WIDGET_DATA.time_period = e.target.value
        switch (e.target.value) {
            case "today":
                $("#hourlyOption").prop("disabled", false);
                $("#dailyOption").prop("disabled", false);

                $("#weeklyOption").prop("disabled", true);
                $("#monthlyOption").prop("disabled", true);
                $("#quartarlyOption").prop("disabled", true);
                if ($("#resolution").val() != 1 || $("#resolution").val() != 0) {
                    $("#resolution").val(0);
                }
                if (WIDGET_DATA.chart_type == "label" || WIDGET_DATA.chart_type == "pie") {
                    $("#resolution").val(0);
                }
                break;
            case "yesterday":
                $("#hourlyOption").prop("disabled", false);
                $("#dailyOption").prop("disabled", false);

                $("#weeklyOption").prop("disabled", true);
                $("#monthlyOption").prop("disabled", true);
                $("#quartarlyOption").prop("disabled", true);
                if ($("#resolution").val() != 1 || $("#resolution").val() != 0) {
                    $("#resolution").val(0);
                }
                if (WIDGET_DATA.chart_type == "label" || WIDGET_DATA.chart_type == "pie") {
                    $("#resolution").val(1);
                }
                break;
            case "this_week":
                $("#dailyOption").prop("disabled", false);
                $("#weeklyOption").prop("disabled", false);

                $("#hourlyOption").prop("disabled", true);
                $("#monthlyOption").prop("disabled", true);
                $("#quartarlyOption").prop("disabled", true);
                if ($("#resolution").val() != 2 || $("#resolution").val() != 1) {
                    $("#resolution").val(2);
                }
                if (WIDGET_DATA.chart_type == "label" || WIDGET_DATA.chart_type == "pie") {
                    $("#resolution").val(2);
                }
                break;
            case "last_week":
                $("#dailyOption").prop("disabled", false);
                $("#weeklyOption").prop("disabled", false);

                $("#hourlyOption").prop("disabled", true);
                $("#monthlyOption").prop("disabled", true);
                $("#quartarlyOption").prop("disabled", true);
                if ($("#resolution").val() != 2 || $("#resolution").val() != 1) {
                    $("#resolution").val(2);
                }
                if (WIDGET_DATA.chart_type == "label" || WIDGET_DATA.chart_type == "pie") {
                    $("#resolution").val(2);
                }
                break;
            case "this_month":
                $("#dailyOption").prop("disabled", false);
                $("#weeklyOption").prop("disabled", false);
                $("#monthlyOption").prop("disabled", false);

                $("#hourlyOption").prop("disabled", true);
                $("#quartarlyOption").prop("disabled", true);
                if ($("#resolution").val() != 3 || $("#resolution").val() != 2 || $("#resolution").val() != 1) {
                    $("#resolution").val(3);
                }
                if (WIDGET_DATA.chart_type == "label" || WIDGET_DATA.chart_type == "pie") {
                    $("#resolution").val(3);
                }
                break;
            case "last_month":
                $("#dailyOption").prop("disabled", false);
                $("#weeklyOption").prop("disabled", false);
                $("#monthlyOption").prop("disabled", false);

                $("#hourlyOption").prop("disabled", true);
                $("#quartarlyOption").prop("disabled", true);
                if ($("#resolution").val() != 3 || $("#resolution").val() != 2 || $("#resolution").val() != 1) {
                    $("#resolution").val(3);
                }
                if (WIDGET_DATA.chart_type == "label" || WIDGET_DATA.chart_type == "pie") {
                    $("#resolution").val(3);
                }
                break;
            case "this_year":
                $("#weeklyOption").prop("disabled", false);
                $("#monthlyOption").prop("disabled", false);
                $("#quartarlyOption").prop("disabled", false);

                $("#hourlyOption").prop("disabled", true);
                $("#dailyOption").prop("disabled", true);
                if ($("#resolution").val() != 4 || $("#resolution").val() != 3 || $("#resolution").val() != 2) {
                    $("#resolution").val(4);
                }
                if (WIDGET_DATA.chart_type == "label" || WIDGET_DATA.chart_type == "pie") {
                    $("#resolution").val(4);
                }
                break;
            case "last_year":
                $("#weeklyOption").prop("disabled", false);
                $("#monthlyOption").prop("disabled", false);
                $("#quartarlyOption").prop("disabled", false);

                $("#hourlyOption").prop("disabled", true);
                $("#dailyOption").prop("disabled", true);
                if ($("#resolution").val() != 4 || $("#resolution").val() != 3 || $("#resolution").val() != 2) {
                    $("#resolution").val(4);
                }
                if (WIDGET_DATA.chart_type == "label" || WIDGET_DATA.chart_type == "pie") {
                    $("#resolution").val(4);
                }
                break;
            case "custom":
                $("#weeklyOption").prop("disabled", false);
                $("#monthlyOption").prop("disabled", false);
                $("#quartarlyOption").prop("disabled", false);
                $("#hourlyOption").prop("disabled", false);
                $("#dailyOption").prop("disabled", false);
                $("#customDate").removeClass("d-none")

                break;
            default:
                $("#customDate").addClass("d-none")
                break;
        }
        autoFillWidgetTitle();
    });

    $("#widgetTitle").change(function (e) {
        WIDGET_DATA.title = e.target.value;
    });

    $("#seriesSelection").change(function (e) {
        SERIES_DATA.analysis_of = e.target.value;
        $("#name").val(formateSeriesName($("#seriesSelection").val()));
    });

    $("#chartType").change(function (e) {
        SERIES_DATA.chartType = e.target.value;
    });

    $("#color").change(function (e) {
        SERIES_DATA.color = e.target.value;
    });

    $("#btnAdd").on('click', function (e) {
        if (SELECTED_BRANCHES.length == 0) {
            _jsHelpers._handle_notify('danger', "branch shouldn't be empty");
            return;
        }
        SERIES_DATA.analysis_of = $("#seriesSelection").val();
        SERIES_DATA.chart_type = $("#chartType").val();
        SERIES_DATA.color = $("#color").val();
        SERIES_DATA.name = $("#name").val();
        WIDGET_DATA.series.push(SERIES_DATA);
        SERIES_DATA.id = WIDGET_DATA.series.length;
        renderSeries(WIDGET_DATA.series)
        SERIES_DATA = {
            name: "",
            analysis_of: "",
            chart_type: "line",
            color: "red",
            branch_ids: []
        }
    });

    function handelTempletSaveChanges(e, type) {
        e.preventDefault();
        $.ajax({
            type: type == "post" ? "POST" : "PUT",
            url: _jsHelpers._PREFIX_ADMIN_API + "/dashboard/template/" + (type == "post" ? "" : $("#templetsList").val()),
            headers: _jsHelpers._getHeaders(),
            data: {
                "name": $("#templetName").val(),
                "company_default": $("#companyDefault").prop("checked"),
                "my_default": $("#myDefault").prop("checked"),
                "type": DYANMIC_TYPE,
                "public": $("#public").prop("checked")
            }
        }).done((res) => {
            if (!res.error) {
                location.reload();
            } else {
                _jsHelpers._handle_notify('danger', res.message);
            }
            _jsHelpers._unBlockUi();
        }).fail((error) => {
            _jsHelpers._unBlockUi();
            _jsHelpers._handle_notify('danger', error.responseJSON.message);
        });
    }

    $("#btnSaveNewTempete").on("click", e => handelTempletSaveChanges(e, "post"));
    $("#btnSaveTempete").on("click", e => handelTempletSaveChanges(e, "put"));

    $("#isChartNumbers").change(function (e) {
        WIDGETS.forEach(widget => {
            if (widget.chart_type !== "table" || widget.chart_type !== "label") {
                if (CHARTS[widget.id]) {
                    CHARTS[widget.id].updateOptions({
                        dataLabels: {
                            enabled: this.checked,
                            style: {
                                fontFamily: 'Helvetica, Arial, sans-serif',
                                fontWeight: 'bold',
                                colors: ['#202121']
                            },
                        }
                    });
                }
            }
        });
    })


    $(document).on("click", ".select2-branch", function () {
        _jsBranchesModalHelper._showMoreBranshesModal__A((data) => {
            SELECTED_BRANCHES = data;
            renderSelectedBranches(SELECTED_BRANCHES)
        }, SELECTED_BRANCHES, selected_groubs);
    });

    $('#addCompany').on('click', (e) => {
        e.preventDefault();
        _jsBranchesCompaniesModalHelper._showMoreBranshesModal__A((data) => {
            SELECTED_BRANCHES = data;
            renderSelectedBranches(SELECTED_BRANCHES)
        }, SELECTED_BRANCHES, selected_groubs);
    });


    const handleBranchCompaniesSelection_CB = (selectedBranches) => {
        SELECTED_BRANCHES = selectedBranches;
        $(".select_company").empty();
        if (SELECTED_BRANCHES.length > 0) {
            SELECTED_BRANCHES.forEach((item) => {
                $(".select_company").append(`<div><p>${item.name}</p></div>`);
            });
        } else {
            $(".select_company").append(
                '<span style="color:#fd5454;">No Branches Selected</span>'
            );
        }
    };
    function renderSelectedBranches(branches) {
        select_branch_add.empty();
        select_branch_add_companies.empty();

        if (SELECTED_BRANCHES == 0) {
            select_branch_add.empty();
            select_branch_add.append(`<option>${_jsHelpers._trans("selectBranch")}</option>`);

            select_branch_add_companies.empty();
            select_branch_add_companies.append(`<option>${_jsHelpers._trans("selectBranch")}</option>`);
        }

        branches.map((branch) => {
            select_branch_add.append(`
              <option id="${branch.id}" class="item-branch1 ckeckboxes "selected"
              " data-id="${branch.id}" data-name="">
              <p>${branch.name}</p>
              </option>
              `);

            select_branch_add_companies.append(`
              <option id="${branch.id}" class="item-branch1 ckeckboxes "selected"
              " data-id="${branch.id}" data-name="">
              <p>${branch.name}</p>
              </option>
              `);
        });

        if (branches.length == 0) {
            select_branch_add.empty();
            select_branch_add.append(`<option>${_jsHelpers._trans("selectBranch")}</option>`);

            select_branch_add_companies.empty();
            select_branch_add_companies.append(`<option>${_jsHelpers._trans("selectBranch")}</option>`);
            $("#select2-branch").height("33px");
            $("#addCompany").height("33px");

        } else {
            $("#addCompany").height("100px");
            $("#select2-branch").height("100px");

        }

        SERIES_DATA.branch_ids = SELECTED_BRANCHES.map(item => item.id);
    }

};

function handleEditTempletBtnClick() {
    $("#templeteOptions").modal("show");
    $("#templetName").val(TEMPLET_DATA.data.template.name);
    $("#myDefault").prop("checked", TEMPLET_DATA.data.template.my_default == "true");
    $("#companyDefault").prop("checked", TEMPLET_DATA.data.template.company_default == "true");
    $("#public").prop("checked", TEMPLET_DATA.data.template.public == "true");
    $("#btnSaveTempete").removeClass("d-none");
    $(".btnSaveTempeteDiv").removeClass("d-none");

    $(".btnSaveNewTempeteDiv").addClass("d-none");
    $("#btnSaveNewTempete").addClass("d-none");
}

function handleDashboardLogic() {



    $("#btnEditTemplet").on('click', function (e) {
        if (IS_EDITING_MODE && IS_GRID_EDITED) {
            _jsHelpers._isConfirmed("You might want to save your changes first!!", () => {
                saveGridChanges().then(() => {
                    handleEditTempletBtnClick();
                });
            }, () => {
                handleEditTempletBtnClick();
            })
        } else {
            handleEditTempletBtnClick();
        }

    });

    $("#btnCopyTempete").on("click", function (e) {
        _jsHelpers._isConfirmed(`${_jsHelpers._trans("copyTemplateMsg")}`, () => {

            _jsHelpers._blockUI();
            $.ajax({
                type: "GET",
                url: _jsHelpers._PREFIX_ADMIN_API + "/dashboard/clone_template/" + $("#templetsList").val(),
                headers: _jsHelpers._getHeaders(),
            }).done((res) => {
                if (!res.error) {
                    location.reload();
                }
                _jsHelpers._unBlockUi();
            }).fail((error) => {
                _jsHelpers._unBlockUi();
                _jsHelpers._handle_notify('danger', error.responseJSON.message);
            });
        });

    })

    $("#deleteTemplet").on("click", function (e) {
        _jsHelpers._isConfirmed(`${_jsHelpers._trans("deleteWidget")}`, () => {
            _jsHelpers._blockUI();
            $.ajax({
                type: "DELETE",
                url: _jsHelpers._PREFIX_ADMIN_API + "/dashboard/template/" + $("#templetsList").val(),
                headers: _jsHelpers._getHeaders(),
            }).done((res) => {
                if (!res.error) {
                  _jsHelpers._handle_notify('success', res.message);

                    location.reload();
                }else {
                  _jsHelpers._handle_notify('danger', res.message);

                }
                _jsHelpers._unBlockUi();
            }).fail((error) => {
                _jsHelpers._unBlockUi();
                _jsHelpers._handle_notify('danger', error.responseJSON.message);
            });
        });
    });

    $("#companyDefault").change(function (e) {
        if (this.checked) {
            $("#public").prop("checked", true);
        }
    });

    $("#public").change(function (e) {
        if (!this.checked) {
            $("#companyDefault").prop("checked", false);
        }
    })

    $('#btnAddTemplet').on("click", function (e) {
        $("#templeteOptions").modal("show");
        $("#templetName").val("");
        $("#btnSaveTempete").addClass("d-none");
        $(".btnSaveTempeteDiv").addClass("d-none");

        $("#btnSaveNewTempete").removeClass("d-none");
        $(".btnSaveNewTempeteDiv").removeClass("d-none");

        $("#myDefault").prop("checked", false);
        $("#companyDefault").prop("checked", false);
        $("#public").prop("checked", false);
    });

    function handelAddWidgetBtnClick() {
        document.getElementById("time-lape-from").valueAsDate = new Date();
        document.getElementById("time-lape-to").valueAsDate = new Date();
        $("#widgetModal").modal("show");
        $("#widgetTitle").val("");
        $("#widgetText").val("");
        $("#resolution").val("0");
        WIDGET_DATA.series = [];
        $("#timeLape").val("today");
        $("#results").val("global");
        $("#seriesList").empty();
        $(".carousel-item").removeClass("active");
        $(".carousel-item").first().addClass("active");
        $('.btnNext').removeClass("d-none");
        $('.btnSaveWidgetChanges').addClass('d-none');
        $("#btnAddSeries").removeClass("d-none");
        $('#widgetWizardFooter').addClass("d-none");
    }

    $('#autoFillName').change(e => autoFillWidgetTitle());

    $('#widgetTitle').on('input', function() {
        $("#autoFillName").prop("checked" , false);

    });

    $(".btnAddWidget").on('click', function (e) {
        if (IS_EDITING_MODE && IS_GRID_EDITED) {
            _jsHelpers._isConfirmed("You might want to save your changes first!!", () => {
                saveGridChanges().then(() => {
                    handelAddWidgetBtnClick();
                });
            }, () => {
                handelAddWidgetBtnClick();
            })
        } else {
            handelAddWidgetBtnClick();
        }
    });



    async function startEditingMode() {
        _jsHelpers._blockUI();
        await $.ajax({
            type: "GET",
            url: _jsHelpers._PREFIX_ADMIN_API + "/dashboard/availability/" + $("#templetsList").val(),
            headers: _jsHelpers._getHeaders(),
        }).done((res) => {
            if (res.error) {
                _jsHelpers._handle_notify('danger', res.message);
                $("#temolatePanel").addClass("d-none");
                $("#editModePanel").addClass("d-none");
                $(".widgetPanel").hide();
                $("#btnSettings").removeClass("d-none");
                grid.enableMove(false);
                grid.enableResize(false);
                IS_EDITING_MODE = false
                IS_GRID_EDITED = false;
            } else {
                $("#temolatePanel").removeClass("d-none");
                $("#editModePanel").removeClass("d-none");
                $(".widgetPanel").show();
                $("#btnSettings").addClass("d-none");
                grid.enableMove(true);
                grid.enableResize(true);
                IS_EDITING_MODE = true;
            }
            _jsHelpers._unBlockUi();
        }).fail((error) => {
            _jsHelpers._unBlockUi();
            _jsHelpers._handle_notify('danger', error.responseJSON.message);
        });
    }

    $("#btnEditTemplate").on("click", function (e) {
        startEditingMode();
    });

    $("#cancelEditMode").on("click", function (e) {
        $("#temolatePanel").addClass("d-none");
        $("#editModePanel").addClass("d-none");
        $(".widgetPanel").hide();
        $("#btnSettings").removeClass("d-none");
        grid.enableMove(false);
        grid.enableResize(false);
        IS_EDITING_MODE = false
        IS_GRID_EDITED = false;

        $.ajax({
            type: "GET",
            url: _jsHelpers._PREFIX_ADMIN_API + "/dashboard/leave_edit/" + $("#templetsList").val(),
            headers: _jsHelpers._getHeaders(),
        }).done((res) => {
            if (res.error) {
                _jsHelpers._handle_notify('danger', res.message);
                $("#temolatePanel").removeClass("d-none");
                $("#editModePanel").removeClass("d-none");
                $(".widgetPanel").show();
                $("#btnSettings").addClass("d-none");
                grid.enableMove(true);
                grid.enableResize(true);
                IS_EDITING_MODE = true;
            } else { }
            _jsHelpers._unBlockUi();
        }).fail((error) => {
            _jsHelpers._unBlockUi();
            _jsHelpers._handle_notify('danger', error.responseJSON.message);
        });

    });

    $("#btnSaveEditMode").on("click", function () {
        saveGridChanges();
    })

    $(".btnCompact").on("click", e => grid.compact())
};

async function saveGridChanges() {
    IS_GRID_EDITED = false;
    serializedFull = grid.save(true, true);
    serializedData = serializedFull.children;
    var positions = serializedFull.children.map(item => {
        let obj = {};
        obj.id = item.id;
        obj.position = JSON.stringify({
            x: item.x,
            y: item.y,
            h: item.h,
            w: item.w
        });
        return obj;
    });
    _jsHelpers._blockUI();
    if (!positions.length) {
        $("#temolatePanel").addClass("d-none");
        $("#editModePanel").addClass("d-none");
        $(".widgetPanel").hide();
        $("#btnSettings").removeClass("d-none");
        $("#NoWidgets").removeClass("d-none")
        grid.enableMove(false);
        grid.enableResize(false);
        IS_EDITING_MODE = false;
        _jsHelpers._handle_notify('success', "Changed Saved");
    }
    await $.ajax({
        type: "PUT",
        url: _jsHelpers._PREFIX_ADMIN_API + "/dashboard/update_wedgit_position",
        headers: _jsHelpers._getHeaders(),
        data: {
            positions
        }
    }).done((res) => {
        if (!res.error) {
            $("#temolatePanel").addClass("d-none");
            $("#editModePanel").addClass("d-none");
            $(".widgetPanel").hide();
            $("#btnSettings").removeClass("d-none");
            grid.enableMove(false);
            grid.enableResize(false);
            IS_EDITING_MODE = false;
            _jsHelpers._handle_notify('success', "Changed Saved");
        }
        _jsHelpers._unBlockUi();
    }).fail((error) => {
        _jsHelpers._unBlockUi();
        _jsHelpers._handle_notify('danger', error.responseJSON.message);
    });
}

function handelWidgetModalLogic(value) {
    $("#timeLapeContainer").removeClass("d-none");
    $("#seriesListContainer").removeClass("d-none");
    $("#widgetTextContainer").addClass('d-none');
    $("#results").val('global').change();
    $("#resolutionContainer").removeClass("d-none");
    $("#resultsContainer").removeClass("d-none");

    autoFillWidgetTitle();

    switch (value) {
        case "bar":
            WIDGET_DATA.resolution = 1;
            $("#autoLabel").removeClass("d-none");
            $("#autoFillName").removeClass("d-none");
            $(".color_dev").removeClass("d-none");

            break;
        case "mixed":
            WIDGET_DATA.resolution = 1;
            WIDGET_DATA.position.h = 4;
            WIDGET_DATA.position.w = 5;
            $("#autoLabel").removeClass("d-none");
            $("#autoFillName").removeClass("d-none");
            $(".color_dev").removeClass("d-none");

            break;
        case "pie":
            WIDGET_DATA.position.h = 4;
            WIDGET_DATA.position.w = 5;
            $("#resolutionContainer").addClass("d-none");
            $("#autoLabel").removeClass("d-none");
            $("#autoFillName").removeClass("d-none");
            $(".color_dev").addClass("d-none");

            break;
        case "table":
            WIDGET_DATA.resolution = 1;
            WIDGET_DATA.position.h = 4;
            WIDGET_DATA.position.w = 5;
            $("#autoLabel").removeClass("d-none");
            $("#autoFillName").removeClass("d-none");
            $(".color_dev").addClass("d-none");
            if ($('#results').val() == "comparative") {
                $("#resolutionContainer").addClass("d-none");   
                return;
            }

            break;
        case "label":
            $("#resolutionContainer").addClass("d-none");
            $("#resultsContainer").addClass("d-none");
            $("#autoLabel").removeClass("d-none");
            $("#autoFillName").removeClass("d-none");
            $(".color_dev").addClass("d-none");
            WIDGET_DATA.position.h = 2;
            WIDGET_DATA.position.w = 3;
            break;
        case "text":
            WIDGET_DATA.position.h = 2;
            WIDGET_DATA.position.w = 3;
            $("#widgetTextContainer").removeClass('d-none');
            $("#timeLapeContainer").addClass("d-none");
            $("#resolutionContainer").addClass("d-none");
            $("#resultsContainer").addClass("d-none");
            $("#seriesListContainer").addClass("d-none");
            $("#autoLabel").addClass("d-none");
            $("#autoFillName").addClass("d-none");
            $("#widgetTitle").val(' ');
            break;
    }
};

const formateDateToString = (date) => {
    date = new Date(date)
    return `${String(date.getDate()).padStart(2, '0')}/${String(date.getMonth() + 1).padStart(2, '0')}/${String(date.getFullYear()).padStart(2, '0')}`;
};

function getDaysArray(start, end, type = 1) {
    start = new Date(`${start.split("/")[1]}-${start.split("/")[0]}-${start.split("/")[2]}`);
    end = new Date(`${end.split("/")[1]}-${end.split("/")[0]}-${end.split("/")[2]}`);
    for (var arr = [], dt = new Date(start); dt <= end; dt.setDate(dt.getDate() + 1)) {
        arr.push(formateDateToString(dt));
    }

    switch (String(type)) {
        case '0':
            let new_arr = []
            let allDaysHours = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"]
            arr.forEach(day => {
                allDaysHours.forEach(hour => {
                    new_arr.push(day + " " + hour)
                });
            });
            return new_arr;
        case '1':
            return arr;
        case '2':
            let new_arr1 = [];
            arr.forEach((item, idx) => {
                let day = item.split("/")[0];
                if ((Number(day) - 1) % 7 == 0) {
                    new_arr1.push(item);
                }
            });
            return new_arr1;
        case '3':
            let new_arr2 = [];
            arr.forEach((item, idx) => {
                let month = item.split("/")[1];
                if (!new_arr2.includes(month)) {
                    new_arr2.push(month);
                }
            });
            return new_arr2;
    }
};

function Num2Time(stay_time) {
    min = stay_time - Math.floor(stay_time / 60) * 60
    sec = (Number(min - Math.floor(min)).toFixed(2)) * 60;
    return `${String(Math.floor(stay_time / 60)).padStart(2, "0")}:${String(Math.floor(min)).padStart(2, "0")}:${String(Math.floor(sec)).padStart(2, "0")}`
}

function convertDataToSeries(widget) {
    var series = [];
    var categories = [];
    switch (widget.chart_type) {
        case "mixed":
            if (widget.results == "global") {
                widget.data.forEach(dataItem => {
                    series.push({
                        name: dataItem.name,
                        type: dataItem.type,
                        data: dataItem.data.totals
                    });
                });

                return {
                    series,
                    categories: widget.categories
                };
            } else {
                widget.data.forEach(dataItem => {
                    var data = [];
                    Object.keys(dataItem.data).forEach(branch => {
                        if (branch != "totals") {
                            categories.push(branch);
                            if (percentVariables.includes(dataItem.analysis_of)) {
                                data.push(dataItem.data[branch].reduce((partialSum, a) => Number(partialSum) + Number(a), 0) / dataItem.data[branch].length);
                            } else {
                                data.push(dataItem.data[branch].reduce((partialSum, a) => Number(partialSum) + Number(a), 0));
                            }
                        }
                    });

                    series.push({
                        name: dataItem.name,
                        type: dataItem.type,
                        data: data
                    });
                });
                return {
                    series,
                    categories
                };
            }

        case "pie":
            if (widget.results == "global") {
                widget.data.forEach(dataItem => {
                    categories.push(dataItem.name);
                    var total = 0;
                    Object.keys(dataItem.data).map(branch => {
                        total = total + Number(dataItem.data[branch]);
                    })
                    series.push(total)
                })
            } else {
                series = Object.keys(widget.data[0].data).map(e => widget.data[0].data[e][0]);
                categories = Object.keys(widget.data[0].data);
            }
            return {
                series, categories
            };
    }
}

function convertUnitToString(value, unit) {
    switch (unit) {
        case 'number':
            return value;
        case 'second':
            return Num2Time(value);
        case 'percent':
            return `${Number(value).toFixed(0)}%`
    }
}

function generateWidgetBranchFilter(widget) {
    if (widget.results == "gloabl") {
        return widget.data.map((dataItem, idx) => {
            return `<div style="margin:0px 2px;">
                        <button data-collapsed="false" data-class="${dataItem.name}" type="button" class="collapsible">${dataItem.name}</button>
                      <div class="${dataItem.name}-dtl" style="display:none;">
                          ${Object.keys(dataItem.data).map(key => {
                return `<div style="display: flex; margin: 2px;">
                                <input data-id="${widget.id}" data-name="${key}" data-class="${dataItem.name}-${widget.id}-${idx}" type="checkbox" class="${dataItem.name}-${widget.id}-${idx} dropdown-item widgetFilteritem" name="${dataItem.name}-${key}" id="${dataItem.name}-${key}" checked>
                                <label class="widgetFilteritem_label" for="${dataItem.name}-${key}" style="margin-left: 5px;">${key}</label>
                            </div>`}).join("")}
                          </div>
                  </div>`}).join("");
    } else {
        return "";
    }
}

function findTheLongestDateArray(arr) {
    longestArr = [];
    arr.forEach(item=>{
        if(longestArr.length < item.date.length){
            longestArr = item.date
        }
    });
    return longestArr;
}

function convertResponseToWidgets() {
    WIDGETS = [];
    if (TEMPLET_DATA?.data.template?.widgets?.length > 0) {
        TEMPLET_DATA.data.template.widgets.forEach((widget, idx) => {
            var widgetNode = JSON.parse(widget.position);

            widgetNode.id = widget.id;
            widgetNode.results = widget.results;
            widgetNode.resolution = widget.resolution;
            widgetNode.title = widget.title;
            widgetNode.time_period = widget.time_period;
            widgetNode.data = [].map(e => e);
            widgetNode.colors = [].map(e => e);
            widgetNode.chart_type = widget.chart_type;
            widgetNode.text_title = widget.text_title;

            widget.series.forEach((variable, idx) => {
                widgetNode.colors.push(variable.color);
                let obj = {};
                obj.name = variable.name == null ? " " : variable.name;
                obj.analysis_of = variable.analysis_of;
                obj.type = variable.chart_type;
                obj.data = {};
                switch (widget.chart_type) {
                    case "mixed":
                        var totals = [];
                        try {
                            // widgetNode.categories = variable.data[0].date;
                            widgetNode.categories = findTheLongestDateArray(variable.data);
                            widgetNode.categories.forEach((date, idx) => {
                                variable.data.forEach(dataItem => {
                                    obj.data[dataItem.branch] = dataItem.data;
                                    totals[idx] = Number(totals[idx] ? totals[idx] : 0) + Number(dataItem.data[idx] ? dataItem.data[idx] : 0)
                                });
                            });
                            if (widget.results == "global") {
                                if (variable.data[0].result == "avg") {
                                    obj.data["totals"] = totals.map(e => e / Object.keys(obj.data).length);
                                } else {
                                    obj.data["totals"] = totals;
                                }
                            }
                        } catch (error) {
                            widgetNode.categories = [];
                            obj.data["totals"] = [];
                        }
                        break;
                    case "label":
                        var totals = [];
                        try {
                            obj.img_path = variable.data[0].icon
                            var totals = 0;
                            // widgetNode.categories = variable.data[0].date;
                            widgetNode.categories = findTheLongestDateArray(variable.data);
                            variable.data.forEach(dataItem => {
                                obj.data[dataItem.branch] = dataItem.total;
                                totals = totals + dataItem.total;
                            })
                            if (variable.data[0].result == "avg") {
                                obj.data["totals"] = convertUnitToString([Number(totals / Object.keys(obj.data).length).toFixed(1)], variable.data[0].unit);
                            } else {
                                obj.data["totals"] = Number(convertUnitToString([totals], variable.data[0].unit)).toFixed(1);
                            }
                        } catch (error) {
                            obj.data["totals"] = 0;
                        }
                        break;
                    case "pie":
                        try {
                            widgetNode.categories = variable.data.map(e => e.branch);
                            variable.data.forEach(dataItem => {
                                obj.data[dataItem.branch] = [dataItem.total];
                            });
                        } catch (error) {
                            console.error(error);
                        }
                        break;
                    case "table":
                        var totals = [];
                        widgetNode.categories = findTheLongestDateArray(variable.data);//getDaysArray(widget.from_date, widget.to_date, widget.resolution);
                        try {
                            variable.data.forEach(dataItem => {
                                totals.push(dataItem.totals);
                                obj.data[dataItem.branch] = dataItem.data;
                            });
                        } catch (error) {
                            console.error(error);
                        }
                        obj.data["totals"] = totals;
                        break;
                    default:
                        break;
                }
                if (widgetNode.data) {
                    widgetNode.data.push(obj)
                } else {
                    widgetNode.data = [];
                    widgetNode.data.push(obj);
                }
            });
            if (screen_width_size < '700') {
                widgetNode.w = 12;
                widgetNode.h = 12;
            }
            WIDGETS.push(widgetNode);
        });
    }

    // -----------Render The Created Widgets
    WIDGETS.forEach(widget => {
        var cwidgetControleEle = "";
        let data = convertDataToSeries(widget);
        switch (widget.chart_type) {
            case "mixed":
                cwidgetControleEle = `
                <div id="widget-${widget.id}" class="widgetContainer">
                    <div style="display:flex;justify-content:space-between;padding: 6px;">
                        <h5 style="text-align: initial;">${widget.title}</h5>
                        <div class="widgetControle">
                            <div>
                                <div class="btn-group">
                                    <i data-id="${widget.id}" class="bi bi-gear btnWidgetFilter d-none"></i>
                                    <div id="filterList-${widget.id}" class="filterList d-none">
                                        <div style="flex-wrap: nowrap;justify-content: space-around;padding: 3px;">
                                            ${generateWidgetBranchFilter(widget)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widgetPanel">
                                <i data-id="${widget.id}" style="margin-left:5px" class="bi bi-pencil editWidget"></i>
                                <i data-id="${widget.id}" class="bi bi-x deleteWidget"></i>
                            </div>
                        </div>
                    </div>
                    <div id="chart-${widget.id}"></div>
                </div>`;
                widget.content = cwidgetControleEle;
                addNewWidget(widget);
                mixedChartOptions.series = data.series;
                mixedChartOptions.xaxis.categories = data.categories;
                if (widget.resolution == 0 && widget.results == 'global') {
                    mixedChartOptions.xaxis.categories = data.categories.map((e,idx)=>{
                        if (e.split(" ").length == 1) {
                            return e;
                        }
                        let str1 = e.split(" ")[0].split("/")[0];
                        let str2 = e.split(" ")[1];
                        return `${str2}`;
                    })
                }
                mixedChartOptions.colors = widget.colors
                mixedChartOptions.fill.opacity = series.map(e => e.type == "area" ? 0.2 : 1);
                const chart = new ApexCharts(document.querySelector(`#chart-${widget.id}`), mixedChartOptions);
                let id = widget.id;
                CHARTS[id] = chart;
                chart.render();
                break;
            case "pie":
                cwidgetControleEle = `
        <div id="widget-${widget.id}" class="widgetContainer">
        <div style="display:flex;justify-content:space-between;padding: 9px;">
        <h5 style="text-align: initial;">${widget.title}</h5>
            <div class="widgetControle">
                <div>
                    <div class="btn-group">
                        <i data-id="${widget.id}" class="bi bi-gear btnWidgetFilter d-none"></i>
                        <div id="filterList-${widget.id}" class="filterList d-none">
                            <div style="flex-wrap: nowrap;justify-content: space-around;padding: 3px;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widgetPanel">
                    <i data-id="${widget.id}" style="margin-left:5px"  class="bi bi-pencil editWidget"></i>
                    <i data-id="${widget.id}" class="bi bi-x deleteWidget"></i>
                </div>
            </div>
            </div>
            <div style="margin-top: -10px;" id="chart-${widget.id}"></div>
        </div>`;
                widget.content = cwidgetControleEle;
                addNewWidget(widget);
                pieChartOptions.series = data.series; //Object.keys(widget.data[0].data).map(e => widget.data[0].data[e][0]);
                pieChartOptions.labels = data.categories; //Object.keys(widget.data[0].data);
                generateUniqueColor(data.categories.length);
                pieChartOptions.colors = linkersGraphColors,
                pieChartOptions.xaxis.categories = data.categories; //Object.keys(widget.data[0].data);
                const chart1 = new ApexCharts(document.querySelector(`#chart-${widget.id}`), pieChartOptions);
                let id1 = widget.id;
                CHARTS[id1] = chart1;
                chart1.render();
                break;
            case "label":
                cwidgetControleEle = `
            <div id="widget-${widget.id}" class="widgetContainer_label">
                <div class="widgetControle_label">

                <p  class="time_period_label">${_jsHelpers._capitalizeFirstLetter(widget.title)}</p>

                <div><i data-id="${widget.id}" class="bi bi-gear btnWidgetFilter d-none"></i></div>
                
                    <div class="widgetPanel">
                        <i data-id="${widget.id}" style="margin-left:5px" class="bi bi-pencil editWidget"></i>
                        <i data-id="${widget.id}" class="bi bi-x deleteWidget"></i>
                    </div>
                </div>
                <div class="box box1">
                    <div style="display: flex;justify-content: space-between;width: 90%;margin-top:1px">
                        <div>
                            <img width="40px" src="${widget.data[0].img_path}.png"/>
                        </div>
                        <div>
                            <p class="label-title">${widget.data[0].name == null ? ' ' : widget.data[0].name}</p>
                            <p id="label-${widget.id}" class="label_count">${widget.data[0].data.totals}</p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
          
        </div>`;
                widget.content = cwidgetControleEle;
                addNewWidget(widget);
                break;
            case "text":
                cwidgetControleEle = `
        <div id="widget-${widget.id}" class="widgetContainer_text">
            <div class="widgetControle">
            <p class="text_title">${widget.title}</p>

            <div><i data-id="${widget.id}" class="bi bi-gear btnWidgetFilter d-none"></i></div>
                <div class="widgetPanel">
                    <i data-id="${widget.id}" class="bi bi-pencil editWidget"></i>
                    <i data-id="${widget.id}" class="bi bi-x deleteWidget"></i>
                </div>
            </div>
            <div class="details text-center">
                <p style="margin:0px">${widget.text_title ? widget.text_title : ""}</p>
            </div>
        </div>`;
                widget.content = cwidgetControleEle;
                addNewWidget(widget);
                break;
            case "table":
                cwidgetControleEle = `
        <div id="widget-${widget.id}" class="widgetContainer">
            <div class="widgetControle">
                <div>
                  <i data-id="${widget.id}" class="bi bi-gear btnWidgetFilter d-none"></i>
                </div>
                <div class="widgetPanel">
                    <i data-id="${widget.id}" style="margin-left:5px" class="bi bi-pencil editWidget"></i>
                    <i data-id="${widget.id}" class="bi bi-x deleteWidget"></i>
                </div>
            </div>
            <div class="">
            <h5 style="text-align: initial;padding:10px;">${widget.title}</h5>
              <table id="table-${widget.id}"></table>
            </div>
        </div>`;

                let _id = 1;
                try {
                    series = [];
                    if (widget.results == 'global') {
                        widget.data.forEach(variable => {
                            widget.categories.forEach((date, idx) => {
                                let objRow = {};
                                objRow.id = _id;
                                objRow["variable"] = variable.name;
                                objRow.date = date;
                                var value = 0
                                Object.keys(variable.data).forEach(branchName => {
                                    value += variable.data[branchName][idx] ? parseInt(variable.data[branchName][idx]) : 0;
                                })

                                var _value;
                                if (percentVariables.includes(variable.analysis_of)) {
                                    _value = value == 0 ? 0 : Number(value / (Object.keys(variable.data).length - 1)).toFixed(1);
                                    if (variable.analysis_of == "stay_time") {
                                        _value = Num2Time(_value);
                                    } else {
                                        _value = _value + "%"
                                    }
                                } else {

                                    _value = value;
                                }

                                objRow["value"] = _value
                                _id++;
                                series.push(objRow)
                            })
                        })
                    } else {
                        widget.categories.forEach((date, idx) => {
                            Object.keys(widget.data[0].data).forEach(branch_name => {
                                if (branch_name != "totals") {
                                    let objRow = {};
                                    objRow.id = _id;
                                    objRow.branch = branch_name;
                                    objRow.date = date;
                                    widget.data.forEach(variable => {
                                        objRow[variable.name] = variable.data[branch_name][idx] ? variable.data[branch_name][idx] : 0;
                                    });
                                    _id++;
                                    series.push(objRow)
                                }
                            })
                        });
                    }
                } catch (error) {
                    console.error(error);
                    series = [];
                }

                widget.content = cwidgetControleEle;
                addNewWidget(widget);
                $(`#table-${widget.id}`).dxDataGrid({
                    dataSource: series,
                    keyExpr: 'id',
                    columnsAutoWidth: true,
                    allowColumnResizing: true,
                    allowColumnReordering: true,
                    showBorders: true,
                    grouping: {
                        autoExpandAll: false,
                    },
                    paging: {
                        pageSize: 5,
                    },
                    pager: {
                        visible: true,
                        // allowedPageSizes: [5, 10],
                        // showPageSizeSelector: true,
                        showInfo: true,
                        showNavigationButtons: true,
                    },
                    summary: {
                        groupItems: widget.data.map(item => {
                            let e = item.analysis_of
                            return {
                                'column': e,
                                'summaryType': percentVariables.includes(e) ? e != 'stay_time' ? 'avg' : 'avg' : "sum",
                                'valueFormat': percentVariables.includes(e) ? 'percent' : '',
                                customizeText(data) {
                                    if (percentVariables.includes(e)) {
                                        if (e == "stay_time") {
                                            return Num2Time(Number(data.value).toFixed(2));
                                        }
                                        if (e == "avg_ticket_size") {
                                            return Number(data.value).toFixed(2);
                                        }
                                        return Number(data.value).toFixed(2) + "%";
                                    }
                                    return Number(data.value);
                                },
                                'showInGroupFooter': false,
                            }
                        }),
                    },
                    filterRow: {
                        visible: true,
                    },
                    groupPanel: {
                        visible: true,
                    },
                    export: {
                        enabled: true,
                    },
                    onExporting(e) {
                        const workbook = new ExcelJS.Workbook();
                        const worksheet = workbook.addWorksheet('Linkers Analytics Center');

                        DevExpress.excelExporter.exportDataGrid({
                            component: e.component,
                            worksheet,
                            autoFilterEnabled: true,
                        }).then(() => {
                            workbook.xlsx.writeBuffer().then((buffer) => {
                                saveAs(new Blob([buffer], {
                                    type: 'application/octet-stream'
                                }), `${$("#companyName")[0].innerText + new Date().toLocaleDateString().split("/").join("")}` + '.xlsx');
                            });
                        });
                        e.cancel = true;
                    },
                });

                break;
        }

        $(".collapsible").off("click");
        $(".collapsible").on("click", function (e) {
            let dtls = $(`.${$(this).data("class")}-dtl`);
            if ($(this).data("collapsed")) {
                dtls.hide();
                $(this).data("collapsed", false);
            } else {
                dtls.show();
                $(this).data("collapsed", true);
            }
        });

        if (!$("#editModePanel").hasClass("d-none")) {
            $(".widgetPanel").show();
        }
        $(".btnWidgetFilter").off("click");
        $(".btnWidgetFilter").on("click", function (e) {
            let selectedWidget = WIDGETS.filter(x => x.id == $(this).data("id"))[0];
            let list = $(`#filterList-${selectedWidget.id}`);
            if (list.hasClass("d-none")) {
                list.removeClass("d-none");
            } else {
                list.addClass("d-none");
            }

        });
        $(".editWidget").off("click");
        $(".editWidget").on("click", function (e) {
            if (IS_GRID_EDITED) {
                _jsHelpers._isConfirmed("You might want to save your changes first!!", () => {
                    saveGridChanges().then(() => {
                        handleEditWiidgetBtnClick(e);
                    });
                }, () => {
                    handleEditWiidgetBtnClick(e);
                })
            } else {
                handleEditWiidgetBtnClick(e);
            }
        });

        function handleEditWiidgetBtnClick(e) {
            let selectedWidget = TEMPLET_DATA.data.template.widgets[TEMPLET_DATA.data.template.widgets.findIndex(widget => widget.id == e.target.getAttribute('data-id'))];
            WIDGET_DATA = selectedWidget;
            $("#widgetModal").modal("show");
            $(".carousel-item").removeClass("active");
            $(".carousel-item").first().addClass("active");
            $('.btnNext').removeClass("d-none");
            $('#widgetTitle').val(selectedWidget.title);
            $('#resolution').val(selectedWidget.resolution);
            $('#timeLape').val(selectedWidget.time_period);
            if (selectedWidget.time_period == 'custom') {
                $("#custom-date").show();
                let from = selectedWidget.from_date.split("/");
                let to = selectedWidget.to_date.split("/");
                $("#time-lape-from").val(`${from[2]}-${from[1]}-${from[0]}`);
                $("#time-lape-to").val(`${to[2]}-${to[1]}-${to[0]}`);
            }
            $('#results').val(selectedWidget.results);
            $('#widgetText').val(selectedWidget.text_title);
            $('#seriesList').empty();
            renderSeries(selectedWidget.series);
            $('.btnNext').addClass('d-none');
            $('.btnSaveWidgetChanges').removeClass("d-none");
            handelWidgetModalLogic(selectedWidget.chart_type);
            $(".carousel-item").removeClass("active");
            $(".carousel-item").eq(1).addClass("active");
            $('#widgetWizardFooter').removeClass("d-none");
        }
        $(".deleteWidget").off("click");
        $(".deleteWidget").on("click", function (e) {
            _jsHelpers._isConfirmed(`${_jsHelpers._trans("areYouSure")}`, () => {
                _jsHelpers._blockUI();
                $.ajax({
                    type: "DELETE",
                    url: _jsHelpers._PREFIX_ADMIN_API + "/dashboard/widget/" + $(this).data("id"),
                    headers: _jsHelpers._getHeaders(),
                }).done((res) => {
                    if (!res.error) {
                        el = $(this).closest('.grid-stack-item')[0];
                        grid.removeWidget(el);
                        serializedFull = grid.save(true, true);
                        if (!serializedFull.children.length) {
                            $("#NoWidgets").removeClass("d-none");
                            $("#dateFilteContainer").addClass("d-none");

                        }
                    }
                    _jsHelpers._unBlockUi();
                }).fail((error) => {
                    _jsHelpers._unBlockUi();
                    _jsHelpers._handle_notify('danger', error.responseJSON.message);
                });
            });
        });

        $(".widgetFilteritem").off("change");
        $(".widgetFilteritem").on("change", function (e) {
            if ($(this).data("name") == "totals") {
                $("." + $(this).data("class")).prop("checked", this.checked);
            }
            if ($(`.${$(this).data("class")}:checked`).length == 0) {
                $(`.${$(this).data("class")}`).first().prop("checked", true);
            }
            let selectedWidget = WIDGETS[WIDGETS.findIndex(x => x.id == $(this).data("id"))];
            var series = [];
            if (selectedWidget.results == "global") {
                selectedWidget.data.forEach((variable, idx) => {
                    var arrList = [];
                    var _break = false;
                    $(`.${variable.analysis_of}-${selectedWidget.id}-${idx}:checked`).each(function () {
                        if (!_break) {
                            if ($(this).data("name") == "totals") {
                                arrList = [];
                                _break = true;
                            }
                            arrList.push(variable.data[$(this).data("name")]);
                        }
                    });
                    let sumData = arrList[0].map((x, idx) => arrList.reduce((sum, curr) => sum + curr[idx], 0));
                    let obj = {
                        name: variable.name,
                        type: variable.type,
                        data: sumData
                    }
                    series.push(obj);
                });
                let selectedChart = CHARTS[$(this).data("id")];
                selectedChart.updateSeries(series)
            } else {
                var categories = [];
                selectedWidget.data.forEach((variable, idx) => {
                    var arrList = [];
                    var _break = false;
                    $(`.${variable.name}-${selectedWidget.id}-${idx}:checked`).each(function () {
                        if (!_break) {
                            if ($(this).data("name") == "totals") {
                                arrList = [];
                                _break = true;
                            }
                            categories.push($(this).data("name"));
                            arrList.push(variable.data[$(this).data("name")].reduce((partialSum, a) => partialSum + a, 0));
                        }
                    });
                    let obj = {
                        name: variable.name,
                        type: variable.type,
                        data: arrList
                    }
                    series.push(obj);
                });

                let selectedChart = CHARTS[$(this).data("id")];
                selectedChart.updateOptions({
                    xaxis: {
                        categories: categories
                    },
                    series: series,
                });
            }
        });

    });


};

function renderWidgetAfterBranchFilter() {
    let newWidgets = WIDGETS.map(widget => {
        let obj = {};
        obj = JSON.parse(JSON.stringify(widget));
        obj.data.forEach(series => {
            series.data.totals = [];
            Object.keys(series.data).forEach(branchName => {
                if (branchName != "totals") {
                    let branch = SELECTED_BRANCHES_FILTER.find(e => e.name == branchName);
                    if (!branch) {
                        delete series.data[branchName];
                    }
                }
            });
            try {
                series.data[Object.keys(series.data)[0]].forEach((date, idx) => {
                    var total = 0;
                    Object.keys(series.data).forEach(branch => {
                        if (branch != "totals") {
                            total += Number(series.data[branch][idx])
                        }
                    })
                    series.data.totals.push(total);
                });
            } catch (error) {
                var sum = 0;
                Object.keys(series.data).forEach(branch => {
                    if (branch != "totals") {
                        sum = sum + Number(series.data[branch]);
                    }
                });
                series.data.totals = sum;
            }
        })
        return obj
    });
    renderWidgets(newWidgets);
}

function renderWidgets(widgets) {
    widgets.forEach(widget => {
        let data = convertDataToSeries(widget);
        switch (widget.chart_type) {
            case "mixed":
                mixedChartOptions.series = data.series;
                mixedChartOptions.xaxis.categories = data.categories;
                mixedChartOptions.colors = widget.colors
                mixedChartOptions.fill.opacity = series.map(e => e.type == "area" ? 0.2 : 1);
                CHARTS[widget.id].updateOptions(mixedChartOptions);
                break;
            case "pie":
                let pieSeries = Object.keys(widget.data[0].data).map(e => widget.data[0].data[e][0]);
                let pieLabels = Object.keys(widget.data[0].data)
                pieSeries.pop();
                pieLabels.pop();
                pieChartOptions.series = pieSeries;
                pieChartOptions.labels = pieLabels;
                generateUniqueColor(pieLabels.length);
                pieChartOptions.colors = linkersGraphColors,
                    pieChartOptions.xaxis.categories = pieLabels;
                CHARTS[widget.id].updateOptions(pieChartOptions);
                break;
            case "label":
                let total = widget.data[0].data.totals;//.reduce((partialSum, a) => Number(partialSum) + Number(a), 0)
                var label;
                if (percentVariables.includes(widget.data[0].analysis_of)) {
                    // label = Number(total / (widget.data[0].data.totals.length - 1)).toFixed(1);
                    label = Number(total / (Object.keys(widget.data[0].data).length - 1)).toFixed(1);
                    if (widget.data[0].analysis_of == "stay_time") {
                        label = Num2Time(label);
                    } else {
                        if (widget.data[0].analysis_of != "avg_ticket_size") {
                            label = label + "%"
                        }
                    }
                } else {
                    label = Number(total).toFixed(2);
                }
                $(`#label-${widget.id}`).text(label);

            case "table":
                let _id = 1;
                widget.categories.forEach((date, idx) => {
                    Object.keys(widget.data[0].data).forEach(branch_name => {
                        if (branch_name != "total") {
                            let objRow = {};
                            objRow.id = _id;
                            objRow.branch = branch_name;
                            objRow.date = date;
                            widget.data.forEach(variable => {
                                objRow[variable.name] = variable.data[branch_name][idx] ? variable.data[branch_name][idx] : 0;
                            });
                            _id++;
                            series.push(objRow)
                        }
                    })
                });

                $(`#table-${widget.id}`).dxDataGrid({
                    dataSource: series,
                    keyExpr: 'id',
                    columnsAutoWidth: true,
                    allowColumnResizing: true,
                    allowColumnReordering: true,
                    showBorders: true,
                    grouping: {
                        autoExpandAll: false,
                    },
                    paging: {
                        pageSize: 5,
                    },
                    pager: {
                        visible: true,
                        // allowedPageSizes: [5, 10],
                        // showPageSizeSelector: true,
                        showInfo: true,
                        showNavigationButtons: true,
                    },
                    summary: {
                        groupItems: widget.data.map(item => {
                            let e = item.name
                            return {
                                'column': e,
                                'summaryType': percentVariables.includes(e) ? e != 'stay_time' ? 'avg' : 'avg' : "sum",
                                'valueFormat': percentVariables.includes(e) ? 'percent' : '',
                                customizeText(data) {
                                    if (percentVariables.includes(e)) {
                                        if (e == "stay_time") {
                                            return Num2Time(Number(data.value).toFixed(2));
                                        }
                                        if (e == "avg_ticket_size") {
                                            return Number(data.value).toFixed(2);
                                        }
                                        return Number(data.value).toFixed(2) + "%";
                                    }
                                    return Number(data.value);
                                },
                                'showInGroupFooter': false,
                            }
                        }),
                    },
                    filterRow: {
                        visible: true,
                    },
                    groupPanel: {
                        visible: true,
                    },
                    export: {
                        enabled: true,
                    },
                    onExporting(e) {
                        const workbook = new ExcelJS.Workbook();
                        const worksheet = workbook.addWorksheet('Linkers Analytics Center');

                        DevExpress.excelExporter.exportDataGrid({
                            component: e.component,
                            worksheet,
                            autoFilterEnabled: true,
                        }).then(() => {
                            workbook.xlsx.writeBuffer().then((buffer) => {
                                saveAs(new Blob([buffer], {
                                    type: 'application/octet-stream'
                                }), `${$("#companyName")[0].innerText + new Date().toLocaleDateString().split("/").join("")}` + '.xlsx');
                            });
                        });
                        e.cancel = true;
                    },
                });

                break;
        }
    });
}


function autoFillWidgetTitle() {
    if ($("#autoFillName").prop("checked")) {
        let firstName = formateResolutionName($("#resolution").val());
        let secondName = formateTimeLapsName($("#timeLape").val());
        let thirdName = formateSeriesNames();
        let lastName = formateResultsName($("#results").val());
        $("#widgetTitle").val(`${firstName} ${secondName} ${thirdName} ${lastName}`);
    }

    function formateResultsName(str) {
        switch (str) {
            case "global":
                return _jsHelpers._trans("Global");
            case "comparative":
                return _jsHelpers._trans("Comparative");
        }
    }

    function formateTimeLapsName(str) {
        switch (str) {
            case "today":
                return _jsHelpers._trans("today");
            case "yesterday":
                return _jsHelpers._trans("yesterday");
            case "this_week":
                return _jsHelpers._trans("thisWeek");
            case "last_week":
                return _jsHelpers._trans("lastWeek");
            case "this_month":
                return _jsHelpers._trans("thisMonth");
            case "last_month":
                return _jsHelpers._trans("lastMonth");
            case "this_year":
                return _jsHelpers._trans("thisYear");
            case "last_year":
                return _jsHelpers._trans("lastYear");
            case "custom":
                return _jsHelpers._trans("custom");
            default:
                return "";
        }
    }

    function formateResolutionName(str) {
        switch (str) {
            case "0":
                return _jsHelpers._trans("hourly");
            case "1":
                return _jsHelpers._trans("daily");
            case "2":
                return _jsHelpers._trans("weekly");
            case "3":
                return _jsHelpers._trans("monthly");
            case "4":
                return _jsHelpers._trans("quartarly");
            default:
                return "";
        }
    }

    function formateSeriesNames() {
        if (WIDGET_DATA.series.length == 0) return "";
        let str = WIDGET_DATA.series.map(series => {
            return series.name
        });
        return str.join(" & ")
    }

}
