<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>


    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/theme/assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/assets/theme/assets/css/bootstrap-extended.css">
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css" /> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css" />
    

    <link rel="stylesheet" type="text/css" href="/assets/theme/assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/assets/theme/assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/assets/theme/assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/assets/theme/assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/theme/assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/assets/theme/assets/css/pages/authentication.css">
    <!-- END: Page CSS-->



    <link rel="stylesheet" type="text/css" href="/assets/theme/assets/vendors/css/extensions/toastr.css">

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/admin/argon/base_login.css">
    <link rel="stylesheet" type="text/css" href="/assets/admin/modulos/login/login.css">
    <!-- END: Custom CSS-->

    
</head>
<body>
    <div class="container">
        <div class="row justify-content-center ">
            <div class="my-5 col-6">
                @include('partials.login.form', ['field' => 'login'])
            </div>
        </div>
        
    </div>
    <!-- BEGIN: Page JS-->
    <script src="/assets/theme/assets/js/core/libraries/jquery.min.js" type="text/javascript"></script>
    <script src="/assets/theme/assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="/assets/theme/assets/vendors/js/extensions/toastr.min.js"></script>
    <script src="/assets/theme/assets/vendors/js/ui/blockUI.min.js"></script>
    <script src="/assets/admin/argon/_jsHelpers.js"></script>
    <script src="/assets/admin/modulos/login/login.js"></script>
    <!-- END: Page JS -->
</body>
</html>