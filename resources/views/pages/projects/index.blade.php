@extends('theme.argon.layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
    @include('theme.argon.layouts.navbars.auth.topnav', ['title' => 'Tables'])
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        <h6>Projects table</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center justify-content-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Project</th>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Event Repetition</th>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Goal</th>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Payment schedule</th>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Fees</th>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Status</th>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder text-center opacity-7 ps-2">
                                            Completion</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($projects as $project)
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2">
                                                <div>
                                                    <img src="/assets/theme/argon/img/small-logos/logo-spotify.svg"
                                                        class="avatar avatar-sm rounded-circle me-2" alt="spotify">
                                                </div>
                                                <div class="my-auto">
                                                    <h6 class="mb-0 text-sm">{{$project['name']}}</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-sm font-weight-bold mb-0">{{$project['iteration']}}</p>
                                        </td>
                                        <td>
                                            <p class="text-sm font-weight-bold mb-0">{{$project['total']}}</p>
                                        </td>
                                        <td>
                                            <span class="text-xs font-weight-bold">{{$project['payment_period']}}</span>
                                        </td>
                                        <td>
                                            <span class="text-xs font-weight-bold">{{$project['fees']}}</span>
                                        </td>
                                        <td>
                                            <span class="text-xs font-weight-bold">{{$project['status']}}</span>
                                        </td>
                                        <td class="align-middle text-center">
                                            <div class="d-flex align-items-center justify-content-center">
                                                <span class="me-2 text-xs font-weight-bold">60%</span>
                                                <div>
                                                    <div class="progress">
                                                        <div class="progress-bar bg-gradient-info" role="progressbar"
                                                            aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                                            style="width: 60%;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="align-middle">
                                            <button id="moreoption" class="btn btn-link text-secondary mb-0"
                                            data-bs-toggle='dropdown'>
                                                <i class="fa fa-ellipsis-v text-xs"></i>
                                            </button>
                                            <!-- Link or button to toggle dropdown -->
                                            <ul id="dropdown" class="dropdown-menu" role="menu" aria-labelledby="moreoption">
                                                <li type="button" data-bs-toggle="modal" data-bs-target="#editProject">Edit</li>
                                                <li type="button" data-bs-toggle="modal" data-bs-target="#deleteProject">Delete</li>
                                                <hr>
                                                <li><a tabindex="-1" href="#">Access this project</a></li>
                                            </ul>
                                            @include('pages.projects.modals.edit')
                                            @include('pages.projects.modals.delete')
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('theme.argon.layouts.footers.auth.footer')
    </div>
@endsection
