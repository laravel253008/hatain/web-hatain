<div id="editProject" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Project</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-wrap">
            <div class="card-body">
                        <form role="form">
                            <div class="col-lg-6 m-auto">
                                <img
                                    id="project_photo"
                                    src="https://mdbcdn.b-cdn.net/img/Photos/Thumbnails/Slides/1.webp"
                                    data-mdb-img="https://mdbcdn.b-cdn.net/img/Photos/Slides/1.webp"
                                    alt="Table Full of Spices"
                                    class="w-100 m-a"
                                />
                            </div>
                            <div class="mb-3">
                            <label class="custom-control-label" for="name">PROJECT NAME</label>
                                <input id="name" name="name" type="text" class="form-control" placeholder="hatain" aria-label="Name" value="{{$project['name']}}">
                            </div>
                            <div class="mb-3">
                                <label class="custom-control-label" for="choices-button">EVENT REPETITION</label>
                                <p>
                                    <select class="form-control" name="repetition" id="choices-button" placeholder="Departure">
                                        <option value="yearly">Yearly</option>
                                        <option value="monthly" selected>Monthly</option>
                                        <option value="weekly">Weekly</option>
                                        <option value="daily">Daily</option>
                                    </select>
                                </p>
                            </div>
                            <div class="mb-3">
                                <label class="custom-control-label" for="goal">TARGET AMOUNT</label>
                                <input id="goal" name="goal" type="number" class="form-control" placeholder="3000" aria-label="goal" value="{{$project['total']}}">
                            </div>
                            <div class="mb-3">
                                <label class="custom-control-label" for="choices-button">PAYMENT SCHEDULE</label>
                                <p>
                                    <select class="form-control" name="schedule" id="choices-button" placeholder="Departure">
                                        <option value="yearly" selected="">Yearly</option>
                                        <option value="monthly">Monthly</option>
                                        <option value="weekly">Weekly</option>
                                        <option value="daily">Daily</option>
                                    </select>
                                </p>
                            </div>
                            <div class="mb-3">
                                <label class="custom-control-label" for="fees">JOINING FEES</label>
                                <input id="fees" type="number" class="form-control" placeholder="10" aria-label="Email"  value="{{$project['fees']}}">
                            </div>
                        </form>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>