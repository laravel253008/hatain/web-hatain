<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/apple-icon.png">
    <link rel="icon" type="image/png" href="/img/favicon.png">
    <title>
        hatain
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href=" {{\_html::getVersion(THEME_ASSETS . '/assets/css/nucleo-icons.css')}}" rel="stylesheet" />
    <link href="{{\_html::getVersion(THEME_ASSETS . '/assets/css/nucleo-svg.css')}}" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="{{\_html::getVersion(THEME_ASSETS . '/assets/css/nucleo-svg.css')}}" rel="stylesheet" />
    <!-- CSS Files -->    
    <link id="pagestyle" href="{{\_html::getVersion(THEME_ASSETS . '/assets/css/argon-dashboard.css')}}" rel="stylesheet" />
    <script src="{{\_html::getVersion(BASE_ASSETS . '/_jsHelpers.js')}}" type="text/javascript"></script>

    @if(isset($assets_css_plugins))
    @foreach($assets_css_plugins as $asset_css)
    <link href="{{\_html::getVersion(THEME_ASSETS .'/'. $asset_css)}}" rel="stylesheet" type="text/css" />
    @endforeach
    @endif

    @if(isset($assets_css_modulos))
    @foreach($assets_css_modulos as $asset_css_modulo)
    <link href=" {{\_html::getVersion(MODULOS_ASSETS . '/' . $asset_css_modulo)}}" rel="stylesheet" type="text/css" />
    @endforeach
    @endif

    @if(isset($extra_css))
    @foreach($extra_css as $link_css)
    <link href="{{$link_css}}" rel="stylesheet" type="text/css" />
    @endforeach
    @endif

</head>

<body class="{{ $class ?? '' }}">

    @if (in_array(request()->path(), ['login']))
        @yield('content')
    @else
        @if (!in_array(request()->path(), ['profile', 'profile-static']))
            <div class="min-height-300 bg-primary position-absolute w-100"></div>
        @elseif (in_array(request()->path(), ['profile-static', 'profile']))
            <div class="position-absolute w-100 min-height-300 top-0" style="background-image: url('https://raw.githubusercontent.com/creativetimofficial/public-assets/master/argon-dashboard-pro/assets/img/profile-layout-header.jpg'); background-position-y: 50%;">
                <span class="mask bg-primary opacity-6"></span>
            </div>
        @endif
        @include('theme.argon.layouts.navbars.auth.sidenav')
            <main class="main-content border-radius-lg">
                @yield('content')
            </main>
        @include('theme.argon.components.fixed-plugin')
    @endif
    <input type="hidden" id="_tokenizer" value="{{ csrf_token() }}"/>
    <!--   Core JS Files   -->
    <script src="{{\_html::getVersion(THEME_ASSETS . '/assets/js/core/popper.min.js')}}"></script>
    <script src="{{\_html::getVersion(THEME_ASSETS . '/assets/js/core/bootstrap.min.js')}}"></script>
    <script src="{{\_html::getVersion(THEME_ASSETS . '/assets/js/plugins/perfect-scrollbar.min.js')}}"></script>
    <script src="{{\_html::getVersion(THEME_ASSETS . '/assets/js/plugins/smooth-scrollbar.min.js')}}"></script>
    <script src="/assets/theme/assets/js/core/libraries/jquery.min.js" type="text/javascript"></script>
    <script src="/assets/theme/assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    @if(isset($assets_js_plugins))
    @foreach($assets_js_plugins as $asset_js)
    <script src="{{\_html::getVersion(THEME_ASSETS . '/' .$asset_js)}}" type="text/javascript"></script>
    @endforeach
    @endif

    @if(isset($assets_js_base))
    @foreach($assets_js_base as $asset_js_base)
    <script src="{{\_html::getVersion(BASE_ASSETS .'/'. $asset_js_base)}}" type="text/javascript"></script>
    @endforeach
    @endif

    @if(isset($assets_scripts_modulos))
    @foreach($assets_scripts_modulos as $asset_script_modulo)
    <script src="{{\_html::getVersion(MODULOS_ASSETS .'/'. $asset_script_modulo)}}" type="text/javascript"></script>
    @endforeach
    @endif

    @if(isset($assets_js_modulos))
    @foreach($assets_js_modulos as $asset_js_modulo)
    <script src="{{\_html::getVersion(MODULOS_ASSETS .'/'. $asset_js_modulo)}}" type="text/javascript"></script>
    @endforeach
    @endif
    <script>
        var win = navigator.platform.indexOf('Win') > -1;
        if (win && document.querySelector('#sidenav-scrollbar')) {
            var options = {
                damping: '0.5'
            }
            Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
        }
    </script>
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{\_html::getVersion(THEME_ASSETS . '/assets/js/argon-dashboard.js')}}"></script>
    @stack('js');
</body>

</html>
