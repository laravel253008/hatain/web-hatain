<div class="px-4 pt-4">
    @if(isset($success) && count($success) > 0)
    @foreach( $success->all() as $message )
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <p class="text-white mb-0">{{ $message }}</p>
        </div>
    @endforeach
    @endif
    @if(isset($errors) && count($errors) > 0)
    @foreach( $errors->all() as $message )
    <div class="alert alert-danger" role="alert">
        <p class="text-white mb-0">{{ $message }}</p>
    </div>
    @endforeach
    @endif
</div>
