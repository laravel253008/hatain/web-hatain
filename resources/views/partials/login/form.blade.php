<form id="{{ $field ?? 'login'}}" method="POST" action="/user/login">
@csrf
    @if(count($errors) > 0)
    @foreach( $errors->all() as $message )
    <div class="alert alert-danger display-hide">
    <button class="close" data-close="alert"></button>
    <span>{{ $message }}</span>
    </div>
    @endforeach
    @endif
    <!-- Email input -->
    <div class="form-outline mb-4">
        <input id="email" type="email" class="form-control" name="email" required/>
        <label class="form-label" for="email">Email address</label>
        <p class="error">@error('email') {{$message}} @enderror</p>
    </div>

    <!-- Password input -->
    <div class="form-outline mb-4">
        <input id="password" type="password" class="form-control" name="password" required minlength="6"/>
        <label class="form-label" for="password">Password</label>
        <p class="error">@error('password') {{$message}} @enderror</p>
        
    </div>

    <!-- 2 column grid layout for inline styling -->
    <div class="row mb-4">
        <div class="col d-flex justify-content-center">
        <!-- Checkbox -->
        <div class="form-check">
            <input id="remember" class="form-check-input" type="checkbox" value="" name="remember_me" checked />
            <label class="form-check-label" for="remember"> remember </label>
            <p class="error">@error('remember_me') {{$message}} @enderror</p>
        </div>
        </div>

        <div class="col">
        <!-- Simple link -->
        <a href="#!">Forgot password?</a>
        </div>
    </div>

    <!-- Submit button -->
    <button type="submit" class="btn btn-primary btn-block mb-4">Sign in</button>

    <!-- Register buttons -->
    <div class="text-center">
        <p>Not a member? <a href="#!">Register</a></p>
        <p>or sign up with:</p>
        <button type="button" class="btn btn-link btn-floating mx-1">
        <i class="fab fa-facebook-f"></i>
        </button>

        <button type="button" class="btn btn-link btn-floating mx-1">
        <i class="fab fa-google"></i>
        </button>

        <button type="button" class="btn btn-link btn-floating mx-1">
        <i class="fab fa-twitter"></i>
        </button>

        <button type="button" class="btn btn-link btn-floating mx-1">
        <i class="fab fa-github"></i>
        </button>
    </div>
</form>

